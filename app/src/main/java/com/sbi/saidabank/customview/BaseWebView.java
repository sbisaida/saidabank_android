package com.sbi.saidabank.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.net.HttpUtils;

/**
 * WebView Custom
 */
@SuppressLint("NewApi")
public class BaseWebView extends WebView {
	public Context mContext;

	public BaseWebView(Context context) {
		super(context);
		if(!isInEditMode())
            init(context);
	}

	public BaseWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if(!isInEditMode())
            init(context);
	}

	public BaseWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if(!isInEditMode())
            init(context);
	}

	@Override
	public void destroy() {
		ViewParent parent = this.getParent();
		if(parent instanceof ViewGroup){
			((ViewGroup) parent).removeView(this);
		}

		loadUrl("about:blank");

		try{
			removeAllViews();
		}catch (Exception e){
			e.printStackTrace();
		}
		super.destroy();
	}

	public void init(Context context) {
		Logs.e("BaseWebview - init");
        mContext = context;
        setDefaultWebSettings();
	}

	public void setDefaultWebSettings() {

		WebSettings settings = getSettings();

		//User Agent를 설정한다.
		String userAgent = HttpUtils.makeUserAgentString(mContext, settings.getUserAgentString());
		Logs.i("*********************************************");
		Logs.i("setDefaultWebSettings - userAgent" + userAgent);
		Logs.i("*********************************************");
		settings.setUserAgentString(userAgent);

		settings.setJavaScriptEnabled(true);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setBuiltInZoomControls(false);
		settings.setSupportZoom(false);
        settings.setSaveFormData(false);
		settings.setDisplayZoomControls(false);

		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setLoadsImagesAutomatically(true);

		settings.setAllowFileAccess(true);
		settings.setGeolocationEnabled(true);
		settings.setSupportMultipleWindows(true); //이거 검색해서 사용법 알아두자
		settings.setJavaScriptCanOpenWindowsAutomatically(true);


		//WebView inside Browser doesn't want initial focus to be set.
		settings.setNeedInitialFocus(false);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			settings.setEnableSmoothTransition(true);
		}

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
			settings.setPluginState(WebSettings.PluginState.ON);
			settings.setSavePassword(false);
			settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
			// enable smooth transition for better performance during panning or
			// zooming
			settings.setEnableSmoothTransition(true);
		}



		//disable content url access
		settings.setAllowContentAccess(false);

		//HTML5 API flags
		settings.setDomStorageEnabled(true);
		settings.setDatabaseEnabled(true);
		settings.setAppCacheEnabled(true);

		// https로 로드된 페이지에서 http링크로된 리소스 연결할 때 block 방지
		settings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);

		settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);

		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			settings.setTextZoom(100);
		}

		// HTML5 configuration parametersettings.
		settings.setAppCachePath(mContext.getDir("appcache", 0).getPath());
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
			settings.setGeolocationDatabasePath(mContext.getDir("geolocation", 0).getPath());
		}
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			settings.setDatabasePath("/data/data/" + mContext.getPackageName() + "/databases/");
		}

		// origin policy for file access
		settings.setAllowUniversalAccessFromFileURLs(false);
		settings.setAllowFileAccessFromFileURLs(false);

		//쿠키 동기화
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);

		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
			CookieManager.getInstance().setAcceptThirdPartyCookies(this,true);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		} else {
			setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}

		setHorizontalScrollBarEnabled(true);
		setVerticalScrollBarEnabled(true);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			setHorizontalScrollbarOverlay(true);
			setVerticalScrollbarOverlay(true);
		}

		setScrollbarFadingEnabled(true);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			WebView.setWebContentsDebuggingEnabled(true);
		}

	}
}