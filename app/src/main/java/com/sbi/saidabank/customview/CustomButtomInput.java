package com.sbi.saidabank.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;

/**
 * Saidabanking_android
 * <p>
 * Class: CustomButtomInput
 * Created by 950485 on 2018. 12. 05..
 * <p>
 * Description: 입력 폼 내 버튼
 */

public class CustomButtomInput extends LinearLayout {
    private TextView textView;

    private Context      context;

    public CustomButtomInput(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomButtomInput);

        textView = new TextView(context);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity= Gravity.CENTER_VERTICAL;
        int padding  = ta.getDimensionPixelSize(R.styleable.CustomButtomInput_textPadding, 9);
        params.setMargins(padding, 0, padding, 0);
        textView.setLayoutParams(params);
        textView.setBackgroundColor(0xFFFFFFFF); // hex color 0xAARRGGBB
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        textView.setTextColor(0xFF5A5A5A);
        textView.setIncludeFontPadding(false);

        CharSequence textButton = ta.getString(R.styleable.CustomButtomInput_textButton);
        if (!TextUtils.isEmpty(textButton))
            textView.setText(textButton);

        addView(textView);
    }

    public CustomButtomInput(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomButtomInput);

        textView = new TextView(context);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity= Gravity.CENTER_VERTICAL;
        int padding  = ta.getDimensionPixelSize(R.styleable.CustomButtomInput_textPadding, 9);
        params.setMargins(padding, 0, padding,0);
        textView.setLayoutParams(params);
        textView.setBackgroundColor(0xFFFFFFFF); // hex color 0xAARRGGBB
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        textView.setTextColor(0xFF5A5A5A);
        textView.setIncludeFontPadding(false);

        CharSequence textButton = ta.getString(R.styleable.CustomButtomInput_textButton);
        if (!TextUtils.isEmpty(textButton))
            textView.setText(textButton);

        addView(textView);
    }

    public void setTextButton(String textButton) {
        textView.setText(textButton);
    }

    public void setTextPadding(float padding) {
        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
        mlp.setMargins((int) Utils.dpToPixel(context, padding), 0, (int) Utils.dpToPixel(context, padding), 0);
    }
}
