package com.sbi.saidabank.customview;

import android.os.SystemClock;
import android.view.View;

/**
 * Saidabanking_android
 * <p>
 * Class: OnSingleClickListener
 * Created by 950546 on 2019. 03. 04
 * <p>
 * Description:버튼 중복클릭 방지용 클릭리스너
 */
public abstract class OnSingleClickListener implements View.OnClickListener {

    private static final long MIN_CLICK_INTERVAL = 600;    // ms
    private long mLastClickTime;

    public abstract void onSingleClick(View v);

    @Override
    public void onClick(View view) {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복클릭 방지
            return;
        }

        onSingleClick(view);
    }
}
