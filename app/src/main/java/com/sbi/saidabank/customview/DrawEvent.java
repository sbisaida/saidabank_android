package com.sbi.saidabank.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;

import static com.sbi.saidabank.customview.patternlockview.utils.RandomUtils.randInt;

public class DrawEvent extends View {
    Bitmap bEvent;

    private int xMin = 0;
    private int xMax;
    private int yMin = 0;
    private int yMax;
    private float eventWidth;
    private float eventHeight;
    private float eventX;
    private float eventY;
    private float eventSpeedX = 10;
    private float eventSpeedY = 10;
    private Paint paint;

    private TouchViewListener touchViewListener;

    public DrawEvent(Context context, TouchViewListener touchViewListener) {
        super(context);

        bEvent = BitmapFactory.decodeResource(getResources(), R.drawable.img_event_gift_default);
        eventWidth = Utils.dpToPixel(context, 70);
        eventHeight = Utils.dpToPixel(context, 70);
        eventY = eventHeight + randInt() % 1000;
        eventX = eventWidth + randInt() % 500;
        paint = new Paint();
        this.setFocusableInTouchMode(true);
        this.touchViewListener = touchViewListener;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        update();
        canvas.drawBitmap(bEvent, eventX, eventY, paint);

        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {

        }

        invalidate();
    }

    // Detect collision and update the position of the ball.
    private void update() {
        // Get new (x,y) position
        eventX += eventSpeedX;
        eventY += eventSpeedY;

        // Detect collision and react
        if (eventX + eventWidth > xMax) {
            eventSpeedX = 7 + randInt() % 5;
            eventSpeedX = -eventSpeedX;
            eventX = xMax - eventWidth;
        } else if (eventX < xMin) {
            eventSpeedX = -(7 + randInt() % 5);
            eventSpeedX = -eventSpeedX;
            eventX = xMin;
        }

        if (eventY + eventHeight > yMax) {
            eventSpeedY = 7 + randInt() % 5;
            eventSpeedY = -eventSpeedY;
            eventY = yMax - eventHeight;
        } else if (eventY < yMin) {
            eventSpeedY = -(7 + randInt() % 5);
            eventSpeedY = -eventSpeedY;
            eventY = yMin;
        }
    }

    // Called back when the view is first created or its size changes.
    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        xMax = w - 1;
        yMax = h - 1;
    }

    // Touch-input handler
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float currentX = event.getX();
        float currentY = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (currentX > eventX && currentX < eventX + eventWidth &&
                    currentY > eventY && currentY < eventY + eventHeight) {
                    bEvent = BitmapFactory.decodeResource(getResources(), R.drawable.img_event_gift_pressed);
                    touchViewListener.OnTouchView();
                    return true;
                }
                break;

            default:
                break;
        }
        return false;  // Event handled
    }

    public interface TouchViewListener {
        public void OnTouchView();
    }
}