package com.sbi.saidabank.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

import java.util.Calendar;

/**
 * Saidabanking_android
 * <p>
 * Class: LineProgressView
 * Created by 950485 on 2018. 12. 12..
 * <p>
 * Description:선그래프
 */

public class LineProgressView extends View {
    private Context           mContext;
    private Paint             mPaintBar;
    private Paint             mPaintBarBG;
    private Paint             mPaintText;
    private Paint             mPaintTextBG;
    private Paint             mPaintTextTriangleBG;
    private float             mPercent;
    private int               mLimitCount;
    private int               mStartColor;
    private int               mEndColor;
    private float             mHeight;
    private String            mText;
    private float             mTextMarginTop;
    private float             mTextMargin;
    private long              mSpeed;
    private Handler           mHandler;
    private boolean          mIsInverse;

    private int[] mColor;
    private int[] mReverseColor;
    private float[] mPostion;

    public LineProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mPaintBar = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBarBG = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBar.setStyle(Paint.Style.STROKE);
        mPaintBarBG.setStyle(Paint.Style.STROKE);
        mPaintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintTextBG = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintTextTriangleBG = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPercent = 0.0F;
        mLimitCount = 100;
        mStartColor = mContext.getResources().getColor(R.color.color03DC83);
        mEndColor = mContext.getResources().getColor(R.color.color00EBFF);
        mSpeed = 1L;
        mHeight = 1;
        mHandler = new Handler();
        mText = "";
        mTextMarginTop = Utils.dpToPixel(context, 6);
        mTextMargin = 0;
        mIsInverse = false;

        mColor = new int[] { mStartColor, mEndColor };
        mReverseColor = new int[] { mEndColor, mStartColor };
        mPostion = new float[] { 0, 1f };
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float dpHeight = Utils.pixelToDp(mContext, canvas.getHeight());
        float topMargin = Utils.dpToPixel(mContext,dpHeight - mHeight);
        int cornersRadius = 25;

        RectF rectFBG = new RectF(
                0,
                topMargin,
                canvas.getWidth(),
                canvas.getHeight()
        );

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
                    (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            mPaintBarBG.setColor(mContext.getResources().getColor(R.color.color4B555F));
        } else {
            mPaintBarBG.setColor(mContext.getResources().getColor(R.color.colorEBF5F6));
        }

        canvas.drawRoundRect(
                rectFBG,
                cornersRadius,
                cornersRadius,
                mPaintBarBG
        );

        float moveWidth;
        RectF rectF;
        if (mPercent > 0 && mPercent <= 2) {
            moveWidth = (float) (canvas.getWidth() * 0.02);
        } else {
            moveWidth = (float) canvas.getWidth() * (mPercent / 100);

            if (mIsInverse && mPercent == 0)
                moveWidth = (float) canvas.getWidth();
        }

        if (!mIsInverse) {
            rectF = new RectF(
                    0,
                    topMargin,
                    moveWidth,
                    canvas.getHeight()
            );

            Shader gradient = new LinearGradient(0, 0, rectF.width(), 0, mColor, mPostion, Shader.TileMode.CLAMP);
            mPaintBar.setShader(gradient);
        } else {
            rectF = new RectF(
                    0,
                    topMargin,
                    moveWidth,
                    canvas.getHeight()
            );

            Shader gradient = new LinearGradient(0, 0, rectF.width(), 0, mReverseColor, mPostion, Shader.TileMode.CLAMP);
            mPaintBar.setShader(gradient);
        }

        canvas.drawRoundRect(
                rectF,
                cornersRadius,
                cornersRadius,
                mPaintBar
        );

        if (!TextUtils.isEmpty(mText)) {
            int cornersRadiusText = 15;

            if (mIsInverse && mPercent == 0) {
                Rect rectText = new Rect();
                mPaintText.getTextBounds(mText, 0, mText.length(), rectText);

                RectF rectBubbleRight = new RectF((int) (moveWidth - rectText.width() - (mTextMargin * 2)),
                        0,
                        (int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 8));

                mPaintTextBG.setColor(getResources().getColor(R.color.colorC9F5FF));
                canvas.drawRoundRect(
                        rectBubbleRight,
                        cornersRadiusText,
                        cornersRadiusText,
                        mPaintTextBG
                );

                Point point01 = new Point((int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 4));
                Point point02 = new Point((int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 11));
                Point point03 = new Point((int) (moveWidth) - (int) Utils.dpToPixel(mContext, 6),
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 11));
                drawTriangle(point01, point02, point03, canvas, mPaintTextTriangleBG);

                mPaintText.setColor(getResources().getColor(R.color.black));
                canvas.drawText(mText, moveWidth - rectText.width() - mTextMargin, rectText.height() + mTextMarginTop, mPaintText);

            } else if (mPercent <= 50) {
                Rect rectText = new Rect();
                mPaintText.getTextBounds(mText, 0, mText.length(), rectText);

                RectF rectBubbleLeft = new RectF((int) moveWidth,
                        0,
                        (int) (moveWidth + rectText.width() + (mTextMargin * 2)),
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 8));

                mPaintTextBG.setColor(getResources().getColor(R.color.colorC9F5FF));
                canvas.drawRoundRect(
                        rectBubbleLeft,
                        cornersRadiusText,
                        cornersRadiusText,
                        mPaintTextBG
                );

                Point point01 = new Point((int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 4));
                Point point02 = new Point((int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 11));
                Point point03 = new Point((int) moveWidth + (int) Utils.dpToPixel(mContext, 6),
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 11));
                drawTriangle(point01, point02, point03, canvas, mPaintTextTriangleBG);

                mPaintText.setColor(getResources().getColor(R.color.black));
                canvas.drawText(mText, moveWidth + mTextMargin, rectText.height() + mTextMarginTop, mPaintText);
            } else {
                Rect rectText = new Rect();
                mPaintText.getTextBounds(mText, 0, mText.length(), rectText);

                RectF rectBubbleRight = new RectF((int) (moveWidth - rectText.width() - (mTextMargin * 2)),
                        0,
                        (int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 8));

                mPaintTextBG.setColor(getResources().getColor(R.color.colorC9F5FF));
                canvas.drawRoundRect(
                        rectBubbleRight,
                        cornersRadiusText,
                        cornersRadiusText,
                        mPaintTextBG
                );

                Point point01 = new Point((int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 4));
                Point point02 = new Point((int) moveWidth,
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 11));
                Point point03 = new Point((int) (moveWidth) - (int) Utils.dpToPixel(mContext, 6),
                        canvas.getHeight() - (int) Utils.dpToPixel(mContext, mHeight) - (int) Utils.dpToPixel(mContext, 11));
                drawTriangle(point01, point02, point03, canvas, mPaintTextTriangleBG);

                mPaintText.setColor(getResources().getColor(R.color.black));
                canvas.drawText(mText, moveWidth - rectText.width() - mTextMargin, rectText.height() + mTextMarginTop, mPaintText);
            }
        }
    }

    final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!mIsInverse) {
                if (mPercent < mLimitCount) {
                    mPercent = mPercent + 1.0F;
                    mHandler.postDelayed(runnable, mSpeed);
                }
            } else {
                if (mPercent > mLimitCount) {
                    mPercent = mPercent - 1.0F;
                    mHandler.postDelayed(runnable, mSpeed);
                }
            }
            invalidate();
        }
    };

    /**
     * 그래프 그리기 시작
     */
    public void startDraw() {
        mPaintBar.setStrokeWidth(1);
        mPaintBar.setStyle(Paint.Style.FILL);

        mPaintBarBG.setStrokeWidth(1);
        mPaintBarBG.setStyle(Paint.Style.FILL);

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
            (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            mPaintBarBG.setColor(mContext.getResources().getColor(R.color.color4B555F));
        } else {
            mPaintBarBG.setColor(mContext.getResources().getColor(R.color.colorEBF5F6));
        }

        mPaintText.setColor(mContext.getResources().getColor(R.color.black));
        mPaintText.setTextSize(Utils.dpToPixel(mContext, 12));

        mHandler.post(runnable);
    }

    /**
     * 그리기 스피드 설정
     * default : 0
     *
     * @param speed : 0~5 step (1L ~ 80L)
     */
    public void setSpeed(int speed) {
        int mSpeed = 0;
        if (speed < 0)
            speed = 0;
        else if (speed > 4)
            speed = 4;

        switch (speed) {
            case 0:
                this.mSpeed = 1L;
                break;
            case 1:
                this.mSpeed = 5L;
                break;
            case 2:
                this.mSpeed = 10L;
                break;
            case 3:
                this.mSpeed = 20L;
                break;
            case 4:
                this.mSpeed = 40L;
                break;
            case 5:
                this.mSpeed = 80L;
                break;
            default:
                this.mSpeed = mSpeed;
                break;
        }
    }

     /**
     * 그래프 컬러 설정
     * default : Green, Blue
     *
     * @param startColor : 시작 컬러
     * @param endColor   종료 컬러
     */
    public void setColor(int startColor, int endColor) {
        mStartColor = startColor;
        mEndColor = endColor;
    }

    /**
     * 그래프 두께 설정
     * default : 0
     *
     * @param height : 두께(0 - 4)
     */
    public void setHeight(int height) {
        mHeight = height;
    }

    public void setText(String text) {
        mText = text;
    }

    public void setTextMargin(int height) {
        mTextMargin = Utils.dpToPixel(mContext, height);
    }

    public void setIsInverse(boolean isInverse) {
        mIsInverse = isInverse;
    }

    /**
     * 그래프 범위 설정
     * default : 100
     *
     * @param percent : 0 - 100
     */
    public void setPercent(int percent) {
        if (percent < 0)
            mLimitCount = 0;
        else if (percent > 100)
            mLimitCount = 100;
        else {
            if (mIsInverse)
                mLimitCount = 100 - percent;
            else
                mLimitCount = percent;
        }

        if (!mIsInverse)
            this.mPercent = 0.0F;
        else
            this.mPercent = 100.0F;
    }

    public void drawTriangle(Point point01, Point point02, Point point03, Canvas canvas, Paint paint) {
        paint.setColor(getResources().getColor(R.color.colorC9F5FF));

        paint.setStrokeWidth(1);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(point01.x, point01.y);
        path.lineTo(point01.x, point01.y);
        path.lineTo(point02.x, point02.y);
        path.lineTo(point03.x, point03.y);
        path.close();

        canvas.drawPath(path, paint);
    }
}
