package com.sbi.saidabank.common.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.activity.transaction.TransferPhoneActivity;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CancelContinueDialog;
import com.sbi.saidabank.common.dialog.SelectTransferDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.LineProgressView;
import com.sbi.saidabank.customview.RollingMainTextView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.AccountInfo;
import com.sbi.saidabank.define.datatype.AdsItemInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.define.datatype.SlidingItemInfo;
import com.sbi.saidabank.define.datatype.TagItemInfo;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabank_android
 * Class: AccountAdapter
 * Created by 950485 on 2019. 03. 18..
 * <p>
 * Description:메인화면 표시 계좌 adapter
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountListViewHolder> {
    private BaseActivity activity;

    private ArrayList<AccountInfo>  listAccountInfo;
    private boolean                 isExistCard;
    private int                     showAdsIndex = 0;
    private ClickListener           listener;

    private String[] codeWeek = {"일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"};

    public AccountAdapter(BaseActivity activity, ArrayList<AccountInfo> listAccountInfo, boolean isExistCard, ClickListener listener) {
        this.activity = activity;
        this.listAccountInfo = listAccountInfo;
        this.isExistCard = isExistCard;
        this.listener = listener;
    }

    public class AccountListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RelativeLayout   layoutListMain;

        public RelativeLayout   layoutMainTop;
        public ImageView        imageAlram;
        public ImageView        imageAlramNoti;
        public ImageView        imageProfile;
        public ImageView        imageProfileBg;
        public TextView         textUserName;
        public TextView         textUserNameDesc;
        public LinearLayout     layoutRollingView;
        public FrameLayout      layoutRollingTextView;
        public TextView         textTagTitle;
        public LinearLayout     layoutListTag;
        public RecyclerView     recyclerviewTaglist;
        public ImageView        imageStar01;
        public ImageView        imageStar02;
        public ImageView        imageStar03;
        public ImageView        imageStar04;
        public ImageView        imageStar05;
        public ImageView        imageStar06;

        public LinearLayout     layoutJoin;
        public View             viewAddAccount;
        public TextView         textAddAccount;
        public ImageView        btnAddAccount;

        public LinearLayout     layoutAccount;
        public LinearLayout     layoutAccountTop;
        public TextView         textType;
        public View             viewBar;
        public TextView         textNameAccount;
        public TextView         textAmountAccount;
        public TextView         textACNO;
        public ImageView        btnCopyAccount;
        public TextView         textMsgAccount;

        public LinearLayout     layoutNormalType;
        public RelativeLayout   btnTransferNormal;
        public RelativeLayout   btnTransCardNormal;
        public RelativeLayout   btnCardNormal;
        public RelativeLayout   btnManageNormal;

        public LinearLayout     layoutGoOnType;
        public RelativeLayout   btnContinueCancel;
        public ImageView        imageContinueCancel;
        public RelativeLayout   btnContinueJoin;

        public LinearLayout     layoutMinusType;
        public LineProgressView lineViewMinus;
        public TextView         textAmountMinus;
        public TextView         textMsgMinus;
        public RelativeLayout   btnTransferMinus;
        public RelativeLayout   btnTransCardMinus;
        public RelativeLayout   btnCardMinus;
        public RelativeLayout   btnManageMinus;

        public LinearLayout     layoutSavingsType;
        public LineProgressView lineViewSavings;
        public TextView         textPayDateSavings;
        public TextView         textDueDateSavings;
        public LinearLayout     layoutSavings;
        public RelativeLayout   btnManageSavings;
        public RelativeLayout   btnSetExpireSavings;
        public RelativeLayout   btnFinishExpireSavings;
        public LinearLayout     layoutInstallSavings;
        public RelativeLayout   btnAddPayInstallSavings;
        public RelativeLayout   btnSetExpireInstallSavings;
        public RelativeLayout   btnFinishExpireInstallSavings;
        public RelativeLayout   btnManageInstallSavings;

        public LinearLayout     layoutLoanType;
        public View             viewLoanBar;
        public LineProgressView lineViewLoan;
        public TextView         textPayDateLoan;
        public TextView         textAmountLoan;
        public RelativeLayout   btnLoanRepay;
        public RelativeLayout   btnLoanManage;
        public TextView         textJoinLoadTitle;
        public RelativeLayout   layoutJoinLoadCircle;
        public ImageView        imageJoinLoadCircle;
        public TextView         textJoinLoadDays;
        public TextView         textJoinLoadState;
        public TextView         textJoinLoadDesc;

        public LinearLayout     layoutReportCredit;
        public View             viewReportBar;
        public TextView         textReportCreditTitle;
        public TextView         textReportCreditSubtitle01;
        public TextView         textReportCreditSubtitle02;
        public LinearLayout     btnLinkReport;
        public TextView         textInquireCredit;
        public ImageView        imageInquireArrow;

        public LinearLayout     layoutAdsType;
        public View             viewAdsBar;
        public ImageView        imageAds;

        private SelectTransferDialog dlgSelectTransfer;

        public AccountListViewHolder(Context context, View view, int viewType) {
            super(view);
            layoutListMain = (RelativeLayout) view.findViewById(R.id.layout_main_account_list);
            layoutMainTop = (RelativeLayout) view.findViewById(R.id.layout_main_top);
            imageAlram = (ImageView) view.findViewById(R.id.imageview_alram_title);
            imageAlram.setOnClickListener(this);
            imageAlramNoti = (ImageView) view.findViewById(R.id.imageview_alram_noti);
            imageProfile = (ImageView) view.findViewById(R.id.imageview_profile);
            imageProfile.setOnClickListener(this);
            imageProfileBg = (ImageView) view.findViewById(R.id.imageview_profile_bg);
            textUserName = (TextView) view.findViewById(R.id.textview_user_name);
            textUserNameDesc = (TextView) view.findViewById(R.id.textview_user_name_desc);
            layoutRollingView = (LinearLayout) view.findViewById(R.id.layout_rolling_textview);
            layoutRollingTextView = view.findViewById(R.id.ll_rollingtextview);
            textTagTitle = (TextView) view.findViewById(R.id.recyclerview_tag_title);
            layoutListTag = (LinearLayout) view.findViewById(R.id.layout_transfer_recently_tag);
            recyclerviewTaglist = (RecyclerView) view.findViewById(R.id.recyclerview_tag_list);
            imageStar01 = (ImageView) view.findViewById(R.id.imageview_star01);
            imageStar02 = (ImageView) view.findViewById(R.id.imageview_star02);
            imageStar03 = (ImageView) view.findViewById(R.id.imageview_star03);
            imageStar04 = (ImageView) view.findViewById(R.id.imageview_star04);
            imageStar05 = (ImageView) view.findViewById(R.id.imageview_star05);
            imageStar06 = (ImageView) view.findViewById(R.id.imageview_star06);
            layoutJoin = (LinearLayout) view.findViewById(R.id.layout_join_account);
            viewAddAccount = (View) view.findViewById(R.id.view_add_account);
            textAddAccount = (TextView) view.findViewById(R.id.textview_add_account);
            btnAddAccount = (ImageView) view.findViewById(R.id.imageview_add_account);
            btnAddAccount.setOnClickListener(this);
            layoutAccount = (LinearLayout)  view.findViewById(R.id.layout_account);
            layoutAccount.setOnClickListener(this);
            layoutAccountTop = (LinearLayout) view.findViewById(R.id.layout_account_top);
            textType = (TextView) view.findViewById(R.id.textview_account_type);
            viewBar = (View) view.findViewById(R.id.view_account_bar);
            textNameAccount = (TextView) view.findViewById(R.id.textview_name_account);
            textAmountAccount = (TextView) view.findViewById(R.id.textview_amount_account);
            textACNO = (TextView) view.findViewById(R.id.textview_ac_no);
            btnCopyAccount = (ImageView) view.findViewById(R.id.imageview_copy_account);
            textMsgAccount = (TextView) view.findViewById(R.id.textview_msg_account);
            btnCopyAccount.setOnClickListener(this);

            layoutGoOnType = (LinearLayout) view.findViewById(R.id.layout_go_on_type);
            viewLoanBar = (View) view.findViewById(R.id.view_loan_bar);
            textJoinLoadTitle = (TextView) view.findViewById(R.id.textview_join_loan_title);
            layoutJoinLoadCircle = (RelativeLayout) view.findViewById(R.id.layout_join_loan_circle);
            imageJoinLoadCircle = (ImageView) view.findViewById(R.id.imageview_join_loan_circle);
            textJoinLoadDays = (TextView) view.findViewById(R.id.textview_join_loan_days);
            textJoinLoadState = (TextView) view.findViewById(R.id.textview_join_loan_state);
            textJoinLoadDesc = (TextView) view.findViewById(R.id.textview_join_loan_desc);
            btnContinueCancel = (RelativeLayout) view.findViewById(R.id.layout_continue_cancel);
            btnContinueCancel.setOnClickListener(this);
            imageContinueCancel = (ImageView) view.findViewById(R.id.imageview_continue_cancel);
            btnContinueJoin = (RelativeLayout) view.findViewById(R.id.layout_continue_join_loan);
            btnContinueJoin.setOnClickListener(this);

            layoutNormalType = (LinearLayout) view.findViewById(R.id.layout_normal_type);
            btnTransferNormal = (RelativeLayout) view.findViewById(R.id.layout_transfer_normal);
            btnTransferNormal.setOnClickListener(this);
            btnTransCardNormal = (RelativeLayout) view.findViewById(R.id.layout_transaction_card_normal);
            btnTransCardNormal.setOnClickListener(this);
            btnCardNormal = (RelativeLayout) view.findViewById(R.id.layout_card_normal);
            btnCardNormal.setOnClickListener(this);
            btnManageNormal = (RelativeLayout) view.findViewById(R.id.layout_manage_normal);
            btnManageNormal.setOnClickListener(this);

            layoutMinusType = (LinearLayout) view.findViewById(R.id.layout_minus_type);
            lineViewMinus = (LineProgressView) view.findViewById(R.id.lineview_minus);
            textAmountMinus = (TextView) view.findViewById(R.id.textview_amount_minus);
            textMsgMinus = (TextView) view.findViewById(R.id.textview_msg_minus);
            btnTransferMinus = (RelativeLayout) view.findViewById(R.id.layout_transfer_minus);
            btnTransferMinus.setOnClickListener(this);
            btnTransCardMinus = (RelativeLayout) view.findViewById(R.id.layout_transaction_card_minus);
            btnTransCardMinus.setOnClickListener(this);
            btnCardMinus = (RelativeLayout) view.findViewById(R.id.layout_card_minus);
            btnCardMinus.setOnClickListener(this);
            btnManageMinus = (RelativeLayout) view.findViewById(R.id.layout_manage_minus);
            btnManageMinus.setOnClickListener(this);

            layoutSavingsType = (LinearLayout) view.findViewById(R.id.layout_savings_type);
            lineViewSavings = (LineProgressView) view.findViewById(R.id.lineview_savings);
            textPayDateSavings = (TextView) view.findViewById(R.id.textview_pay_date_savings);
            textDueDateSavings = (TextView) view.findViewById(R.id.textview_due_date_savings);
            layoutSavings = (LinearLayout) view.findViewById(R.id.layout_savings);
            btnManageSavings = (RelativeLayout) view.findViewById(R.id.layout_manage_savings);
            btnManageSavings.setOnClickListener(this);
            btnSetExpireSavings = (RelativeLayout) view.findViewById(R.id.layout_set_expire_savings);
            btnSetExpireSavings.setOnClickListener(this);
            btnFinishExpireSavings = (RelativeLayout) view.findViewById(R.id.layout_finish_expire_savings);
            btnFinishExpireSavings.setOnClickListener(this);
            layoutInstallSavings = (LinearLayout) view.findViewById(R.id.layout_install_savings);
            btnAddPayInstallSavings = (RelativeLayout) view.findViewById(R.id.layout_add_pay_install_savings);
            btnAddPayInstallSavings.setOnClickListener(this);
            btnSetExpireInstallSavings = (RelativeLayout) view.findViewById(R.id.layout_set_expire_install_savings);
            btnSetExpireInstallSavings.setOnClickListener(this);
            btnFinishExpireInstallSavings = (RelativeLayout) view.findViewById(R.id.layout_finish_expire_install_savings);
            btnFinishExpireInstallSavings.setOnClickListener(this);
            btnManageInstallSavings = (RelativeLayout) view.findViewById(R.id.layout_manage_install_savings);
            btnManageInstallSavings.setOnClickListener(this);

            layoutLoanType = (LinearLayout) view.findViewById(R.id.layout_loan_type);
            lineViewLoan = (LineProgressView) view.findViewById(R.id.lineview_loan);
            textPayDateLoan = (TextView) view.findViewById(R.id.textview_pay_date_loan);
            textAmountLoan = (TextView) view.findViewById(R.id.textview_amount_loan);
            btnLoanRepay = (RelativeLayout) view.findViewById(R.id.layout_loan_repay);
            btnLoanRepay.setOnClickListener(this);
            btnLoanManage = (RelativeLayout) view.findViewById(R.id.layout_loan_manage);
            btnLoanManage.setOnClickListener(this);

            layoutReportCredit = (LinearLayout) view.findViewById(R.id.layout_report_credit);
            viewReportBar = (View) view.findViewById(R.id.view_report_credit_bar);
            textReportCreditTitle = (TextView) view.findViewById(R.id.textview_report_credit_title);
            textReportCreditSubtitle01 = (TextView) view.findViewById(R.id.textview_report_credit_subtitle01);
            textReportCreditSubtitle02 = (TextView) view.findViewById(R.id.textview_report_credit_subtitle02);
            btnLinkReport = (LinearLayout) view.findViewById(R.id.layout_inquire_credit);
            btnLinkReport.setOnClickListener(this);
            textInquireCredit = (TextView) view.findViewById(R.id.textview_inquire_credit);
            imageInquireArrow = (ImageView) view.findViewById(R.id.imageview_inquire_credit_arrow);

            layoutAdsType = (LinearLayout) view.findViewById(R.id.layout_ads_type);
            layoutAdsType.setOnClickListener(this);
            viewAdsBar = (View) view.findViewById(R.id.view_ads_bar);
            imageAds = (ImageView) view.findViewById(R.id.image_view_adsview);
        }

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if (view == imageProfile) {
                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.MYP0010100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                activity.startActivity(intent);
                return;

            } else if (view == imageAlram) {
                if (imageAlramNoti.isShown())
                    imageAlramNoti.setVisibility(View.GONE);

                listener.OnAlramClickListener();

            } else if (view == layoutJoin) {
                listener.OnJoinClickListener();

            } else if (view == btnAddAccount) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                Const.ADD_ACCOUNT_MODE mode = accountInfo.getADD_ACCOUNT_MODE();
                if (mode == Const.ADD_ACCOUNT_MODE.NOMAL_MODE)
                    listener.OnAddClickListener();
                else if (mode == Const.ADD_ACCOUNT_MODE.SAVINGS_MODE)
                    listener.OnJoinClickListener();

            } else if (view == layoutAccount) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String DSCT_CD = accountInfo.getDSCT_CD();
                String CMPH_BNKB_LOAN_YN = accountInfo.getCMPH_BNKB_LOAN_YN();
                String ACNO = accountInfo.getACNO();
                if (Const.ACCOUNT_NOMAL_REP.equalsIgnoreCase(DSCT_CD) ||
                    Const.ACCOUNT_NOMAL.equalsIgnoreCase(DSCT_CD)) {
                    if (TextUtils.isEmpty(ACNO))
                        return;

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.INQ0010100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param =  "ACNO=" + ACNO;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    activity.startActivity(intent);

                } else if (Const.REQUEST_WAS_YES.equalsIgnoreCase(CMPH_BNKB_LOAN_YN)) {
                    if (TextUtils.isEmpty(ACNO))
                        return;

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.INQ0010100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param =  "ACNO=" + ACNO;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    activity.startActivity(intent);

                } else if (Const.ACCOUNT_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
                    if (TextUtils.isEmpty(ACNO))
                        return;

                    String url = WasServiceUrl.UNT0360100.getServiceUrl();
                    Intent intent = new Intent(activity, WebMainActivity.class);
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param =  "ACNO=" + ACNO;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    activity.startActivity(intent);

                } else if (Const.ACCOUNT_INSTALL_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
                    if (TextUtils.isEmpty(ACNO))
                        return;

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.UNT0370100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param =  "ACNO=" + ACNO;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    activity.startActivity(intent);

                } else if (Const.ACCOUNT_LOAN.equalsIgnoreCase(DSCT_CD)) {
                    String ACCO_IDNO = accountInfo.getACCO_IDNO();
                    if (TextUtils.isEmpty(ACNO)  || TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                        return;

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.LON0050200.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String SUBJ_CD = ACCO_IDNO.substring(0, 2);
                    String param =  "ACNO=" + ACNO + "&ACCO_IDNO=" + ACCO_IDNO + "&SUBJ_CD=" + SUBJ_CD;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    activity.startActivity(intent);
                }
            } else if (view == btnCopyAccount) {
                String accountNo = textACNO.getText().toString();
                if (TextUtils.isEmpty(accountNo))
                    return;

                accountNo = accountNo.replaceAll("[^\\d]", "");

                ClipboardManager cm = (ClipboardManager) activity.getSystemService(activity.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText(activity.getString(R.string.app_name), accountNo);
                cm.setPrimaryClip(clipData);

                Toast.makeText(activity, "계좌번호가 복사되었습니다.", Toast.LENGTH_SHORT).show();

            } else if (view == btnTransferNormal) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                final String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                if (dlgSelectTransfer != null)
                    dlgSelectTransfer = null;

                dlgSelectTransfer = new SelectTransferDialog(activity, new SelectTransferDialog.OnConfirmListener() {
                    @Override
                    public void onConfirmPress(int selectIndex) {
                        if (selectIndex == 1) {
                            Intent intent = new Intent(activity, TransferAccountActivity.class);
                            if (!TextUtils.isEmpty(ACNO))
                                intent.putExtra(Const.INTENT_MAIN_TRANSFER_SAVINGS, ACNO);
                            activity.startActivityForResult(intent, Const.REQUEST_TRANFER_SAVINGS);
                        } else if (selectIndex == 2) {
                            Intent intent = new Intent(activity, TransferPhoneActivity.class);
                            if (!TextUtils.isEmpty(ACNO))
                                intent.putExtra(Const.INTENT_MAIN_TRANSFER_SAVINGS, ACNO);
                            activity.startActivityForResult(intent, Const.REQUEST_TRANFER_SAVINGS);
                        }

                        dlgSelectTransfer.dismiss();
                    }
                });
                dlgSelectTransfer.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (dlgSelectTransfer != null)
                            dlgSelectTransfer = null;
                    }
                });
                dlgSelectTransfer.show();

            } else if (view == btnTransCardNormal) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String CANO = accountInfo.getCANO();
                if (TextUtils.isEmpty(CANO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.CKC0040101.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "CANO=" + CANO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnCardNormal) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                final String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.CKC0010101.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnManageNormal) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0060100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnTransferMinus) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                final String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                if (dlgSelectTransfer != null)
                    dlgSelectTransfer = null;

                dlgSelectTransfer = new SelectTransferDialog(activity, new SelectTransferDialog.OnConfirmListener() {
                    @Override
                    public void onConfirmPress(int selectIndex) {
                        if (selectIndex == 1) {
                            Intent intent = new Intent(activity, TransferAccountActivity.class);
                            if (!TextUtils.isEmpty(ACNO))
                                intent.putExtra(Const.INTENT_MAIN_TRANSFER_SAVINGS, ACNO);
                            activity.startActivityForResult(intent, Const.REQUEST_TRANFER_SAVINGS);
                        } else if (selectIndex == 2) {
                            Intent intent = new Intent(activity, TransferPhoneActivity.class);
                            if (!TextUtils.isEmpty(ACNO))
                                intent.putExtra(Const.INTENT_MAIN_TRANSFER_SAVINGS, ACNO);
                            activity.startActivityForResult(intent, Const.REQUEST_TRANFER_SAVINGS);
                        }

                        dlgSelectTransfer.dismiss();
                    }
                });
                dlgSelectTransfer.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (dlgSelectTransfer != null)
                            dlgSelectTransfer = null;
                    }
                });
                dlgSelectTransfer.show();

            } else if (view == btnContinueCancel) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                showCancelContinue(accountInfo);

            } else if (view == btnContinueJoin) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String DSCT_CD = accountInfo.getDSCT_CD();
                if (TextUtils.isEmpty(DSCT_CD))
                    return;

                if (Const.ACCOUNT_GO_ON_NOMAL.equalsIgnoreCase(DSCT_CD))  {
                    String ACCO_IDNO = accountInfo.getACCO_IDNO();
                    if (TextUtils.isEmpty(ACCO_IDNO))
                        return;

                    requestGoOnNomal(ACCO_IDNO);
                    return;
                }

                String CANO = accountInfo.getCANO();
                String PROP_STEP_CD = accountInfo.getPROP_STEP_CD();
                if (TextUtils.isEmpty(CANO) || TextUtils.isEmpty(PROP_STEP_CD))
                    return;

                requestGoOn(DSCT_CD, CANO, PROP_STEP_CD);

            } else if (view == btnTransCardMinus) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String CANO = accountInfo.getCANO();
                if (TextUtils.isEmpty(CANO)) {
                    return;
                }

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.CKC0040101.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "CANO=" + CANO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnCardMinus) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                final String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.CKC0010101.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnManageMinus) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0060100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnManageSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0260100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnSetExpireSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0290100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnFinishExpireSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                String DSCT_CD = accountInfo.getDSCT_CD();
                if (TextUtils.isEmpty(DSCT_CD) || TextUtils.isEmpty(ACNO))
                    return;

                requestFinishAccount(DSCT_CD, ACNO);

            } else if (view == btnAddPayInstallSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0190100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnSetExpireInstallSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0210100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnFinishExpireInstallSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0250100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnManageInstallSavings) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0180100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String param =  "ACNO=" + ACNO;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnLoanRepay) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                String ACCO_IDNO = accountInfo.getACCO_IDNO();
                if (TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                    return;

                String SUBJ_CD = ACCO_IDNO.substring(0, 2);

                requestLoanRepay(ACNO, ACCO_IDNO, SUBJ_CD);

            } else if (view == btnLoanManage) {
                AccountInfo accountInfo = listAccountInfo.get(position);
                if (accountInfo == null)
                    return;

                String ACNO = accountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                String ACCO_IDNO = accountInfo.getACCO_IDNO();
                if (TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                    return;

                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.LON0050100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String SUBJ_CD = ACCO_IDNO.substring(0, 2);
                String param =  "ACNO=" + ACNO + "&ACCO_IDNO=" + ACCO_IDNO + "&SUBJ_CD=" + SUBJ_CD;
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);

            } else if (view == btnLinkReport) {
                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.CRD0010100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                activity.startActivity(intent);
            } else if (view == layoutAdsType) {
                listener.OnAdsItemClickListener(showAdsIndex);
            }
        }
    }

    @Override
    public AccountAdapter.AccountListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_account_list_item, parent, false);
        AccountListViewHolder viewHolder = new AccountListViewHolder(activity, itemView, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AccountListViewHolder viewHolder, final int position) {
        viewHolder.layoutJoin.setTag(position);

        viewHolder.btnAddAccount.setTag(position);

        viewHolder.imageAlram.setTag(position);
        viewHolder.imageProfile.setTag(position);
        viewHolder.layoutAccount.setTag(position);
        viewHolder.layoutAccountTop.setTag(position);
        viewHolder.recyclerviewTaglist.setTag(position);
        viewHolder.layoutRollingTextView.setTag(position);

        viewHolder.btnCopyAccount.setTag(position);
        viewHolder.btnTransferNormal.setTag(position);
        viewHolder.btnTransCardNormal.setTag(position);
        viewHolder.btnCardNormal.setTag(position);
        viewHolder.btnManageNormal.setTag(position);

        viewHolder.btnTransferMinus.setTag(position);
        viewHolder.btnTransCardMinus.setTag(position);
        viewHolder.btnCardMinus.setTag(position);
        viewHolder.btnManageMinus.setTag(position);

        viewHolder.btnContinueCancel.setTag(position);
        viewHolder.btnContinueJoin.setTag(position);

        viewHolder.btnManageSavings.setTag(position);
        viewHolder.btnSetExpireSavings.setTag(position);
        viewHolder.btnFinishExpireSavings.setTag(position);
        viewHolder.btnAddPayInstallSavings.setTag(position);
        viewHolder.btnSetExpireInstallSavings.setTag(position);
        viewHolder.btnFinishExpireInstallSavings.setTag(position);
        viewHolder.btnManageInstallSavings.setTag(position);

        viewHolder.btnLoanRepay.setTag(position);
        viewHolder.btnLoanManage.setTag(position);

        viewHolder.btnLinkReport.setTag(position);

        viewHolder.layoutAdsType.setTag(position);

        showTimeMode(viewHolder);

        AccountInfo accountInfo = listAccountInfo.get(position);
        if (accountInfo == null)
            return;

        String TRNF_DEPR_NM = accountInfo.getTRNF_DEPR_NM();
        if (!TextUtils.isEmpty(TRNF_DEPR_NM)) {
            viewHolder.textNameAccount.setText(TRNF_DEPR_NM);
        }

        String ACNO = accountInfo.getACNO();
        if (!TextUtils.isEmpty(ACNO)) {
            String account = ACNO.substring(0, 5) + "-" + ACNO.substring(5, 7) + "-" + ACNO.substring(7, ACNO.length());
            viewHolder.textACNO.setText(account);
        }

        String DSCT_CD = accountInfo.getDSCT_CD();
        String CMPH_BNKB_LOAN_YN = accountInfo.getCMPH_BNKB_LOAN_YN();
        if (Const.ACCOUNT_TOP.equalsIgnoreCase(DSCT_CD)) {
            setMainTop(viewHolder, accountInfo);

        } if (Const.ACCOUNT_NOMAL_REP.equalsIgnoreCase(DSCT_CD)) {
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(CMPH_BNKB_LOAN_YN)) {
                setMinusType(viewHolder, accountInfo);
            } else
                setNormalType(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_NOMAL.equalsIgnoreCase(DSCT_CD)) {
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(CMPH_BNKB_LOAN_YN)) {
                setMinusType(viewHolder, accountInfo);
            } else
                setNormalType(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_GO_ON_NOMAL.equalsIgnoreCase(DSCT_CD) ||
            Const.ACCOUNT_GO_ON_LOAN.equalsIgnoreCase(DSCT_CD) ||
            Const.ACCOUNT_GO_ON_MINUS.equalsIgnoreCase(DSCT_CD) ||
            Const.ACCOUNT_GO_ON_SMALL_MINUS.equalsIgnoreCase(DSCT_CD)) {
            setGoOnType(viewHolder, accountInfo);

        } else if (Const.REQUEST_WAS_YES.equalsIgnoreCase(CMPH_BNKB_LOAN_YN)) {
            setMinusType(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
            setSavingsType(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_INSTALL_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
            setInstallSavingsType(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_LOAN.equalsIgnoreCase(DSCT_CD)) {
            setLoanType(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_NO.equalsIgnoreCase(DSCT_CD)) {
            setAddAccount(viewHolder, accountInfo);

        } else if (Const.ACCOUNT_REPORT.equalsIgnoreCase(DSCT_CD)) {
            setListReportCredit(viewHolder);

        } else if (Const.ACCOUNT_ADS.equalsIgnoreCase(DSCT_CD)) {
            setListAds(viewHolder, accountInfo);
        }
    }

    private void setMainTop(AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        boolean isPushData = Prefer.getFlagShowFirstNotiMsgBox(activity);
        if (isPushData) {
            if (!viewHolder.imageAlramNoti.isShown())
                viewHolder.imageAlramNoti.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageAlramNoti.setVisibility(View.GONE);
        }

        ArrayList<TagItemInfo> listTags = accountInfo.getListTags();
        if (listTags == null || listTags.size() <= 0) {
            viewHolder.layoutListTag.setVisibility(View.GONE);
        } else {
            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(activity);
            layoutManager.setJustifyContent(JustifyContent.FLEX_START);
            layoutManager.setFlexWrap(FlexWrap.WRAP);
            viewHolder.recyclerviewTaglist.setLayoutManager(layoutManager);
            viewHolder.recyclerviewTaglist.setHasFixedSize(true);
            viewHolder.recyclerviewTaglist.setAdapter(new TagAdapter(activity, listTags, new TagAdapter.TagClickListener() {
                @Override
                public void onGetSelectTag(int position, TagItemInfo tagInfo) {
                    listener.OnGetSelectTag(position, tagInfo);
                }
            }));
            viewHolder.layoutListTag.setVisibility(View.VISIBLE);
        }

        ArrayList<SlidingItemInfo> listSlidings = accountInfo.getListSlidings();
        if (listSlidings == null || listSlidings.size() <= 0)
            viewHolder.layoutRollingView.setVisibility(View.GONE);
        else {
            int size = listSlidings.size();

            String[] slideEvent = new String[size];
            viewHolder.layoutRollingTextView.removeAllViews();

            for (int index = 0; index < size; index++) {
                SlidingItemInfo slidingInfo = listSlidings.get(index);
                String FAX_SEND_PRGS_CNTN = slidingInfo.getFAX_SEND_PRGS_CNTN();
                if (!TextUtils.isEmpty(FAX_SEND_PRGS_CNTN))
                    slideEvent[index] = FAX_SEND_PRGS_CNTN;
                else
                    slideEvent[index] = "";
            }

            RollingMainTextView rollingTextView = new RollingMainTextView(viewHolder.layoutRollingTextView, slideEvent);
            rollingTextView.setOnRollingTextListener(new RollingMainTextView.IRollingTextSelected() {
                @Override
                public void OnRollingTextSelect(int index) {
                    listener.OnRollingTextSelect(index);
                }
            });

            viewHolder.layoutRollingView.setVisibility(View.VISIBLE);
        }

        String CUST_NM = LoginUserInfo.getInstance().getCUST_NM();
        if (!TextUtils.isEmpty(CUST_NM)) {
            CUST_NM = CUST_NM + "님";
            viewHolder.textUserName.setText(CUST_NM);
        }

        File storageDir = activity.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
        File file = new File(storageDir, Const.CROP_IMAGE_PATH);
        if (file.exists()) {
            viewHolder.imageProfile.setImageResource(0);
            viewHolder.imageProfile.setImageURI(Uri.fromFile(file));
        } else {
            viewHolder.imageProfile.setImageResource(0);
            viewHolder.imageProfile.setImageResource(R.drawable.ico_profile);
        }

        viewHolder.textType.setText("");
        viewHolder.layoutMainTop.setVisibility(View.VISIBLE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.GONE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);
    }

    private void setNormalType(AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        viewHolder.textType.setText("입출금");
        viewHolder.textMsgAccount.setText("");
        viewHolder.btnCopyAccount.setVisibility(View.VISIBLE);

        String LMIT_LMT_ACCO_YN = accountInfo.getLMIT_LMT_ACCO_YN();
        if (!TextUtils.isEmpty(LMIT_LMT_ACCO_YN)) {
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(LMIT_LMT_ACCO_YN)) {
                viewHolder.textMsgAccount.setText("한도제한계좌");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.color009BEB));
            }
        }

        // 1:거래정지, 2:지급정지, 3:출금정지
        String ACDT_DCL_CD = accountInfo.getACDT_DCL_CD();
        if (!TextUtils.isEmpty(ACDT_DCL_CD)) {
            if ("1".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("거래정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("2".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("지급정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("3".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("출금정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            }

            viewHolder.btnTransferNormal.setVisibility(View.GONE);
            viewHolder.btnTransCardNormal.setVisibility(View.GONE);
            viewHolder.btnCardNormal.setVisibility(View.GONE);
            viewHolder.btnManageNormal.setVisibility(View.VISIBLE);
        } else {
            viewHolder.btnTransferNormal.setVisibility(View.VISIBLE);
            viewHolder.btnTransCardNormal.setVisibility(View.GONE);
            viewHolder.btnCardNormal.setVisibility(View.GONE);
            viewHolder.btnManageNormal.setVisibility(View.GONE);
        }

        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.VISIBLE);
        viewHolder.layoutNormalType.setVisibility(View.VISIBLE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);

        String BLNC = accountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            viewHolder.textAmountAccount.setText(BLNC + activity.getString(R.string.won));
        }

        if (!isExistCard) {
            String CHCR_PROP_POSB_YN = accountInfo.getCHCR_PROP_POSB_YN();
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(CHCR_PROP_POSB_YN)) {
                viewHolder.btnCardNormal.setVisibility(View.VISIBLE);
            } else
                viewHolder.btnCardNormal.setVisibility(View.GONE);

            viewHolder.btnTransCardNormal.setVisibility(View.GONE);
        } else {
            String CANO = accountInfo.getCANO();
            String CHCR_PROP_PRGS_STCD = accountInfo.getCHCR_PROP_PRGS_STCD();
            if (!TextUtils.isEmpty(CANO) && "00".equalsIgnoreCase(CHCR_PROP_PRGS_STCD)) {
                viewHolder.btnTransCardNormal.setVisibility(View.VISIBLE);
            } else {
                viewHolder.btnTransCardNormal.setVisibility(View.GONE);
            }

            viewHolder.btnCardNormal.setVisibility(View.GONE);
        }
    }

    private void setGoOnType(AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        viewHolder.textType.setText("");
        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.GONE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.VISIBLE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);

        String REMN_DCNT = accountInfo.getREMN_DCNT();
        if (!TextUtils.isEmpty(REMN_DCNT)) {
            String days = "D-" + REMN_DCNT;
            viewHolder.textJoinLoadDays.setText(days);

            String desc = String.format(activity.getString(R.string.join_loan_desc), REMN_DCNT);
            viewHolder.textJoinLoadDesc.setText(desc);
        }

        String DSCT_CD = accountInfo.getDSCT_CD();
        String TRNF_DEPR_NM = accountInfo.getTRNF_DEPR_NM();
        if (Const.ACCOUNT_GO_ON_NOMAL.equalsIgnoreCase(DSCT_CD)) {
            if (TextUtils.isEmpty(TRNF_DEPR_NM))
                viewHolder.textJoinLoadTitle.setText(activity.getString(R.string.join_nomal_title));
            else
                viewHolder.textJoinLoadTitle.setText(TRNF_DEPR_NM);

            viewHolder.textJoinLoadState.setText(activity.getString(R.string.join_nomal_state_ing));

        } else if (Const.ACCOUNT_GO_ON_LOAN.equalsIgnoreCase(DSCT_CD)) {
            if (TextUtils.isEmpty(TRNF_DEPR_NM))
                viewHolder.textJoinLoadTitle.setText("");
            else
                viewHolder.textJoinLoadTitle.setText(TRNF_DEPR_NM);

            viewHolder.textJoinLoadState.setText(activity.getString(R.string.join_loan_state_ing));

        } else if (Const.ACCOUNT_GO_ON_MINUS.equalsIgnoreCase(DSCT_CD)) {
            if (TextUtils.isEmpty(TRNF_DEPR_NM))
                viewHolder.textJoinLoadTitle.setText("");
            else
                viewHolder.textJoinLoadTitle.setText(TRNF_DEPR_NM);

            viewHolder.textJoinLoadState.setText(activity.getString(R.string.join_loan_state_ing));

        } else if (Const.ACCOUNT_GO_ON_SMALL_MINUS.equalsIgnoreCase(DSCT_CD)) {
            if (TextUtils.isEmpty(TRNF_DEPR_NM))
                viewHolder.textJoinLoadTitle.setText("");
            else
                viewHolder.textJoinLoadTitle.setText(TRNF_DEPR_NM);

            viewHolder.textJoinLoadState.setText(activity.getString(R.string.join_loan_state_ing));
        }
    }

    private void setMinusType(final AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        viewHolder.textType.setText("마이너스");
        viewHolder.textMsgAccount.setText("");
        viewHolder.textMsgMinus.setText("");
        viewHolder.btnCopyAccount.setVisibility(View.VISIBLE);

        String LMIT_LMT_ACCO_YN = accountInfo.getLMIT_LMT_ACCO_YN();
        if (!TextUtils.isEmpty(LMIT_LMT_ACCO_YN)) {
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(LMIT_LMT_ACCO_YN)) {
                viewHolder.textMsgAccount.setText("한도제한계좌");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.color009BEB));
            }
        }

        // 1:거래정지, 2:지급정지, 3:출금정지
        String ACDT_DCL_CD = accountInfo.getACDT_DCL_CD();
        if (!TextUtils.isEmpty(ACDT_DCL_CD)) {
            if ("1".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("거래정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("2".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("지급정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("3".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("출금정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            }

            viewHolder.btnTransferMinus.setVisibility(View.GONE);
            viewHolder.btnTransCardMinus.setVisibility(View.GONE);
            viewHolder.btnCardMinus.setVisibility(View.GONE);
            viewHolder.btnManageMinus.setVisibility(View.VISIBLE);
        } else {
            viewHolder.btnTransferMinus.setVisibility(View.VISIBLE);
            viewHolder.btnTransCardMinus.setVisibility(View.GONE);
            viewHolder.btnCardMinus.setVisibility(View.GONE);
            viewHolder.btnManageMinus.setVisibility(View.GONE);
        }

        String OVRD_DVCD = accountInfo.getOVRD_DVCD();
        if ("1".equalsIgnoreCase(OVRD_DVCD)) {
            viewHolder.textMsgAccount.setText(activity.getString(R.string.msg_overdue));
        } else if ("2".equalsIgnoreCase(OVRD_DVCD)) {
            viewHolder.textMsgAccount.setText(activity.getString(R.string.loss_due_date));

            viewHolder.btnTransferMinus.setVisibility(View.GONE);
            viewHolder.btnTransCardMinus.setVisibility(View.GONE);
            viewHolder.btnCardMinus.setVisibility(View.GONE);
            viewHolder.btnManageMinus.setVisibility(View.VISIBLE);
        }

        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.VISIBLE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.VISIBLE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);

        final String BLNC = accountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            String amount = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            viewHolder.textAmountAccount.setText(amount + activity.getString(R.string.won));
        }

        final String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
        if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
            String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
            viewHolder.textAmountMinus.setText("출금가능 " + amount + activity.getString(R.string.won));
        }

        String CNTR_AMT = accountInfo.getCNTR_AMT();
        if (!TextUtils.isEmpty(CNTR_AMT)) {
            String amount = Utils.convertHangul(CNTR_AMT);
            viewHolder.textMsgMinus.setText("대출한도 " + amount + activity.getString(R.string.won));
        } else {
            viewHolder.textMsgMinus.setText("");
        }

        if (!TextUtils.isEmpty(WTCH_POSB_AMT) && !TextUtils.isEmpty(CNTR_AMT)) {
            final double dWTCH_POSB_AMT = Double.valueOf(WTCH_POSB_AMT);
            final double dCNTR_AMT = Double.valueOf(CNTR_AMT);
            final double dBLNC = Double.valueOf(BLNC);

            double gap = dCNTR_AMT - Math.abs(dWTCH_POSB_AMT);
            final int percent;

            if (dCNTR_AMT > 0 && dWTCH_POSB_AMT == 0) {
                percent = 100;
            } else if (dCNTR_AMT > 0 && dBLNC < 0) {
                percent = (int) ((Math.abs(gap) * 100) / dCNTR_AMT);
            } else {
                percent = 0;
            }

            Handler delayHandler = new Handler();
            delayHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolder.lineViewMinus.setPercent(percent);
                    viewHolder.lineViewMinus.setSpeed(0);
                    viewHolder.lineViewMinus.setHeight(8);
                    viewHolder.lineViewMinus.setTextMargin(10);
                    viewHolder.lineViewMinus.startDraw();
                }
            }, 100);
        } else {
            viewHolder.lineViewMinus.setVisibility(View.GONE);
        }

        if (!isExistCard) {
            String CHCR_PROP_POSB_YN = accountInfo.getCHCR_PROP_POSB_YN();
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(CHCR_PROP_POSB_YN))
                viewHolder.btnCardMinus.setVisibility(View.VISIBLE);
            else
                viewHolder.btnCardMinus.setVisibility(View.GONE);

            viewHolder.btnTransCardMinus.setVisibility(View.GONE);
        } else {
            String CANO = accountInfo.getCANO();
            String CHCR_PROP_PRGS_STCD = accountInfo.getCHCR_PROP_PRGS_STCD();
            if (!TextUtils.isEmpty(CANO) && "00".equalsIgnoreCase(CHCR_PROP_PRGS_STCD)) {
                viewHolder.btnTransCardMinus.setVisibility(View.VISIBLE);
            } else {
                viewHolder.btnTransCardMinus.setVisibility(View.GONE);
            }

            viewHolder.btnCardMinus.setVisibility(View.GONE);
        }
    }

    private void setSavingsType(final AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        viewHolder.textType.setText("예금");
        viewHolder.textMsgAccount.setText("");
        viewHolder.textDueDateSavings.setText("");
        viewHolder.textPayDateSavings.setText("");
        viewHolder.btnCopyAccount.setVisibility(View.GONE);

        // 1:거래정지, 2:지급정지, 3:출금정지
        String ACDT_DCL_CD = accountInfo.getACDT_DCL_CD();
        if (!TextUtils.isEmpty(ACDT_DCL_CD)) {
            if ("1".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("거래정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("2".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("지급정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("3".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("출금정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            }
        }

        String BLNC = accountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            viewHolder.textAmountAccount.setText(BLNC + activity.getString(R.string.won));
        }

        String EXPT_DT = accountInfo.getEXPT_DT();
        if (!TextUtils.isEmpty(EXPT_DT)) {
            String year = EXPT_DT.substring(0, 4);
            String month = EXPT_DT.substring(4, 6);
            String day = EXPT_DT.substring(6, 8);
            viewHolder.textDueDateSavings.setText("만기일 " + year + "." + month + "." + day);
        } else
            viewHolder.textDueDateSavings.setText("");

        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.VISIBLE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.VISIBLE);
        viewHolder.layoutSavings.setVisibility(View.VISIBLE);
        viewHolder.layoutInstallSavings.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);

        String TOT_DCNT = accountInfo.getTOT_DCNT();
        String REMN_DCNT = accountInfo.getREMN_DCNT();
        long lREMN_DCNT = 0;
        if (!TextUtils.isEmpty(TOT_DCNT) && !TextUtils.isEmpty(REMN_DCNT)) {
            viewHolder.lineViewSavings.setVisibility(View.VISIBLE);

            long lTOT_DCNT = Long.valueOf(TOT_DCNT);
            lREMN_DCNT = Long.valueOf(REMN_DCNT);

            if (lREMN_DCNT == 0) {
                REMN_DCNT = "만기";
                lTOT_DCNT = 1;
            }
            else {
                REMN_DCNT = "D-" + REMN_DCNT;
            }

            final String dspREMN_DCNT = REMN_DCNT;

            final int pecent;

            if (lTOT_DCNT > 0)
                pecent = (int) ((lTOT_DCNT - lREMN_DCNT) * 100 / lTOT_DCNT);
            else
                pecent = 0;

            Handler delayHandler = new Handler();
            delayHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolder.lineViewSavings.setPercent(pecent);
                    viewHolder.lineViewSavings.setSpeed(0);
                    viewHolder.lineViewSavings.setHeight(8);
                    viewHolder.lineViewSavings.setText(dspREMN_DCNT);
                    viewHolder.lineViewSavings.setTextMargin(10);
                    viewHolder.lineViewSavings.startDraw();
                }
            }, 100);
        } else {
            viewHolder.lineViewSavings.setVisibility(View.GONE);
        }

        String ATMT_EXPT_EXTS_CD = accountInfo.getATMT_EXPT_EXTS_CD();
        if (lREMN_DCNT <= 0) {
            viewHolder.btnManageSavings.setVisibility(View.VISIBLE);
            viewHolder.btnSetExpireSavings.setVisibility(View.GONE);
            viewHolder.btnFinishExpireSavings.setVisibility(View.VISIBLE);
        } else {
            if ("1".equalsIgnoreCase(ATMT_EXPT_EXTS_CD) ||
                "3".equalsIgnoreCase(ATMT_EXPT_EXTS_CD)) {
                viewHolder.btnManageSavings.setVisibility(View.VISIBLE);
                viewHolder.btnSetExpireSavings.setVisibility(View.GONE);
                viewHolder.btnFinishExpireSavings.setVisibility(View.GONE);
            } else if ("0".equalsIgnoreCase(ATMT_EXPT_EXTS_CD) ||
                "4".equalsIgnoreCase(ATMT_EXPT_EXTS_CD)) {
                viewHolder.btnManageSavings.setVisibility(View.VISIBLE);
                viewHolder.btnSetExpireSavings.setVisibility(View.VISIBLE);
                viewHolder.btnFinishExpireSavings.setVisibility(View.GONE);
            }
        }
    }

    private void setInstallSavingsType(final AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        viewHolder.textType.setText("적금");
        viewHolder.textMsgAccount.setText("");
        viewHolder.textDueDateSavings.setText("");
        viewHolder.textPayDateSavings.setText("");
        viewHolder.btnCopyAccount.setVisibility(View.VISIBLE);

        String BLNC = accountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            viewHolder.textAmountAccount.setText(BLNC + activity.getString(R.string.won));
        }

        String ATMT_EXPT_EXTS_CD = accountInfo.getATMT_EXPT_EXTS_CD();
        String CANO = accountInfo.getCANO();
        if (!TextUtils.isEmpty(CANO)) {
            String MM_PI_DD = accountInfo.getMM_PI_DD();
            String MM_INSA = accountInfo.getMM_INSA();
            if ("01".equalsIgnoreCase(CANO)) {
                if (!TextUtils.isEmpty(MM_PI_DD))
                    viewHolder.textPayDateSavings.setText("납입예정일 매월 " + MM_PI_DD + "일");
                else
                    viewHolder.textPayDateSavings.setText("");

                if (!TextUtils.isEmpty(MM_INSA)) {
                    MM_INSA = Utils.convertHangul(MM_INSA);
                    viewHolder.textMsgAccount.setText("매월 " + MM_INSA + activity.getString(R.string.won));
                    viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.color009BEB));
                }
            } else if ("07".equalsIgnoreCase(CANO)) {
                if (!TextUtils.isEmpty(MM_PI_DD)) {
                    int code = Integer.valueOf(MM_PI_DD);
                    String strCode = codeWeek[code];
                    if (!TextUtils.isEmpty(strCode))
                        viewHolder.textPayDateSavings.setText("납입예정일 매주 "  + strCode);
                    else
                        viewHolder.textPayDateSavings.setText("");
                }

                if (!TextUtils.isEmpty(MM_INSA)) {
                    MM_INSA = Utils.convertHangul(MM_INSA);
                    viewHolder.textMsgAccount.setText("매주 " + MM_INSA + activity.getString(R.string.won));
                    viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.color009BEB));
                }
            } else if ("99".equalsIgnoreCase(CANO)) {
                if (!TextUtils.isEmpty(MM_INSA)) {
                    //MM_INSA = Utils.moneyFormatToWon(Double.valueOf(MM_INSA));
                    MM_INSA = Utils.convertHangul(MM_INSA);
                    viewHolder.textMsgAccount.setText("매일 " + MM_INSA + activity.getString(R.string.won));
                    viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.color009BEB));
                }
                viewHolder.textPayDateSavings.setText("납입예정일 매일");
            }
        }

        // 1:거래정지, 2:지급정지, 3:출금정지
        String ACDT_DCL_CD = accountInfo.getACDT_DCL_CD();
        if (!TextUtils.isEmpty(ACDT_DCL_CD)) {
            if ("1".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("거래정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("2".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("지급정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            } else if ("3".equalsIgnoreCase(ACDT_DCL_CD)) {
                viewHolder.textMsgAccount.setText("출금정지");
                viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
            }
        }

        String EXPT_DT = accountInfo.getEXPT_DT();
        if (!TextUtils.isEmpty(EXPT_DT)) {
            String year = EXPT_DT.substring(0, 4);
            String month = EXPT_DT.substring(4, 6);
            String day = EXPT_DT.substring(6, 8);
            viewHolder.textDueDateSavings.setText("만기일 " + year + "." + month + "." + day);
        } else
            viewHolder.textDueDateSavings.setText("");

        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.VISIBLE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.VISIBLE);
        viewHolder.layoutSavings.setVisibility(View.GONE);
        viewHolder.layoutInstallSavings.setVisibility(View.VISIBLE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);

        String TOT_DCNT = accountInfo.getTOT_DCNT();
        long lREMN_DCNT = 0;
        String REMN_DCNT = accountInfo.getREMN_DCNT();
        if (!TextUtils.isEmpty(TOT_DCNT) && !TextUtils.isEmpty(REMN_DCNT)) {
            viewHolder.lineViewSavings.setVisibility(View.VISIBLE);

            long lTOT_DCNT = Long.valueOf(TOT_DCNT);
            lREMN_DCNT = Long.valueOf(REMN_DCNT);

            if (lREMN_DCNT == 0) {
                REMN_DCNT = "만기";
                lTOT_DCNT = 1;
            }
            else {
                REMN_DCNT = "D-" + REMN_DCNT;
            }

            final String dspREMN_DCNT = REMN_DCNT;

            final int pecent;

            if (lTOT_DCNT > 0)
                pecent = (int) ((lTOT_DCNT - lREMN_DCNT) * 100 / lTOT_DCNT);
            else
                pecent = 0;

            Handler delayHandler = new Handler();
            delayHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolder.lineViewSavings.setPercent(pecent);
                    viewHolder.lineViewSavings.setSpeed(0);
                    viewHolder.lineViewSavings.setHeight(8);
                    viewHolder.lineViewSavings.setText(dspREMN_DCNT);
                    viewHolder.lineViewSavings.setTextMargin(10);
                    viewHolder.lineViewSavings.startDraw();
                }
            }, 100);
        } else {
            viewHolder.lineViewSavings.setVisibility(View.GONE);
        }

        if (lREMN_DCNT <= 0) {
            viewHolder.textPayDateSavings.setText("");

            viewHolder.btnAddPayInstallSavings.setVisibility(View.VISIBLE);
            viewHolder.btnSetExpireInstallSavings.setVisibility(View.GONE);
            viewHolder.btnFinishExpireInstallSavings.setVisibility(View.GONE);
            viewHolder.btnManageInstallSavings.setVisibility(View.VISIBLE);
        } else {
            if ("1".equalsIgnoreCase(ATMT_EXPT_EXTS_CD) ||
                "3".equalsIgnoreCase(ATMT_EXPT_EXTS_CD)) {
                viewHolder.btnAddPayInstallSavings.setVisibility(View.VISIBLE);
                viewHolder.btnSetExpireInstallSavings.setVisibility(View.GONE);
                viewHolder.btnFinishExpireInstallSavings.setVisibility(View.GONE);
                viewHolder.btnManageInstallSavings.setVisibility(View.VISIBLE);
            } else if ("0".equalsIgnoreCase(ATMT_EXPT_EXTS_CD) ||
                "4".equalsIgnoreCase(ATMT_EXPT_EXTS_CD)) {
                viewHolder.btnAddPayInstallSavings.setVisibility(View.VISIBLE);
                viewHolder.btnSetExpireInstallSavings.setVisibility(View.VISIBLE);
                viewHolder.btnFinishExpireInstallSavings.setVisibility(View.GONE);
                viewHolder.btnManageInstallSavings.setVisibility(View.GONE);
            }
        }
    }

    private void setLoanType(final AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        viewHolder.textType.setText("대출");
        viewHolder.textMsgAccount.setText("");
        viewHolder.btnCopyAccount.setVisibility(View.GONE);

        String OVRD_DVCD = accountInfo.getOVRD_DVCD();
        if ("1".equalsIgnoreCase(OVRD_DVCD)) {
            viewHolder.textMsgAccount.setText(activity.getString(R.string.msg_overdue));
            viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
        } else if ("2".equalsIgnoreCase(OVRD_DVCD)) {
            viewHolder.textMsgAccount.setText(activity.getString(R.string.loss_due_date));
            viewHolder.textMsgAccount.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
        }

        String BLNC = accountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            viewHolder.textAmountAccount.setText(BLNC + activity.getString(R.string.won));
        }

        String MM_PI_DD = accountInfo.getMM_PI_DD();
        if (!TextUtils.isEmpty(MM_PI_DD))
            viewHolder.textPayDateLoan.setText("납입예정일 매월 " + MM_PI_DD + "일");
        else
            viewHolder.textPayDateLoan.setText("");

        String CNTR_AMT = accountInfo.getCNTR_AMT();
        if (!TextUtils.isEmpty(CNTR_AMT)) {
            CNTR_AMT = Utils.convertHangul(CNTR_AMT);
            viewHolder.textAmountLoan.setText("대출금액 " + CNTR_AMT + activity.getString(R.string.won));
        } else {
            viewHolder.textAmountLoan.setText("");
        }

        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.VISIBLE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.VISIBLE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);

        String PI_RT = accountInfo.getPI_RT();
        if (!TextUtils.isEmpty(PI_RT)) {
            viewHolder.lineViewLoan.setVisibility(View.VISIBLE);

            final float fPI_RT = Float.valueOf(PI_RT);
            Handler delayHandler = new Handler();
            delayHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolder.lineViewLoan.setIsInverse(true); // 역방향 플래그(제일 처음설정 필요)
                    viewHolder.lineViewLoan.setPercent((int) fPI_RT);
                    viewHolder.lineViewLoan.setSpeed(0);
                    viewHolder.lineViewLoan.setHeight(8);
                    viewHolder.lineViewLoan.setText(fPI_RT + "% 상환");
                    viewHolder.lineViewLoan.setTextMargin(10);
                    viewHolder.lineViewLoan.startDraw();
                }
            }, 100);
        } else {
            viewHolder.lineViewLoan.setVisibility(View.VISIBLE);
        }
    }

    private void setAddAccount(AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        if (accountInfo.getADD_ACCOUNT_MODE() == Const.ADD_ACCOUNT_MODE.NOMAL_MODE) {
            viewHolder.textAddAccount.setText(activity.getString(R.string.join_nomal_account));
        } else if (accountInfo.getADD_ACCOUNT_MODE() == Const.ADD_ACCOUNT_MODE.SAVINGS_MODE) {
            viewHolder.textAddAccount.setText(activity.getString(R.string.join_savings_account));
        }

        viewHolder.textMsgAccount.setText("");
        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.VISIBLE);
        viewHolder.layoutAccount.setVisibility(View.GONE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);
    }

    private void setListReportCredit(AccountListViewHolder viewHolder) {
        viewHolder.textMsgAccount.setText("");
        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.GONE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.VISIBLE);
        viewHolder.layoutAdsType.setVisibility(View.GONE);
    }

    private void setListAds(AccountListViewHolder viewHolder, AccountInfo accountInfo) {
        ArrayList<AdsItemInfo> listAds = accountInfo.getListAds();
        if (listAds == null || listAds.size() <= 0)
            return;

        AdsItemInfo adsInfo = listAds.get(showAdsIndex);
        if (adsInfo != null) {
            String IMG_URL_ADDR = adsInfo.getIMG_URL_ADDR();
            if (!TextUtils.isEmpty(IMG_URL_ADDR)) {
                String baseUrl = WasServiceUrl.getUrl();
                baseUrl += IMG_URL_ADDR;

                Picasso.with(activity)
                        .load(baseUrl)
                        .fit()
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .into(viewHolder.imageAds);
            }
        }

        viewHolder.textMsgAccount.setText("");
        viewHolder.layoutMainTop.setVisibility(View.GONE);
        viewHolder.layoutJoin.setVisibility(View.GONE);
        viewHolder.layoutAccount.setVisibility(View.GONE);
        viewHolder.layoutNormalType.setVisibility(View.GONE);
        viewHolder.layoutGoOnType.setVisibility(View.GONE);
        viewHolder.layoutMinusType.setVisibility(View.GONE);
        viewHolder.layoutSavingsType.setVisibility(View.GONE);
        viewHolder.layoutLoanType.setVisibility(View.GONE);
        viewHolder.layoutReportCredit.setVisibility(View.GONE);
        viewHolder.layoutAdsType.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return listAccountInfo.size();
    }

    public void setListAccountInfo(ArrayList<AccountInfo> listAccountInfo) {
        this.listAccountInfo = listAccountInfo;
        notifyDataSetChanged();
    }

    public void setAdsIndex(int index) {
        this.showAdsIndex = index;
    }

    private void showTimeMode(AccountListViewHolder viewHolder) {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
                    (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            viewHolder.layoutListMain.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.layoutMainTop.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.imageAlram.setImageResource(R.drawable.ico_bell_n);
            viewHolder.imageProfileBg.setImageResource(R.drawable.img_user_circle_n);
            viewHolder.textUserName.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textUserNameDesc.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textTagTitle.setTextColor(activity.getResources().getColor(R.color.colorC9F5FF));
            viewHolder.layoutListTag.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            Glide.with(activity)
                    .load(R.drawable.main_night_bg_img_star01)
                    .into(viewHolder.imageStar01);
            viewHolder.imageStar01.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.main_night_bg_img_star02)
                    .into(viewHolder.imageStar02);
            viewHolder.imageStar02.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.main_night_bg_img_star03)
                    .into(viewHolder.imageStar03);
            viewHolder.imageStar03.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.main_night_bg_img_star04)
                    .into(viewHolder.imageStar04);
            viewHolder.imageStar04.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.main_night_bg_img_star05)
                    .into(viewHolder.imageStar05);
            viewHolder.imageStar05.setVisibility(View.VISIBLE);
            viewHolder.imageStar06.setVisibility(View.VISIBLE);
            viewHolder.textType.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.viewBar.setBackgroundColor(activity.getResources().getColor(R.color.color4B555F));
            viewHolder.textAddAccount.setTextColor(activity.getResources().getColor(R.color.colorEBF5F6));
            viewHolder.layoutJoin.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.viewAddAccount.setBackgroundColor(activity.getResources().getColor(R.color.color4B555F));
            viewHolder.layoutAccount.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.layoutNormalType.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.layoutSavingsType.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.layoutLoanType.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            viewHolder.textNameAccount.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textAmountAccount.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textACNO.setTextColor(activity.getResources().getColor(R.color.white));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1)
                viewHolder.btnCopyAccount.setImageDrawable(activity.getResources().getDrawable(R.drawable.ico_copy_n));
            else
                viewHolder.btnCopyAccount.setImageDrawable(activity.getResources().getDrawable(R.drawable.ico_copy_n, null));
            viewHolder.textMsgAccount.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textJoinLoadTitle.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.layoutJoinLoadCircle.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                viewHolder.imageJoinLoadCircle.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_date_circle_n));
            } else {
                viewHolder.imageJoinLoadCircle.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_date_circle_n, null));
            }
            viewHolder.textJoinLoadDays.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textJoinLoadState.setTextColor(activity.getResources().getColor(R.color.color00EBFF));
            viewHolder.textJoinLoadDesc.setTextColor(activity.getResources().getColor(R.color.colorAFB2B5));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                viewHolder.imageContinueCancel.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_cancle_n));
            } else {
                viewHolder.imageContinueCancel.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_cancle_n, null));
            }
            viewHolder.viewLoanBar.setBackgroundColor(activity.getResources().getColor(R.color.color4B555F));
            viewHolder.viewReportBar.setBackgroundColor(activity.getResources().getColor(R.color.color4B555F));
            viewHolder.textReportCreditTitle.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.textReportCreditSubtitle01.setTextColor(activity.getResources().getColor(R.color.color00EBFF));
            viewHolder.textReportCreditSubtitle02.setTextColor(activity.getResources().getColor(R.color.colorAFB2B5));
            viewHolder.textInquireCredit.setTextColor(activity.getResources().getColor(R.color.white));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                viewHolder.imageInquireArrow.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_link_arrow_n));
            } else {
                viewHolder.imageInquireArrow.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_link_arrow_n, null));
            }
            viewHolder.viewAdsBar.setBackgroundColor(activity.getResources().getColor(R.color.color4B555F));
            viewHolder.layoutAdsType.setBackgroundColor(activity.getResources().getColor(R.color.color39434E));
        } else {
            viewHolder.layoutListMain.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.layoutMainTop.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.imageAlram.setImageResource(R.drawable.ico_bell);
            viewHolder.imageProfileBg.setImageResource(R.drawable.img_user_circle);
            viewHolder.textUserName.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textUserNameDesc.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textTagTitle.setTextColor(activity.getResources().getColor(R.color.color8994AA));
            viewHolder.layoutListTag.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.imageStar01.setVisibility(View.GONE);
            viewHolder.imageStar02.setVisibility(View.GONE);
            viewHolder.imageStar03.setVisibility(View.GONE);
            viewHolder.imageStar04.setVisibility(View.GONE);
            viewHolder.imageStar05.setVisibility(View.GONE);
            viewHolder.imageStar06.setVisibility(View.GONE);
            viewHolder.textType.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.viewBar.setBackgroundColor(activity.getResources().getColor(R.color.colorEBF5F6));
            viewHolder.textAddAccount.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.layoutJoin.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.viewAddAccount.setBackgroundColor(activity.getResources().getColor(R.color.colorEBF5F6));
            viewHolder.layoutAccount.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.layoutNormalType.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.layoutSavingsType.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.layoutLoanType.setBackgroundColor(activity.getResources().getColor(R.color.white));
            viewHolder.textNameAccount.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textAmountAccount.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textACNO.setTextColor(activity.getResources().getColor(R.color.black));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1)
                viewHolder.btnCopyAccount.setImageDrawable(activity.getResources().getDrawable(R.drawable.ico_copy));
            else
                viewHolder.btnCopyAccount.setImageDrawable(activity.getResources().getDrawable(R.drawable.ico_copy, null));
            viewHolder.textMsgAccount.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textJoinLoadTitle.setTextColor(activity.getResources().getColor(R.color.color555555));
            viewHolder.layoutJoinLoadCircle.setBackgroundColor(activity.getResources().getColor(R.color.white));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                viewHolder.imageJoinLoadCircle.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_date_circle));
            } else {
                viewHolder.imageJoinLoadCircle.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_date_circle, null));
            }
            viewHolder.textJoinLoadDays.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textJoinLoadState.setTextColor(activity.getResources().getColor(R.color.color00A2B3));
            viewHolder.textJoinLoadDesc.setTextColor(activity.getResources().getColor(R.color.color888888));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                viewHolder.imageContinueCancel.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_cancle));
            } else {
                viewHolder.imageContinueCancel.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_cancle, null));
            }
            viewHolder.viewLoanBar.setBackgroundColor(activity.getResources().getColor(R.color.colorEBF5F6));
            viewHolder.viewReportBar.setBackgroundColor(activity.getResources().getColor(R.color.colorEBF5F6));
            viewHolder.textReportCreditTitle.setTextColor(activity.getResources().getColor(R.color.black));
            viewHolder.textReportCreditSubtitle01.setTextColor(activity.getResources().getColor(R.color.color00A2B3));
            viewHolder.textReportCreditSubtitle02.setTextColor(activity.getResources().getColor(R.color.color888888));
            viewHolder.textInquireCredit.setTextColor(activity.getResources().getColor(R.color.color00A2B3));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                viewHolder.imageInquireArrow.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_link_arrow_m));
            } else {
                viewHolder.imageInquireArrow.setImageDrawable(activity.getResources().getDrawable(R.drawable.btn_link_arrow_m, null));
            }
            viewHolder.viewAdsBar.setBackgroundColor(activity.getResources().getColor(R.color.colorEBF5F6));
            viewHolder.layoutAdsType.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }
    }

    /**
     * 상환 요청
     */
    private void requestLoanRepay(String ACNO, String ACCO_IDNO, String SUBJ_CD) {
        Map param = new HashMap();
        param.put("ACNO", ACNO);
        param.put("ACCO_IDNO", ACCO_IDNO);
        param.put("SUBJ_CD", SUBJ_CD);

        activity.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.LON0050100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                activity.dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(activity.getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(activity.getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = activity.getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        activity.showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.LON0050900.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    activity.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 보통예금이어하기
     */
    private void requestGoOnNomal(final String PROP_NO) {
        Map param = new HashMap();

        activity.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                activity.dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(activity.getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(activity.getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = activity.getString(R.string.common_msg_no_reponse_value_was);

                        msg = msg.replaceAll("\\\\n", "\n");
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        activity.showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String SCRN_ID = object.optString("SCRN_ID");
                    String SBK_PROP_STEP_CD = object.optString("SBK_PROP_STEP_CD");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.getUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = "";
                    if (TextUtils.isEmpty(SBK_PROP_STEP_CD) && !TextUtils.isEmpty(PROP_NO)) {
                        param = "PROP_NO=" + PROP_NO;
                    } else if (!TextUtils.isEmpty(SBK_PROP_STEP_CD) && !TextUtils.isEmpty(PROP_NO)) {
                        param = "SBK_PROP_STEP_CD=" + SBK_PROP_STEP_CD + "&PROP_NO=" + PROP_NO;
                    }
                    intent.putExtra(Const.INTENT_PARAM, param);

                    activity.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이어하기
     */
    private void requestGoOn(String DSCT_CD, String PROP_NO, String PROP_STEP_CD) {
        Map param = new HashMap();
        param.put("PROP_NO", PROP_NO);
        param.put("LOAN_LNKN_PRGS_STEP_DVCD", PROP_STEP_CD);

        String url = "";
        if (Const.ACCOUNT_GO_ON_LOAN.equalsIgnoreCase(DSCT_CD)) {
            url = WasServiceUrl.MAI0010300A01.getServiceUrl();

        } else if (Const.ACCOUNT_GO_ON_MINUS.equalsIgnoreCase(DSCT_CD)) {
            url = WasServiceUrl.MAI0010300A02.getServiceUrl();

        } else if (Const.ACCOUNT_GO_ON_SMALL_MINUS.equalsIgnoreCase(DSCT_CD)) {
            url = WasServiceUrl.MAI0010300A03.getServiceUrl();
        }

        activity.showProgressDialog();
        HttpUtils.sendHttpTask(url, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                activity.dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(activity.getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(activity.getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = activity.getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        activity.showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String SCRN_ID = object.optString("SCRN_ID");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(activity, WebMainActivity.class);
                    String url = WasServiceUrl.getUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    activity.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 만기해지
     */
    private void requestFinishAccount(final String DSCT_CD, final String ACNO) {
        Map param = new HashMap();
        param.put("ACNO", ACNO);

        activity.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A05.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                activity.dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(activity.getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(activity.getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = activity.getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        activity.showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String url = "";
                    Intent intent = new Intent(activity, WebMainActivity.class);
                    if (Const.ACCOUNT_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
                        url = WasServiceUrl.UNT0340100.getServiceUrl();

                    } else if (Const.ACCOUNT_INSTALL_SAVINGS.equalsIgnoreCase(DSCT_CD)){
                        url = WasServiceUrl.UNT0250100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    }
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param =  "ACNO=" + ACNO;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    activity.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 오류 메세지 표시
     * @param msg 오류 메세지
     */
    public void showErrorMessage(String msg) {
        msg = msg.replaceAll("\\\\n", "\n");

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        final AlertDialog alertDialog = new AlertDialog(activity);
        alertDialog.mPListener = okClick;
        alertDialog.msg = msg;
        alertDialog.show();
    }

    /**
     * 이어하기 취소 확인창
     */
    private void showCancelContinue(final AccountInfo accountInfo) {
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestCancelContinue(accountInfo);
            }
        };

        final CancelContinueDialog dialog = new CancelContinueDialog(activity);
        dialog.mNListener = cancelClick;
        dialog.mPListener = okClick;

        String DSCT_CD = accountInfo.getDSCT_CD();
        if (!TextUtils.isEmpty(DSCT_CD)) {
            if (Const.ACCOUNT_GO_ON_NOMAL.equalsIgnoreCase(DSCT_CD))
                dialog.msg = activity.getString(R.string.msg_cancel_cotinue_desc_nomal);
            else
                dialog.msg = activity.getString(R.string.msg_cancel_cotinue_desc);
        }
        dialog.show();
    }

    /**
     * 이어하기 취소
     */
    private void requestCancelContinue(AccountInfo accountInfo) {
        Map param = new HashMap();

        String DSCT_CD = accountInfo.getDSCT_CD();
        if (TextUtils.isEmpty(DSCT_CD))
            return;

        String url = "";

        if (Const.ACCOUNT_GO_ON_NOMAL.equalsIgnoreCase(DSCT_CD)) {
            url = WasServiceUrl.MAI0010400A01.getServiceUrl();
            String ACCO_IDNO = accountInfo.getACCO_IDNO();
            if (TextUtils.isEmpty(ACCO_IDNO))
                return;

            param.put("PROP_NO", ACCO_IDNO);

        } else if (Const.ACCOUNT_GO_ON_LOAN.equalsIgnoreCase(DSCT_CD) ||
            Const.ACCOUNT_GO_ON_MINUS.equalsIgnoreCase(DSCT_CD) ||
            Const.ACCOUNT_GO_ON_SMALL_MINUS.equalsIgnoreCase(DSCT_CD)) {
            url = WasServiceUrl.MAI0010400A02.getServiceUrl();
            String CANO = accountInfo.getCANO();
            if (TextUtils.isEmpty(CANO))
                return;

            param.put("PROP_NO", CANO);
        }

        activity.showProgressDialog();
        HttpUtils.sendHttpTask(url, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                activity.dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(activity.getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(activity.getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = activity.getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        activity.showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    listener.OnJoinCancelClickListener();

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    public interface ClickListener {
        void OnAlramClickListener();
        void OnRollingTextSelect(int position);
        void OnAddClickListener();
        void OnJoinClickListener();
        void OnItemClickListener(int position);
        void OnJoinCancelClickListener();
        void OnGetSelectTag(int position, TagItemInfo tagInfo);
        void OnAdsItemClickListener(int position);
    }
}
