package com.sbi.saidabank.common.adapter;

import android.content.Context;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.TransferAccountInfo;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Saidabank_android
 * Class: TransferAccountAdapter
 * Created by 950485 on 2019. 01. 02..
 * <p>
 * Description:아래에서 위로 올라오는 아래에서 위로 올라오는 최근/자주 이체한 계좌번호 다이얼로그 adapter
 */

public class TransferRecentlyAdapter extends BaseAdapter {
    private Context                        context;
    private ArrayList<TransferAccountInfo> listAccountInfo;
    private int                            type;   // 0 : 계좌, 1 : 휴대폰번호
    private OnItemClickListener            listener;

    public TransferRecentlyAdapter(Context context, int type, ArrayList<TransferAccountInfo> listAccountInfo, OnItemClickListener listener) {
        this.context = context;
        this.type = type;
        this.listAccountInfo = listAccountInfo;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return listAccountInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return listAccountInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final TransferRecentlyAdapter.AccountViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dialog_transfer_account_list_item, null);

            viewHolder = new TransferRecentlyAdapter.AccountViewHolder();
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textview_name_transfer_account);
            viewHolder.textAlias = (TextView) convertView.findViewById(R.id.textview_alias_transfer_account);
            viewHolder.textBank = (TextView) convertView.findViewById(R.id.textview_bank_transfer_account);
            viewHolder.textAccountNum = (TextView) convertView.findViewById(R.id.textview_account_num_transfer_account);
            viewHolder.layoutFavirte = (RelativeLayout) convertView.findViewById(R.id.layout_favorite_account);
            viewHolder.btnFavirte = (ImageView) convertView.findViewById(R.id.btn_favorite_account);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TransferRecentlyAdapter.AccountViewHolder) convertView.getTag();
        }

        TransferAccountInfo accountInfo = (TransferAccountInfo) listAccountInfo.get(position);
        if (accountInfo != null) {
            String MNRC_ACCO_DEPR_NM = accountInfo.getMNRC_ACCO_DEPR_NM();
            if (!TextUtils.isEmpty(MNRC_ACCO_DEPR_NM))
                viewHolder.textName.setText(MNRC_ACCO_DEPR_NM);
            else
                viewHolder.textName.setText("");

            if (type == 0) {
                String MNRC_ACCO_ALS = accountInfo.getMNRC_ACCO_ALS();
                if (!TextUtils.isEmpty(MNRC_ACCO_ALS))
                    viewHolder.textAlias.setText(MNRC_ACCO_ALS);
                else
                    viewHolder.textAlias.setText("");

                String MNRC_BANK_NM = accountInfo.getMNRC_BANK_NM();
                if (!TextUtils.isEmpty(MNRC_BANK_NM))
                    viewHolder.textBank.setText(MNRC_BANK_NM);
                else
                    viewHolder.textBank.setText("");

                String MNRC_ACNO = accountInfo.getMNRC_ACNO();
                if (!TextUtils.isEmpty(MNRC_ACNO)) {
                    String MNRC_BANK_CD = accountInfo.getMNRC_BANK_CD();
                    String account;
                    if (!TextUtils.isEmpty(MNRC_BANK_CD) &&
                       (("000".equalsIgnoreCase(MNRC_BANK_CD) || "028".equalsIgnoreCase(MNRC_BANK_CD))))
                        account = MNRC_ACNO.substring(0, 5) + "-" + MNRC_ACNO.substring(5, 7) + "-" + MNRC_ACNO.substring(7, MNRC_ACNO.length());
                    else
                        account = MNRC_ACNO;

                    viewHolder.textAccountNum.setText(account);
                }
            } else {
                viewHolder.textAlias.setVisibility(View.GONE);
                viewHolder.textBank.setVisibility(View.GONE);

                String MNRC_TLNO = accountInfo.getMNRC_TLNO();
                if (!TextUtils.isEmpty(MNRC_TLNO)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        MNRC_TLNO = PhoneNumberUtils.formatNumber(MNRC_TLNO, Locale.getDefault().getCountry());
                    }
                    viewHolder.textAccountNum.setText(MNRC_TLNO);
                }
            }

            String favo = accountInfo.getFAVO_ACCO_YN();
            if (!TextUtils.isEmpty(favo) && Const.REQUEST_WAS_YES.equalsIgnoreCase(favo)) {
                viewHolder.btnFavirte.setImageResource(R.drawable.btn_favorite_on);
            } else {
                viewHolder.btnFavirte.setImageResource(R.drawable.btn_favorite_off);
            }

            final int pos = position;
            viewHolder.layoutFavirte.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    listener.OnFavirteListener(pos);
                }
            });
        }

        return convertView;
    }

    public void updateDataList(ArrayList<TransferAccountInfo> listAccountInfo) {
        this.listAccountInfo = listAccountInfo;
        this.notifyDataSetChanged();
    }

    class AccountViewHolder {
        TextView       textName;
        TextView       textAlias;
        TextView       textBank;
        TextView       textAccountNum;
        RelativeLayout layoutFavirte;
        ImageView      btnFavirte;
    }

    public interface OnItemClickListener {
        void OnFavirteListener(int position);
    }
}