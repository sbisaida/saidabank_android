package com.sbi.saidabank.common.download;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.sbi.saidabank.common.util.Files;
import com.sbi.saidabank.common.util.Logs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadAsync extends AsyncTask<String,Integer,Long> {
	static final String TAG = DownloadAsync.class.getSimpleName();
	
	private Context mContext;
	private ProgressDialog pDialog;

	private String mDownUrl;
	private HttpURLConnection conn;
	private String mSaveFilePath;
	private String mSaveFileName;
	
	private InputStream mInputStream;
	private OutputStream mOutputStream;
	
	private long mTotalDownloadSize=0;
    private int mReadCount=0;
    private int mPercent=0;
    private int mContentSize = 0;
    private byte[] mDownloadBuffer;    
    private boolean bDelOldFile;
    private DownloadCallback mListener;

    public DownloadAsync(Context context, boolean delOldFile, DownloadCallback l){
    	mContext = context;
    	mListener = l;
		bDelOldFile = delOldFile;
    }
    
	@Override
	protected void onPreExecute() {
		pDialog = new ProgressDialog(mContext);
		pDialog.setMessage("파일을 다운로드 합니다.");
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		pDialog.setMax(100);
		pDialog.setProgress(0);
		pDialog.setCancelable(false);
		pDialog.show();

		mTotalDownloadSize = 0;
		mReadCount=0;
		mPercent=0;		
		mContentSize = 0;		
		mDownloadBuffer = new byte[1024*20];

		super.onPreExecute();
	}
	
	@Override
	protected Long doInBackground(String... param) {
		try {
			mDownUrl = param[0];

			mSaveFilePath = Files.getOutputMediaPath(mContext,"SaidaBank").getAbsolutePath();
			Logs.e("mFolderPath :" + mSaveFilePath);


        	URL url = new URL(mDownUrl);
            conn = (HttpURLConnection)url.openConnection();
            
            if(conn != null){            
            	conn.setRequestMethod("GET");
            	conn.setDoInput(true);
                conn.setConnectTimeout(10000);

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){

					/**
					 * 파일 이름을 받아서 파일을 생성한다.
					 */

					String depo = conn.getHeaderField("Content-Disposition");
					Logs.e("Content-Disposition : " + depo);

					String depoSplit[] = depo.split(";");
					int size = depoSplit.length;
					for(int i = 0; i < size; i++)
					{
						if(depoSplit[i].contains("filename="))
						{
							String nameSplit[] = depoSplit[i].split("=");
							if(nameSplit.length>=2)
								mSaveFileName = nameSplit[1].trim();

							break;
						}
					}
					File saveFile = new File(mSaveFilePath,mSaveFileName);
					if(saveFile.exists())
						saveFile.delete();

					Logs.e("mFileName :" + mSaveFileName);

					/**
					 * 파일 다운로드를 시작한다.
					 */

                	mContentSize = conn.getContentLength();
                	
                	mInputStream = new BufferedInputStream(url.openStream());
    		        mOutputStream = new FileOutputStream(saveFile);
    		        
    		        while((mReadCount = mInputStream.read(mDownloadBuffer)) != -1){
    									
    					mOutputStream.write(mDownloadBuffer, 0, mReadCount);
    	    	        
    					mTotalDownloadSize = mTotalDownloadSize + mReadCount;
    					int diff = (int)((float)mTotalDownloadSize*100/(float)mContentSize);			
    					if(diff != mPercent){
    						mPercent = diff;
							publishProgress(mPercent);
    					}
    		        }    					                	
                }else{
					Logs.e("error - conn.getResponseCode : " + conn.getResponseCode());
				}
                conn.disconnect();
                conn = null;
            }
		} catch(Exception e){
        	Logs.printException(e);
        }  		
		return null;
	}
	
	@Override
	protected void onCancelled(Long result) {
		super.onCancelled(result);
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		pDialog.setProgress(values[0]);
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Long result) {
		if(pDialog != null && pDialog.isShowing()){
			pDialog.dismiss();
			pDialog=null;
		}
		mListener.onDownloadComplete(mSaveFilePath,mSaveFileName);
		super.onPostExecute(result);
	}
}
