package com.sbi.saidabank.common;

import android.app.Activity;
import android.content.Context;

import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.WasServiceUrl;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * SaidaBanking
 * Class: LogOutTimeChecker
 * Created by 950469 on 2018. 8. 22..
 * <p>
 * Description:
 * 로그인 후에 일정 시간후 로그아웃 팝업을 출력하기 위한 타임 체커
 */
public class LogoutTimeChecker {
    private static final int AUTO_LOGOUT_TIME = 10 * 60;
    private static final int READY_LOGOUT_TIME = 1 * 60;

    private WeakReference<BaseActivity> mWeakActivity;
    private static LogoutTimeChecker instance;
    private Context mContext;
    private Timer autoLogoutTimer = null;
    private long interactionTime = 0;
    private LogoutTimeCheckerListener mListener;
    private boolean mIsBackground;

    public static LogoutTimeChecker getInstance(Context context) {
        if(instance == null) {
            synchronized (LogoutTimeChecker.class) {
                if (instance == null) {
                    instance = new LogoutTimeChecker(context);
                }
            }
        }else{
            instance.updateContext(context);
        }
        return instance;
    }

    public static void clearInstance(){
        if(instance == null)
            return;

        instance.autoLogoutStop();
        instance = null;
    }

    private LogoutTimeChecker(Context context){
        mWeakActivity = new WeakReference<BaseActivity>((BaseActivity) context);
        mContext = context;
        mListener = (LogoutTimeCheckerListener)context;
        mIsBackground = false;
    }

    private void updateContext(Context context){
        mWeakActivity = new WeakReference<BaseActivity>((BaseActivity) context);
        mContext = context;
        mListener = (LogoutTimeCheckerListener)context;
    }

    public void autoLogoutStart() {
        interactionTime = System.currentTimeMillis();
        Logs.i("interactionTime : " + interactionTime);

        if (autoLogoutTimer == null) {
            autoLogoutTimer = new Timer();
            autoLogoutTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    final long duration = (System.currentTimeMillis() - interactionTime) / 1000;
                    //Logs.d("autoLogout Timer : " + duration);
                    BaseActivity weakactivity = mWeakActivity.get();

                    if (weakactivity != null) {
                        if (duration >= AUTO_LOGOUT_TIME) {
                            autoLogoutStop();
                            ((Activity)mContext).runOnUiThread(new Runnable() {
                                public void run() {
                                    mListener.OnEndLogoutTime(false);
                                }
                            });

                        } else if ((AUTO_LOGOUT_TIME - duration) < READY_LOGOUT_TIME) {
                            ((Activity)mContext).runOnUiThread(new Runnable() {
                                public void run() {
                                    mListener.OnReadyLogoutTime(AUTO_LOGOUT_TIME - duration);
                                }
                            });
                        }
                    }

                }
            }, 0, 1000);
        }

        Logs.i("autoLogoutStart");
    }

    public void autoLogoutRestart() {
        interactionTime = System.currentTimeMillis();
        mListener.OnEndLogoutTime(true);
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
                    @Override
                    public void endHttpRequest(String ret) {
                    }
                });
        Logs.i("autoLogoutRestart");
    }

    public void autoLogoutUserInteraction(boolean isShownLogoutPopup) {
        if (autoLogoutTimer != null) {
            if (!isShownLogoutPopup)
                interactionTime = System.currentTimeMillis();
        }
        Logs.i("autoLogout userInteraction");
    }

    public void autoLogoutStop() {
        try {
            interactionTime = 0;
            if(autoLogoutTimer != null) {
                autoLogoutTimer.cancel();
                autoLogoutTimer = null;
            }
        } catch(Exception e) {
            Logs.printException(e.getMessage());
        }

        Logs.i("autoLogoutStop");
    }

    public interface LogoutTimeCheckerListener {
        void OnReadyLogoutTime(long remainTime);
        void OnEndLogoutTime(boolean isRestart);
    }

    public boolean ismIsBackground() {
        return mIsBackground;
    }

    public void setmIsBackground(boolean mIsBackground) {
        this.mIsBackground = mIsBackground;
    }
}
