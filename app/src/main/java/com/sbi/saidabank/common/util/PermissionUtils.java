package com.sbi.saidabank.common.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.sbi.saidabank.R;

import java.net.URI;

public class PermissionUtils {
    public static boolean checkPermission(Activity activity, String[] pList,int requestCode){

        for(int i=0;i<pList.length;i++){
            int perStatus = ContextCompat.checkSelfPermission(activity, pList[i]);
            if(perStatus == PackageManager.PERMISSION_DENIED){
                requestPermission(activity,pList,requestCode);
                return false;
            }
        }

        return true;

    }

    public static void requestPermission(Activity activity,String[] pList,int requestCode){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(activity, pList, requestCode);
        }

    }
/*
    public static String getPermissionString(Context context, String permission){
        String msg = "";
        if(permission.equals(Manifest.permission.READ_SMS)||permission.equals(Manifest.permission.RECEIVE_SMS)){
            msg = "SMS";
        }else if(permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)){
            msg = "위치";
        }else if(permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)||permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            msg = "저장소";
        }else if(permission.equals(Manifest.permission.READ_PHONE_STATE)){
            msg = "전화";
        }else if(permission.equals(Manifest.permission.READ_CONTACTS)){
            msg = "주소록";
        }else if(permission.equals(Manifest.permission.CAMERA)){
            msg = "카메라";
        }

        return context.getString(R.string.msg_permission_move_app_permission,msg);
    }
*/
    public static void goAppSettingsActivity(Context context){
        Uri uri = Uri.parse("package:"+ context.getPackageName());
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(uri);
        context.startActivity(intent);
    }
}
