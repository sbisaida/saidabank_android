package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.customview.RoundedCornersTransformation;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.MainPopupInfo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class MainNoticeDialog extends Dialog {
    private Context          mContext;
    private MainPopupInfo    mMainPopupInfo;
    private OnListener       mListener;

    public MainNoticeDialog(Context context, MainPopupInfo mainPopupInfo, OnListener clickListener) {
        super(context, R.style.TransparentProgressDialog);
        mContext = context;
        mMainPopupInfo = mainPopupInfo;
        mListener = clickListener;
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_main_notice);
        setDialogSize();

        initUX();
    }

    @Override
    public void show() {
        super.show();
    }

    protected void setDialogSize() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        WindowManager.LayoutParams lp = getWindow().getAttributes( ) ;
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        getWindow().setAttributes(lp);
    }

    private void initUX() {
        String PUP_CNTN_DVCD = mMainPopupInfo.getPUP_CNTN_DVCD();

        ImageView imageNotice = (ImageView) findViewById(R.id.imageview_main_notice);
        TextView textNotice = (TextView) findViewById(R.id.textview_main_notice);

        if ("I".equalsIgnoreCase(PUP_CNTN_DVCD)) {
            String IMG_URL_ADDR = mMainPopupInfo.getIMG_URL_ADDR();
            if (!TextUtils.isEmpty(IMG_URL_ADDR)) {
                float sCorner = Utils.dpToPixel(getContext(), 5);

                final Transformation transformation = new RoundedCornersTransformation((int) sCorner, 0, RoundedCornersTransformation.CornerType.TOP);

                IMG_URL_ADDR = WasServiceUrl.getUrl() + "/" + IMG_URL_ADDR;
                Picasso.with(mContext)
                     .load(IMG_URL_ADDR)
                      .fit()
                      .transform(transformation)
                     .into(imageNotice);
            }
            imageNotice.setVisibility(View.VISIBLE);
            textNotice.setVisibility(View.GONE);
        } else {
            String PUP_CNTN = mMainPopupInfo.getPUP_CNTN();
            if (!TextUtils.isEmpty(PUP_CNTN)) {
                textNotice.setText(PUP_CNTN);
            }
            imageNotice.setVisibility(View.GONE);
            textNotice.setVisibility(View.VISIBLE);
        }

        imageNotice.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mListener.onImageClick();
                dismiss();
            }
        });

        Button btnNoShow = (Button) findViewById(R.id.btn_no_show_today);
        btnNoShow.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mListener.onNoShowClick();
                dismiss();
            }
        });

        Button btnClose = (Button) findViewById(R.id.btn_close_main_notice);
        btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnListener {
        void onImageClick();
        void onNoShowClick();
    }
}