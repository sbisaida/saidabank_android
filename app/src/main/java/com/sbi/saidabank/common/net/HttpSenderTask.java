package com.sbi.saidabank.common.net;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.solution.jex.JexAESEncrypt;

/**
 * Saidabank_android
 * Class: HttpSenderTask
 * Created by 950469 on 2018. 8. 24..
 * <p>
 * Description:Http Connect를 할경우 AsyncTask로 접속해야 해서 추가함.
 *
 * 사용법은 SoutionTestActivity의 http_test버튼 클릭을 참조.
 * Post방식에서 자료형은 String,Map만 지원함.
 */
public class HttpSenderTask extends AsyncTask<String, Void, String>{
    private String mUrl;
    private HttpRequestListener mListener;
    public HttpSenderTask(String url, HttpRequestListener l){
        mUrl = url;
        mListener = l;
    }
    @Override
    protected String doInBackground(String... params) {
        String retString=null;

        /*WAS로 전송하는 데이타는 구간 암호화를 하기위해 */
        //20190328 - Jex파일만 암호화 하도록 수정
        if(mUrl.contains(SaidaUrl.DEV_SERVER)|| mUrl.contains(SaidaUrl.OPER_SERVER) || mUrl.contains(SaidaUrl.TEST_SERVER)) {
            if(mUrl.contains(".act") || mUrl.contains(".jct")){
                /**
                 *  SBI서버로 접근하는 데이타는 Json형식을 유지해야됨.
                 *  암호화 하기 전에 데이타 체크필요.
                 */
                Logs.e("requestJexEncryptPost : " + params[0]);
                try {
                    String encText = JexAESEncrypt.encrypt((String) params[0]);

                    String encRetText = HttpSender.requestJexEncryptPost(mUrl, encText);
                    retString = JexAESEncrypt.decrypt(encRetText);
                } catch (Exception e) {
                    Logs.printException(e);
                }
            } else {
                Logs.e("HttpSenderTask : " + params[0]);
                if(TextUtils.isEmpty(params[0])){
                    retString = HttpSender.requestGet(mUrl);
                }else{
                    retString = HttpSender.requestPost(mUrl,(String)params[0]);
                }
            }
        }else{
            Logs.e("HttpSenderTask : " + params[0]);
            if(TextUtils.isEmpty(params[0])){
                retString = HttpSender.requestGet(mUrl);
            }else{
                retString = HttpSender.requestPost(mUrl,(String)params[0]);
            }
        }

        return retString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mListener.endHttpRequest(s);
    }

    public interface HttpRequestListener{
        void endHttpRequest(String ret);
    }
}
