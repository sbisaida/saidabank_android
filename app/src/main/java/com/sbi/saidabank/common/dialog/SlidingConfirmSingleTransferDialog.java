package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.TransferManager;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.define.datatype.VerifyTransferInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Saidabank_android
 * Class: SlidingConfirmSingleTransferDialog
 * Created by 950485 on 2018. 11. 29..
 * <p>
 * Description:아래에서 위로 올라오는 단건이체 확인 다이얼로그
 */

public class SlidingConfirmSingleTransferDialog extends SlidingBaseDialog {
    private Context   context;

    private String                     withdrawAccount;
    private String                     alias;
    private FinishListener             finishListener;

    public SlidingConfirmSingleTransferDialog(@NonNull Context context, String withdrawAccount, String alias, FinishListener finishListener) {
        super(context);
        this.context = context;
        this.withdrawAccount = withdrawAccount;
        this.alias = alias;
        this.finishListener = finishListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_confirm_single_transfer);
        setDialogWidth();
        setCanceledOnTouchOutside(true);

        initUX();
    }

    private void initUX() {
        VerifyTransferInfo verifyTransferInfo = TransferManager.getInstance().get(0);

        TextView textName = (TextView) findViewById(R.id.textview_recipient_name_single);
        String name = verifyTransferInfo.getRECV_NM();
        if (!TextUtils.isEmpty(name)) {
            textName.setText(name);
        }

        TextView textAccount = (TextView) findViewById(R.id.textview_recipient_account_single);

        String MNRC_BANK_NM = verifyTransferInfo.getMNRC_BANK_NM();
        String CNTP_BANK_ACNO = verifyTransferInfo.getCNTP_BANK_ACNO();
        String CNTP_FIN_INST_CD = verifyTransferInfo.getCNTP_FIN_INST_CD();
        String account;
        if (!TextUtils.isEmpty(CNTP_FIN_INST_CD) &&
            ("000".equalsIgnoreCase(CNTP_FIN_INST_CD) || "028".equalsIgnoreCase(CNTP_FIN_INST_CD)))
            account = CNTP_BANK_ACNO.substring(0, 5) + "-" + CNTP_BANK_ACNO.substring(5, 7) + "-" + CNTP_BANK_ACNO.substring(7, CNTP_BANK_ACNO.length());
        else
            account = CNTP_BANK_ACNO;

        MNRC_BANK_NM = MNRC_BANK_NM + " " + account + "으로";
        if (!TextUtils.isEmpty(MNRC_BANK_NM))
            textAccount.setText(MNRC_BANK_NM);

        TextView textAmount = (TextView) findViewById(R.id.textview_amount_single_account);
        String amount = verifyTransferInfo.getTRN_AMT();
        if (!TextUtils.isEmpty(amount)) {
            amount = Utils.moneyFormatToWon(Double.valueOf(amount));
            textAmount.setText(amount);
        }

        TextView textFee = (TextView) findViewById(R.id.textview_fee_single_account);
        String FEE = verifyTransferInfo.getFEE();
        if (!TextUtils.isEmpty(FEE))
            textFee.setText(FEE);

        TextView textWithdrawBankName = (TextView) findViewById(R.id.textview_withdraw_single_bank_name);
        String BANK_NM = verifyTransferInfo.getBANK_NM();
        if (TextUtils.isEmpty(alias)) {
            alias = BANK_NM;
        }
        textWithdrawBankName.setText(alias);

        TextView textWithdrawAccount = (TextView) findViewById(R.id.textview_withdraw_single_account);
        if (!TextUtils.isEmpty(withdrawAccount)) {
            int len = withdrawAccount.length();
            withdrawAccount =  withdrawAccount.substring(0, 5) + "-" + withdrawAccount.substring(5, 7) + "-" + withdrawAccount.substring(7, len);
            textWithdrawAccount.setText(withdrawAccount);
        }

        Button btnAddTransfer = (Button) findViewById(R.id.btn_add_confirm_single_tranfser);
        btnAddTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnAddTransfer();
                dismiss();
            }
        });

        RelativeLayout layoutSendTime = (RelativeLayout) findViewById(R.id.layout_send_time);
        TextView textSendTime = (TextView) findViewById(R.id.textview_send_time);
        RelativeLayout layoutSendDate = (RelativeLayout) findViewById(R.id.layout_send_date);
        TextView textSendDate = (TextView) findViewById(R.id.textview_send_date);

        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String DLY_TRNF_SVC_ENTR_YN = loginUserInfo.getDLY_TRNF_SVC_ENTR_YN();
        String TRNF_DVCD = verifyTransferInfo.getTRNF_DVCD();
        String DSGT_MNRC_ACCO_SVC_ENTR_YN = loginUserInfo.getDSGT_MNRC_ACCO_SVC_ENTR_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DLY_TRNF_SVC_ENTR_YN)) {
            btnAddTransfer.setVisibility(View.GONE);
            if ("2".equalsIgnoreCase(TRNF_DVCD)) {
                String TRNF_DMND_DT = verifyTransferInfo.getTRNF_DMND_DT();
                String TRNF_DMND_TM = verifyTransferInfo.getTRNF_DMND_TM();

                if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                    String year = TRNF_DMND_DT.substring(0, 4);
                    String month = TRNF_DMND_DT.substring(4, 6);
                    String day = TRNF_DMND_DT.substring(6, 8);
                    String hour = TRNF_DMND_TM.substring(0, 2);
                    String date = year + "년 " + month + "월 " + day + "일 " + hour + "시";
                    textSendDate.setText(date);
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.VISIBLE);
                } else {
                    String SVC_DVCD = verifyTransferInfo.getSVC_DVCD();
                    if ("1".equalsIgnoreCase(SVC_DVCD)) {
                        textSendTime.setText(context.getString(R.string.Immediately));
                    } else {
                        textSendTime.setText(context.getString(R.string.msg_after_3_hour));
                    }

                    layoutSendTime.setVisibility(View.VISIBLE);
                    layoutSendDate.setVisibility(View.GONE);
                }
            } else if ("1".equalsIgnoreCase(TRNF_DVCD)) {
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.GONE);
            } else if ("3".equalsIgnoreCase(TRNF_DVCD)) {
                String TRNF_DMND_DT = verifyTransferInfo.getTRNF_DMND_DT();
                String TRNF_DMND_TM = verifyTransferInfo.getTRNF_DMND_TM();

                if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                    String year = TRNF_DMND_DT.substring(0, 4);
                    String month = TRNF_DMND_DT.substring(4, 6);
                    String day = TRNF_DMND_DT.substring(6, 8);
                    String hour = TRNF_DMND_TM.substring(0, 2);
                    String date = year + "년 " + month + "월 " + day + "일 " + hour + "시";
                    textSendDate.setText(date);
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.VISIBLE);
                }
            }
        } else if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DSGT_MNRC_ACCO_SVC_ENTR_YN)) {
            btnAddTransfer.setVisibility(View.GONE);

            String TRNF_DMND_DT = verifyTransferInfo.getTRNF_DMND_DT();
            String TRNF_DMND_TM = verifyTransferInfo.getTRNF_DMND_TM();
            if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                String year = TRNF_DMND_DT.substring(0, 4);
                String month = TRNF_DMND_DT.substring(4, 6);
                String day = TRNF_DMND_DT.substring(6, 8);
                String hour = TRNF_DMND_TM.substring(0, 2);
                String date = year + "년 " + month + "월 " + day + "일 " + hour + "시";
                textSendDate.setText(date);
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.VISIBLE);
            } else {
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.GONE);
            }
        } else {
            if ("3".equalsIgnoreCase(TRNF_DVCD)) {
                String TRNF_DMND_DT = verifyTransferInfo.getTRNF_DMND_DT();
                String TRNF_DMND_TM = verifyTransferInfo.getTRNF_DMND_TM();
                if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                    String year = TRNF_DMND_DT.substring(0, 4);
                    String month = TRNF_DMND_DT.substring(4, 6);
                    String day = TRNF_DMND_DT.substring(6, 8);
                    String hour = TRNF_DMND_TM.substring(0, 2);
                    String date = year + "년 " + month + "월 " + day + "일 " + hour + "시";
                    textSendDate.setText(date);
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.VISIBLE);
                } else {
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.GONE);
                }
            } else {
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.GONE);
            }
        }

        Button btnCancel = (Button) findViewById(R.id.btn_cancel_confirm_single_transfer);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                if (TransferManager.getInstance().getIsNotRemove()) {
                    TransferManager.getInstance().setIsNotRemove(false);
                    dismiss();
                    return;
                }

                requestRemoveTransfer();
            }
        });

        Button btnOK = (Button) findViewById(R.id.btn_ok_confirm_single_transfer);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnOKListener();
            }
        });

        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                if (TransferManager.getInstance().getIsNotRemove()) {
                    TransferManager.getInstance().setIsNotRemove(false);
                    dismiss();
                    return;
                }

                requestRemoveTransfer();
            }
        });
    }

    public void requestRemoveTransfer() {
        VerifyTransferInfo verifyTransferInfo = TransferManager.getInstance().get(0);
        if (verifyTransferInfo == null)
            return;

        String UUID_NO = verifyTransferInfo.getUUID_NO();
        if (TextUtils.isEmpty(UUID_NO)) {
            showErrorMessage(context.getString(R.string.msg_debug_err_response));
            return;
        }

        Map param = new HashMap();
        param.put("UUID_NO", UUID_NO);
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0010400A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    String msg = context.getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        String msg = context.getString(R.string.msg_debug_err_response);
                        showErrorMessage(msg);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(context.getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = context.getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        showErrorMessage(msg);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        ((BaseActivity) context).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    TransferManager.getInstance().clear();
                    dismiss();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    public void showErrorMessage(String msg) {
        msg = msg.replaceAll("\\\\n", "\n");

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.mPListener = okClick;
        alertDialog.msg = msg;
        alertDialog.show();
    }

    public interface FinishListener {
        void OnOKListener();
        void OnAddTransfer();
    }
}
