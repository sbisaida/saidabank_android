package com.sbi.saidabank.common.adapter;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.TagItemInfo;

import java.util.ArrayList;
import java.util.Calendar;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.TagViewHolder> {
    private static final long MIN_CLICK_INTERVAL = 600;    // ms
    private long mLastClickTime;

    private Context                context;
    private ArrayList<TagItemInfo> listTagItem;
    private TagClickListener       listener;

    public TagAdapter(Context context, ArrayList<TagItemInfo> listTagItem, TagClickListener listener) {
        this.context = context;
        this.listTagItem = listTagItem;
        this.listener = listener;
    }

    @NonNull
    @Override
    public TagViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tagview_item, viewGroup, false);
        return new TagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TagViewHolder tagViewHolder, int i) {
        showTimeMode(tagViewHolder);

        TagItemInfo tagItem = listTagItem.get(i);
        if (tagItem == null)
            return;

        String tag = tagItem.getTRNF_DEPR_NM();
        tag = "#" + tag;
        tagViewHolder.textViewTag.setText(tag);
    }

    @Override
    public int getItemCount() {
        return listTagItem.size();
    }

    public class TagViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewTag;
        private LinearLayout tagContainer;

        TagViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTag = itemView.findViewById(R.id.textview_tag);
            tagContainer = itemView.findViewById(R.id.tag_container);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            long currentClickTime = SystemClock.uptimeMillis();
            long elapsedTime = currentClickTime - mLastClickTime;
            mLastClickTime = currentClickTime;

            if (elapsedTime <= MIN_CLICK_INTERVAL) {
                // 중복클릭 방지
                return;
            }

            if (listener != null) {
                TagItemInfo tagItem = listTagItem.get(getAdapterPosition());
                listener.onGetSelectTag(getAdapterPosition(), tagItem);
            }
        }
    }

    public void setListTag(ArrayList<TagItemInfo> listTagItem) {
        this.listTagItem = listTagItem;
        notifyDataSetChanged();
    }

    private void showTimeMode(TagViewHolder tagViewHolder) {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
                    (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            tagViewHolder.textViewTag.setBackgroundColor(context.getResources().getColor(R.color.color39434E));
            tagViewHolder.textViewTag.setTextColor(context.getResources().getColor(R.color.color009BEB));
            tagViewHolder.tagContainer.setBackground(ActivityCompat.getDrawable(context, R.drawable.backrogund_tagview_item_box_n));
        } else {
            tagViewHolder.textViewTag.setBackgroundColor(context.getResources().getColor(R.color.white));
            tagViewHolder.textViewTag.setTextColor(context.getResources().getColor(R.color.color009BEB));
            tagViewHolder.tagContainer.setBackground(ActivityCompat.getDrawable(context, R.drawable.backrogund_tagview_item_box));
        }
    }

    public interface TagClickListener {
        void onGetSelectTag(int position, TagItemInfo tagInfo);
    }
}
