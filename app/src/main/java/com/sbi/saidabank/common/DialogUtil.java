package com.sbi.saidabank.common;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.ErrorDialog;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.SlidingCertDeleteDialog;
import com.sbi.saidabank.common.util.Prefer;


public class DialogUtil {

	/**
	 * =====================================
	 * Alert, Confirm Dialog
	 * =====================================
	 */
	private static AlertDialog mAlertDialog;

	/**
	 * Alert Dialog
	 * @param context
	 * @param msgId 메세지 문자열ID
	 */
	public static void alert(Context context, int msgId){
		alert(context, context.getString(msgId));
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param msg 메세지 문자열
	 */
	public static void alert(Context context, String msg){
		alert(context, null, msg, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title 타이틀 문자열
	 * @param msg   메세지 문자열
	 */
	public static void alert(Context context, String title, String msg){
		alert(context, title, msg, null);
	}

	/**
	 *
	 * @param context
	 * @param titleId 타이틀 문자열ID
	 * @param msgId   메세지 문자열ID
	 */
	public static void alert(Context context, int titleId, int msgId){
		alert(context, context.getString(titleId), context.getString(msgId));
	}

	/**
	 *
	 * @param context
	 * @param msgId   메세지 문자열ID
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, int msgId, View.OnClickListener pListener){
		alert(context, context.getString(R.string.common_confirm), "", context.getString(msgId), pListener, null);
	}

	/**
	 *
	 * @param context
	 * @param pId	버튼 문자열ID
	 * @param msg	메세지 문자열
	 * @param pListener	버튼 콜백 리스너
	 */
	public static void alert(Context context, int pId, String msg, View.OnClickListener pListener){
		alert(context, context.getString(pId), "", msg, pListener, null);
	}

	/**
	 *
	 * @param context
	 * @param msg	메세지 문자열
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, String msg, View.OnClickListener pListener){
		alert(context, context.getString(R.string.common_confirm), "", msg, pListener, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg, View.OnClickListener pListener){
		alert(context, title, context.getString(R.string.common_confirm), "", msg, pListener, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg,String pBtn ,View.OnClickListener pListener){
		alert(context, title, pBtn, "", msg, pListener, null);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param msg	메세지 문자열ID
	 * @param pListener	확인버튼 콜백 리스너
	 * @param nListener 취소버튼 콜백 리스너
	 */
	public static void alert(Context context, int msg, View.OnClickListener pListener, View.OnClickListener nListener){
		alert(context, context.getString(R.string.common_confirm), context.getString(R.string.common_cancel), context.getString(msg), pListener, nListener);
	}

	/**
	 *
	 * @param context
	 * @param msg
	 * @param pListener
	 * @param nListener
	 */
	public static void alert(Context context, String msg, View.OnClickListener pListener, View.OnClickListener nListener){
		alert(context, context.getString(R.string.common_confirm), context.getString(R.string.common_cancel), msg, pListener, nListener);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	확인버튼 콜백 리스너
	 * @param nListener 취소버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg, View.OnClickListener pListener, View.OnClickListener nListener){
		alert(context, title, context.getString(R.string.common_confirm), context.getString(R.string.common_cancel), msg, pListener, nListener);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param pBtn	positive 버튼 문자열
	 * @param nBtn  nagative 버튼 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	positive 버튼 콜백 리스너
	 * @param nListener nagative 버튼 콜백 리스터
	 */
	public static void alert(Context context, String pBtn, String nBtn, String msg, View.OnClickListener pListener, View.OnClickListener nListener){
		alert(context, null, pBtn, nBtn, msg, pListener, nListener);
	}

	/**
	 * Alert, Confirm Dialog 구성함수
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param pBtn	Positive 버튼 문자열
	 * @param nBtn	nagative 버튼 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	positive 버튼 콜백 리스너
	 * @param nListener	nagative 버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String pBtn, String nBtn, String msg, View.OnClickListener pListener, View.OnClickListener nListener){

		mAlertDialog = new AlertDialog(context);

/*
		if(title != null && !"".equals(title)){
			mAlertDialog.title = title;
		}
*/
		if(msg != null && !"".equals(msg)){
			mAlertDialog.msg = msg;
		}

		if(nBtn != null && !"".equals(nBtn)){
			mAlertDialog.mNBtText = nBtn;
		}

		if(pBtn != null && !"".equals(pBtn)){
			mAlertDialog.mPBtText = pBtn;
		}

		if(nListener != null){
			mAlertDialog.mNListener = nListener;
		}

		if(pListener != null){
			mAlertDialog.mPListener = pListener;
		}

        if(context instanceof Activity && !((Activity)context).isFinishing()) {
            mAlertDialog.show();
        }
	}

	/**
	 * =====================================
	 * Error Dialog
	 * =====================================
	 */
	private static ErrorDialog mErrorDialog;

	/**
	 * Error Dialog 구성 함수
	 *
	 * @param context
	 * @param title
	 * @param msg
	 * @param pBtn
	 * @param pListener
	 */
	public static void error(Context context, String title, String msg, String pBtn, View.OnClickListener pListener){

		mErrorDialog = new ErrorDialog(context);
/*
		if(title != null && !"".equals(title)){
			mErrorDialog.title = title;
		}
*/
		if(msg != null && !"".equals(msg)){
			mErrorDialog.msg = msg;
		}

		if(pBtn != null && !"".equals(pBtn)){
			mErrorDialog.mPBtText = pBtn;
		}

		if(pListener != null){
			mErrorDialog.mPListener = pListener;
		}

		if(context instanceof Activity && !((Activity)context).isFinishing()) {
			mErrorDialog.show();
		}
	}

	/**
	 * Fido Dialog 구성 함수
	 *
	 * @param context
	 * @param isCancelBtn
	 * @param pListener
	 */
	public static FidoDialog fido(Context context, boolean isCancelBtn, View.OnClickListener pListener, Dialog.OnDismissListener dListener){

		FidoDialog fidoDialog = new FidoDialog(context, isCancelBtn);


		if(pListener != null){
			fidoDialog.mPListener = pListener;
		}

		if(dListener != null){
			fidoDialog.mDismissListener = dListener;
		}

		if(context instanceof Activity && !((Activity)context).isFinishing()) {
			fidoDialog.show();
		}

		return fidoDialog;
	}

}
