package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

import com.sbi.saidabank.R;


/**
 * Saidabank_android
 * Class: SlidingBaseDialog
 * Created by 950469 on 2018. 10. 11..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 다이얼로그 Base
 */

public class SlidingBaseDialog extends BaseDialog {

    public SlidingBaseDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature( Window.FEATURE_NO_TITLE ) ;

        getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) ) ;
        getWindow().setGravity(Gravity.BOTTOM);
    }

    protected void setDialogWidth() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
        LayoutParams lp = getWindow().getAttributes( ) ;
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        lp.width = size.x;
        getWindow().setAttributes( lp ) ;
    }
}
