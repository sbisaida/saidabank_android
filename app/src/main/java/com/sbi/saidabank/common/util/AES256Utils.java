package com.sbi.saidabank.common.util;

import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * Saidabank_android
 * Class: AES256Utils
 * Created by 950546
 * Date: 2019-03-06
 * Time: 오전 9:50
 * Description: AES256 암/복호화 유틸
 */
public class AES256Utils {
    //private static final byte[] keyValue = new byte[] { 0x41, 0x63, 0x75, 0x6F, 0x6E, 0x53, 0x61, 0x76, 0x69, 0x6E, 0x67, 0x73, 0x42, 0x61, 0x6E, 0x6B};
    private String charset = Charset.forName("UTF-8").name();
    private String enCryptkey = "FAFBA8B5916DDEA9AED7637BC04B81C2".substring(0, 128 / 8);
    private String ciperTrans = "AES/CBC/PKCS5Padding";
    private Key keySpec;
    byte[] keyData;

    public AES256Utils() throws UnsupportedEncodingException {

        keyData = enCryptkey.getBytes(charset);
        SecretKeySpec keySpec = new SecretKeySpec(keyData, "AES");
        this.keySpec = keySpec;
    }


    // 암호화
    public String aesEncode(String str) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {

        /*
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, keySpec);

        byte[] encrypted = c.doFinal(str.getBytes(charset));
        String enStr = Base64.encodeToString(encrypted, Base64.NO_WRAP);
        */


        Cipher c = Cipher.getInstance(ciperTrans);
        c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(keyData));

        byte[] encrypted = c.doFinal(str.getBytes(charset));
        String enStr = Base64.encodeToString(encrypted, Base64.NO_WRAP);


        return enStr;
    }

    //복호화
    public String aesDecode(String str) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        /*
        byte[] byteStr = Base64.decode(str.getBytes("UTF-8"), Base64.NO_WRAP);

        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, keySpec);
        */

        byte[] byteStr = Base64.decode(str.getBytes(charset), Base64.NO_WRAP);

        Cipher c = Cipher.getInstance(ciperTrans);
        c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(keyData));

        return new String(c.doFinal(byteStr), charset);
    }

    public static String decodeAES(String str) {
        String decodeStr = "";

        if (!TextUtils.isEmpty(str)) {
            try {
                decodeStr = new AES256Utils().aesDecode(str);
            } catch (UnsupportedEncodingException e) {
                Logs.printException(e);
            } catch (NoSuchAlgorithmException e) {
                Logs.printException(e);
            } catch (NoSuchPaddingException e) {
                Logs.printException(e);
            } catch (InvalidKeyException e) {
                Logs.printException(e);
            } catch (InvalidAlgorithmParameterException e) {
                Logs.printException(e);
            } catch (IllegalBlockSizeException e) {
                Logs.printException(e);
            } catch (BadPaddingException e) {
                Logs.printException(e);
            }
        }

        return decodeStr;
    }

    public static String encodeAES(String str) {
        String endcodeStr = "";
        if (!TextUtils.isEmpty(str)) {
            try {
                endcodeStr = new AES256Utils().aesEncode(str);
            } catch (UnsupportedEncodingException e) {
                Logs.printException(e);
            } catch (NoSuchAlgorithmException e) {
                Logs.printException(e);
            } catch (NoSuchPaddingException e) {
                Logs.printException(e);
            } catch (InvalidKeyException e) {
                Logs.printException(e);
            } catch (InvalidAlgorithmParameterException e) {
                Logs.printException(e);
            } catch (IllegalBlockSizeException e) {
                Logs.printException(e);
            } catch (BadPaddingException e) {
                Logs.printException(e);
            }
        }

        return endcodeStr;
    }
}

