package com.sbi.saidabank.common.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.web.JavaScriptBridgeDlg;

public class MainEventDialog extends Dialog {
    private Context             mContext;
    private String              mEVNT_ID;
    private OnListener          mListener;

    private JavaScriptBridgeDlg mJsBridge;
    private FrameLayout         mContainer;

    public MainEventDialog(Context context, String EVNT_ID, OnListener clickListener) {
        super(context, R.style.TransparentProgressDialog);
        mContext = context;
        mEVNT_ID = EVNT_ID;
        mListener = clickListener;
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_main_event);
        setDialogSize();

        initUX();
    }

    @Override
    public void show() {
        super.show();
    }

    protected void setDialogSize() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        WindowManager.LayoutParams lp = getWindow().getAttributes( ) ;
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        getWindow().setAttributes(lp);
    }

    @SuppressLint("JavascriptInterface")
    private void initUX() {
        mContainer = (FrameLayout) findViewById(R.id.container_webview);
        BaseWebView webviewEvent = (BaseWebView) findViewById(R.id.webview_main_event);

        mJsBridge = new JavaScriptBridgeDlg(MainEventDialog.this, mContext, mContainer);
        webviewEvent.addJavascriptInterface(mJsBridge, JavaScriptBridgeDlg.CALL_NAME);

        String url = WasServiceUrl.MAI0010500.getServiceUrl();

        if (TextUtils.isEmpty(mEVNT_ID))
            return;

        String param = "EVNT_ID=" + mEVNT_ID;
        webviewEvent.postUrl(url, param.getBytes());
    }

    public interface OnListener {
        void onFinishEvent(String moveUrl);
    }
}