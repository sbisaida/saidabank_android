package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


public class PinErrorDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;
    private String mErrCode;
    private String mScrnId;
    private JSONObject mObjentHead;

    private LinearLayout        mLayoutSendErr;
    private ImageView           mLoadingImageView;
    private Button              mBtChatbot;
    private Button              mBtPositive;

    public PinErrorDialog(Context context, String errCode, String scrnId, JSONObject objectHead) {
        super(context);
        mContext = context;
        mErrCode = errCode;
        mScrnId = scrnId;
        mObjentHead = objectHead;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_pin_error);

        setDialogWidth();

        initView();
    }

    private void initView() {
        setCancelable(false);

        if (mErrCode == null || "".equals(mErrCode)) {
            mErrCode = "에러코드 없음";
        }

        mLoadingImageView = (ImageView) findViewById(R.id.img_progress_webview);

        //릴리즈 버전에서 P002 오류 추가 정보 표시하지 않음
        if ("P002".equals(mErrCode) && BuildConfig.DEBUG) {
            TextView tvDesc = findViewById(R.id.tv_desc);
            TextView tvIP = findViewById(R.id.tv_ip);
            TextView tvDate = findViewById(R.id.tv_date);
            String date = Utils.getCurrentDate(".") + " " + Utils.getCurrentTime(":");
            String desc = StonePassManager.getInstance(((Activity) mContext).getApplication()).getErrorDesc();
            String ip = StonePassManager.getInstance(((Activity) mContext).getApplication()).getErrorIP();
            if (TextUtils.isEmpty(desc))
                tvDesc.setText("No Description");
            else
                tvDesc.setText(desc);

            tvDate.setText(date);
            tvDesc.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(ip)) {
                tvIP.setText(ip);
                tvIP.setVisibility(View.VISIBLE);
            }
            tvDate.setVisibility(View.VISIBLE);
        }

        ((TextView) findViewById(R.id.tv_errcode)).setText(mErrCode);

        mBtChatbot = (Button) findViewById(R.id.btn_chatbot);
        mBtChatbot.setOnClickListener(this);

        mBtPositive = (Button) findViewById(R.id.btn_confirm);
        mBtPositive.setOnClickListener(this);

        mLayoutSendErr = (LinearLayout) findViewById(R.id.ll_sendimage);
        mLayoutSendErr.setOnClickListener(this);

        if (!LoginUserInfo.getInstance().isLogin()) {
            mLayoutSendErr.setVisibility(View.GONE);
            mBtChatbot.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_chatbot: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_TEST) {
                        Logs.shwoToast(mContext, "테스트 서버에서는 챗봇 사용 불가입니다.");
                    } else {
                        String TRN_CD = "";
                        if (mObjentHead != null && mObjentHead.has("DATA")) {
                            JSONObject data = mObjentHead.optJSONObject("DATA");

                            if (data.has("__INFO__")) {
                                JSONObject info;
                                info = data.optJSONObject("__INFO__");
                                TRN_CD = info.optString("TRN_CD");
                            }
                        }
                        StringBuilder param = new StringBuilder();

                        try {
                            param.append("TRN_CD" + "=" + URLEncoder.encode(TRN_CD, "EUC-KR"));
                            param.append("&");
                            param.append("EROR_CD" + "=" + URLEncoder.encode(mErrCode, "EUC-KR"));
                            param.append("&");
                            param.append("ERROR_MESSAGE" + "=" + URLEncoder.encode("인증비밀번호 인증 중 문제가 발생했습니다."));
                        } catch (UnsupportedEncodingException e) {
                            //e.printStackTrace();
                        }
                        String url = SaidaUrl.ReqUrl.URL_CHATBOT.getReqUrl() + "?" + param.toString();
                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        intent.putExtra("url", url);
                        mContext.startActivity(intent);
                        dismiss();
                    }
                }
                break;
            }
            case R.id.btn_confirm: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    dismiss();
                }
                break;
            }

            case R.id.ll_sendimage: {
                captureScreen();
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /**
     * 에러 다이얼로그가 호출되면 화면을 캡쳐하여 WAS로 전달한다.
     */
    private void captureScreen() {
        Map param = new HashMap();

        //채널 was와 오류났을 경우
        if (mObjentHead != null && mObjentHead.has("DATA")) {
            JSONObject data = mObjentHead.optJSONObject("DATA");

            if (data != null) {
                if (data.has("__INFO__")) {
                    JSONObject info = data.optJSONObject("__INFO__");
                    if (info != null) {
                        String CH_GLOBAL_ID = info.optString("CH_GLOBAL_ID");
                        String TRN_DT = info.optString("TRN_DT");
                        String TRN_DTTM = info.optString("TRN_DTTM");
                        String GLOBAL_ID = info.optString("GLOBAL_ID");

                        param.put("CH_GLOBAL_ID", CH_GLOBAL_ID);
                        param.put("TRN_DT", TRN_DT);
                        param.put("TRN_DTTM", TRN_DTTM);
                        param.put("GLOBAL_ID", GLOBAL_ID);
                    }
                }
            }
        }

        param.put("SCRN_ID", mScrnId);
        param.put("ERO_CD", mErrCode);

        View activityView = ((Activity) mContext).getWindow().getDecorView().getRootView();
        View dialogView = getWindow().getDecorView().getRootView();
        byte[] imgData = captureSave(activityView, dialogView);

        param.put("IMG_CNTN", Base64.encodeToString(imgData, Base64.DEFAULT));

        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                if (TextUtils.isEmpty(ret)) {
                    Logs.shwoToast(mContext, "이미지 전송 실패하였습니다.");
                    return;
                }
                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.shwoToast(mContext, "이미지 전송 실패하였습니다.");
                        return;
                    }
                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        Logs.shwoToast(mContext, "이미지 전송 실패하였습니다.");
                        return;
                    }
                    Logs.shwoToast(mContext, "이미지 전송 완료하였습니다.");
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    public static byte[] captureSave(View view, View view2) {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();

        view2.setDrawingCacheEnabled(true);
        view2.buildDrawingCache();
        Bitmap viewBmp2 = view2.getDrawingCache();

        if (viewBmp != null && viewBmp2 != null) {
            Bitmap combineBmp = ImgUtils.combineBitmap(viewBmp, viewBmp2);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            combineBmp.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            view2.setDrawingCacheEnabled(false);
            view2.destroyDrawingCache();
            viewBmp2.recycle();
            combineBmp.recycle();
            return byteArrayOutputStream.toByteArray();
        }

        return null;
    }

    private void showProgress() {
        if (mLayoutSendErr != null)
            mLayoutSendErr.setClickable(false);
        if (mBtChatbot != null)
            mBtChatbot.setClickable(false);
        if (mBtPositive != null)
            mBtPositive.setClickable(false);
        if (mLoadingImageView != null && mLoadingImageView.getVisibility() != View.VISIBLE) {
            mLoadingImageView.setVisibility(View.VISIBLE);
            ((AnimationDrawable) mLoadingImageView.getBackground()).start();
        }
    }

    private void dismissProgress() {
        if (mLayoutSendErr != null)
            mLayoutSendErr.setClickable(true);
        if (mBtChatbot != null)
            mBtChatbot.setClickable(true);
        if (mBtPositive != null)
            mBtPositive.setClickable(true);
        if (mLoadingImageView != null) {
            mLoadingImageView.setVisibility(View.GONE);
        }
    }
}
