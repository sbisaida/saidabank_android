package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.adapter.JoinGoodAdapter;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.BankGoodInfo;

import java.util.ArrayList;

public class JoinGoodDialog extends Dialog {
    private Context mContext;

    private ArrayList<BankGoodInfo> mListBankGoodInfo = new ArrayList<>();
    private JoinGoodAdapter mJoinGoodAdapter;

    public JoinGoodDialog(Context context, ArrayList<BankGoodInfo> listBankGoodInfo) {
        super(context, R.style.TransparentProgressDialog);
        mContext = context;
        mListBankGoodInfo = listBankGoodInfo;
        //setCancelable(false);
        //setOnCancelListener(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_join_good);
        setDialogSize();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        initUX();
    }

    @Override
    public void show() {
        super.show();
    }

    protected void setDialogSize() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        WindowManager.LayoutParams lp = getWindow().getAttributes( ) ;
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        lp.width = size.x;
        lp.height = size.y;
        getWindow().setAttributes(lp);
    }

    private void initUX() {
        TextView btnCancel = (TextView) findViewById(R.id.textview_cancel_join_good);
        btnCancel.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });

        RecyclerView recyclerViewGood = (RecyclerView) findViewById(R.id.recyclerview_good_list);
        if (mJoinGoodAdapter == null) {
            mJoinGoodAdapter = new JoinGoodAdapter(mContext, mListBankGoodInfo,
                new JoinGoodAdapter.ClickListener() {
                    @Override
                    public void OnItemClickListener(int position) {
                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        BankGoodInfo goodInfo = mListBankGoodInfo.get(position);
                        if (goodInfo == null)
                            return;

                        String url = goodInfo.getMENU_URL_ADDR();
                        String fullUrl = WasServiceUrl.getUrl() + "/" + url;
                        intent.putExtra(Const.INTENT_MAINWEB_URL, fullUrl);
                        mContext.startActivity(intent);
                        dismiss();
                    }
            });
            recyclerViewGood.setAdapter(mJoinGoodAdapter);

            LinearLayoutManager llm = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            recyclerViewGood.setLayoutManager(llm);
            recyclerViewGood.setHasFixedSize(true);
        }
    }
}