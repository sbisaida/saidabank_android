package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.netfunnel.api.ContinueData;
import com.netfunnel.api.Netfunnel;
import com.sbi.saidabank.R;

import java.text.DecimalFormat;


public class NetfunnelDialog extends Dialog implements Netfunnel.Listener {

    private static NetfunnelDialog instance_ = null;
    private static Context context_= null;

    public static NetfunnelDialog getInstance(Context context)
    {
        if(context == null) return(null);
        if(context_ !=  context) {
            Dismiss(instance_);
            instance_ = null;
        }

        if(instance_ == null) {
            instance_ = new NetfunnelDialog(context);
            context_ = context;
        }
        return(instance_);
    }

    public static void Close()
    {
        Dismiss(instance_);
        instance_ = null;
    }
    public static NetfunnelDialog Show(Context context)
    {
        NetfunnelDialog dialog = NetfunnelDialog.getInstance(context);
        dialog.show();
        return dialog;
    }

    public static void Dismiss(NetfunnelDialog dialog)
    {
        if(dialog == null) return;
        try { dialog.dismiss(); }
        catch (Exception e){ }
    }

    public static NetfunnelDialog Create(Context context,boolean isShow)
    {
        NetfunnelDialog dialog = new NetfunnelDialog(context);
        if(isShow) dialog.show();
        return dialog;
    }

    private LinearLayout mLoNetfunnelMain_ = null;
    private TextView mTxtNetfunnelMessageRunning_ = null;
    private ProgressBar mPgbNetfunnelWaitPercent_ = null;
    private TextView mTxtNetfunnelWaitTime_ = null;
    private TextView mTxtNetfunnelWaitCount_ = null;
    private TextView mBtnNetfunnelStop_ = null;

    private Handler netfunnel_handler_ = null;
    private Netfunnel netfunnel_ = null;

    public NetfunnelDialog(Context context) {
        super(context, R.style.netfunnel_simple_transparent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_netfunnel);

        setCancelable(false);
        setCanceledOnTouchOutside(false);

        mTxtNetfunnelMessageRunning_ = (TextView) findViewById(R.id.txtNetfunnelMessageRunning);
        mTxtNetfunnelWaitTime_ = (TextView) findViewById(R.id.txtNetfunnelWaitTime);
        mPgbNetfunnelWaitPercent_ = (ProgressBar) findViewById(R.id.pgbNetfunnelWaitPercent);
        mBtnNetfunnelStop_ = (TextView) findViewById(R.id.btnNetfunnelStop);
        mTxtNetfunnelWaitCount_ = (TextView) findViewById(R.id.txtNetfunnelWaitCount);

        mLoNetfunnelMain_ = (LinearLayout) findViewById(R.id.loNetfunnelMain);
        mLoNetfunnelMain_.setVisibility(View.INVISIBLE);

        final  NetfunnelDialog dialog = this;
        mBtnNetfunnelStop_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(netfunnel_ == null) {
                        dialog.dismiss();
                        return;
                    }
                    netfunnel_.StopContinue();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        netfunnel_handler_ = new Handler(){
            public void handleMessage(Message msg)
            {
                try {
                    Bundle data = msg.getData();
                    Netfunnel.EvnetCode code = Netfunnel.EvnetCode.toEnum(msg.what);
                    Netfunnel netfunnel = (Netfunnel)msg.obj;

                    if(code.isContinue()){
                        ContinueData continue_data = netfunnel.getContinueData();

                        int frame_width = ((LinearLayout) findViewById(R.id.loNetfunnelMessage)).getWidth() -70 ;
                        if (continue_data.getAcountNotice() == 1) {
                            mLoNetfunnelMain_.setVisibility(View.VISIBLE);
                            applyNewLineCharacter((TextView) findViewById(R.id.txtNetfunnelWaitCountText1),frame_width);
                            applyNewLineCharacter((TextView) findViewById(R.id.txtNetfunnelWaitCountText2),frame_width);
                        }

                        if (code == Netfunnel.EvnetCode.ContinueInterval) {
                        } else {  // Netfunnel.EvnetCode.Continue
                            DecimalFormat df = new DecimalFormat("#,##0");
                            mPgbNetfunnelWaitPercent_.setProgress((int) continue_data.getCurrentWaitPercent());


                            float wait_time = continue_data.getCurrentWaitTimeSecond();
                            if(netfunnel.getProperty().getUiWaitTimeLimit() > 0 && continue_data.getCurrentWaitTimeSecond() > netfunnel.getProperty().getUiWaitTimeLimit())
                                wait_time =  netfunnel.getProperty().getUiWaitTimeLimit();
                            String wait_time_text = getContext().getString(R.string.netfunnel_waittime_text);
                            wait_time_text = wait_time_text.replace("[[wait_time]]", ""+ df.format((int)Math.ceil(wait_time)));
                            mTxtNetfunnelWaitTime_.setText(Html.fromHtml(wait_time_text));

                            int wait_count = continue_data.getCurrentWaitCount();
                            if(netfunnel.getProperty().getUiWaitCountLimit() > 0 && continue_data.getCurrentWaitCount() > netfunnel.getProperty().getUiWaitCountLimit())
                                wait_count =  netfunnel.getProperty().getUiWaitTimeLimit();
                            int next_count = continue_data.getCurrentNextCount();
                            if(netfunnel.getProperty().getUiNextCountLimit() > 0 && continue_data.getCurrentNextCount() > netfunnel.getProperty().getUiNextCountLimit())
                                next_count =  netfunnel.getProperty().getUiWaitTimeLimit();

                            String waitcount_text = getContext().getString(R.string.netfunnel_waitcount_text);
                            waitcount_text = waitcount_text.replace("[[wait_before_count]]",df.format(wait_count));
                            waitcount_text = waitcount_text.replace("[[wait_after_count]]",df.format(next_count));
                            mTxtNetfunnelWaitCount_.setText(Html.fromHtml(waitcount_text));

                            // 글자쌕이 없어진다.
                            //applyNewLineCharacter(mTxtNetfunnelWaitCount_,frame_width);
                        }

                        int pos = continue_data.getAcountNotice() % 3;
                        String pos_text = ".";
                        if (pos == 0)
                            pos_text = ".";
                        else if (pos == 1)
                            pos_text = "..";
                        else if (pos == 2)
                            pos_text = "...";
                        mTxtNetfunnelMessageRunning_.setText(pos_text);
                    }else if(code.isSuccess()){
                        if(code == Netfunnel.EvnetCode.Success) {
                            // 주의) 여기에만 AliveNotice()을 사용할수 있는 곳
                        }else if(code == Netfunnel.EvnetCode.NotUsed){
                        }else if(code == Netfunnel.EvnetCode.Bypass){
                        }else if(code == Netfunnel.EvnetCode.ErrorBypass){
                        }else if(code == Netfunnel.EvnetCode.ExpressNumber){
                        }
                    }else if(code.isBlocking()){
                        if(code == Netfunnel.EvnetCode.Block) {
                            // 서비스 차단
                        }else if(code == Netfunnel.EvnetCode.IpBlock){
                            // 사용자 사용패턴에 의한 차단
                        }
                    }else if(code.isStop()){
                    }else{ // }else if(code.isError()){
                        if(code == Netfunnel.EvnetCode.ErrorService) {
                        }else if(code == Netfunnel.EvnetCode.ErrorSystem){
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
    }

    private void applyNewLineCharacter(TextView textView,int frameWidth)
    {
        Paint paint = textView.getPaint();
        String text = "" + textView.getText();
        int startIndex = 0;
        int endIndex = paint.breakText(text , true, frameWidth, null);
        String save = text.substring(startIndex, endIndex);

        while(true)
        {
            startIndex = endIndex;
            text = text.substring(startIndex);
            if(text.length() == 0) break;

            endIndex = paint.breakText(text, true, frameWidth, null);
            save += "\n" + text.substring(0, endIndex);
        }
        textView.setText(save);
    }


    @Override
    public void onBackPressed() {
    }

    @Override
    public void netfunnelMessage(Netfunnel netfunnel, Netfunnel.EvnetCode code){
        try {
            netfunnel_ = netfunnel;
            Message msg = netfunnel_handler_.obtainMessage();
            msg.what = code.value();
            msg.obj = netfunnel;
            netfunnel_handler_.sendMessage(msg);
        }catch (Exception e){
            e.printStackTrace();
        }
        return;
    }
}
