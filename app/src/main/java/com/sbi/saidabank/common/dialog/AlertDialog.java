package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;


public class AlertDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;

    //public String title;
    public String msg;

    public String mLinkText = "";
    public String mNBtText = "";
    public String mPBtText = "";
    public String mSubText = "";

    public View.OnClickListener mLinkListener;
    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;
    private OnDismissListener   _dismissListener;

    public AlertDialog(Context context) {
        super(context);
        mContext = context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_alert);

        setDialogWidth();

        initView();
    }

    private void initView(){
        setCancelable(false);
/*
        if(title == null || "".equals(title)){
            title = mContext.getString(R.string.common_notice);
        }
*/
        if(msg == null || "".equals(msg)){
            msg = mContext.getString(R.string.common_no_msg);
        }
        //((TextView)findViewById(R.id.tv_title)).setText(title);

        //((TextView)findViewById(R.id.tv_msg)).setText(msg);

        if (msg.contains("font color"))
            ((TextView)findViewById(R.id.tv_msg)).setText(Html.fromHtml(msg));
        else
            ((TextView)findViewById(R.id.tv_msg)).setText(msg);

        if (!TextUtils.isEmpty(mSubText)) {
            ((TextView) findViewById(R.id.tv_submsg)).setText(mSubText);
            findViewById(R.id.tv_submsg).setVisibility(View.VISIBLE);
        }

        Button mBtNegative = (Button)findViewById(R.id.bt_negative);
        Button mBtPositive = (Button)findViewById(R.id.bt_positive);
        mBtNegative.setOnClickListener(this);
        mBtPositive.setOnClickListener(this);

        if(mNBtText != null && !"".equals(mNBtText)){
            mBtNegative.setText(mNBtText);
        }

        if(mPBtText != null && !"".equals(mPBtText)){
            mBtPositive.setText(mPBtText);
        }

        if(mNListener == null){
            mBtNegative.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        }else{
            mBtNegative.setBackgroundResource(R.drawable.selector_radius_left_cancelbtn);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_right_okbtn);
        }

        if (mLinkListener != null) {
            LinearLayout llLink = (LinearLayout) findViewById(R.id.ll_alertlink);
            llLink.setVisibility(View.VISIBLE);
            TextView tvAlertLink = (TextView) findViewById(R.id.tv_linkmsg);
            tvAlertLink.setText(mLinkText);
            llLink.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_negative:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (_dismissListener != null)
                        _dismissListener.onDismiss();
                    dismiss();
                }
                if(mNListener != null) {
                    mNListener.onClick(view);
                }
                break;
            case R.id.bt_positive:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (_dismissListener != null)
                        _dismissListener.onDismiss();
                    dismiss();
                }
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                break;
            case R.id.ll_alertlink:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (_dismissListener != null)
                        _dismissListener.onDismiss();
                    dismiss();
                }
                if (mLinkListener != null) {
                    mLinkListener.onClick(view);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (_dismissListener != null)
            _dismissListener.onDismiss();
    }

    public void setOnDismissListener(OnDismissListener listener) {
        _dismissListener = listener;
    }

    public interface OnDismissListener {
        void onDismiss();
    }

}
