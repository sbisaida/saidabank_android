package com.sbi.saidabank.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.datatype.BankGoodInfo;

import java.util.ArrayList;

public class JoinGoodAdapter extends RecyclerView.Adapter<JoinGoodAdapter.JoinGoodListViewHolder> {
    private Context    context;

    private ArrayList<BankGoodInfo>  listBankGoodInfo;
    private ClickListener listener;

    public JoinGoodAdapter(Context context, ArrayList<BankGoodInfo> listBankGoodInfo, ClickListener listener) {
        this.context = context;
        this.listBankGoodInfo = listBankGoodInfo;
        this.listener = listener;
    }

    public class JoinGoodListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView        textTitle;
        public TextView        textRate;
        public ImageView       btnAdd;

        public JoinGoodListViewHolder(Context context, View view, int viewType) {
            super(view);

            textTitle = (TextView) view.findViewById(R.id.textview_good_title);
            textRate = (TextView) view.findViewById(R.id.textview_good_rate);
            btnAdd = (ImageView) view.findViewById(R.id.imageview_join_good);
            btnAdd.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if (view == btnAdd) {
                listener.OnItemClickListener(position);
            }
        }
    }

    @Override
    public JoinGoodAdapter.JoinGoodListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_join_good_list_item, parent, false);
        JoinGoodListViewHolder viewHolder = new JoinGoodListViewHolder(context, itemView, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(JoinGoodListViewHolder viewHolder, int position) {
        viewHolder.btnAdd.setTag(position);

        BankGoodInfo goodInfo = listBankGoodInfo.get(position);
        if (goodInfo == null)
            return;

        String EPOS_PROD_NM = goodInfo.getEPOS_PROD_NM();
        if (!TextUtils.isEmpty(EPOS_PROD_NM)) {
            viewHolder.textTitle.setText(EPOS_PROD_NM);
        }

        String PROD_INTR_INFO_CNTN = goodInfo.getPROD_INTR_INFO_CNTN();
        if (!TextUtils.isEmpty(PROD_INTR_INFO_CNTN)) {
            viewHolder.textRate.setText(PROD_INTR_INFO_CNTN);
        }
    }

    @Override
    public int getItemCount() {
        return listBankGoodInfo.size();
    }

    public interface ClickListener {
        void OnItemClickListener(int position);
    }
}
