package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: FidoAuthGuideDialog
 * Created by 950546
 * Date: 2019-01-24
 * Time: 오후 2:41
 * Description: 인증수단관리 지문 사용 안내팝업
 */
public class FidoAuthGuideDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;

    private LinearLayout mCautionMsg;

    public String mPBtText = "";

    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;
    public OnDismissListener mDismissListener;

    public FidoAuthGuideDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_fido_auth_guide);

        setDialogWidth();

        TextView descTextView = (TextView) findViewById(R.id.textview_fido_dialog_desc);
        String msg = mContext.getString(R.string.msg_fido_auth_guide);
        descTextView.setText(Html.fromHtml(msg));

        //setCanceledOnTouchOutside(false);

        Button btnPositive = (Button)findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(this);

        Button btnCancel = (Button)findViewById(R.id.btn_negative);
        btnCancel.setOnClickListener(this);

        if(mPBtText != null && !"".equals(mPBtText)){
            btnPositive.setText(mPBtText);
        }

        mCautionMsg = findViewById(R.id.layout_fingerprint_msg);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_negative: {
                if(mNListener != null) {
                    mNListener.onClick(v);
                }
                break;
            }

            case R.id.btn_positive: {
                if(mPListener != null) {
                    mPListener.onClick(v);
                }
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void dismiss() {
        super.dismiss();
        if(mDismissListener != null) {
            mDismissListener.onDismiss(this);
        }
    }
}
