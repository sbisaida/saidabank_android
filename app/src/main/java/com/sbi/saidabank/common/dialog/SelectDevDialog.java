package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

/**
 * Saidabank_android
 * Class: SelectDevDialog
 * Created by 950546 on 2019. 02. 26..
 * <p>
 * Description: 디버깅용 서버 선택 다이얼로그
 */
public class SelectDevDialog extends BaseDialog implements View.OnClickListener {

    private Context mContext;
    private SelectDevDialog.OnConfirmListener _listener;


    public SelectDevDialog(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_dev);

        setDialogWidth();

        WifiManager manager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = manager.getConnectionInfo();
        String SSID = wifiInfo.getSSID();

        if (!TextUtils.isEmpty(SSID))
            ((TextView) findViewById(R.id.tv_wifiname)).setText("연결된 WiFi : " + SSID);
        findViewById(R.id.btn_dev).setOnClickListener(this);
        //운영계 선택 버튼 비활성화
        if (false) {
            findViewById(R.id.btn_op).setOnClickListener(this);
        } else {
            findViewById(R.id.btn_op).setVisibility(View.GONE);
        }
        findViewById(R.id.btn_test).setOnClickListener(this);

        ((TextView)findViewById(R.id.tv_version)).setText("app version : " + Utils.getVersionName(mContext));
    }

    @Override
    public void onBackPressed() {
        if (_listener != null) {
            _listener.onConfirmPress(Const.DEBUGING_SERVER_NONE);
        }
        dismiss();
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_op: {
                if (_listener != null) {
                    _listener.onConfirmPress(Const.DEBUGING_SERVER_OPERATION);
                }
                dismiss();
                break;
            }

            case R.id.btn_dev: {
                if (_listener != null) {
                    _listener.onConfirmPress(Const.DEBUGING_SERVER_DEV);
                }
                dismiss();
                break;
            }


            case R.id.btn_test: {
                if (_listener != null) {
                    _listener.onConfirmPress(Const.DEBUGING_SERVER_TEST);
                }
                dismiss();
                break;
            }

            default:
                break;
        }
    }

    public void setOnCofirmListener(OnConfirmListener listener) {
        _listener = listener;
    }

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
    }

    // 다이얼로그 바깥 영역 터치 시 종료되지 않도록 처리
    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            return false;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }
}
