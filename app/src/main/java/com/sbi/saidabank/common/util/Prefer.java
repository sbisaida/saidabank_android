package com.sbi.saidabank.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Prefer {
    /*
     * Intro Permission Dialog출력여부
     * */
    static public boolean getPermissionGuideShowStatus(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean status  = prefs.getBoolean("pref_permission_show_status", false);
        return status;
    }

    static public void setPermissionGuideShowStatus(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_permission_show_status",val);
        editor.apply();
    }

    /*
    * 생체등록 사용자 아이디 저장
    * 센스톤 지문,패턴,핀 로그인시 사용되는 사용자 아이디.
    * 휴대폰 본인인증하면 전달 받는다.
    * 32자리
    * Connecting Information
    * */
    static public String getAuthPhoneCI(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_auth_phone_ci", "");
    }

    static public void setAuthPhoneCI(Context context, String ci){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_auth_phone_ci",ci);
        editor.apply();
    }

    /*
     * 휴대폰 본인인증하면 전달 받는다.
     * Duplication Information
     * */
    static public String getAuthPhoneDI(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_auth_phone_di", "DI0000032Len");
    }

    static public void setAuthPhoneDI(Context context, String ci){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_auth_phone_di",ci);
        editor.apply();
    }

    /*
     For Test.추후 삭제 요망
     */
    static public String getSolutionWebAddress(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_solution_web_address", "https://devapp.saidabank.co.kr/index.jsp");
    }

    static public void setSolutionWebAddress(Context context, String url){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_solution_web_address",url);
        editor.apply();
    }

    /**
     * 지문 등록 상태 체크
     */
    static public boolean getFidoRegStatus(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean status  = prefs.getBoolean("pref_fido_reg_status", false);
        return status;
    }

    static public void setFidoRegStatus(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_fido_reg_status",val);
        editor.apply();
    }

    /**
     * 패턴 등록 상태 체크
     */
    static public boolean getPatternRegStatus(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean status  = prefs.getBoolean("pref_pattern_reg_status", false);
        return status;
    }

    static public void setPatternRegStatus(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_pattern_reg_status",val);
        editor.apply();
    }

    /**
     * 핀코드 등록 상태 체크
     */
    static public boolean getPincodeRegStatus(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean status  = prefs.getBoolean("pref_pincode_reg_status", false);
        return status;
    }

    static public void setPincodeRegStatus(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_pincode_reg_status",val);
        editor.apply();
    }

    /**
     * 서버 버전
     */
    static public String getServerAppVersion(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_server_app_version", "0.0.0");
    }

    static public void setServerAppVersion(Context context, String version){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_server_app_version",version);
        editor.apply();
    }

    /*
     푸시 관련
     */
    static public String getPushGCMToken(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_push_gcm_token", "");
    }

    static public void setPushGCMToken(Context context, String str){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_push_gcm_token",str);
        editor.apply();
    }

    static public String getPushRegID(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_push_regid", "");
    }

    static public void setPushRegID(Context context, String str){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_push_regid",str);
        editor.apply();
    }


    /*
    * 푸시 알림 다이얼로그 여부 저장
    * */
    static public boolean getFlagShowFirstNotiMsgBox(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("pref_first_show_noti_msgbox", false);
    }

    static public void setFlagShowFirstNotiMsgBox(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_first_show_noti_msgbox",val);
        editor.apply();
    }

    /*
     * 디버깅 모드 체크 다이얼로그 보이기 여부 저장
     * */
    static public boolean getFlagDontShowDevModeCheckDialog(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("pref_show_devmode_dialog", false);
    }

    static public void setFlagDontShowDevModeCheckDialog(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_show_devmode_dialog",val);
        editor.apply();
    }

    /*
     * 지문정보 변경 여부 저장
     * */
    static public boolean getFlagFingerPrintChange(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("pref_fingerprint_chagne", false);
    }

    static public void setFlagFingerPrintChange(Context context, boolean val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_fingerprint_chagne",val);
        editor.apply();
    }

    /*
     * cdd/edd 고객정보 재확인 일주일간 열지않기 선택 여부
     * */
    static public String getCDDEDDNotOpenFor1Week(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_open_cddedd_for_1week", "");
    }

    static public void setCDDEDDNotOpenFor1Week(Context context, String val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_open_cddedd_for_1week",val);
        editor.apply();
    }

    /*
     * 패턴 오류 횟수 저장/조회
     * */
    static public int getPatternErrorCount(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("pref_pattern_error_count", 0);
    }

    static public void setPatternErrorCount(Context context, int val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("pref_pattern_error_count",val);
        editor.apply();
    }

    /*
     * 핀 오류 횟수 저장/조회
     * */
    static public int getPinErrorCount(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("pref_pin_error_count", 0);
    }

    static public void setPinErrorCount(Context context, int val){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("pref_pin_error_count",val);
        editor.apply();
    }

    /*
     * 입력패턴 숨기기 여부
     */
    static public boolean getUsePatternStealth(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("pref_use_pattern_stealth", false);
    }

    static public void setUsePatternStealth(Context context, boolean val) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_use_pattern_stealth", val);
        editor.apply();
    }

    /**
     * Tutorial 상태 체크
     */
    static public boolean getTutorialStatus(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean status  = prefs.getBoolean("pref_tutorial_status", false);
        return status;
    }

    static public void setTutorialStatus(Context context, boolean val) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_tutorial_status",val);
        editor.apply();
    }

    /*
     * 저장된 인증관련 프리퍼런스를 삭제. 회원 탈퇴 시에만 호출
     */
    static public void clearAuthSharedPreferences(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("pref_auth_phone_ci").commit();
        editor.remove("pref_fido_reg_status").commit();
        editor.remove("pref_pattern_reg_status").commit();
        editor.remove("pref_pincode_reg_status").commit();
        editor.remove("pref_fingerprint_chagne").commit();
        editor.remove("pref_open_cddedd_for_1week").commit();
        editor.remove("pref_pattern_error_count").commit();
        editor.remove("pref_use_pattern_stealth").commit();
    }
}
