package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.MultiTransferListAdapter;
import com.sbi.saidabank.activity.transaction.TransferManager;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.VerifyTransferInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Saidabank_android
 * Class: SlidingConfirmSingleTransferDialog
 * Created by 950485 on 2018. 11. 29..
 * <p>
 * Description:아래에서 위로 올라오는 다건이체 확인 다이얼로그
 */

public class SlidingConfirmMultiTransferDialog extends Dialog {
    private Context   context;

    private TextView  textMsgTransferNum;
    private TextView  textMsgAmount;
    private TextView  textMsgAccount;

    private String           withdrawAccount;
    private FinishListener   finishListener;

    private Double          dLimitOneDay = -1.0;
    private boolean         isLastRemove;   // 마지막 리스트 삭제 여부 - '아니오' 선택 시, 마지막 항목 삭제하면 안됨

    private MultiTransferListAdapter multiTransferListAdapter;

    public SlidingConfirmMultiTransferDialog(@NonNull Context context, String withdrawAccount, FinishListener finishListener) {
        //super(context);
        super(context, R.style.SlideDialog);
        this.context = context;
        this.withdrawAccount = withdrawAccount;
        this.finishListener = finishListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        setContentView(R.layout.dialog_confirm_multi_transfer);

        initUX();
    }

    private void initUX() {
        textMsgTransferNum = (TextView) findViewById(R.id.textview_transfer_num_multi);
        textMsgAmount = (TextView) findViewById(R.id.textview_transfer_amount_multi);
        textMsgAccount = (TextView) findViewById(R.id.textview_withdraw_account_multi);

        updateTransferCount();

        RecyclerView recyclerViewReceipt = (RecyclerView) findViewById(R.id.recyclerview_receiptInfo);
        if (TransferManager.getInstance().size() > 0) {
            if (multiTransferListAdapter == null) {
                multiTransferListAdapter = new MultiTransferListAdapter(context,
                         new MultiTransferListAdapter.RemoveListener() {
                    @Override
                    public void OnRemoveListener(int position) {
                        showRemoveMessage(position);
                    }
                });
                recyclerViewReceipt.setAdapter(multiTransferListAdapter);

                LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                recyclerViewReceipt.setLayoutManager(llm);
                recyclerViewReceipt.setHasFixedSize(true);
            }
        }

        Button btnAddTransfer = (Button) findViewById(R.id.btn_add_confirm_multi_transfer);
        btnAddTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                if (multiTransferListAdapter != null) {
                    int size = multiTransferListAdapter.getItemCount();
                    if (size >= Const.MAX_TRANSFER_AT_ONCE) {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        };

                        final AlertDialog alertDialog = new AlertDialog(context);
                        alertDialog.mPListener = okClick;
                        alertDialog.msg = String.format(context.getString(R.string.msg_max_transfer_at_once));
                        alertDialog.show();
                        return;
                    }
                }

                finishListener.OnAddTransfer();
            }
        });

        Button btnCancel = (Button) findViewById(R.id.btn_cancel_confirm_multi_transfer);
        btnCancel.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnCancelListener(isLastRemove, dLimitOneDay);
                dismiss();
            }
        });

        Button btnOK = (Button) findViewById(R.id.btn_ok_confirm_multi_transfer);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                double dAmount = TransferManager.getInstance().getTotalTransferAmount();
                if (dAmount >= Const.SHOW_VOICE_PHISHING_AMOUNT) {
                    showAdvanceVoicePhishing();
                } else {
                    finishListener.OnOKListener(isLastRemove);
                }
                dismiss();
            }
        });
    }

    private void updateTransferCount() {
        int totalSize = TransferManager.getInstance().size();
        if (totalSize == 0) {
            finishListener.OnRemoveAllListener();
            dismiss();
            return;
        }

        textMsgTransferNum.setText(String.valueOf(totalSize));
        textMsgAccount.setText(withdrawAccount);

        Double totalAmount = TransferManager.getInstance().getTotalTransferAmount();
        String msgAmount = String.format(context.getString(R.string.msg_ask_comfirm_transfer), Utils.moneyFormatToWon(totalAmount));
        textMsgAmount.setText(msgAmount);
    }

    /**
     * 다건이체 삭제 메세지 출력
     */
    private void showRemoveMessage(final int position) {
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestRemoveTransfer(position);
            }
        };

        VerifyTransferInfo transferInfo = TransferManager.getInstance().get(position);
        String name = transferInfo.getRECV_NM();
        String msg = String.format(context.getString(R.string.msg_remove_transfer), name);
        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.mNListener = cancelClick;
        alertDialog.mPListener = okClick;
        alertDialog.msg = msg;
        alertDialog.show();
    }

    public void requestRemoveTransfer(final int position) {
        VerifyTransferInfo transferInfo = TransferManager.getInstance().get(position);
        if (transferInfo == null)
            return;

        String UUID_NO = transferInfo.getUUID_NO();
        if (TextUtils.isEmpty(UUID_NO)) {
            showErrorMessage(context.getString(R.string.msg_debug_err_response));
            return;
        }

        Map param = new HashMap();
        param.put("UUID_NO", UUID_NO);
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0010400A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    String msg = context.getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        String msg = context.getString(R.string.msg_debug_err_response);
                        showErrorMessage(msg);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(context.getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = context.getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        ((BaseActivity) context).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    if (position == TransferManager.getInstance().size() - 1)
                        isLastRemove = true;

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        dLimitOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                        finishListener.OnChangeLimit(dLimitOneDay);
                    }

                    TransferManager.getInstance().clear();
                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        VerifyTransferInfo item = new VerifyTransferInfo();

                        String CNTP_FIN_INST_CD = jsonObject.optString("CNTP_FIN_INST_CD");
                        if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                            CNTP_FIN_INST_CD = "028";
                        }
                        item.setCNTP_FIN_INST_CD(CNTP_FIN_INST_CD);

                        String CNTP_BANK_ACNO= jsonObject.optString("CNTP_BANK_ACNO");
                        item.setCNTP_BANK_ACNO(CNTP_BANK_ACNO);

                        String CNTP_ACCO_DEPR_NM = jsonObject.optString("CNTP_ACCO_DEPR_NM");
                        item.setRECV_NM(CNTP_ACCO_DEPR_NM);

                        String DEPO_BNKB_MRK_NM = jsonObject.optString("DEPO_BNKB_MRK_NM");
                        item.setDEPO_BNKB_MRK_NM(DEPO_BNKB_MRK_NM);

                        String TRAN_BNKB_MRK_NM = jsonObject.optString("TRAN_BNKB_MRK_NM");
                        item.setTRAN_BNKB_MRK_NM(TRAN_BNKB_MRK_NM);

                        String TRN_AMT = jsonObject.optString("TRN_AMT");
                        item.setTRN_AMT(TRN_AMT);

                        String FEE = jsonObject.optString("FEE");
                        item.setFEE(FEE);

                        String TRNF_DMND_DT = getJsonString(jsonObject, "TRNF_DMND_DT");
                        if (!TextUtils.isEmpty(TRNF_DMND_DT)) {
                            item.setTRNF_DMND_DT(TRNF_DMND_DT);
                        } else {
                            item.setTRNF_DMND_DT("");
                        }

                        String TRNF_DMND_TM = getJsonString(jsonObject, "TRNF_DMND_TM");
                        if (!TextUtils.isEmpty(TRNF_DMND_TM)) {
                            item.setTRNF_DMND_TM(TRNF_DMND_TM);
                        } else {
                            item.setTRNF_DMND_TM("");
                        }

                        String TRN_MEMO_CNTN = getJsonString(jsonObject, "TRN_MEMO_CNTN");
                        if (!TextUtils.isEmpty(TRN_MEMO_CNTN)) {
                            item.setTRN_MEMO_CNTN(TRN_MEMO_CNTN);
                        }

                        String TRNF_DVCD = jsonObject.optString("TRNF_DVCD");
                        item.setTRNF_DVCD(TRNF_DVCD);

                        String MNRC_BANK_NM = jsonObject.optString("MNRC_BANK_NM");
                        item.setMNRC_BANK_NM(MNRC_BANK_NM);

                        String BANK_NM = jsonObject.optString("BANK_NM");
                        item.setBANK_NM(BANK_NM);

                        String UUID_NO = jsonObject.optString("UUID_NO");
                        item.setUUID_NO(UUID_NO);

                        TransferManager.getInstance().add(index, item);
                    }

                    if (multiTransferListAdapter != null) {
                        multiTransferListAdapter.notifyDataSetChanged();
                    }
                    updateTransferCount();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    public void showErrorMessage(String msg) {
        msg = msg.replaceAll("\\\\n", "\n");

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        final AlertDialog alertDialog = new AlertDialog(context);
        alertDialog.mPListener = okClick;
        alertDialog.msg = msg;
        alertDialog.show();
    }

    /**
     * // key가 존재하는지 체크한다.
     * @param json
     * @param key
     * @return
     */
    public String getJsonString(JSONObject json, String key){
        String outValue = null;

        if (json.has(key)) {
            try {
                outValue = json.getString(key);
            } catch (JSONException e) {
                //e.printStackTrace();
            }
        }

        return outValue;
    }

    /**
     * 보이스피싱 확인화면
     */
    private void showAdvanceVoicePhishing() {
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishListener.OnOKListener(isLastRemove);
                dismiss();
            }
        };

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishListener.OnShowVoicePhishing();
                dismiss();
            }
        };

        SlidingVoicePhishingDialog dialog = new SlidingVoicePhishingDialog(context, cancelClick, okClick);
        dialog.setCancelable(false);
        dialog.show();
    }

    public interface FinishListener {
        void OnCancelListener(boolean isLastRemove, Double dLimitOneDay);
        void OnOKListener(boolean isLastRemove);
        void OnRemoveAllListener();
        void OnAddTransfer();
        void OnChangeLimit(double dLimitOneDay);
        void OnShowVoicePhishing();
    }
}
