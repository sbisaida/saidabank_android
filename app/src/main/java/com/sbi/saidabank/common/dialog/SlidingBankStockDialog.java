package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.datatype.RequestCodeInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: SlidingBankStockDialog
 * Created by 950485 on 2018. 11. 14..
 * <p>
 * Description:아래에서 위로 올라오는 은행/증권회사 다이얼로그
 */

public class SlidingBankStockDialog extends SlidingBaseDialog {
    private TextView         mTextTabBank;
    private ListView         mListBank;
    private TextView         mTextTabStock;
    private ListView         mListStock;

    private Context      context;
    private String       packageName;

    private ArrayList<RequestCodeInfo> listBank;
    private ArrayList<RequestCodeInfo> listStock;
    private BankStockSelectListener  bankStockSelectListener;

    public SlidingBankStockDialog(@NonNull Context context,
                                  ArrayList<RequestCodeInfo> listBank,
                                  ArrayList<RequestCodeInfo> listStock,
                                  BankStockSelectListener bankClickListener) {
        super(context);
        this.context = context;
        this.listBank = listBank;
        this.listStock = listStock;
        this.bankStockSelectListener = bankClickListener;

        packageName = context.getPackageName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_bank_stock);
        setDialogWidth();

        initUX();
    }

    private void initUX() {
        SlidingBankStockDialog.BankAdapter bankAdapter = new SlidingBankStockDialog.BankAdapter();
        mListBank = (ListView) findViewById(R.id.listview_bank);
        mListBank.setAdapter(bankAdapter);

        SlidingBankStockDialog.StockAdapter stockAdapter = new SlidingBankStockDialog.StockAdapter();
        mListStock = (ListView) findViewById(R.id.listview_stock);
        mListStock.setAdapter(stockAdapter);

        mTextTabBank = (TextView) findViewById(R.id.textview_tab_bank);
        mTextTabStock = (TextView) findViewById(R.id.textview_tab_stock);

        mTextTabBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mListBank.isShown()) {
                    mListBank.setVisibility(View.VISIBLE);
                    mTextTabBank.setBackgroundResource(R.drawable.background_tab_select_box);
                    mTextTabBank.setTextColor(context.getResources().getColor(R.color.black));
                }

                if (mListStock.isShown()) {
                    mListStock.setVisibility(View.GONE);
                    mTextTabStock.setBackgroundResource(R.drawable.background_tab_unselect_box);
                    mTextTabStock.setTextColor(context.getResources().getColor(R.color.color666666));
                }
            }
        });

        mTextTabStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mListStock.isShown()) {
                    mListStock.setVisibility(View.VISIBLE);
                    mTextTabStock.setBackgroundResource(R.drawable.background_tab_select_box);
                    mTextTabStock.setTextColor(context.getResources().getColor(R.color.black));
                }

                if (mListBank.isShown()) {
                    mListBank.setVisibility(View.GONE);
                    mTextTabBank.setBackgroundResource(R.drawable.background_tab_unselect_box);
                    mTextTabBank.setTextColor(context.getResources().getColor(R.color.color666666));
                }
            }
        });
    }

    class BankAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return listBank.size();
        }

        @Override
        public Object getItem(int position) {
            return listBank.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final BankViewHolder viewBankHolder;

            if (convertView == null) {
                viewBankHolder = new BankAdapter.BankViewHolder();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.dialog_bankstock_list_item, null);
                viewBankHolder.layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_bankstock_item);
                viewBankHolder.imageBank = (ImageView) convertView.findViewById(R.id.imageview_bankstock);
                viewBankHolder.textBank = (TextView) convertView.findViewById(R.id.textview_bankstock);
                convertView.setTag(viewBankHolder);
            } else {
                viewBankHolder = (BankViewHolder) convertView.getTag();
            }

            final RequestCodeInfo bankInfo = listBank.get(position);
            final String name = bankInfo.getCD_NM();
            final String code = bankInfo.getSCCD();

            if (bankInfo != null) {
                if (!TextUtils.isEmpty(name))
                    viewBankHolder.textBank.setText(name);

                int resourceID = getImageBank(code);
                if (resourceID > 0)
                    viewBankHolder.imageBank.setImageResource(resourceID);
            }

            viewBankHolder.layoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (context instanceof Activity && ((Activity) context).isFinishing())
                        return;

                    bankStockSelectListener.onSelectListener(name, code);
                    dismiss();
                }
            });

            return convertView;
        }

        class BankViewHolder {
            LinearLayout layoutItem;
            ImageView    imageBank;
            TextView     textBank;
        }
    }

    class StockAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return listStock.size();
        }

        @Override
        public Object getItem(int position) {
            return listStock.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final StockViewHolder viewStockHolder;

            if (convertView == null) {
                viewStockHolder = new StockViewHolder();

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.dialog_bankstock_list_item, null);
                viewStockHolder.layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_bankstock_item);
                viewStockHolder.imageStock = (ImageView) convertView.findViewById(R.id.imageview_bankstock);
                viewStockHolder.textStock = (TextView) convertView.findViewById(R.id.textview_bankstock);
                convertView.setTag(viewStockHolder);
            } else {
                viewStockHolder = (StockViewHolder) convertView.getTag();
            }

            final RequestCodeInfo stockInfo = listStock.get(position);
            final String name = stockInfo.getCD_NM();
            final String code = stockInfo.getSCCD();

            if (stockInfo != null) {
                if (!TextUtils.isEmpty(name))
                    viewStockHolder.textStock.setText(name);

                int resourceID = getImageBank(code);
                if (resourceID > 0)
                    viewStockHolder.imageStock.setImageResource(resourceID);
            }

            viewStockHolder.layoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bankStockSelectListener.onSelectListener(name, code);
                    dismiss();
                }
            });

            return convertView;
        }

        class StockViewHolder {
            LinearLayout layoutItem;
            ImageView    imageStock;
            TextView     textStock;
        }
    }

    private int getImageBank(String code) {
        int resourceID = -1;

        String resourcename = "img_logo_" + code;
        if ("028".equalsIgnoreCase(code))
            resourcename = "img_logo_000";

        resourceID = context.getResources().getIdentifier(resourcename, "drawable", packageName);

        return resourceID;
    }

    public interface BankStockSelectListener {
        void onSelectListener(String bank, String code);
    }
}
