package com.sbi.saidabank.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {
    /*
     * JSON 형태의 스트링을 hash map으로 변경
     *
     * @param jsonString 스트링
     * @return hash map
     **/
    public static LinkedHashMap<String, Object> jsonStringToMap(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);

        return jsonToMap(jsonObject);
    }

    /*
     * JSON object를 hash map으로 변경
     *
     * @param json JSON object
     * @return hash map
     **/
    public static LinkedHashMap<String, Object> jsonToMap(JSONObject json) throws JSONException {
        LinkedHashMap<String, Object> retMap = new LinkedHashMap<>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    /*
     * JSON object를 hash map으로 변경
     *
     * @param json JSON object
     * @return hash map
     **/
    public static LinkedHashMap<String, Object> toMap(JSONObject object) throws JSONException {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    /*
     * JSON array를 list로 변경
     *
     * @param array JSON array
     * @return list
     **/
    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    /*
     * hash map에서 특정 키값의 스트링 추출
     *
     * @param hm hash map
     * @param key 키값
     * @return 스트링
     **/
    public static String getHashValueString(HashMap<String, Object> hm, String key) {
        for (String keys :  hm.keySet()) {
            Object value = hm.get(key);
            if (value instanceof String) {
                if (hm.containsKey(key))
                    return (String) hm.get(key);
            }
        }
        return "";
    }

    /*
     * hash map에서 특정 키값의 boolean 추출
     *
     * @param hm hash map
     * @param key 키값
     * @return boolean
     **/
    public static Boolean getHashValueBoolean(HashMap<String, Object> hm, String key) {
        for (String keys :  hm.keySet()) {
            Object value = hm.get(key);
            if (value instanceof Boolean) {
                if(hm.containsKey(key))
                    return (Boolean) hm.get(key);
            }
        }
        return false;
    }

    /*
     * hash map에서 특정 키값의 integer 추출
     *
     * @param hm hash map
     * @param key 키값
     * @return integer
     **/
    public static Integer getHashValueInteger(HashMap<String, Object> hm, String key) {
        for (String keys :  hm.keySet()) {
            Object value = hm.get(key);
            if (value instanceof Integer) {
                if (hm.containsKey(key))
                    return (Integer) hm.get(key);
            }
        }
        return 0;
    }

    /*
     * hash map에서 특정 키값의 array list 추출
     *
     * @param hm hash map
     * @param key 키값
     * @return array list
     **/
    public static ArrayList getHashValueArrayList(HashMap<String, Object> hm, String key) {
        for (String keys :  hm.keySet()) {
            Object value = hm.get(key);
            if (value instanceof ArrayList) {
                if (hm.containsKey(key))
                    return (ArrayList) hm.get(key);
            }
        }
        return null;
    }

    /*
     * hash map에서 특정 키값의 hash map 추출
     *
     * @param hm hash map
     * @param key 키값
     * @return hash map
     **/
    public static LinkedHashMap getHashValueLinkedHashMap(HashMap<String, Object> hm, String key) {
        for (String keys :  hm.keySet()) {
            Object value = hm.get(key);
            if (value instanceof HashMap) {
                if (hm.containsKey(key))
                    return (LinkedHashMap) hm.get(key);
            }
        }
        return null;
    }

    /**
     * Hashmap을 Json문자열로 변환한다.
     *
     * @param map
     * @return
     */
    public static JSONObject mapToJson(Map<String, Object> map) {
        JSONObject jsonObj = new JSONObject();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            try {
                jsonObj.put(entry.getKey(), entry.getValue());
            } catch (JSONException e) {
                Logs.printException(e);
            }
        }

        return jsonObj;
    }


}