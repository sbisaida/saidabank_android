package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

/**
 * Saidabank_android
 * Class: ShotIDFailDialog
 * Created by 950546 on 2019. 04. 10..
 * <p>
 * Description: 신분증 촬영 실패 다이얼로그
 */
public class ShotIDFailDialog extends BaseDialog implements View.OnClickListener {

    private Context mContext;
    private int     mIdFailType;
    private ShotIDFailDialog.OnConfirmListener _listener;


    public ShotIDFailDialog(@NonNull Context context, int idFailType) {
        super(context);
        mContext = context;
        mIdFailType = idFailType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mIdFailType == Const.ID_SHOT_FAIL_SHOT)
            setContentView(R.layout.dialog_id_shot_fail);
        else
            setContentView(R.layout.dialog_id_check_fail);

        setDialogWidth();

        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_confirm: {
                if (_listener != null) {
                    _listener.onConfirmPress();
                }
                dismiss();
                break;
            }

            default:
                break;
        }
    }

    public void setOnCofirmListener(OnConfirmListener listener) {
        _listener = listener;
    }

    public interface OnConfirmListener {
        void onConfirmPress();
    }
}
