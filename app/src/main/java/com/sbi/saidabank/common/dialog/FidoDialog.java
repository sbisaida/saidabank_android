package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;

public class FidoDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;

    private LinearLayout mCautionMsg;

    private boolean mIsCancelBtn;

    public View.OnClickListener mPListener;
    public Dialog.OnDismissListener mDismissListener;
    public Dialog.OnDismissListener mBackKeyListener;

    public FidoDialog(Context context, boolean isCancelBtn) {
        super(context);
        mContext = context;
        mIsCancelBtn = isCancelBtn;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_fido);

        Button btnPositive = (Button)findViewById(R.id.btn_finger_ok);
        Button btnNegative = (Button)findViewById(R.id.btn_finger_cancel);

        if (mIsCancelBtn) {
            btnNegative.setVisibility(View.VISIBLE);
            btnPositive.setVisibility(View.GONE);
            btnNegative.setOnClickListener(this);
        } else {
            btnNegative.setVisibility(View.GONE);
            btnPositive.setVisibility(View.VISIBLE);
            btnPositive.setOnClickListener(this);
        }

        setDialogWidth();

        //setCanceledOnTouchOutside(false);

        mCautionMsg = findViewById(R.id.layout_fingerprint_msg);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_finger_ok: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    dismiss();
                }
                if (mPListener != null) {
                    mPListener.onClick(v);
                }
                break;
            }

            case R.id.btn_finger_cancel: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    dismiss();
                }
                if (mPListener != null) {
                    mPListener.onClick(v);
                }
                break;
            }

            default:
                break;
        }
    }

    public void showFidoCautionMsg(boolean show){
        if(show){
            mCautionMsg.setVisibility(View.VISIBLE);
        }else{
            mCautionMsg.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(mBackKeyListener != null) {
            mBackKeyListener.onDismiss(this);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if(mDismissListener != null) {
            mDismissListener.onDismiss(this);
        }
    }
}
