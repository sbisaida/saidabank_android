package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

/**
 * Saidabank_android
 * Class: SlidingConfirmSingleTransferDialog
 * Created by 950485 on 2019. 04. 17..
 * <p>
 * Description:이체 방법 선택 다이얼로그
 */

public class SelectTransferDialog extends BaseDialog {

    private Context            context;
    private OnConfirmListener  listener;

    public SelectTransferDialog(@NonNull Context context, OnConfirmListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_transfer);
        setDialogWidth();
        setCancelable(true);

        RelativeLayout btnAccount = (RelativeLayout) findViewById(R.id.layout_transfer_account);
        btnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onConfirmPress(1);
            }
        });

        RelativeLayout btnPhone = (RelativeLayout) findViewById(R.id.layout_transfer_phone);
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onConfirmPress(2);
            }
        });
    }

    public void setOnCofirmListener(SelectDevDialog.OnConfirmListener listener) {
        listener = listener;
    }

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
    }

}
