package com.sbi.saidabank.common;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;

public class PhoneStatReceiver extends BroadcastReceiver {
    private static boolean incomingFlag = false;
    private static boolean callingFlag = false;
    private static String   incomingNumber = null;

    @Override

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            incomingFlag = false;
            callingFlag = false;
            Const.isIncomingARS = false;
            //String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        } else {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Service.TELEPHONY_SERVICE);
            switch (tm.getCallState()) {
                // 통화벨 울리는중
                case TelephonyManager.CALL_STATE_RINGING :
                    incomingFlag = true;
                    incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    Logs.i("RINGING :"+ incomingNumber);
                    break;

                // 통화중
                case TelephonyManager.CALL_STATE_OFFHOOK :
                    if (incomingFlag) {
                        String verifyNumber = context.getResources().getString(R.string.common_ars_phone);
                        if (verifyNumber.contains(incomingNumber)) {
                            callingFlag = true;
                        }

                        Logs.i("incoming accept :" + incomingNumber);
                    }
                    break;

                // 통화종료 혹은 통화벨 종료
                case TelephonyManager.CALL_STATE_IDLE :
                    if (incomingFlag && callingFlag) {
                        Const.isIncomingARS = true;
                        Logs.i("incoming finish");
                    }
                    incomingFlag = false;
                    callingFlag = false;
                    incomingNumber = null;
                    break;

                default:
                    break;
            }
        }
    }
}