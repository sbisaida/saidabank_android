package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: PermissionDialog
 * Created by 950546
 * Date: 2018-10-17
 * Time: 오후 2:41
 * Description: 접근권한 팝업
 */
public class PermissionDialog extends Dialog implements View.OnClickListener{

    private Button mBtPositive;
    private Context mcontext;

    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;

    public PermissionDialog(Context context) {
        //super(context);
        super(context, R.style.AppThemeNoActionBar);
        mcontext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        setContentView(R.layout.dialog_permission);

        mBtPositive = (Button)findViewById(R.id.btn_confirm);
        mBtPositive.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_confirm:
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                dismiss();
                break;

            default:
                break;
        }
    }
}
