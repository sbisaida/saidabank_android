package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class ErrorDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;

    //public String title;
    public String msg;

    public String mPBtText = "";

    public View.OnClickListener mPListener;

    public ErrorDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_error);

        setDialogWidth();

        initView();
    }

    private void initView(){
        setCancelable(false);
/*
        if(title == null || "".equals(title)){
            title = mContext.getString(R.string.common_notice);
        }
*/
        if(msg == null || "".equals(msg)){
            msg = mContext.getString(R.string.common_no_msg);
        }
        //((TextView)findViewById(R.id.tv_title)).setText(title);
        ((TextView)findViewById(R.id.tv_msg)).setText(msg);



        Button mBtPositive = (Button)findViewById(R.id.bt_positive);
        mBtPositive.setOnClickListener(this);


        if(mPBtText != null && !"".equals(mPBtText)){
            mBtPositive.setText(mPBtText);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_positive:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                captureScreen();
                break;

            default:
                break;
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();


    }

    /**
     * 에러 다이얼로그가 호출되면 화면을 캡쳐하여 WAS로 전달한다.
     */
    private void captureScreen(){
        String mPath = Environment.getExternalStorageDirectory().toString() + "/" + Utils.getCurrentDate("") + ".jpg";

        //배경 윈도우의 비트맵을 가져온다.
        View activityView = ((Activity)mContext).getWindow().getDecorView().getRootView();
        activityView.setDrawingCacheEnabled(true);
        Bitmap activityBmp = Bitmap.createBitmap(activityView.getDrawingCache());
        activityView.setDrawingCacheEnabled(false);

        //다이얼로그의 비트앱을 가져온다.
        View dialogView = getWindow().getDecorView().getRootView();
        dialogView.setDrawingCacheEnabled(true);
        Bitmap dialogBmp = Bitmap.createBitmap(dialogView.getDrawingCache());
        dialogView.setDrawingCacheEnabled(false);

        if(activityBmp != null && dialogBmp != null){
            //두 비트맵을 합친다.
            Bitmap combineBmp = ImgUtils.combineBitmap(activityBmp,dialogBmp);

            //이미지 파일 생성
            File imageFile = new File(mPath);
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(imageFile);

                combineBmp.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (FileNotFoundException e) {
                Logs.printException(e);
            } catch (IOException e) {
                Logs.printException(e);
            }
            combineBmp.recycle();
        }

        if(activityBmp !=null)
            activityBmp.recycle();

        if(dialogBmp !=null)
            dialogBmp.recycle();

    }
}
