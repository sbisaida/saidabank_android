package com.sbi.saidabank.common.download;

public interface DownloadCallback {
    void onDownloadComplete(String pathName, String fileName);
}
