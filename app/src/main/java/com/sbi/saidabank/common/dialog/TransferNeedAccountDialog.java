package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;


public class TransferNeedAccountDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;

    public TransferNeedAccountDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_transfer_need_account);

        setDialogWidth();

        initView();
    }

    private void initView() {
        setCancelable(false);

        Button mBtPositive = (Button) findViewById(R.id.btn_confirm);
        mBtPositive.setOnClickListener(this);

        LinearLayout llMakeAccount = (LinearLayout) findViewById(R.id.ll_makeaccount);
        llMakeAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    dismiss();
                }
                break;
            }

            case R.id.ll_makeaccount: {
                dismiss();
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
}
