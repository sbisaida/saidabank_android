package com.sbi.saidabank.common.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.sbi.saidabank.R;

public class TutorialAdapter extends PagerAdapter {
    private Context    context;

    private final int[] drawableImgs = new int[] {
            R.drawable.img_tutorial_01,
            R.drawable.img_tutorial_02,
            R.drawable.img_tutorial_03,
            R.drawable.img_tutorial_04,
            R.drawable.img_tutorial_05
    };

    private final int[] drawableImgs800 = new int[] {
            R.drawable.img_tutorial_01_800,
            R.drawable.img_tutorial_02_800,
            R.drawable.img_tutorial_03_800,
            R.drawable.img_tutorial_04_800,
            R.drawable.img_tutorial_05_800
    };

    public TutorialAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return drawableImgs.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.tutorial_item, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageview_tutorial);

        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        float density  = context.getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        float rate = dpHeight / dpWidth;
        if (rate < 1.70) {
            Bitmap drawImg = BitmapFactory.decodeResource(context.getResources(), drawableImgs800[position]);
            imageView.setImageBitmap(drawImg);
        } else {
            Bitmap drawImg = BitmapFactory.decodeResource(context.getResources(), drawableImgs[position]);
            imageView.setImageBitmap(drawImg);
        }

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
