package com.sbi.saidabank.common.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

import com.sbi.saidabank.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Saidabank_android
 *
 * Class: ImgUtils
 * Created by 950469 on 2018. 8. 28..
 * <p>
 * Description:
 * Bitmap작업 관련 함수들의 모음.
 */
public class ImgUtils {
    public static Bitmap byteArrayToBitmap(byte[] mbyteArray ) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;

        Bitmap bitmap = BitmapFactory.decodeByteArray( mbyteArray, 0, mbyteArray.length, options) ;
        return bitmap ;
    }

    public static byte[] bitmapToByteArray( Bitmap mbitmap ) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
        mbitmap.compress( Bitmap.CompressFormat.JPEG, 70, stream) ;
        byte[] byteArray = stream.toByteArray() ;
        return byteArray ;
    }

    public static Bitmap cropBitmap( Bitmap mbitmap, Rect mrect) {
        Bitmap cropImage = Bitmap.createBitmap(mbitmap, mrect.left, mrect.top, mrect.width(), mrect.height());

        return cropImage;
    }

    public static Bitmap combineBitmap(Bitmap original,Bitmap overlay){

        //결과값 저장을 위한 Bitmap
        Bitmap resultOverlayBmp = Bitmap.createBitmap(original.getWidth()
                , original.getHeight()
                , original.getConfig());

        //캔버스를 통해 비트맵을 겹치기한다.
        Canvas canvas = new Canvas(resultOverlayBmp);
        canvas.drawBitmap(original, new Matrix(), null);

        int left = original.getWidth()/2 - overlay.getWidth()/2;
        int top  = original.getHeight()/2 - overlay.getHeight()/2;

        Rect overlayRect = new Rect(left, top, left+overlay.getWidth(), top + overlay.getHeight());


        canvas.drawBitmap(overlay,null,overlayRect,null);

        return resultOverlayBmp;

    }

    public static boolean captureSave(Activity activity,String fileName){
        View activityView = activity.getWindow().getDecorView().getRootView();
        activityView.setDrawingCacheEnabled(true);
        activityView.buildDrawingCache();
        Bitmap activityBmp = Bitmap.createBitmap(activityView.getDrawingCache());


        if(activityBmp != null){
            //이미지 파일 생성
            File imageFile = new File(Files.getOutputMediaPath(activity,activity.getString(R.string.app_eng_name)),fileName);
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(imageFile);

                activityBmp.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (FileNotFoundException e) {
                Logs.printException(e);
            } catch (IOException e) {
                Logs.printException(e);
            }
            activityView.destroyDrawingCache();
            activityView.setDrawingCacheEnabled(false);
            activityBmp.recycle();
            return true;
        }

        return false;

    }

    /**
     * 화면 캡쳐 후 byte로 리턴
     * @param view 캡쳐할 화면뷰
     * @return img bytearray
     */
    public static byte[] captureSave(View view) {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();

        if (viewBmp != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            viewBmp.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            return byteArrayOutputStream.toByteArray();
        }

        return null;
    }

    /**
     * 화면 캡쳐 후 로컬 저장
     * @param activity context
     * @param view 캡쳐할 화면뷰
     * @param fileName 저장할 파일이름
     * @return 저장 성공 여부
     */
    public static boolean captureSave(Activity activity,View view,String fileName){
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();

        if(viewBmp != null){
            //이미지 파일 생성
            File storageDir = Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_PICTURES);
            if (!storageDir.exists()) {
                storageDir.mkdir();
            }
            File imageFile = new File(storageDir, fileName);
            //File imageFile = new File(Files.getOutputMediaPath(activity,activity.getString(R.string.app_eng_name)),fileName);
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(imageFile);

                viewBmp.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (FileNotFoundException e) {
                Logs.printException(e);
            } catch (IOException e) {
                Logs.printException(e);
            }
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            Uri uri = Uri.fromFile(imageFile);
            activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
            return true;
        }

        return false;

    }
}
