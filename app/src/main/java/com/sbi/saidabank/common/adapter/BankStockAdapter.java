package com.sbi.saidabank.common.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.RequestCodeInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: BankStockAdapter
 * Created by 950485 on 2019. 01. 02..
 * <p>
 * Description:아래에서 위로 올라오는 은행/증권회사 다이얼로그 adapter
 */

public class BankStockAdapter extends BaseAdapter {
    private Context                  context;
    private ArrayList<RequestCodeInfo> listBank;
    private ArrayList<RequestCodeInfo> listStock;
    private String       packageName;

    private Const.BANK_STOCK_MODE bankStockMode;

    public BankStockAdapter(Context context, ArrayList<RequestCodeInfo> listBank, ArrayList<RequestCodeInfo> listStock) {
        this.context = context;
        this.listBank = listBank;
        this.listStock = listStock;

        bankStockMode = Const.BANK_STOCK_MODE.BANK_MODE;
        packageName = context.getPackageName();
    }

    @Override
    public int getCount() {
        if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE)
            return listBank.size() + 1;
        else
            return listStock.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE)
            return listBank.get(position - 1);
        else
            return listStock.get(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final BankStockAdapter.BankStockViewHolder viewBankHolder;

        if (convertView == null) {
            viewBankHolder = new BankStockAdapter.BankStockViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dialog_bankstock_list_item, null);
            viewBankHolder.layoutTab = (LinearLayout) convertView.findViewById(R.id.layout_tab_bankstock);
            viewBankHolder.textTabBank = (TextView) convertView.findViewById(R.id.textview_tab_bank);
            viewBankHolder.textTabStock = (TextView) convertView.findViewById(R.id.textview_tab_stock);
            viewBankHolder.layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_bankstock_item);
            viewBankHolder.imageBank = (ImageView) convertView.findViewById(R.id.imageview_bankstock);
            viewBankHolder.textBank = (TextView) convertView.findViewById(R.id.textview_bankstock);
            convertView.setTag(viewBankHolder);
        } else {
            viewBankHolder = (BankStockAdapter.BankStockViewHolder) convertView.getTag();
        }

        if (position == 0) {
            if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE) {
                viewBankHolder.textTabBank.setBackgroundResource(R.drawable.background_tab_select_box);
                viewBankHolder.textTabBank.setTextColor(context.getResources().getColor(R.color.black));
                viewBankHolder.textTabStock.setBackgroundResource(R.drawable.background_tab_unselect_box);
                viewBankHolder.textTabStock.setTextColor(context.getResources().getColor(R.color.color666666));
            } else if (bankStockMode == Const.BANK_STOCK_MODE.STOCK_MODE) {
                viewBankHolder.textTabStock.setBackgroundResource(R.drawable.background_tab_select_box);
                viewBankHolder.textTabStock.setTextColor(context.getResources().getColor(R.color.black));
                viewBankHolder.textTabBank.setBackgroundResource(R.drawable.background_tab_unselect_box);
                viewBankHolder.textTabBank.setTextColor(context.getResources().getColor(R.color.color666666));
            }
            
            viewBankHolder.textTabBank.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bankStockMode = Const.BANK_STOCK_MODE.BANK_MODE;
                    viewBankHolder.textTabBank.setBackgroundResource(R.drawable.background_tab_select_box);
                    viewBankHolder.textTabBank.setTextColor(context.getResources().getColor(R.color.black));
                    viewBankHolder.textTabStock.setBackgroundResource(R.drawable.background_tab_unselect_box);
                    viewBankHolder.textTabStock.setTextColor(context.getResources().getColor(R.color.color666666));
                    notifyDataSetChanged();
                }
            });

            viewBankHolder.textTabStock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bankStockMode = Const.BANK_STOCK_MODE.STOCK_MODE;
                    viewBankHolder.textTabStock.setBackgroundResource(R.drawable.background_tab_select_box);
                    viewBankHolder.textTabStock.setTextColor(context.getResources().getColor(R.color.black));
                    viewBankHolder.textTabBank.setBackgroundResource(R.drawable.background_tab_unselect_box);
                    viewBankHolder.textTabBank.setTextColor(context.getResources().getColor(R.color.color666666));
                    notifyDataSetChanged();
                }
            });

            viewBankHolder.layoutTab.setVisibility(View.VISIBLE);
            viewBankHolder.layoutItem.setVisibility(View.GONE);
        } else {
            RequestCodeInfo bankInfo = null;
            if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE)
                bankInfo = listBank.get(position - 1);
            else if (bankStockMode == Const.BANK_STOCK_MODE.STOCK_MODE)
                bankInfo = listStock.get(position - 1);

            final String name = bankInfo.getCD_NM();
            final String code = bankInfo.getSCCD();

            if (bankInfo != null) {
                if (!TextUtils.isEmpty(name))
                    viewBankHolder.textBank.setText(name);

                int resourceID = getImageBank(code);
                if (resourceID > 0)
                    viewBankHolder.imageBank.setImageResource(resourceID);
            }

            viewBankHolder.layoutTab.setVisibility(View.GONE);
            viewBankHolder.layoutItem.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private int getImageBank(String code) {
        int resourceID = -1;

        String resourcename = "img_logo_" + code;
        if ("028".equalsIgnoreCase(code))
            resourcename = "img_logo_000";

        resourceID = context.getResources().getIdentifier(resourcename, "drawable", packageName);

        return resourceID;
    }

    public Const.BANK_STOCK_MODE getTabState() {
        return bankStockMode;
    }

    class BankStockViewHolder {
        LinearLayout layoutTab;
        LinearLayout layoutItem;
        TextView     textTabBank;
        TextView     textTabStock;
        ImageView    imageBank;
        TextView     textBank;
    }
}