package com.sbi.saidabank.common.crop;

public enum CropImagePreset {
    RECT,
    CIRCULAR,
    CUSTOMIZED_OVERLAY,
    MIN_MAX_OVERRIDE,
    SCALE_CENTER_INSIDE,
    CUSTOM
}
