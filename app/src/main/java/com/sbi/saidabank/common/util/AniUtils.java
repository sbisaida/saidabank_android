package com.sbi.saidabank.common.util;

import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Saidabank_android
 *
 * Class: ImgUtils
 * Created by 950469 on 2018. 11. 21..
 * <p>
 * Description:
 * Ani작업 관련 함수들의 모음.
 */
public class AniUtils {
    //애니메이션 시간
    //단위 milisecond
    public static int ANIMATION_TIME = 400;

    /**
     * 해당뷰를 좌우로 흔드는 에니메이션
     *
     * @param view : 흔들 뷰
     */
    public static void shakeView(View view){
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 50f,-50f,50f,-50f,0);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 해당뷰를 좌우로 흔드는 에니메이션
     *
     * @param view : 흔들 뷰
     * @param offset : 흔들리는 범위
     */
    public static void shakeView(View view, float offset){
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", offset,-offset,offset,-offset,0);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 해당뷰를 X좌표 시작 위치에서 설정한 위치로 이동시키는 애니에션
     *
     * @param view : 이동시킬 뷰
     * @param startPos : 애니메에션 시작 위치 +,- 값을 넣어주면 됨.
     */
    public static void moveHorizontalView(View view,float startPos){
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", startPos,0);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 해당뷰를 Y좌표 시작 위치에서 설정한 위치로 이동시키는 애니에션
     *
     * @param view : 이동시킬 뷰
     * @param startPos : 애니메에션 시작 위치 +,- 값을 넣어주면 됨.
     */
    public static void moveVerticalView(View view,float startPos){
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationY", startPos,0);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }
}
