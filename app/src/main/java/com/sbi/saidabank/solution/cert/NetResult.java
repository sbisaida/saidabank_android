package com.sbi.saidabank.solution.cert;

import java.util.ArrayList;

public class NetResult {
	private boolean mIsOk;
	private String mResCode;
	private ArrayList<String> mResMsg;
	
	public NetResult() {
		mIsOk = false;
		mResCode = "";
		mResMsg = new ArrayList<String>();
	}
	
	public void setResCode(String resCode) {
		mResCode = resCode;
		if (mResCode.equalsIgnoreCase("OK") || mResCode.equalsIgnoreCase("000")) {
			mIsOk = true;
		}
	}
	public void setResMessage(String resMessage) {
		mResMsg.add(resMessage);
	}
	
	public int getLength() { return mResMsg.size(); }
	public boolean isOk() { return mIsOk; }
	public String getResCode() { return mResCode; }
	
	public String getResMessage(int index) {
		return mResMsg.get(index); 
	}
	
	public String getFullResMessage() {
		String res = null;
		int len = getLength();
		
		if (len == 0)
			return null;
		res = getResMessage(0);
		for (int i=1; i<len; i++) {
			res = res + " : " + getResMessage(i);  
		}
		return res;
	}
}
