package com.sbi.saidabank.solution.espider;

public class EspiderData {
    public static final String TAG = EspiderData.class.getSimpleName();

    private int type;
    private String status;
    private String result;

    public int getType()  {
        return type;
    }

    public void setType(int type)  {
        this.type = type;
    }

    public String getStatus()  {
        return status;
    }

    public void setStatus(String status)  {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}