package com.sbi.saidabank.solution.v3;

import android.content.Context;

import com.ahnlab.enginesdk.RootChecker;
import com.ahnlab.enginesdk.SDKManager;
import com.ahnlab.enginesdk.SDKResultCode;
import com.ahnlab.enginesdk.SDKVerify;
import com.ahnlab.enginesdk.TaskObserver;
import com.ahnlab.enginesdk.Updater;
import com.ahnlab.enginesdk.rc.RootCheckCallback;
import com.ahnlab.enginesdk.rc.RootCheckElement;
import com.ahnlab.enginesdk.rc.RootCheckInfo;
import com.ahnlab.enginesdk.up.UpdateCallback;
import com.ahnlab.enginesdk.up.UpdateElement;
import com.ahnlab.enginesdk.up.UpdateResult;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.LicenseKey;

public class RootCheckerManager {

    public static final String RC_CHECK_COMPLETE    = "0000";
    public static final String RC_CHECK_FIND        = "0001";
    public static final String RC_CHECK_ERROR       = "0002";
    public static final String RC_UPDATE_COMPLETE   = "1000";
    public static final String RC_UPDATE_CANCEL     = "1001";
    public static final String RC_UPDATE_ERROR      = "1002";

    private static RootCheckerManager instance = null;
    private static Context mCtx;
    private static boolean isOnUnloadEngineSDK = false;

    private SDKManager mSDKManager = null;
    private RootCheckerEventListener mListner = null;

    /**
     * Singleton instance 생성
     *
     * @param aCtx  컨테스트
     * @return instance
     */
    public static RootCheckerManager getInstance(Context aCtx) {
        if (instance == null) {
            synchronized (RootCheckerManager.class) {
                if (instance == null) {
                    instance = new RootCheckerManager(aCtx);
                }
            }
        }
        return instance;
    }

    /**
     * 생성자
     *
     * @param aCtx  컨테스트
     */
    private RootCheckerManager(Context aCtx) {
        // Context를 받는다.
        mCtx = aCtx;
    }

    /**
     * RootChecker 초기화
     * 루팅 검사 로직 초기화
     * code.prod 파일 배치 파일이 없으면 initialize 과정에서 Exception이 발생 - 추가 후 사용 필요
     * 클라이언트 기반 애플리케이션 위변조 검사 기능을 사용하기 위해서는
     * code.prod 생성 요청 시 output.tdd 파일을 기술지원 담당자에게 전달해야 함
     * allow.fc 파일을 포함시켰을 경우, 애플리케이션 상태 검사 결과가 양성일 시 강제 종료가 발생한다.
     * 강제 종료 없이 진단 값만 받아 처리하고 싶다면 allow.fc 파일을 제거하고 패키징해야 함
     *
     * @return boolean 성공 여부
     */
    public boolean initAhnLabMobileRootChecker() {
        try {
            SDKManager.initialize(mCtx, LicenseKey.RC_LICENSE_KEY);
            mSDKManager = SDKManager.getInstance();
            if (mSDKManager == null) {
                return false;
            }
        } catch (SDKVerify.InvalidDataException e)  {
            // 앱 재설치가 필요함
            return false;
        } catch (UnsatisfiedLinkError e) {
            // 앱 재시작 또는 재설치가 필요함
            return false;
        } catch (Throwable why) {
            stopEngineSDK();
            return false;
        }

        return true;
    }

    /**
     * Root Checker 동작 완료 리스너를 등록한다.
     *
     * @param listner
     */
    public void setRootCheckEventListener(RootCheckerEventListener listner) {
        mListner = listner;
    }

    /**
     *  루팅 검사한다
     */
    public boolean checkAhnLabMobileRootChecker() {
        if (isOnUnloadEngineSDK) {
            //내부망 테스트 중 에러 발생. 내부망 해결 후 다시 적용 예정
            //throw new IllegalStateException("on unloading");
            return false;
        }

        try {
            if (mSDKManager == null) {
                boolean isRet = initAhnLabMobileRootChecker();
                if (!isRet) {
                    return false;
                }
            }

            RootChecker rootChecker = (RootChecker) mSDKManager.getEngine(SDKManager.ENGINE_ID.ROOT_CHECKER);
            if (rootChecker == null) {
                return false;
            }

            // 주기 검사는 사용 안함
            RootCheckElement element = new RootCheckElement.Builder(mCtx)
                    //.setCheckScope(RootChecker.CHECK_SCOPE.VM | RootChecker.CHECK_SCOPE.DEBUGGER)
                    .setCheckScope(RootChecker.CHECK_SCOPE.VM)
                    //.setInterval(60) // 주기적 검사를 60s 마다 동작하도록 설정
                    .setOptions(RootChecker.OPTION_APPLICATION)
                    .build();

            // 1회성 검사 시작
            rootChecker.startCheck(element, new RootCheckCallback() {
                @Override
                public void onCheck(int result, RootCheckElement rootCheckElement, RootCheckInfo rootCheckInfo) {
                    if (result < 0) {
                        // 기기 환경적인 이슈로 인해 루팅 검사 실패
                        if (mListner != null)
                            mListner.onCheckRootingResult(RC_CHECK_ERROR,rootCheckInfo);
                        return;
                    }

                    if (rootCheckInfo.getRuleID() == 0) {
                        // 루팅되지 않은 기기
                        if (mListner != null)
                            mListner.onCheckRootingResult(RC_CHECK_COMPLETE,rootCheckInfo);
                    } else {
                        // 루팅 탐지함
                        // 값으로 진단 번호 확인 가능
                        // 추가적인 진단 정보가 있을 경우 rootCheckInfo.getDescription()으로 확인 가능
                        //String description = rootCheckInfo.getDescription();

                        if (mListner != null)
                            mListner.onCheckRootingResult(RC_CHECK_FIND,rootCheckInfo);
                    }
                }

                @Override
                public void onStop() {
                    // 주기적 검사 종료시 호출됨
                }
            });
        } catch (Throwable why) {
            return false;
        }

        return true;
    }

    /**
     *  루팅 솔루션 업데이트한다
     */
    public boolean updateAhnLabMobileRootChecker() {
        if (mSDKManager == null) {
            initAhnLabMobileRootChecker();
        }

        try {
            Updater updater = (Updater) mSDKManager.getEngine(SDKManager.ENGINE_ID.UPDATER);
            if (updater == null) {
                return false;
            }

            UpdateElement element = new UpdateElement.Builder(mCtx)
                    .setCountryCode(82)
                    .setNetworkOpt(Updater.NETWORK_OPTION.OPT_MOBILE_OR_WIFI)
                    .setStableEngineUpdate(true)
                    .build();
            updater.update(element, new UpdateCallback() {
                @Override
                public void onUpdateProgress(int progress, int currentStep, int currentDownCount,
                                             int totalDownCount, int currentCopyCount, int totalCopyCount) {
                    // 진행율
                    Logs.i(":UPDATE", "progress: " + progress + "%");
                    Logs.i(":UPDATE", "currentStep: " + currentStep);
                    Logs.i(":UPDATE", "currentDownCount: " + currentDownCount);
                    Logs.i(":UPDATE", "totalDownCount: " + totalDownCount);
                    Logs.i(":UPDATE", "currentCopyCount: " + currentCopyCount);
                    Logs.i(":UPDATE", "totalCopyCount: " + totalCopyCount);
                }

                // 업데이트 성공 시 호출되는 callback
                @Override
                public void onComplete(int ret, UpdateResult updateResult) {
                    Logs.i("RC update-onComplete: " + ret);
                    //int auth = updateResult.getAuthResult();
                    //int remainDays = updateResult.getRemainDays();

                    if (mListner != null)
                        mListner.onUpdateResult(RC_UPDATE_COMPLETE,updateResult);
                }

                // 업데이트가 업데이트가 취소되었을 때 호출되는 callback
                @Override
                public void onCancel(int ret, UpdateResult updateResult) {
                    Logs.i("RC  update-onCancel: " + ret);
                    if (mListner != null)
                        mListner.onUpdateResult(RC_UPDATE_CANCEL,updateResult);
                }

                // 업데이트 도중 오류가 발생했을 때 호출되는 호출되는 메서드
                @Override
                public void onError(int ret, UpdateResult updateResult) {
                    Logs.i("RC update-onError: " + ret);
                    // SDKResultCode.RET_MGR_RESTORE_REQUIRED 또는 SDKResultCode.RET_PATCH_REQUIRED 발생 시,
                    // ENGINE을 restore 해야 함. restore 실행 타 기능을 수행할 경우 다시 initialize부터 호출해야 함
                    if (ret == SDKResultCode.RET_MGR_RESTORE_REQUIRED ||
                            ret == SDKResultCode.RET_PATCH_REQUIRED) {
                        stopEngineSDK();
                    } else {
                        String msg = "";
                        if (mListner != null)
                            mListner.onUpdateResult(RC_UPDATE_ERROR,updateResult);
                    }
                }
            });
        } catch (Throwable why) {
            return false;
        }

        return true;
    }

    /**
     *  RootChecker 종료한다
     */
    public synchronized void stopEngineSDK() {
        if (mSDKManager == null)
            return;

        int ret = mSDKManager.unloadAll(new TaskObserver() {
            @Override
            public void done(int ret) {
                if (ret == SDKResultCode.ERR_NOERROR) {
                    // unload 성공
                    mSDKManager = null;
                } else {
                }

                synchronized (this) {
                    isOnUnloadEngineSDK = false;
                }

                //if (mListner != null)
                //    mListner.onEndUnloadRootChecker();
            }
        });

        if (ret != SDKResultCode.RET_PENDING) {
            // unloadAll() 호출 실패
        } else {
            synchronized (this) {
                isOnUnloadEngineSDK = true;
            }
        }
    }

    public interface RootCheckerEventListener {
        //root 체크시
        void onCheckRootingResult(String code,RootCheckInfo rootCheckInfo);
        //엔진 업데이트시
        void onUpdateResult(String code, UpdateResult updateResult);
        // root checker 종료 종료 시
        //void onEndUnloadRootChecker();
    }
}