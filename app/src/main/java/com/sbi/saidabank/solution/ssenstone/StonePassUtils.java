package com.sbi.saidabank.solution.ssenstone;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.util.Logs;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StonePassUtils {
    /**
     * 지문인식 지원 여부를 확인한다
     *
     * @param context
    -  0:지원가능
    - -1:지원버전아님(SDK 6.0이상 지원)
    - -2:지원기기아님
    - -3:등록한지문이 없습니다.
    - -4:지원기기검사오류
     */
    public static int isUsableFingerprint(Context context) {
        // 지문 인식 지원 OS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            if (fingerprintManager == null)
                return -4;

            // 지문 인식 하드웨어 존재 여부
            if (!fingerprintManager.isHardwareDetected())
                return -2;

            // 인증 지문이 등록되어 있는지 여부
            if (!fingerprintManager.hasEnrolledFingerprints())
                return -3;
        } else {
            return -1;
        }

        return 0;
    }

    /**
     * 지문인식 디바이스 존재 유무 확인
     * @param context
     * @return 0이 아니면 지원 불가
     */
    public static int hasFingerprintDevice(Context context) {
        // 지문 인식 지원 OS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            if (fingerprintManager == null)
                return -4;

            // 지문 인식 하드웨어 존재 여부
            if (!fingerprintManager.isHardwareDetected())
                return -2;
        } else {
            return -1;
        }

        return 0;
    }

    /**
     * 서버에 패턴, 지문 인식 값 등록, 인증, 해제 전문 생성
     *
     * @param op
     * @param userName 사용자 아이디
     * @param systemID 서버 아이디
     * @param bioType  패턴,
     * @param deviceID 디바이스 아이디
     * @return 서버에 전송될 전문
     */
    public static String genFidoRequestMsg(String op, String userName, String systemID, String token, String signData, String bioType, String deviceID) {
        String ret = "";

        try {
            JSONObject jsonContextMsg = new JSONObject();
            jsonContextMsg.put("userName", userName);

            if (!TextUtils.isEmpty(bioType))
                jsonContextMsg.put("BIOTYPE", bioType);

            jsonContextMsg.put("MODEL", Build.MODEL);

            if (!TextUtils.isEmpty(deviceID)) {
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    jsonContextMsg.put("DEVICE", deviceID);
                } else {
                    String deviceHash = getSHA256(deviceID);
                    jsonContextMsg.put("DEVICEHASH", deviceHash.toUpperCase());
                }
            }

            if (!TextUtils.isEmpty(signData)) {
                String encodedStr = Base64.encodeToString(signData.getBytes(), Base64.NO_WRAP);
                jsonContextMsg.put("transaction", encodedStr);
            }

            //if (!TextUtils.isEmpty(systemID))
                jsonContextMsg.put("SYSTEMID", systemID);

            if (!TextUtils.isEmpty(token))
                jsonContextMsg.put("token", token);

            JSONObject jsonRequestMsg = new JSONObject();
            jsonRequestMsg.put("op", op);
            jsonRequestMsg.put("context", jsonContextMsg.toString());

            ret = jsonRequestMsg.toString();

        } catch (Exception e) {
            Logs.printException(e);
        }

        return ret;
    }

    /**
     * 서버에 패턴, 지문 인식 값 등록, 인증, 해제 전문 생성
     *
     * @param op
     * @param userName 사용자 아이디
     * @param systemID 서버 아이디

     * @return 서버에 전송될 전문
     */
    /*
    public static String genPinRequestMsg(String op, String userName,String pin, String systemID,String ssid, String token) {
        String ret = "";

        try {
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("ACMD", "op");
            jsonRequest.put("USERID", userName);
            jsonRequest.put("PIN", pin);


            if (secureKey != null && secureKey.length > 0) {
                jsonRequest.put("PINTYPE", "RAON");
                jsonRequest.put("PINDATATYPE", "R01");
                String strSecureKey = Base64.encodeToString(secureKey, Base64.URL_SAFE);
                jsonRequest.put("BASE64PINSECKEY", strSecureKey);
            }

            jsonRequest.put("SYSTEMID", systemID);
            jsonRequest.put("AUTHTYPE", 0x1);
            jsonRequest.put("MULTIAUTH", 0x0);
            jsonRequest.put("SSID", ssid);
            jsonRequest.put("TOKEN", token);


            ret = jsonRequest.toString();

        } catch (Exception e) {
            Logs.printException(e);
        }

        return ret;
    }
*/
    /**
     * 문자를 암호화 한다
     *
     * @param str
     * @return 암호화된 문자
     */
    private static String getSHA256(String str) {
        String SHA = "";
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(str.getBytes());
            byte byteData[] = sh.digest();
            StringBuffer sb = new StringBuffer();
            for(int i = 0 ; i < byteData.length ; i++){
                sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
            }
            SHA = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Logs.printException(e);
            SHA = null;
        }

        return SHA;
    }

    /**
     * 스트링 base64 encoding
     *
     * @param secureKey
     * @return base64 encoding된 스트링
     */
    public static String getBase64Key(String secureKey){
        byte[] secureKeyByte = hexToBytes(secureKey);
        return Base64.encodeToString(secureKeyByte, Base64.URL_SAFE);
    }

    /**
     * hex string -> byte
     * @param str
     * @return byte값
     */
    public static byte[] hexToBytes(String str) {
        if (str==null) {
            return null;
        }

        if (str.length() < 2) {
            return null;
        }

        int len = str.length() / 2;
        byte[] buffer = new byte[len];
        for (int i = 0; i<len ; i++) {
            buffer[i] = (byte) Integer.parseInt(str.substring(i * 2,i * 2 + 2),16);
        }

        return buffer;
    }
}
