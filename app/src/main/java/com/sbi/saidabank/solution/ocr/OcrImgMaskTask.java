package com.sbi.saidabank.solution.ocr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.common.util.ImgUtils;

/**
 * Saidabanking_android
 * Class: OcrImgMaskingTask
 * Created by 950469 on 2018. 8. 17..
 * <p>
 * Description:신분증 이미지의 마스킹 처리를 위한 TASK.
 */
public class OcrImgMaskTask extends AsyncTask<Intent, Void, Boolean> {
    private Bitmap mIDCardBitmap;
    private OcrImgTaskListener mListener;

    public OcrImgMaskTask(OcrImgTaskListener l){
        mListener = l;
    }

    @Override
    protected Boolean doInBackground(Intent... params) {
        Intent data = params[0];

        // 신분증 이미지 영역
        byte[] encryptImage = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);

        // 주민번호 마스킹 영역
        Rect rrnRect = data.getParcelableExtra(CameraActivity.DATA_RRN_RECT);

        // 면허번호 마스킹 영역
        Rect licenseNumberRect = data.getParcelableExtra(CameraActivity.DATA_LICENSE_NUMBER_RECT);

        // 신분증 증명사진 영역
        Rect photoRect = data.getParcelableExtra(CameraActivity.DATA_PHOTO_RECT);

        if (encryptImage == null || rrnRect == null || licenseNumberRect == null) {
            return false;
        } else {

            //마스킹이미지
            //byte[] maskingImage;
            //사진영역 이미지
            //byte[] faceImage;

            if (encryptImage != null) {
                if (mIDCardBitmap != null && !mIDCardBitmap.isRecycled()) {
                    mIDCardBitmap.recycle();
                }

                mIDCardBitmap = ImgUtils.byteArrayToBitmap(encryptImage);

                if (mIDCardBitmap != null && rrnRect != null && licenseNumberRect != null) {
                    Canvas c = new Canvas(mIDCardBitmap);
                    Paint paint = new Paint();
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(Color.BLACK);
                    //주민번호 영역 마스킹
                    c.drawRect(rrnRect, paint);
                    //면허번호 영역 마스킹

                    double shiftlength = (licenseNumberRect.right - licenseNumberRect.left) * 0.5;

                    licenseNumberRect.left = (int)(licenseNumberRect.left + shiftlength);
                    licenseNumberRect.right = (int)(licenseNumberRect.right + shiftlength);

                    c.drawRect(licenseNumberRect, paint);
                    //증명사진 영역 마스킹
                    //paint.setStyle(Paint.Style.STROKE);
                    //paint.setColor(Color.RED);
                    //c.drawRect(photoRect, paint);

                    //신분증 이미지에서 얼굴이미지를 잘라냄
                    Bitmap bitmapFace = ImgUtils.cropBitmap(mIDCardBitmap, photoRect);

                    //maskingImage = OcrUtils.bitmapToByteArray(mIDCardBitmap);
                    //faceImage    = OcrUtils.bitmapToByteArray(bitmapFace);

                    return true;
                }

            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        mListener.OnEndOcrMasking(aBoolean,mIDCardBitmap);
    }

    public interface OcrImgTaskListener{
        void OnEndOcrMasking(boolean result,Bitmap bmp);
    }
}
