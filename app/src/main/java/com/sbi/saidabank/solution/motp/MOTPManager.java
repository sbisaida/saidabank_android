package com.sbi.saidabank.solution.motp;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.atsolutions.secure.util.ByteUtils;
import com.atsolutions.tapagent.TAPAgent;
import com.atsolutions.tapagent.a2a.data.A2AData;
import com.atsolutions.tapagent.a2a.define.A2ADefine;
import com.atsolutions.tapagent.callback.TAPResultCallback;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.datatype.LoginUserInfo;

/**
 * siadabank_android
 * Class: MOTPManager
 * Created by 950546
 * Date: 2019-02-20
 * Time: 오후 3:51
 * Description:
 */
public class MOTPManager implements TAPResultCallback {
    public static final String TAG = MOTPManager.class.getSimpleName();

    private static MOTPManager instance;
    private Context mContext;

    private TAPAgent mAgent;

    private String mSiteCode = "02051";
    private String mTapVersion = "";
    private String mSerialNum = "";
    private String mOTPCode = "";
    private MOTPEventListener _listener = null;
    private boolean mEncrypt = true;

    public static MOTPManager getInstance(Context ctx) {
        if (instance == null) {
            synchronized (MOTPManager.class) {
                if (instance == null) {
                    instance = new MOTPManager(ctx);
                }
            }
        }
        return instance;
    }

    private MOTPManager(Context ctx) {
        mContext = ctx;
        String androidId = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
        mAgent = new TAPAgent(mContext, androidId);
    }

    public static void clearInstance(){
        instance = null;
    }

    @Override
    public void onCommandResult(int apiCode, A2AData result) {
        Logs.e("onCommandResult");
        String resultCode = result.getResultCode();
        String resultMsg = result.getResultMsg();
        Bundle resultData = result.getData();
        String reqData1 = "";
        String reqData2 = "";
        String reqData3 = "";
        String reqData4 = "";

        if (resultCode.equals(A2ADefine.A2aResultCode.A2A_RESULT_CODE_SUCCESS)) {
            switch (apiCode) {
                case A2ADefine.ApiCode.API_GET_TA_INFO:
                    String taVersion = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_TA_VERSION);
                    Logs.d(TAG, "API_GET_TA_INFO::resultCode::" + resultCode);
                    Logs.d(TAG, "API_GET_TA_INFO::resultMsg::" + resultMsg);
                    Logs.d(TAG, "API_GET_TA_INFO::TAVersion : " + taVersion);
                    if (!TextUtils.isEmpty(taVersion))
                        mTapVersion = taVersion;
                    else
                        mTapVersion = "";
                    reqData1 = mTapVersion;
                    break;
                case A2ADefine.ApiCode.API_ISSUE_OTP:
                    Logs.d(TAG, "API_ISSUE_OTP::resultCode::" + resultCode);
                    Logs.d(TAG, "API_ISSUE_OTP::resultMsg::" + resultMsg);
//                    mTransactionManager.requestOtpSetStatus(otpSetStatusHandler, mOtpSN, ByteUtils.BytesToHexString(TAPAgent.OTP_STATUS_ACTIVE.getBytes()));
                    break;
                case A2ADefine.ApiCode.API_GET_SERIAL_NUM:
                    String serialNumber = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_SERIAL_NUMBER);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::resultCode::" + resultCode);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::resultMsg::" + resultMsg);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::SerialNumber : " + serialNumber);
                    if (!TextUtils.isEmpty(serialNumber))
                        mSerialNum = serialNumber;
                    else
                        mSerialNum = "";
                    reqData1 = mSerialNum;
                    break;
                case A2ADefine.ApiCode.API_GENERATE_DS_OTP:
                    Logs.d(TAG, "API_GENERATE_DS_OTP::resultCode::" + resultCode);
                    Logs.d(TAG, "API_GENERATE_DS_OTP::resultMsg::" + resultMsg);
                    Logs.d(TAG, "API_GENERATE_DS_OTP::resultData::" + resultData.toString());
                    String otp = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_OTP);
                    if (!TextUtils.isEmpty(otp))
                        mOTPCode = otp;
                    else
                        mOTPCode = "";
                    reqData1 = mOTPCode;
                    break;
                case A2ADefine.ApiCode.API_DISSUE_OTP:
                    Logs.d(TAG, "API_DISSUE_OTP::resultCode::" + resultCode);
                    break;

                case A2ADefine.ApiCode.API_GET_ALL_INFO:
                    serialNumber = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_SERIAL_NUMBER);
                    taVersion = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_TA_VERSION);
                    String issueDate = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_ISSUE_DATE);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::resultCode::" + resultCode);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::resultMsg::" + resultMsg);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::SerialNumber : " + serialNumber);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::taVersion : " + taVersion);
                    Logs.d(TAG, "API_GET_SERIAL_NUM::issueDate : " + issueDate);
                    Logs.e("result : " + serialNumber + "/" + taVersion);
                    if (!TextUtils.isEmpty(serialNumber)) {
                        mSerialNum = serialNumber;
                        reqData1 = serialNumber;
                        reqData2 = serialNumber.substring(0, 3);
                    }
                    else {
                        mSerialNum = "";
                        reqData1 = "";
                        reqData2 = "";
                        resultCode = "A2106A82";
                        resultMsg = "모바일 OTP정보가 없습니다. 모바일OTP를 발급/재발급 받으시기 바랍니다.";
                    }
                    if (!TextUtils.isEmpty(taVersion)) {
                        mTapVersion = taVersion;
                        reqData4 = taVersion;
                    }
                    else {
                        mTapVersion = "";
                    }
                    if (!TextUtils.isEmpty(issueDate))
                        reqData3 = issueDate;
                    else
                        reqData3 = "";
                    break;

                default:
                    break;
            }
        } else {
            Logs.d(TAG, "resultCode : " + resultCode);
            Logs.d(TAG, "resultMsg : " + resultMsg);
        }

        if (_listener != null)
            _listener.MOTPEventListener(resultCode, resultMsg, reqData1, reqData2, reqData3, reqData4);
    }

    /**
     * 리스너 삭제
     */
    public void removeListener() {
        _listener = null;
    }

    public void getTaVersion(MOTPEventListener listener) {
        Logs.e("getTaVersion");
        _listener = listener;
        mAgent.getTaInfo(this);
    }

    public void saveIssueInfo(MOTPEventListener listener, String optKey1, String optKey2, final String serialNum, final String issueDate) {
        Logs.e("saveIssueInfo");
        _listener = listener;
        final byte[] otpKey1Hex = ByteUtils.HexStringToBytes(optKey1);
        final byte[] otpKey2Hex = ByteUtils.HexStringToBytes(optKey2);
        mAgent.issue(serialNum, issueDate, otpKey1Hex, otpKey2Hex, MOTPManager.this);
    }

    public void getSerialNum(MOTPEventListener listener) {
        _listener = listener;
        mAgent.getAllInfo(mSiteCode, this);
    }

    public void reqOTPCode(MOTPEventListener listener, String otpTime, String serialNum, String hti) {
        _listener = listener;
        byte[] para = ByteUtils.HexStringToBytes(hti);
        mAgent.sign(serialNum, otpTime, para, mSiteCode, mEncrypt, this);
    }

    public void reqDissue(MOTPEventListener listener, String serialNum) {
        _listener = listener;
        mAgent.dissue(serialNum, this);
    }

    public interface MOTPEventListener {
        void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4);
    }
}
