package com.sbi.saidabank.solution.ssenstone;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AES256Utils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.ssenstone.stonepass.libstonepass_sdk.SSUserManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Saidabank_android
 * Class: StonePassManager
 * Created by 950469 on 2018. 9. 6..
 * <p>
 * Description:
 */
public class StonePassManager {
    public static final String TAG = StonePassManager.class.getSimpleName();

    private static StonePassManager instance;
    private SSUserManager mSSUserManager;
    private Context mContext;

    //fido, pattern,pin에서 사용할 변수
    private String mOp;
    private String mUserName;
    private String mSystemId;
    private String mSsId;
    private String mToken;
    private String mSignData;
    private String mBioType;
    private String mDeviceId;
    private String mFcode;
    private String mErrDesc;
    private String mErrIP;
    private Activity mActivity;
    private StonePassInitListener mSpInitListener;
    private StonePassListener mSpListener;
    private FingerPrintDlgListener mFingerPrintDlgListener;
    private Thread mFidoListenerThread;

    public static StonePassManager getInstance(Context ctx) {
        if (instance == null) {
            synchronized (StonePassManager.class) {
                if (instance == null) {
                    instance = new StonePassManager(ctx);
                }
            }
        }
        return instance;
    }

    private StonePassManager(Context ctx) {
        mContext = ctx;
        mSSUserManager = new SSUserManager();
        /*
        mSSUserManager.SSInit(mContext, null, SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/userRequest.jsp");

        mUserName = Prefer.getAuthPhoneCI(ctx);
        try {
            if (!TextUtils.isEmpty(mUserName)) {
                byte[] data = mUserName.getBytes("UTF-8");
                mUserName = Base64.encodeToString(data, Base64.NO_WRAP);
                Logs.e("mUserName : " + mUserName);
            } else {
                Logs.e("mUserName is null");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        mSystemId = "3";
        mDeviceId = "";//mSSUserManager.SSGetDUID();
        mToken = "SBISaidaBank";
        mSsId = "";
        */
    }

    public void StonePassInit(StonePassInitListener listener) {
        mSpInitListener = listener;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mSSUserManager.SSInit(mContext, null, SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/userRequest.jsp");
                mUserName = Prefer.getAuthPhoneCI(mContext);
                try {
                    if (!TextUtils.isEmpty(mUserName)) {
                        byte[] data = mUserName.getBytes("UTF-8");
                        mUserName = Base64.encodeToString(data, Base64.NO_WRAP);
                        Logs.e("mUserName : " + mUserName);
                    } else {
                        Logs.e("mUserName is null");
                    }
                } catch (UnsupportedEncodingException e) {
                    //e.printStackTrace();
                }
                mSystemId = "3";
                mDeviceId = "";//mSSUserManager.SSGetDUID();
                mToken = "SBISaidaBank";
                mSsId = "";
                mSpInitListener.stonePassInitResult();
            }
        });
        thread.start();
    }

    /**
     * StonePassManager 개체 초기화
     */
    public static void clearInstance(){
        if(instance == null)
            return;

        instance = null;
    }

    public void cancelFingerPrint() {
        Logs.e("cancelFingerPrint");
        mSSUserManager.SSCancelFingerprint();
    }

    /***********************************************************************************************
     * 사용자 인증 전문 송신
     *
     * @param op            등록/인증/해제
     */
    public void ssenstoneFIDO(Activity activity, String op, String signData, String bioType, String fcode) {
        mActivity = activity;
        mSpListener = (StonePassListener) activity;

        if (bioType.equalsIgnoreCase("FINGERPRINT") && activity instanceof FingerPrintDlgListener) {
            mFingerPrintDlgListener = (FingerPrintDlgListener) activity;
        } else {
            mFingerPrintDlgListener = null;
        }

        mOp = op;

        mSignData = signData;
        mBioType = bioType;
        mFcode = fcode;


        String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Get.jsp";

        // FIDO 지문인식 사용 가능 프로세스 진행
        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
            // Task가 등록인 경우 분기
            String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
            Logs.d("ssenstone", "---------- ssenstone start pattern reg ---------" );
            HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
            // Task가 인증인 경우 분기
            String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
            Logs.d("ssenstone", "---------- ssenstone start pattern auth ---------" );
            HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_DEREGISTER)) {
            if (bioType.equalsIgnoreCase("FINGERPRINT")) {
                // Task가 해지인 경우 분기
                // 해지 시에는 통신 없이 FIDO 프로세스 호출
                if (mFingerPrintDlgListener != null) {
                    mFingerPrintDlgListener.showFingerPrintDialog();
                }
                FidoProcess(bioType, mFcode);
            } else {
                String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
                HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
            }
        }
    }

    /***********************************************************************************************
     * 사용자 인증 전문 송신
     *
     * @param op            등록/인증/해제
     */
    public void ssenstoneFIDO(Activity activity, StonePassListener spListener, FingerPrintDlgListener fingerPrintDlgListener, String op, String signData, String bioType, String fcode) {
        mActivity = activity;
        mSpListener = spListener;
        if (bioType.equalsIgnoreCase("FINGERPRINT")) {
            mFingerPrintDlgListener = fingerPrintDlgListener;
        }

        mOp = op;
        mSignData = signData;
        mBioType = bioType;
        mFcode = fcode;


        String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Get.jsp";

        // FIDO 지문인식 사용 가능 프로세스 진행
        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
            // Task가 등록인 경우 분기
            String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
            HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
            // Task가 인증인 경우 분기
            String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
            HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_DEREGISTER)) {
            if (bioType.equalsIgnoreCase("FINGERPRINT")) {
                // Task가 해지인 경우 분기
                // 해지 시에는 통신 없이 FIDO 프로세스 호출
                if (mFingerPrintDlgListener != null) {
                    mFingerPrintDlgListener.showFingerPrintDialog();
                }
                FidoProcess(mBioType, mFcode);
            } else {
                String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
                HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
            }
        }
    }

    private HttpSenderTask.HttpRequestListener mFidoHttpListener = new HttpSenderTask.HttpRequestListener() {

        @Override
        public void endHttpRequest(String ret) {
            Logs.d("ssenstone", "---------- ssenstone start ---- end ---------" );
            if (TextUtils.isEmpty(ret)) {
                //네트웍 작업후 값 못가져오면 어떻게 처리할지.
                mSpListener.stonePassResult(mOp, 9998, "");
                Logs.e(mContext.getResources().getString(R.string.msg_debug_no_response));
                return;
            }

            try {
                JSONObject jsonObject = new JSONObject(ret);
                int statusCode = jsonObject.getInt("statusCode");
                if (statusCode == 1200) {
                    String serverRequestMsg = jsonObject.optString("uafRequest");
                    if (!TextUtils.isEmpty(serverRequestMsg)) {
                        //여기서 다이얼 로그 출력한다.
                        if (mBioType.equalsIgnoreCase("FINGERPRINT") && mFingerPrintDlgListener != null) {
                            mFingerPrintDlgListener.showFingerPrintDialog();
                        }
                        Logs.d("ssenstone", "---------- ssenstone start 1 pattern ---------" );
                        FidoProcess(serverRequestMsg, mFcode);
                    }
                } else {
                    //이경우는 사용자가 등록되어 있지 않는 경우이다.
                    mSpListener.stonePassResult(mOp, statusCode, "");
                }
            } catch (Exception e) {
                //에러 발생하면 어떻게 값 전달할지 고민해보자.
                Logs.printException(e);
                mSpListener.stonePassResult(mOp, 9999, e.getMessage());
            }
        }
    };

    /**
     * @author 950546
     * @Descrition: 등록된 lisetner로 처리결과를 전달할 때 UI쓰레드에서 처리
     * @since 2018-10-31 오후 3:49
     **/
    private void ssenstonePassResult(final String op, final int errocode, final String errstr) {

        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mSpListener != null)
                        mSpListener.stonePassResult(op, errocode, errstr);
                }
            });
        }
    }

    private void FidoProcess(final String message, final String fcode) {
        if (mBioType.equalsIgnoreCase("FINGERPRINT")) {
            //아래 함수를 실행하지 않으면 fingerprint device가 초기화 되지 않아 에러가 발생한다.
            int support = mSSUserManager.SSCheckDevice();
        }

        // 정상
        final SSUserManager.SSFidoListener mFidoListener = new SSUserManager.SSFidoListener() {
            @Override
            public void authenticationFailed(int errorCode, String errString) {
                if (mFidoListenerThread != null) {
                    mFidoListenerThread.interrupt();
                    mFidoListenerThread = null;
                }
                Logs.d("ssenstone", "---------- ssenstone end 1 pattern ---------" );
                Logs.d(TAG, "[authenticationFailed] " + errString);
                Logs.d(TAG, "[authenticationFailed] errcode : " + errorCode);

                Logs.e("authenticationFailed");
                ssenstonePassResult(mOp, errorCode, errString);
                //mSpListener.stonePassResult(mOp,errorCode,errString);
            }

            @Override
            public void authenticationSucceeded(String responseMsg) {
                Logs.d("ssenstone", "---------- ssenstone end 1 pattern ---------" );
                if (mFidoListenerThread != null) {
                    mFidoListenerThread.interrupt();
                    mFidoListenerThread = null;
                }

                Logs.d(TAG, "[authenticationSucceeded] " + responseMsg);
                if (!mOp.equalsIgnoreCase(Const.SSENSTONE_DEREGISTER)) {
                    // 등록, 인증
                    String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Send" + mOp + ".jsp";
                    HttpUtils.sendHttpTask(url, responseMsg, new HttpSenderTask.HttpRequestListener() {
                        @Override
                        public void endHttpRequest(String ret) {
                            if (TextUtils.isEmpty(ret)) {
                                //네트웍 작업후 값 못가져오면 어떻게 처리할지.
                                mSpListener.stonePassResult(mOp, 9998, "");
                                Logs.e(mContext.getResources().getString(R.string.msg_debug_no_response));
                                return;
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(ret);
                                int statusCode = jsonObject.getInt("statusCode");
                                Logs.e("authenticationSucceeded : " + statusCode);
                                mSpListener.stonePassResult(mOp, statusCode, "");
                            } catch (JSONException e) {
                                Logs.printException(e);
                                mSpListener.stonePassResult(mOp, 9999, "");
                            }
                        }
                    });
                } else {
                    // 해지
                    try {
                        JSONObject jsonObject = new JSONObject(responseMsg);
                        final int statusCode = jsonObject.getInt("statusCode");
                        if (statusCode == 99) {
                            // 지문인식 성공
                            String requestMsg = StonePassUtils.genFidoRequestMsg(mOp, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
                            String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Get.jsp";
                            HttpUtils.sendHttpTask(url, requestMsg, new HttpSenderTask.HttpRequestListener() {
                                @Override
                                public void endHttpRequest(String ret) {
                                    if (!TextUtils.isEmpty(ret)) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(ret);
                                            int statusCode = jsonObject.getInt("statusCode");
                                            mSpListener.stonePassResult(mOp, statusCode, "");

                                            if (statusCode == 1200 && mBioType.equalsIgnoreCase("PATTERN"))
                                                Prefer.setPatternRegStatus(mContext,true);
                                        } catch (Exception e) {
                                            mSpListener.stonePassResult(mOp, 9999, "");
                                        }
                                    } else {
                                        mSpListener.stonePassResult(mOp, 9998, "");
                                        Logs.e(mContext.getResources().getString(R.string.msg_debug_no_response));
                                    }
                                }
                            });
                        } else if (statusCode == 0) {
                            // FIDO 삭제 성공
                            ssenstonePassResult(mOp, statusCode, "");
                            //mSpListener.stonePassResult(mOp,statusCode,"");
                        } else {
                            ssenstonePassResult(mOp, statusCode, "");
                            //mSpListener.stonePassResult(mOp,statusCode,"");
                        }
                    } catch (JSONException e) {
                        Logs.printException(e);
                        ssenstonePassResult(mOp, 9999, "");
                        //mSpListener.stonePassResult(mOp,9999,"");
                    }
                }
            }
        };
        //mSSUserManager.setFidoListener(mFidoListener, mActivity, mUserName, message, fcode, mBioType);

        mFidoListenerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mSSUserManager.setFidoListener(mFidoListener, mActivity, mUserName, message, fcode, mBioType, true);
            }
        });
        mFidoListenerThread.start();
    }

    /***********************************************************************************************
     * PIN을 등록한다
     *
     * @param pin       입력 핀값
     * @param secureKey nullable
     * @return 리턴 코드값
     * P000 - success
     * P001 - network error
     * P003 - input values error
     * P004 - ocure exeption
     */
    public String registPin(String pin, byte[] secureKey) {
        try {

            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "USERADD");
            jsonServerRequest.put("PIN", pin);
            jsonServerRequest.put("USERID", mUserName);

            if (secureKey != null && secureKey.length > 0) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                String strSecureKey = Base64.encodeToString(secureKey, Base64.URL_SAFE);
                jsonServerRequest.put("BASE64PINSECKEY", strSecureKey);
            }

            String userName = LoginUserInfo.getInstance().getCUST_NM();
            if (TextUtils.isEmpty(userName)) {
                userName = "";
            } else {
                userName = Base64.encodeToString(userName.getBytes(), Base64.NO_WRAP);
            }

            jsonServerRequest.put("USERNAME", userName);
            jsonServerRequest.put("SSID", mSsId);
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            jsonServerRequest.put("AUTHTYPE", 0x1);
            jsonServerRequest.put("MULTIAUTH", 0x0);
            jsonServerRequest.put("ARSPIN", AES256Utils.encodeAES(pin));

            Logs.i("registPin - jsonServerRequest: " + jsonServerRequest.toString());

            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            Logs.i("registPin - strRet : " + strRet);

            if (!TextUtils.isEmpty(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    return jsonObject.optString("ERRORCODE");
                }
            }

        } catch (Exception e) {
            Logs.printException(e);
        } finally {
        }

        return "P004";
    }


    /**
     * PIN 인증한다 (APP 인증)
     *
     * @param pin       입력 핀값
     * @param secureKey nullable
     * @param signData  전자서명값
     * @return P000 - success
     * P001 - network error
     * P003 - input values error
     * P004 - ocure exeption
     */
    public String authenticatePin(String pin, byte[] secureKey, String signData, String signDocData, String ssid, long startTime) {
        try {
            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "AUTH1");
            jsonServerRequest.put("PIN", pin);
            jsonServerRequest.put("USERID", mUserName);

            if (secureKey != null && secureKey.length > 0) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                String strSecureKey = Base64.encodeToString(secureKey, Base64.URL_SAFE);
                jsonServerRequest.put("BASE64PINSECKEY", strSecureKey);
            }

            if (!TextUtils.isEmpty(signData)) {
                String encodedStr = Base64.encodeToString(signData.getBytes(), Base64.NO_WRAP);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(encodedStr);
                jsonServerRequest.put("SIGNDATATOPKCS7", jsonArray); // 서명할 데이터(계좌이체시 필요함)
            }

            if (!TextUtils.isEmpty(signDocData)) {
                String encodedStr = Base64.encodeToString(signDocData.getBytes(), Base64.NO_WRAP);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(encodedStr);
                jsonServerRequest.put("SIGNDATATOPKCS7DETACHED", jsonArray); // 서명할 문서데이터
            }

            long endTime = System.currentTimeMillis();
            long gapTime = endTime - startTime;

            // PIN 입력 시간 (사용자 행동 값)
            jsonServerRequest.put("GT", String.valueOf(gapTime));
            jsonServerRequest.put("SSID", ssid);
            //jsonServerRequest.put("SNUM", "1");
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            jsonServerRequest.put("AUTHTYPE", 0x1);
            jsonServerRequest.put("AUTHPURPOSE", 0x1);
            //jsonServerRequest.put("MULTIAUTH", 0x0);


            Logs.e("authenticatePin - : jsonServerRequest" + jsonServerRequest.toString());
            Logs.d("ssenstone", "---------- ssenstone start pin ----------");
            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            Logs.d("ssenstone", "---------- ssenstone end pin ----------");
            Logs.e("authenticatePin - : strRet" + strRet);

            if (!TextUtils.isEmpty(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    JSONObject jsonServerRequest2 = new JSONObject();
                    jsonServerRequest2.put("ACMD", "SENDSSOTP");
                    //jsonServerRequest2.put("DOCSIGN", "1");
                    strRet = mSSUserManager.StonePASS(jsonServerRequest2.toString());
                    if (!TextUtils.isEmpty(strRet)) {
                        Logs.e("strRet : " + strRet);
                        JSONObject jsonObject2 = new JSONObject(strRet);
                        Logs.e("jsonobject2 : " + jsonObject2);
                        String strResult2 = jsonObject2.getString("RESULT");
                        if (strResult2.equalsIgnoreCase("SUCCESS")) {
                            return "P000";
                        } else if (strResult.equalsIgnoreCase("ERROR")) {
                            String errCode = jsonObject.optString("ERRORCODE");
                            if ("P002".equals(errCode)) {
                                mErrDesc = jsonObject2.optString("DESCRIPTION");
                                mErrIP = jsonObject2.optString("client_ip");
                            }
                            return errCode;
                        }
                    }
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    String errCode = jsonObject.optString("ERRORCODE");
                    if ("P002".equals(errCode)) {
                        mErrDesc = jsonObject.optString("DESCRIPTION");
                        mErrIP = jsonObject.optString("client_ip");
                    }
                    return errCode;
                }
            }
        } catch (Exception e) {
            Logs.printException(e);
        }
        return "P004";
    }

    public String getErrorDesc() {
        return mErrDesc;
    }

    public String getErrorIP() {
        return mErrIP;
    }

    /**
     * 등록한 PIN 해지한다
     *
     * @param pin       입력핀값
     * @param secureKey nullable
     * @return P000 - success
     * P001 - network error
     * P003 - input values error
     * P004 - ocure exeption
     */
    public String deregistPin(String pin, byte[] secureKey) {
        try {

            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "DELUSER");
            jsonServerRequest.put("USERID", mUserName);
            jsonServerRequest.put("PIN", pin);

            if (secureKey != null && secureKey.length > 0) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                String strSecureKey = Base64.encodeToString(secureKey, Base64.URL_SAFE);
                jsonServerRequest.put("BASE64PINSECKEY", strSecureKey);
            }

            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);

            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());

            if (!TextUtils.isEmpty(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    String errCode = jsonObject.optString("ERRORCODE");
                    return errCode;
                }
            }

        } catch (Exception e) {
            Logs.printException(e);
        }
        return "P004";
    }

    /**
     * PIN 변경한다 (APP 인증)
     *
     * @param pin          입력핀코드
     * @param secureKey    nullable
     * @param newPin       신규핀코드
     * @param newSecureKey nullalbe
     * @return
     */
    public String changePin(String pin, String secureKey, String newPin, String newSecureKey) {
        try {

            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "CHANGEPIN");
            jsonServerRequest.put("PIN", pin);
            jsonServerRequest.put("USERID", mUserName);

            if (!TextUtils.isEmpty(secureKey)) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                jsonServerRequest.put("BASE64PINSECKEY", StonePassUtils.getBase64Key(secureKey));
            }

            if (!TextUtils.isEmpty(newSecureKey)) {
                jsonServerRequest.put("NEWBASE64PINSECKEY", StonePassUtils.getBase64Key(secureKey));
            }

            jsonServerRequest.put("NEWPIN", newPin);
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);

            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());

            if (!TextUtils.isEmpty(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    String errCode = jsonObject.optString("ERRORCODE");
                    return errCode;
                }
            }

        } catch (Exception e) {
            Logs.printException(e);
        }
        return "P004";
    }

    /*
     * 등록된 사용자 상태를 체크한다
     * @param userName 사용자 아이디
     * @param systemID 시스템 아이디
     */
    public synchronized String checkUserStatus(Context context, String pin) {
        try {

            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "CHECKUSERSTATUS");
            jsonServerRequest.put("USERID", mUserName);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            jsonServerRequest.put("TOKEN", mToken);

            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            if (!TextUtils.isEmpty(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    String errCode = jsonObject.optString("ERRORCODE");
                    return errCode;
                }
            }

        } catch (Exception e) {
            Logs.printException(e);
        }

        return "P004";
    }

    public interface StonePassInitListener {
        void stonePassInitResult();
    }

    public interface StonePassListener {
        void stonePassResult(String op, int errorCode, String errorMsg);
    }

    public interface FingerPrintDlgListener {
        void showFingerPrintDialog();
    }
}
