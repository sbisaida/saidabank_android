package com.sbi.saidabank.solution.cert;

// index=0&user=홍길동&issuer=INITECH&type=사설&expire=2006-04-12&expired=0
public class CertificateInfo {
	private int index;
	private String user;
	private String issuer;
	private String type;
	private String expireDate;
	private boolean isExpired;
	private boolean isInternal = false;
	private String beforeDate;
	private String serialNumber;

	// user 정보에 '&' 구분자가 있는 경우가 있어 가이드 수정 (예 : index=0&user=홍길동C&T&issuer=INITECH&type=사설&expire=2006-04-12&expired=0
	public CertificateInfo(String certStr) {
		int startPosition = 0;
		int endPosition = 0;


		// index
		int sPos = certStr.indexOf("index=");
		if(sPos >= 0){
			int ePos = certStr.indexOf("&user");
			if(ePos >= 0){
				String str = certStr.substring(sPos,ePos);
				String[] arr = str.split("=");
				index = Integer.parseInt(arr[1]);
			}
		}

		// user
		sPos = certStr.indexOf("&user");
		if(sPos >= 0){
			int ePos = certStr.indexOf("&issuer");
			if(ePos >= 0){
				String str = certStr.substring(sPos,ePos);
				String[] arr = str.split("=");
				user = arr[1];
			}
		}

		// issuer
		sPos = certStr.indexOf("&issuer");
		if(sPos >= 0){
			int ePos = certStr.indexOf("&type");
			if(ePos >= 0){
				String str = certStr.substring(sPos,ePos);
				String[] arr = str.split("=");
				issuer = arr[1];
			}
		}

		// type
		sPos = certStr.indexOf("&type");
		if(sPos >= 0){
			int ePos = certStr.indexOf("&expire");
			if(ePos >= 0){
				String str = certStr.substring(sPos,ePos);
				String[] arr = str.split("=");
				type = arr[1];
			}
		}

		sPos = certStr.indexOf("&expire");
		if(sPos >=0){
			//나머지를 여기서 잘라서 값을 분배한다.
			certStr = certStr.substring(sPos);

			String[] tmp2 = certStr.split("&");

			for(int j=0 ; j<tmp2.length ; j++) {
				String entry = tmp2[j];

				String[] arr = entry.split("=");
				if( arr[0].equals("expire") ){
					expireDate = arr[1];
				}else if( arr[0].equals("expired") ) {
					if( Integer.parseInt(arr[1]) == 0 )
						isExpired = false;
					else
						isExpired = true;
				} else if(arr[0].equals("internal") ) {
					if( Integer.parseInt(arr[1]) == 1 )
						isInternal = true;
				} else if(arr[0].equals("beforeDate") ) {
					beforeDate = arr[1];
				} else if(arr[0].equals("serialNumber") ) {
					serialNumber = arr[1];
				}
			}
		}
	}

	public CertificateInfo(int index, String user, String issuer, String type, String date, boolean isExpired, String beforeDate, String serialNumber) {
		this.index = index;
		this.user = user;
		this.issuer = issuer;
		this.type = type;
		this.expireDate = date;
		this.isExpired = isExpired;
		this.beforeDate = beforeDate;
		this.serialNumber = serialNumber;
	}

	public int getIndex() {
		return index;
	}

	public String getUser() {
		return user;
	}

	public String getIssuer() {
		return issuer;
	}

	public String getType() {
		return type;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public boolean isExpired() {
		return isExpired;
	}

	public boolean isInternal() {
		return isInternal;
	}

	public String getBeforeDate() {
		return beforeDate;
	}

	public String getSerialNumber() {
		return serialNumber;
	}
}
