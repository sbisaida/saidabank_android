package com.sbi.saidabank.solution.cert;

import com.sbi.saidabank.R;

public class CustomRow {
	private CertificateInfo uc;

	public CustomRow(int index, String user, String issuer, String type, String date, boolean isExpired, String beforeDate, String serialNumber) {
		uc = new CertificateInfo(index, user, issuer, type, date, isExpired, beforeDate, serialNumber);
	}
	
	public CustomRow(CertificateInfo uc) {
		this.uc = uc;
	}
	
	public int getIndex() {
		return uc.getIndex();
	}
	
	public int getCertIcon() {
		return R.drawable.ico_certificate;
	}
	
	public String getUserName() {
		return uc.getUser();
	}

	public String getIssuerName() {
		return uc.getIssuer();
	}

	public String getCertificateType() {
		return uc.getType();
	}

	public String getValidityDate() {
		return uc.getExpireDate();
	}

	public String getStorageType() {
		if (uc.isInternal()) {
			return "내부저장소";
		} else {
			return "SDcard";
		}
	}

	public String getSerialNumber() {
		return uc.getSerialNumber();
	}

	public String getBeforeDate() {
		return uc.getBeforeDate();
	}

	public boolean isExpired(){
		return uc.isExpired();
	}
}
