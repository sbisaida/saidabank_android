package com.sbi.saidabank.solution.appsflyer;

import android.content.Context;
import android.text.TextUtils;

import com.appsflyer.AppsFlyerLib;
import com.sbi.saidabank.common.util.JsonUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.datatype.LoginUserInfo;

import java.util.Map;

public class AppsFlyerManager {
    private static AppsFlyerManager instance = null;
    private static Context          mCtx;
    /**
     * Singleton instance 생성
     *
     * @param aCtx  컨테스트
     * @return instance
     */
    public static AppsFlyerManager getInstance(Context aCtx) {
        if (instance == null) {
            synchronized(AppsFlyerManager.class) {
                if (instance == null) {
                    instance = new AppsFlyerManager(aCtx);
                }
            }
        }
        return instance;
    }

    /**
     * 생성자
     *
     * @param aCtx  컨테스트
     */
    private AppsFlyerManager(Context aCtx) {
        mCtx = aCtx;
    }

    /**
     * AppsFlyer 에 원하는 이벤트를 전달시 호출되는 함수
     * 대시보드에 집계되기 위한 이벤트 정보를 전달할 때 호출되는 함수로 웹 컨텐츠로 부터 전달받은 이벤트명과 이벤트값을 셋팅하여 전달한다.
     *
     * @param eventName
     * @param eventValues
     */
    public void sendAppsFlyerTrackEvent(String eventName, String eventValues) {

        //if(eventValues == null || "".equals(eventValues)) return;

        //Map<String, Object> eventValuesMap = convertToEventValuesMap(eventValues);

        if(eventName == null || "".equals(eventName)) return;

        //if(eventValuesMap == null) return;

        Logs.d("AppsFlyer", "eventName : " + eventName);

        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
        if (!TextUtils.isEmpty(mbrNo))
            AppsFlyerLib.getInstance().setCustomerUserId(mbrNo);
        AppsFlyerLib.getInstance().trackEvent(mCtx, eventName, null);

        // AppsFlyer 이벤트 호출 예시
        //Map<String, Object> eventValue = new HashMap<String, Object>();
        //eventValue.put(AFInAppEventParameterName.REVENUE, 200);
        //eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
        //eventValue.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
        //eventValue.put(AFInAppEventParameterName.CURRENCY, "USD");
        //AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.PURCHASE, eventValue);

        /*public interface AFInAppEventParameterName {
            String LEVEL = "af_level";
            String SCORE = "af_score";
            String SUCCESS = "af_success";
            String PRICE = "af_price";
            String CONTENT_TYPE = "af_content_type";
            String CONTENT_ID = "af_content_id";
            String CONTENT_LIST = "af_content_list";
            String CURRENCY = "af_currency";
            String QUANTITY = "af_quantity";
            String REGSITRATION_METHOD = "af_registration_method";
            String PAYMENT_INFO_AVAILIBLE = "af_payment_info_available";
            String MAX_RATING_VALUE = "af_max_rating_value";
            String RATING_VALUE = "af_rating_value";
            String SEARCH_STRING = "af_search_string";
            String DATE_A = "af_date_a";
            String DATE_B = "af_date_b";
            String DESTINATION_A = "af_destination_a";
            String DESTINATION_B = "af_destination_b";
            String DESCRIPTION = "af_description";
            String CLASS = "af_class";
            String EVENT_START = "af_event_start";
            String EVENT_END = "af_event_end";
            String LATITUDE = "af_lat";
            String LONGTITUDE = "af_long";
            String CUSTOMER_USER_ID = "af_customer_user_id";
            String VALIDATED = "af_validated";
            String REVENUE = "af_revenue";
            String PROJECTED_REVENUE = "af_projected_revenue";
            String RECEIPT_ID = "af_receipt_id";
            String TUTORIAL_ID = "af_tutorial_id";
            String ACHIEVEMENT_ID = "af_achievement_id";
            String VIRTUAL_CURRENCY_NAME = "af_virtual_currency_name";
            String DEEP_LINK = "af_deep_link";
            String OLD_VERSION = "af_old_version";
            String NEW_VERSION = "af_new_version";
            String REVIEW_TEXT = "af_review_text";
            String COUPON_CODE = "af_coupon_code";
            String PARAM_1 = "af_param_1";
            String PARAM_2 = "af_param_2";
            String PARAM_3 = "af_param_3";
            String PARAM_4 = "af_param_4";
            String PARAM_5 = "af_param_5";
            String PARAM_6 = "af_param_6";
            String PARAM_7 = "af_param_7";
            String PARAM_8 = "af_param_8";
            String PARAM_9 = "af_param_9";
            String PARAM_10 = "af_param_10";
            String DEPARTING_DEPARTURE_DATE = "af_departing_departure_date";
            String RETURNING_DEPARTURE_DATE = "af_returning_departure_date";
            String DESTINATION_LIST = "af_destination_list";
            String CITY = "af_city";
            String REGION = "af_region";
            String COUNTRY = "af_county";
            String DEPARTING_ARRIVAL_DATE = "af_departing_arrival_date";
            String RETURNING_ARRIVAL_DATE = "af_returning_arrival_date";
            String SUGGESTED_DESTINATIONS = "af_suggested_destinations";
            String TRAVEL_START = "af_travel_start";
            String TRAVEL_END = "af_travel_end";
            String NUM_ADULTS = "af_num_adults";
            String NUM_CHILDREN = "af_num_children";
            String NUM_INFANTS = "af_num_infants";
            String SUGGESTED_HOTELS = "af_suggested_hotels";
            String USER_SCORE = "af_user_score";
            String HOTEL_SCORE = "af_hotel_score";
            String PURCHASE_CURRENCY = "af_purchase_currency";
            String PREFERRED_STAR_RATINGS = "af_preferred_star_ratings";
            String PREFERRED_PRICE_RANGE = "af_preferred_price_range";
            String PREFERRED_NEIGHBORHOODS = "af_preferred_neighborhoods";
            String PREFERRED_NUM_STOPS = "af_preferred_num_stops";
            String AF_CHANNEL = "af_channel";
            String CONTENT = "af_content";
            String AD_REVENUE_AD_TYPE = "af_adrev_ad_type";
            String AD_REVENUE_NETWORK_NAME = "af_adrev_network_name";
            String AD_REVENUE_PLACEMENT_ID = "af_adrev_placement_id";
            String AD_REVENUE_AD_SIZE = "af_adrev_ad_size";
            String AD_REVENUE_MEDIATED_NETWORK_NAME = "af_adrev_mediated_network_name";
        }

        public interface AFInAppEventType {
            String LEVEL_ACHIEVED = "af_level_achieved";
            String ADD_PAYMENT_INFO = "af_add_payment_info";
            String ADD_TO_CART = "af_add_to_cart";
            String ADD_TO_WISH_LIST = "af_add_to_wishlist";
            String COMPLETE_REGISTRATION = "af_complete_registration";
            String TUTORIAL_COMPLETION = "af_tutorial_completion";
            String INITIATED_CHECKOUT = "af_initiated_checkout";
            String PURCHASE = "af_purchase";
            String RATE = "af_rate";
            String SEARCH = "af_search";
            String SPENT_CREDIT = "af_spent_credits";
            String ACHIEVEMENT_UNLOCKED = "af_achievement_unlocked";
            String CONTENT_VIEW = "af_content_view";
            String TRAVEL_BOOKING = "af_travel_booking";
            String SHARE = "af_share";
            String INVITE = "af_invite";
            String LOGIN = "af_login";
            String RE_ENGAGE = "af_re_engage";
            String UPDATE = "af_update";
            String OPENED_FROM_PUSH_NOTIFICATION = "af_opened_from_push_notification";
            String LOCATION_CHANGED = "af_location_changed";
            String LOCATION_COORDINATES = "af_location_coordinates";
            String ORDER_ID = "af_order_id";
            String CUSTOMER_SEGMENT = "af_customer_segment";
            String SUBSCRIBE = "af_subscribe";
            String START_TRIAL = "af_start_trial";
            String AD_CLICK = "af_ad_click";
            String AD_VIEW = "af_ad_view";
        }*/
    }

    /**
     * 웹 컨텐츠로 부터 전달받은 jsonString 객체를 Map 객체로 변환하기 위한 컨버터
     * @param eventValues
     * @return Map
     */
    private Map<String, Object> convertToEventValuesMap(String eventValues) {

        Map<String, Object> convertDataMap = null;
        try {
            convertDataMap = JsonUtils.jsonStringToMap(eventValues);
        } catch (Exception e) {
            Logs.printException(e);
            convertDataMap = null;
        }
        return convertDataMap;
    }
}