package com.sbi.saidabank.solution.jex;

import com.sbi.saidabank.common.util.Logs;

import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * 암호화 String Util
 *
 * @author Webcash Smart
 * @since 2017. 12. 14.
**/
public class SecureString {
    /** 암호화 키 byte[] */
    private byte[] _CRYPT_KEY = new byte[64];
    /** 암호화 데이터 byte[] */
    private byte[] _buff = null;

    /**
     * 생성자
     * @param s 암호화할 평문 String
     */
    public SecureString(String s) {
        initKey();
        _buff = change(s.getBytes());
    }

    /**
     * 암호화 키 초기화 (생성)
     */
    private final void initKey() {
        SecureRandom random = new SecureRandom();
        random.nextBytes(_CRYPT_KEY);
    }

    /**
     * 암호화 byte[] -> String (복호화)
     * @return 평문 String
     */
    public String toString() {
        return new String(change(_buff));
    }

    /**
     * 데이터 변환 (암/복호화)
     * @param buff 변환할 데이터 byte[]
     * @return 변환 데이터 반환
     */
    private final byte[] change(byte[] buff) {
        byte[] result = new byte[buff.length];
        int nKey = 0;
        int nSize = buff.length;
        for (int i = 0; i < nSize; i++)
        {
            result[i] = (byte) (buff[i] ^ _CRYPT_KEY[nKey++]);
            if (nKey >= _CRYPT_KEY.length)
            {
                nKey = 0;
            }
        }
        return result;
    }


    /**
     *  SHA256 패턴 암호화
     * @param planText
     * @return
     */
    public static String encrypt(String planText) {
        try{
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(planText.getBytes());
            byte byteData[] = md.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<byteData.length;i++) {
                String hex=Integer.toHexString(0xff & byteData[i]);
                if(hex.length()==1){
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            return hexString.toString();
        }catch(Exception e){
            Logs.printException(e);
            throw new RuntimeException();
        }
    }
}
