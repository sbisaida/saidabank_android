package com.sbi.saidabank.solution.ocr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.JsonUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.selvasai.selvyocr.idcard.recognition.ImageRecognition;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Saidabanking_android
 * Class: OcrUtils
 * Created by 950469 on 2018. 8. 17..
 * <p>
 * Description:신분증 촬영에 필요한 함수들 모음.
 */
public class OcrUtils {
    public static String parsorIdcardData(ArrayList<String> resultText) {
        HashMap<String,Object> map = new HashMap<>();

        // 신분증 종류
        String typeString = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.TYPE.ordinal());
        map.put("idcard_type",typeString);

        // 면허번호
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String licenseNumber = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.LICENSE_NUMBER.ordinal());
            map.put("license_number",licenseNumber);
        }

        // 이름
        String name = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.NAME.ordinal());
        map.put("name",name);

        // 주민등록번호
        if (!typeString.equalsIgnoreCase(ImageRecognition.TYPE_ALIEN_CARD)) {
            String rrn = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.REGISTRATION_NUMBER.ordinal());
            map.put("RRN",rrn);
        }

        // 발급일
        String date = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.ISSUE_DATE.ordinal());
        map.put("issue_date",date);

        // 발급처
        if (!typeString.equalsIgnoreCase(ImageRecognition.TYPE_ALIEN_CARD)) {
            String oranization = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.ORGANIZATION.ordinal());
            map.put("oranization",oranization);
        }

        // Driver license ACFN
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String license_acfn = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.ACFN.ordinal());
            map.put("license_acfn",license_acfn);
        }

        // Driver license aptitude date
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String license_apitude_date = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.LICENSE_APTITUDE_DATE.ordinal());
            map.put("license_apitude_date",license_apitude_date);
        }

        // Driver license type
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String license_type = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.LICENSE_TYPE.ordinal());
            map.put("license_type",license_type);
        }

        String jsonStr = JsonUtils.mapToJson(map).toString();

        Logs.i(jsonStr);

        return jsonStr;
    }

    public static Bitmap makeMaskImg(Intent data){
        Bitmap idCardBitmap=null;

        // 신분증 이미지 영역
        byte[] encryptImage = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);

        // 주민번호 마스킹 영역
        Rect rrnRect = data.getParcelableExtra(CameraActivity.DATA_RRN_RECT);

        // 면허번호 마스킹 영역
        Rect licenseNumberRect = data.getParcelableExtra(CameraActivity.DATA_LICENSE_NUMBER_RECT);

        // 신분증 증명사진 영역
        Rect photoRect = data.getParcelableExtra(CameraActivity.DATA_PHOTO_RECT);

        if (encryptImage == null || rrnRect == null || licenseNumberRect == null) {
            return null;
        } else {

            //마스킹이미지
            //byte[] maskingImage;
            //사진영역 이미지
            //byte[] faceImage;

            if (encryptImage != null) {
                if (idCardBitmap != null && !idCardBitmap.isRecycled()) {
                    idCardBitmap.recycle();
                }

                idCardBitmap = ImgUtils.byteArrayToBitmap(encryptImage);

                if (idCardBitmap != null && rrnRect != null && licenseNumberRect != null) {
                    Canvas c = new Canvas(idCardBitmap);
                    Paint paint = new Paint();
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(Color.BLACK);
                    //주민번호 영역 마스킹
                    c.drawRect(rrnRect, paint);
                    //면허번호 영역 마스킹

                    double shiftlength = (licenseNumberRect.right - licenseNumberRect.left) * 0.5;

                    licenseNumberRect.left = (int)(licenseNumberRect.left + shiftlength);
                    licenseNumberRect.right = (int)(licenseNumberRect.right + shiftlength);

                    c.drawRect(licenseNumberRect, paint);
                    //증명사진 영역 마스킹
                    //paint.setStyle(Paint.Style.STROKE);
                    //paint.setColor(Color.RED);
                    //c.drawRect(photoRect, paint);

                    //신분증 이미지에서 얼굴이미지를 잘라냄
                    Bitmap bitmapFace = ImgUtils.cropBitmap(idCardBitmap, photoRect);

                    //maskingImage = OcrUtils.bitmapToByteArray(mIDCardBitmap);
                    //faceImage    = OcrUtils.bitmapToByteArray(bitmapFace);

                    return idCardBitmap;
                }

            }
        }

        return null;
    }


}
