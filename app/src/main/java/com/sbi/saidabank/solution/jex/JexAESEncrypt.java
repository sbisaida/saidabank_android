package com.sbi.saidabank.solution.jex;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.webcash.sws.secure.AESUtils;
import com.webcash.sws.secure.HexUtils;

public class JexAESEncrypt {

    private static final byte[] keyValue = new byte[] {0x64,0x70,0x74,0x6D,0x71,0x6C,0x64,0x6B,0x64,0x6C,0x74,0x6B,0x64,0x6C,0x65,0x6B,0x71,0x6F,0x64,0x7A,0x6D};

    private static String getKeyStr(){
        return new String(keyValue);
    }

    public static  String encrypt(String plainText) throws Exception {

        //+ 키생성
        String cipherKey = SecureString.encrypt(getKeyStr()).toLowerCase().substring(10, 26);
        cipherKey =  HexUtils.toHex(cipherKey.getBytes());

        return AESUtils.encryptTypePKCS5Padding(cipherKey,plainText);
    }

    public static String decrypt(String encText) throws Exception {

        //+ 키생성
        String cipherKey = SecureString.encrypt(getKeyStr()).toLowerCase().substring(10, 26);
        cipherKey =  HexUtils.toHex(cipherKey.getBytes());

        return AESUtils.decryptTypePKCS5Padding(cipherKey, encText);
    }
}
