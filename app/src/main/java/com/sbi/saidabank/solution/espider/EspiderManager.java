package com.sbi.saidabank.solution.espider;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Switch;

import com.heenam.espider.Engine;
import com.heenam.espider.EngineInterface;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.web.JavaScriptApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class EspiderManager implements EngineInterface {
    public static final String TAG = EspiderManager.class.getSimpleName();

    private static EspiderManager instance = null;
    private static Context mCtx;

    private Engine mEngine = null; // Espider Engine

    private ArrayList<HashMap<String, HashMap<String, String>>> mJobList; // 스크래핑 될 jobList
    private ArrayList<EspiderData> mJobResultList; //모듈조회시 결과 데이터

    private Handler mHandler;
    private EspiderEventListener mListner = null;

    private CertFileFilter mCertFileFilter;

    private String mCertPath = "";
    private String mCertPassword = "";
    private String mIdentifyNo = "";
    private String mCn;
    private String mDn;
    private String mJavaCallBackName;

    /**
     * Singleton instance 생성
     *
     * @param aCtx 컨테스트
     * @return instance
     */
    public static EspiderManager getInstance(Context aCtx) {
        if (instance == null) {
            synchronized (EspiderManager.class) {
                if (instance == null) {
                    instance = new EspiderManager(aCtx);
                }
            }
        }
        return instance;
    }

    /**
     * EspiderManager 개체 초기화
     */
    public static void clearInstance(){
        if(instance == null)
            return;

        instance.clearEngine();
        instance.removeEspiderEventListener();
        instance = null;
    }

    /**
     * Engine 초기화
     */
    private void clearEngine(){
        if(mEngine == null)
            return;

        mEngine.stopEngine();
        mEngine = null;
    }

    /**
     * Engine 사용가능한 상태인지 확인
     * @return 사용가능하면 true
     */
    public static boolean isEngineEnabled(){
        if(instance != null){
            return (instance.mEngine != null) ? true:false;
        }
        else{
            return false;
        }
    }

    /**
     * 생성자
     *
     * @param aCtx 컨테스트
     */
    private EspiderManager(Context aCtx) {
        //Context를 받는다.
        mCtx = aCtx;
        mHandler = new Handler();
        initList();
    }

    /**
     * Espider 이벤트 리스너를 등록한다.
     *
     * @param listner
     */
    public void setEspiderEventListener(EspiderEventListener listner) {
        mListner = listner;
    }

    /**
     * Espider 이벤트 리스너를 해제한다.
     *
     * @param
     */
    public void removeEspiderEventListener() {
        mListner = null;
    }

    public void startEspider() {
        try {
            mEngine = Engine.getInstatnce(mCtx);  // 엔진의 Instatnce를 가져옵니다.
            mEngine.setInterface(this);          // 엔진 실행시 콜백 받을 Class
            mEngine.setLicenseKey(LicenseKey.ESPIDER_LICENSE_KEY); // 엔진 라이센스키

            Logs.i(TAG, String.format("ENGINE_VERSION = [%s]", mEngine.getVersion()));
            Logs.i(TAG, String.format("ENGINE_DEVICE_APP_ID = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_APP_ID)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_APP_VERSION = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_APP_VERSION)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_UNIQUE_ID = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_UNIQUE_ID)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_UUID = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_UUID)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_MANUFACTURER = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_MANUFACTURER)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_MODEL = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_MODEL)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_OS_NAME = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_OS_NAME)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_OS_VERSION = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_OS_VERSION)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_PLATFORM = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_PLATFORM)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_PLATFORM_NAME = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_PLATFORM_NAME)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_USER_NAME = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_USER_NAME)));
            Logs.i(TAG, String.format("ENGINE_DEVICE_LOCALE_NAME = [%s]", mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_LOCALE_NAME)));

            mEngine.stopEngine();       // 기존 엔진이 실행중이면, 중단
            mEngine.setThreadCount(8);  // 스레드의 개수 설정
            mEngine.setAutoStop(true);  // 엔진 자동중단 실행 여부
            mEngine.startEngine();      // 엔진 기동
            mEngine.startJob();         // 추가된 모듈리스트 조회
        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 사용 리스트를 초기화 한다
     */
    public void initList() {
        if (mJobList == null)
            mJobList = new ArrayList<>();
        else
            mJobList.clear();

        if (mJobResultList == null)
            mJobResultList = new ArrayList<>();
        else
            mJobResultList.clear();
    }

    /**
     * 엔진에 실행중인 모듈List의 갯수를 엔진에 전달한다
     *
     * @param engine Espider Engine
     */
    @Override
    public int numberOfJobInEngine(Engine engine) {
        return mJobList.size();
    }

    /**
     * 실행할 로그인정보(loginInfo) 및 파라메터(paramInfo)를 엔진에 전달하는 Callback 함수
     *
     * @param engine   Espider Engine
     * @param jobIndex
     */
    @Override
    public HashMap<String, String> engineGetJob(Engine engine, int jobIndex) {
        //Logs.i(TAG, "engineGetJob jobIndex[" + String.valueOf(jobIndex) + "]");

        if (mJobList.size() <= jobIndex)
            return null;

        HashMap<String, HashMap<String, String>> element = mJobList.get(jobIndex);
        if (element.containsKey(Engine.ENGINE_JOB_MODULE_KEY)) {
            return element.get(Engine.ENGINE_JOB_MODULE_KEY);
        }

        return null;
    }

    /**
     * 실행할 로그인정보(loginInfo) 및 파라메터(paramInfo)를 엔진에 전달하는 Callback 함수
     */
    @Override
    public String engineGetParam(Engine engine, int threadIndex, int jobIndex, String requireJSONString, boolean bSynchronous) {
        //Logs.i(TAG, "engineGetParam threadIdx[" + String.valueOf(threadIndex) + "] jobIndex[" + String.valueOf(jobIndex)
        //        + "] requireJSONString[" + requireJSONString + "] bSynchronous [" + String.valueOf(bSynchronous) + "]");

        try {
            JSONObject requireJson = new JSONObject(requireJSONString);

            if (bSynchronous) {
                if (requireJson.has(Engine.ENGINE_JOB_PARAM_LOGIN_KEY)) { //로그인정보(loginInfo) 엔진에 전달
                    JSONObject reqJobItem = (JSONObject) requireJson.get(Engine.ENGINE_JOB_PARAM_LOGIN_KEY);
                    HashMap<String, String> jobSourceItem = (HashMap<String, String>) ((HashMap<String, HashMap<String, String>>) mJobList.get(jobIndex))
                            .get(Engine.ENGINE_JOB_PARAM_LOGIN_KEY);

                    Iterator<String> itr = jobSourceItem.keySet().iterator();
                    while (itr.hasNext()) {
                        String key = (String) itr.next();
                        reqJobItem.put(key, jobSourceItem.get(key));
                    }
                }

                if (requireJson.has(Engine.ENGINE_JOB_PARAM_INFO_KEY)) { //파라메터(paramInfo) 엔진에 전달
                    JSONObject reqJobItem = (JSONObject) requireJson.get(Engine.ENGINE_JOB_PARAM_INFO_KEY);
                    HashMap<String, String> jobSourceItem = (HashMap<String, String>) ((HashMap<String, HashMap<String, String>>) mJobList.get(jobIndex))
                            .get(Engine.ENGINE_JOB_PARAM_INFO_KEY);

                    Iterator<String> itr = jobSourceItem.keySet().iterator();
                    while (itr.hasNext()) {
                        String key = (String) itr.next();
                        reqJobItem.put(key, jobSourceItem.get(key));
                    }
                }

                if (requireJson.has(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY)) { //파라메터(paramInfo) 엔진에 전달
                    JSONObject reqJobItem = (JSONObject) requireJson.get(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY);
                    HashMap<String, String> jobSourceItem = (HashMap<String, String>) ((HashMap<String, HashMap<String, String>>) mJobList.get(jobIndex))
                            .get(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY);

                    Iterator<String> itr = jobSourceItem.keySet().iterator();
                    while (itr.hasNext()) {
                        String key = (String) itr.next();
                        reqJobItem.put(key, jobSourceItem.get(key));
                    }
                }

                String retString = requireJson.toString();
                Logs.d(TAG, "requireJson :" + requireJson.toString());
                return retString;
            } else {
                // ParamEx
                if (requireJson.has(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY)) {
                    JSONObject reqJobItem = requireJson.getJSONObject(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY);
                    //2Way로 처리 해야 하는 파라미터를 추가적으로 진행합니다. (Alert형태로 진행하였음.)
                    //getParam2Way(reqJobItem, threadIndex, jobIndex);
                } else {
                    return null;
                }
            }
            return null;
        } catch (Exception e) {
            Logs.printException(e);
        }
        return null;
    }

    /**
     * 엔진이 모듈을 실행 후 결과를 가져올 때 호출된다 (각 조회된 데이터를 처리한다)
     */
    @Override
    public void engineResult(Engine engine, int threadIndex,
                             final int jobIndex, int error, String userError, final String errorMessage, String resultJsonString) {
        //Logs.e(this.getClass().getName(), "engineResult threadIdx[" + String.valueOf(threadIndex) + "] jobIndex[" + String.valueOf(jobIndex) + "] error["
        //      + String.valueOf(error & 0xFFFF) + "] userError[" + userError + "] errorMessage [" + errorMessage + "] resultJsonString [" + resultJsonString + "]");

        synchronized (EspiderManager.class) {
            EspiderData espiderData = mJobResultList.get(jobIndex);
            String code = String.valueOf(error & 0xFFFF);
            espiderData.setStatus(code);

            if (error != 0) {
                String result = "";
                if (!errorMessage.equals(""))
                    result = errorMessage;
                else
                    result = code;

                espiderData.setResult(result);
            } else {
                espiderData.setResult(resultJsonString);
            }

            mJobResultList.set(jobIndex, espiderData);
            /* 모든 job요청이 완료된 후에 한 번만 리턴하도록 수정
            int type = espiderData.getType();
            if (mListner != null)
                mListner.onFinishEspider(type, jobIndex, code, espiderData.getResult());
            */
        }
    }

    /**
     * 엔진시스템의 Error가 발생시 호출된다
     */
    @Override
    public void engineSystemError(Engine engine, int error, String errorMessage) {
        //Logs.i(TAG, "engineSystemError error[" + String.valueOf(error & 0xFFFF) + "] errormessage [" + errorMessage + "]");
        if(mListner != null)
            mListner.onStatusEspider(mJavaCallBackName, error, errorMessage);
    }

    /**
     * 엔진의 상태를 가져온다. (status 값이 0 이면 모든 작업이 완료상태)
     */
    @Override
    public void engineStatus(Engine engine, int status) {
        //Logs.i(this.getClass().getName(), "engineStatus status[" + String.valueOf(status) + "]");

        if (status == 0) {
            //모듈리스트의 업무조회 완료 후 에러가 있을 경우 Error를 출력하는 alertDialog를 실행합니다.
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(mEngine != null)
                        mEngine.stopEngine();
                    if (mListner != null) {
                        mListner.onFinishEspider(mJavaCallBackName,getResultJsonObject());
                        mListner.onStatusEspider(mJavaCallBackName,0, "");
                    }
                }
            });
        }
    }

    /**
     * 실행중인 모듈의 진행중인 상태를 status로 확인할 수 있습니다.(시작, 진행중, 완료 등등)
     */
    @Override
    public void engineJobStatus(Engine engine, int threadIndex, final int jobIndex, final int status) {
        //Logs.i(this.getClass().getName(), "engineJobStatus threadIdx[" + String.valueOf(threadIndex) + "] jobIndex[" + String.valueOf(jobIndex) + "] status["
        //        + String.valueOf(status) + "]");
    }

    /**
     * 실행중인 모듈의 진행상태를 percent로 확인할 수 있습니다.(Progress를 처리 가능.)
     */
    @Override
    public void engineJobPercent(Engine engine, int threadIndex, int jobIndex, final int percent) {
        //Logs.i(this.getClass().getName(), "engineJobPercent threadIdx[" + String.valueOf(threadIndex) + "] jobIndex[" + String.valueOf(jobIndex) + "] percent["
        //        + String.valueOf(percent) + "]");
    }

    /**
     * 건강보험 보험료납부확인
     *
     * @param startDate    조회 시작 년월 (yyyyMM)
     * @param endDate      사용용도 (yyyyMM)
     * @param isShowOrigin 원문 DATA 포함 여부 (default: 미포함)
     */
    public void setHealthInsurancePay(String startDate, String endDate, boolean isShowOrigin) {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();   // 실행 할 모듈정보
            HashMap<String, String> loginInfo = new HashMap<>();    // Login 정보 Parameter
            HashMap<String, String> paramInfo = new HashMap<>();    // 업무에 필요한 추가정보 Parameter
            HashMap<String, String> paramExInfo = new HashMap<>();  // 업무에 필요한 추가정보 Parameter2
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>(); // 실행할 모듈 전체정보

            /**
             * 서비스 명세서를 확인 하여 모듈 정보를 세팅한다
             */
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0002");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010020");

            /**
             * 서비스 명세서를 확인하여 필요한 로그인 정보를 세팅한다
             */
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der")); // 조회할 인증서 signCert.der파일경로
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));   // 조회할 인증서 signPri.key파일경로
            loginInfo.put("reqCertPass", mCertPassword); // 인증서 패스워드
            loginInfo.put("reqIdentity", mIdentifyNo);   // 주민등록번호

            /**
             * 서비스 명세서를 확인하여 필요한 파라미터 정보를 세팅한다
             */
            paramInfo.put("reqUseType", "00");
            paramInfo.put("reqUsePurposes", "2");
            paramInfo.put("commStartDate", startDate);
            paramInfo.put("commEndDate", endDate);

            if (isShowOrigin)
                paramInfo.put("reqOriginDataYN", "1");
            else
                paramInfo.put("reqOriginDataYN", "0");

            // 모듈 실행될 전체 데이터
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule); // 모듈 실행리스트에 추가

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_HEALTH_PAY);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 건강보험 자격득실확인
     *
     * @param isShowJumin  주민번호 뒷자리 공개여부
     * @param isShowOrigin 원문 DATA 포함 여부 (default: 미포함)
     */
    public void setHealthInsuranceQualification(boolean isShowJumin, boolean isShowOrigin) {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0002");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010010");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            paramInfo.put("reqUseType", "0");

            if (isShowJumin)
                paramInfo.put("reqIsIdentityViewYn", "1");
            else
                paramInfo.put("reqIsIdentityViewYn", "0");

            if (isShowOrigin)
                paramInfo.put("reqOriginDataYN", "1");
            else
                paramInfo.put("reqOriginDataYN", "0");

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_HEALTH_QUALIFICATION);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 국민연금 가입자증명 조회
     */
    public void setNationalPensionSubscribers() {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "000050");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_NATIONAL_SUBSCRIBERS);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 국민연금 가입증명서 조회
     */
    public void setNationalPensionJoin() {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010040");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_NATIONAL_PAY);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 국민연금 연금산정용 가입내역확인서 조회
     */
    public void setNationalPensionJoinHistory() {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010060");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 국세청 사업자 등록증명
     *
     * @param identity      사업자번호
     * @param isShowJumin   주민번호 뒷자리 공개여부
     * @param isShowAddress 주소 공개여부
     * @param isShowOrigin  원문 DATA 포함 여부 (default: 포함)
     */
    public void setRevenueBusinessRegister(String identity, boolean isShowJumin, boolean isShowAddress, boolean isShowOrigin) {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010030");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);

            if (!TextUtils.isEmpty(identity))
                paramInfo.put("reqIdentity", identity);

            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");

            if (isShowJumin)
                paramInfo.put("reqIsIdentityViewYn", "1");
            else
                paramInfo.put("reqIsIdentityViewYn", "0");

            if (isShowAddress)
                paramInfo.put("reqIsAddrViewYn", "1");
            else
                paramInfo.put("reqIsAddrViewYn", "0");

            if (isShowOrigin)
                paramInfo.put("reqOriginDataYN", "1");
            else
                paramInfo.put("reqOriginDataYN", "0");

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_REVENUE_REGISTER);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 국세청 소득금액 증명
     *
     * @param startDate     조회 시작 년월 - yyyy (작년기준 5년 이전)
     * @param endDate       조회 종료 년월 - yyyy (작년기준 5년 이전)
     * @param isShowJumin   주민번호 뒷자리 공개여부
     * @param isShowAddress 주소 공개여부
     * @param isShowOrigin  원문 DATA 포함 여부 (default: 포함)
     */
    public void setQualificationIncome(String username, String usertype, String startDate, String endDate, boolean isShowJumin,
                                       boolean isShowAddress, boolean isShowOrigin) {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "012000");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            // 비회원인증 시 주민번호, 이름 필수
            loginInfo.put("reqIdentity", mIdentifyNo);
            loginInfo.put("reqUserName", username);

            // 근로소득자용
            paramInfo.put("reqProofType", usertype);

            paramInfo.put("reqSearchStartYear", startDate);
            paramInfo.put("reqSearchEndYear", endDate);

            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");

            if (isShowJumin)
                paramInfo.put("reqIsIdentityViewYn", "1");
            else
                paramInfo.put("reqIsIdentityViewYn", "0");

            if (isShowAddress)
                paramInfo.put("reqIsAddrViewYn", "1");
            else
                paramInfo.put("reqIsAddrViewYn", "0");

            if (isShowOrigin)
                paramInfo.put("reqOriginDataYN", "1");
            else
                paramInfo.put("reqOriginDataYN", "0");

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_INCOME_QUALIFICATION);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 국세청 부가세과세표준증명
     *
     * @param identity      사업자번호
     * @param startDate     조회 시작 년월 - yyyyMM ([yyyy] 1분기: 5년전 ~ 작년, 그외: 4년전 ~ 금년, [MM] 1기:"01", 2기:"07")
     * @param endDate       조회 종료 년월 - yyyyMM ([yyyy] 1분기: 5년전 ~ 작년, 그외: 4년전 ~ 금년, [MM] 1기:"01", 2기:"07")
     *                      2013년 이후분에 대한 간이과세자의 과세표준증명원은 과세기간을 "1기"로 선택
     * @param isShowJumin   주민번호 뒷자리 공개여부
     * @param isShowAddress 주소 공개여부
     * @param isShowOrigin  원문 DATA 포함 여부 (default: 포함)
     */
    public void setRevenueTax(String identity, String startDate, String endDate, boolean isShowJumin,
                              boolean isShowAddress, boolean isShowOrigin) {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010040");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);

            paramInfo.put("commStartDate", startDate);
            paramInfo.put("commEndDate", endDate);

            if (!TextUtils.isEmpty(identity))
                paramInfo.put("reqIdentity", identity);

            if (isShowJumin)
                paramInfo.put("reqIsIdentityViewYn", "1");
            else
                paramInfo.put("reqIsIdentityViewYn", "0");

            if (isShowAddress)
                paramInfo.put("reqIsAddrViewYn", "1");
            else
                paramInfo.put("reqIsAddrViewYn", "0");

            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");

            if (isShowOrigin)
                paramInfo.put("reqOriginDataYN", "1");
            else
                paramInfo.put("reqOriginDataYN", "0");

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_REVENUE_TAX);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    /**
     * 부가가치세 면세사업자 수입금액증명
     *
     * @param identity    사업자번호
     * @param date        조회년도
     * @param isShowJumin 주민번호 뒷자리 공개여부
     */
    public void setRevenueTaxIncome(String identity, String date, boolean isShowJumin) {
        try {
            HashMap<String, String> moduleInfo = new HashMap<>();
            HashMap<String, String> loginInfo = new HashMap<>();
            HashMap<String, String> paramInfo = new HashMap<>();
            HashMap<String, String> paramExInfo = new HashMap<>();
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();

            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010050");

            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);

            paramInfo.put("reqSearchStartYear", date);
            paramInfo.put("reqSearchEndYear", date);

            if (!TextUtils.isEmpty(identity))
                paramInfo.put("reqIdentity", identity);

            if (isShowJumin)
                paramInfo.put("reqIsIdentityViewYn", "1");
            else
                paramInfo.put("reqIsIdentityViewYn", "0");

            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");

            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExInfo);

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_REVENUE_TAX);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    public ArrayList<EspiderData> getResultList() {
        return mJobResultList;
    }

    /**
     * 요청 타입에 따른 코드값 return
     * @param type 요청 타입
     * @return 요청타입에 따른 코드
     */
    private String getEspiderDataCode(int type) {
        String retdatatype = null;
        switch (type) {
            case Const.ESPIDER_TYPE_HEALTH_PAY: {    // 건강보험 납부
                retdatatype = Const.ESPIDER_TYPE_HEALTH_PAY_CODE;
                break;
            }
            case Const.ESPIDER_TYPE_HEALTH_QUALIFICATION: {    // 건강보험 자격득실
                retdatatype = Const.ESPIDER_TYPE_HEALTH_QUALIFICATION_CODE;
                break;
            }
            case Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY: {  // 국민연금 연금정산용 가입내역
                retdatatype = Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY_CODE;
                break;
            }
            case Const.ESPIDER_TYPE_REVENUE_TAX: {   // 국세청 부가세과세표준증명
                retdatatype = Const.ESPIDER_TYPE_REVENUE_TAX_CODE;
                break;
            }
            case Const.ESPIDER_TYPE_INCOME_QUALIFICATION: {   // 국세청 소득금액 증명
                retdatatype = Const.ESPIDER_TYPE_INCOME_QUALIFICATION_CODE;
                break;
            }
            default:
                break;
        }

        return retdatatype;
    }

    /**
     * 조회결과를 JsonObject로 정리하여 return
     * @return 조회결과
     */
    public JSONObject getResultJsonObject() {
        JSONObject jsonobj = new JSONObject();
        JSONArray jsonarray = new JSONArray();
        try {
            jsonobj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_COMPLETE);
            if (!TextUtils.isEmpty(mCn) && !TextUtils.isEmpty(mDn)) {
               jsonobj.put("cn", mCn);
               jsonobj.put("dn", mDn);
            }
            if (mJobResultList != null && mJobResultList.size() > 0) {
                for (Object obj : mJobResultList) {
                    JSONObject tmpjosobj = new JSONObject();
                    EspiderData espiderdata = (EspiderData) obj;
                    // error인 경우 errorCode 항목을 추가
                    if(!"0".equals(espiderdata.getStatus())){
                        tmpjosobj.put("errorCode", espiderdata.getStatus());
                    }
                    tmpjosobj.put("svc_id", getEspiderDataCode(espiderdata.getType()));
                    tmpjosobj.put("data", espiderdata.getResult());
                    jsonarray.put(tmpjosobj);
                }
            } else {    // resultdata가 없을 경우 공백으로 처리
                JSONObject tmpjosobj = new JSONObject();
                tmpjosobj.put("svc_id", "");
                tmpjosobj.put("data", "");
                jsonarray.put(tmpjosobj);
            }

            jsonobj.put("svc_list", jsonarray);

        } catch (JSONException e) {
            Logs.printException(e);
        }

        return jsonobj;
    }

    public int startEspider(int javaApiNum, String javaCallBack, String certpass, String certpath, String id, String svcdata, String cn, String dn){
        mJavaCallBackName = javaCallBack;
        mCn = cn;
        mDn = dn;
        switch (javaApiNum){
            case JavaScriptApi.API_600:
                return startEspider_API600(certpass, certpath, id, svcdata);
            case JavaScriptApi.API_601:
                return startEspider_API601(certpass, certpath, id, svcdata);
            default:
                break;
        }

        return Const.ESPIDER_TYPE_RETURN_FAIL;
    }

    /**
     * 자바스크립트 인터페이스 API 600번 요청 처리
     * @param certpass 인증서 암호
     * @param certpath 인증서 경로
     * @param id       주민번호
     * @param svcdata  서버로부터 요청받은 jsondata
     * @return ESPIDER_TYPE_RETURN_SUCCESS : 조회요청 성공, ESPIDER_TYPE_RETURN_FAIL : 조회 요청 실패
     */
    private int startEspider_API600(String certpass, String certpath, String id, String svcdata) {

        JSONObject jsonobj;
        JSONArray jsonarray;
        int i = 0;
        boolean hasHQC = false;
        ArrayList<String> al_startdate = new ArrayList<String>();
        ArrayList<String> al_enddate = new ArrayList<String>();

        initList();
        setPassword(certpass);
        setCertPath(certpath);
        setIdentifyNo(id);

        try {
            if (svcdata != null) {
                jsonobj = new JSONObject(svcdata);
                jsonarray = jsonobj.optJSONArray("svc_list");
                Logs.e("jsonaary data : " + jsonarray.toString());

                if (jsonarray != null && jsonarray.length() > 0) {
                    for (i = 0; i < jsonarray.length(); i++) {
                        JSONObject obj = jsonarray.getJSONObject(i);
                        if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_HEALTH_PAY_CODE)) {
                            al_startdate.add(obj.optString("start_date"));
                            al_enddate.add(obj.optString("end_date"));
                        } else if(obj.optString("svc_id").equals(Const.ESPIDER_TYPE_HEALTH_QUALIFICATION_CODE)) {
                            hasHQC = true;
                        }
                    }
                } else {
                    return Const.ESPIDER_TYPE_RETURN_FAIL;
                }
            } else {
                return Const.ESPIDER_TYPE_RETURN_FAIL;
            }
        } catch (JSONException e) {
            Logs.printException(e);
            return Const.ESPIDER_TYPE_RETURN_FAIL;
        }

        for (i = 0; i < al_startdate.size(); i++) {
            setHealthInsurancePay(al_startdate.get(i), al_enddate.get(i), false);
        }

        // 건강보험 자격득실
        if(hasHQC)
            setHealthInsuranceQualification(true, false);

        // espider start
        startEspider();

        return Const.ESPIDER_TYPE_RETURN_SUCCESS;
    }

    /**
     * 자바스크립트 인터페이스 API 601번 요청 처리
     * @param certpass 인증서 암호
     * @param certpath 인증서 경로
     * @param id       주민번호
     * @param svcdata  서버로부터 요청받은 jsondata
     * @return ESPIDER_TYPE_RETURN_SUCCESS : 조회요청 성공, ESPIDER_TYPE_RETURN_FAIL : 조회 요청 실패
     */
    private int startEspider_API601(String certpass, String certpath, String id, String svcdata) {

        JSONObject jsonobj;
        JSONArray jsonarray;
        int i = 0;
        boolean hasNPHC = false;
        ArrayList<String> alKrppStartdate = new ArrayList<String>();
        ArrayList<String> alKrppEnddate = new ArrayList<String>();
        ArrayList<String> alKrntStartdate = new ArrayList<String>();
        ArrayList<String> alKrntEnddate = new ArrayList<String>();
        ArrayList<String> alKrntUserType = new ArrayList<String>();
        String userName;

        initList();
        setPassword(certpass);
        setCertPath(certpath);
        setIdentifyNo(id);

        try {
            if (svcdata != null) {
                jsonobj = new JSONObject(svcdata);
                userName = jsonobj.optString("user_name");
                jsonarray = jsonobj.optJSONArray("svc_list");
                Logs.e("jsonaary data : " + jsonarray.toString());

                if (jsonarray.length() > 0) {
                    for (i = 0; i < jsonarray.length(); i++) {
                        JSONObject obj = jsonarray.getJSONObject(i);
                        if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_HEALTH_PAY_CODE)) {
                            alKrppStartdate.add(obj.optString("start_date"));
                            alKrppEnddate.add(obj.optString("end_date"));
                        } else if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_INCOME_QUALIFICATION_CODE)) {
                            alKrntStartdate.add(obj.optString("start_date"));
                            alKrntEnddate.add(obj.optString("end_date"));
                            alKrntUserType.add(obj.optString("reqProofType"));
                        } else if(obj.optString("svc_id").equals(Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY_CODE)) {
                            hasNPHC = true;
                        }
                    }
                } else {
                    return Const.ESPIDER_TYPE_RETURN_FAIL;
                }
            } else {
                return Const.ESPIDER_TYPE_RETURN_FAIL;
            }
        } catch (JSONException e) {
            Logs.printException(e);
            return Const.ESPIDER_TYPE_RETURN_FAIL;
        }

        for (i = 0; i < alKrppStartdate.size(); i++) {
            setHealthInsurancePay(alKrppStartdate.get(i), alKrppEnddate.get(i), false);
        }

        // 국민연금 연금정산용 가입내역 확인
        if(hasNPHC)
            setNationalPensionJoinHistory();

        // 국세청 소득금액 증명 2년
        for (i = 0; i < alKrntStartdate.size(); i++) {
            setQualificationIncome(userName, alKrntUserType.get(i), alKrntStartdate.get(i), alKrntEnddate.get(i), true, true, false);
        }

        // espider start
        startEspider();

        return Const.ESPIDER_TYPE_RETURN_SUCCESS;
    }

    /**
     * object의 Null상태 체크
     * @param object Null체크할 object
     * @return object가 null이면 true
     */
    private static boolean isNull(Object object) {
        if (object == null) {
            return true;
        }

        if (object instanceof String) {
            String str = (String) object;

            if (str.length() < 1 || str.equals("") || str.equals("null")) {
                return true;
            }
        }

        return false;
    }

    /**
     * 특정 파일이 특정경로에 존재하는지
     *
     * @param filePath 파일 경로
     * @param fileName 파일 이름
     * @return 파일 경로
     */
    private String getCertFileFullPath(String filePath, String fileName) {
        String result = "";

        try {
            File file = new File(filePath);

            if (mCertFileFilter == null) {
                mCertFileFilter = new CertFileFilter();
            }

            mCertFileFilter.setSearchFileName(fileName);

            File dirFiles[] = file.listFiles(mCertFileFilter);

            if (dirFiles != null && dirFiles.length > 0) {
                result = dirFiles[0].getAbsolutePath();
            }
        } catch (Exception e) {
            Logs.printException(e);
            result = "";
        }

        return result;
    }

    private class CertFileFilter implements FilenameFilter {
        private String searchFileName = "";

        public void setSearchFileName(String name) {
            searchFileName = name;
        }

        @Override
        public boolean accept(File dir, String name) {
            boolean isAccept = false;

            if (isNull(searchFileName) == false) {
                if (searchFileName.equalsIgnoreCase(name) == true) {
                    isAccept = true;
                } else {
                    isAccept = false;
                }
            }

            return isAccept;
        }
    }

    public String getCertPath() {
        return mCertPath;
    }

    public void setCertPath(String certPath) {
        mCertPath = certPath;
    }

    public void setPassword(String pw) {
        mCertPassword = pw;
    }

    public void setIdentifyNo(String num) {
        mIdentifyNo = num;
    }

    public static boolean validateDateFormat(String dateToValdate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMM");
        formatter.setLenient(false);
        try {
            Date parsedDate = formatter.parse(dateToValdate);
            System.out.println("++validated DATE TIME ++" + formatter.format(parsedDate));

        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * 조회 요청 결과를 전달받을 리스너
     */
    public interface EspiderEventListener {
        void onFinishEspider(String javaCallBackName,JSONObject returnjsonobj);

        void onStatusEspider(String javaCallBackName,int status, String msg);

        // 2Way 방식을 진행 할 때, 사용자 입력이 필요한 Alert을 생성한다
        // CapTcha(보안문자)를 입력받는 화면 구성이 필요하다
        //void onShowSecureNoDialog(final String url, final int threadCnt, final int jobIndex, final JSONObject paramEx);
        // 2Way 방식을 진행 할 때, 사용자 입력이 필요한 Alert을 생성한다
        // SMS 인증문자를 입력받는 화면 구성이 필요하다
        //void onShowSmsNoDialog(final String phoneNo, final int threadCnt, final int jobIndex, final JSONObject paramEx);
    }
}