package com.sbi.saidabank.activity.test;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.FileProvider;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.download.DownloadAsync;
import com.sbi.saidabank.common.download.DownloadCallback;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.WasServiceUrl;

import java.io.File;

/**
 * Created by 950469 on 2017-11-24.
 */

public class NewApkDownloadActivity extends BaseActivity implements DownloadCallback {
    private static final String APK_DOWN_URL = WasServiceUrl.SMP0030101A03.getServiceUrl();

    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apk_download);
        mContext = this;
        startDownLoadAPK(APK_DOWN_URL);
    }

    private void startDownLoadAPK(String url){
        Logs.e("startDownLoadAPK");
        DownloadAsync daa = new DownloadAsync(mContext,true,this);
        daa.execute(url);
    }

    @Override
    public void onDownloadComplete(String pathName, String fileName) {
        Logs.e("onDownloadComplete - download_path :" + pathName + "/" + fileName);

        File apkFile = new File(pathName,fileName);

        if(apkFile.exists()){

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){

                Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".fileprovider", apkFile);
                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setDataAndType( Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            }else{
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setDataAndType( Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(i);
            }

            finish();
        }else{
            Logs.shwoToast(mContext,fileName + "파일을 찾을수 없습니다.");
        }
    }
}
