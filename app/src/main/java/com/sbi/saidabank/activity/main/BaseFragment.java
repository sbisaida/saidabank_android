package com.sbi.saidabank.activity.main;

import android.content.Intent;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    public abstract void showProgress();
    public abstract void dismissProgress();

    @Override
    public void onResume() {
        super.onResume();
    }

    public void resumeFragment() {

    }

    public void pauseFragment() {

    }

    public void showPushNotiFragment() {

    }

    public void callGlobalBackPressed() {

    }

    public void fragmentForResult (int reqCode, int resCode, Intent intent) {

    }
}
