package com.sbi.saidabank.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.info.DeviceResult;
import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeRegActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.solution.espider.EspiderManager;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptApi;
import com.sbi.saidabank.web.JavaScriptBridge;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Saidabanking_android
 * Class: WebMainActivity
 * Created by 950469 on 2018. 8. 17..
 * <p>
 * Description:하이브리드 웹 처리를 위한 화면.
 * 모질라 PDF라이브러리를 사용함.
 */
public class WebTermsActivity extends BaseActivity implements View.OnClickListener{
    private WebView mWebView;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_terms);

        Intent intent = getIntent();
        String url =intent.getStringExtra("url");
        String title = intent.getStringExtra("title");
        TextView tvTitle = findViewById(R.id.tv_title);
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }

        findViewById(R.id.btn_confirm).setOnClickListener(this);

        mWebView = (WebView) findViewById(R.id.webview);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);

        //settings.setBuiltInZoomControls(true);
        //settings.setSupportZoom(true);

        FrameLayout webViewContainer = (FrameLayout) findViewById(R.id.webview_container);
        BaseWebClient client = new BaseWebClient(this, webViewContainer);
        mWebView.setWebViewClient(client);
        mWebView.setWebChromeClient(new WebChromeClient());

        try {
            String pathStr = URLEncoder.encode(url,"UTF-8");
            mWebView.loadUrl("file:///android_asset/pdfjs/web/viewer.html?file=" + pathStr);
        } catch (UnsupportedEncodingException e) {
            Logs.printException(e);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWebView.clearCache(true);
        mWebView.destroy();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        setResult(RESULT_OK);
        finish();
    }
}
