package com.sbi.saidabank.activity.common;

import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

/**
 * siadabank_android
 * Class: CompleteCostomerInfoConfirmActivity
 * Created by 950546.
 * Date: 2019-01-30
 * Time: 오전 9:50
 * Description: 고객정보 재확인 완료 화면
 */
public class CompleteCostomerInfoConfirmActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_costomerinfo_confirm);
        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
