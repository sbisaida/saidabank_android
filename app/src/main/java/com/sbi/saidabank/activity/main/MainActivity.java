package com.sbi.saidabank.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.customview.CustomViewPager;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.solution.cert.CustomCertManager;
import com.sbi.saidabank.solution.espider.EspiderManager;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.web.JavaScriptApi;

import java.util.Calendar;

public class MainActivity extends BaseActivity implements DatePickerCallback {
    private static final int         TABMENU_COUNT = 4;

    private String [] navLabels = {
            "홈",
            "상품",
            "MY",
            "전체"
    };

    private int[] tabIcons = {
            R.drawable.tabbar_menu_01,
            R.drawable.tabbar_menu_02,
            R.drawable.tabbar_menu_03,
            R.drawable.tabbar_menu_04
    };

    private int[] tabIconsP = {
            R.drawable.tabbar_menu_01_p,
            R.drawable.tabbar_menu_02_p,
            R.drawable.tabbar_menu_03_p,
            R.drawable.tabbar_menu_04_p
    };

    private MainPagerFragmentApdater mPagerFragmentApdater;
    private int                      mCurrentIndex;
    private String[]                 mCurrentWebPage = new String[TABMENU_COUNT];

    private BaseFragment[]           mMainFragment = new BaseFragment[TABMENU_COUNT];
    private CustomViewPager          mViewPager;
    private TabLayout                mBottomNavigationView;
    private boolean[]                mIsErrorPageLoading = new boolean[TABMENU_COUNT];
    private boolean[]                mIsFinishedPageLoading = new boolean[TABMENU_COUNT];

    private boolean                  mIsLeavedFromMainTab;
    private boolean                  mIsFinishedLoadingMainTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initUX();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if ((mCurrentIndex == Const.MainMenu_TYPE_Main || mCurrentIndex == Const.MainMenu_TYPE_My) && mMainFragment[mCurrentIndex] != null) {
            mMainFragment[mCurrentIndex].resumeFragment();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if ((mCurrentIndex == Const.MainMenu_TYPE_Main) && mMainFragment[mCurrentIndex] != null) {
            mMainFragment[mCurrentIndex].pauseFragment();
        }
    }

    @Override
    public void onBackPressed() {
        if (mCurrentIndex == Const.MainMenu_TYPE_Main) {
            if (mIsFinishedLoadingMainTab)
                showLogoutDialog();
        } else {
            mMainFragment[mCurrentIndex].callGlobalBackPressed();

            if (TextUtils.isEmpty(mCurrentWebPage[mCurrentIndex])) {
                showLogoutDialog();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCurrentIndex == Const.MainMenu_TYPE_My && mMainFragment[mCurrentIndex] != null) {
            mMainFragment[mCurrentIndex].fragmentForResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // mainactvity 종료 시 동작 중인 espidermanager 종료 처리
        EspiderManager.clearInstance();
        FDSManager.clearInstance();
        StonePassManager.clearInstance();
        MOTPManager.clearInstance();
        CustomCertManager.clearInstance();
    }

    private void initUX() {
        mCurrentIndex = 0;
        setLeavedFromMainTab(false);
        mViewPager = (CustomViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(TABMENU_COUNT);
        mPagerFragmentApdater = new MainPagerFragmentApdater(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerFragmentApdater);
        mViewPager.setPagingEnabled(false);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tab = mBottomNavigationView.getTabAt(position);
                tab.select();
                mCurrentIndex = position;
            }
        });

        initFooter();
    }

    private void initFooter() {
        mBottomNavigationView = (TabLayout) findViewById(R.id.bottom_navigation);
        mBottomNavigationView.addTab(mBottomNavigationView.newTab());
        mBottomNavigationView.addTab(mBottomNavigationView.newTab());
        mBottomNavigationView.addTab(mBottomNavigationView.newTab());
        mBottomNavigationView.addTab(mBottomNavigationView.newTab());

        for (int i = 0; i < mBottomNavigationView.getTabCount(); i++) {
            LinearLayout tab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.tab_bottom_main, null);

            //ImageView tabDot = (ImageView) tab.findViewById(R.id.imageview_tab_dot);
            ImageView tabIcon = (ImageView) tab.findViewById(R.id.imageview_tab_main);
            TextView tabLabel = (TextView) tab.findViewById(R.id.textview_tab_main);

            if(i == 0) {
                tabIcon.setImageResource(tabIconsP[i]);
                //tabDot.setVisibility(View.VISIBLE);
                tabLabel.setTextColor(getResources().getColor(R.color.black));
            } else {
                tabIcon.setImageResource(tabIcons[i]);
                //tabDot.setVisibility(View.INVISIBLE);
                tabLabel.setTextColor(getResources().getColor(R.color.color8994AA));
            }

            tabLabel.setText(navLabels[i]);

            // finally publish this custom view to navigation tab
            mBottomNavigationView.getTabAt(i).setCustomView(tab);
        }

        mBottomNavigationView.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mCurrentIndex = tab.getPosition();
                mViewPager.setCurrentItem(mCurrentIndex, false);

                if (mCurrentIndex == Const.MainMenu_TYPE_Main) {
                    mIsFinishedLoadingMainTab = false;
                }

                if ((mCurrentIndex == Const.MainMenu_TYPE_Main || mCurrentIndex == Const.MainMenu_TYPE_My) && mMainFragment[mCurrentIndex] != null) {
                    setLeavedFromMainTab(true);
                    mMainFragment[mCurrentIndex].resumeFragment();
                } else if ((mCurrentIndex == Const.MainMenu_TYPE_Goods || mCurrentIndex == Const.MainMenu_TYPE_Menu)
                        && mMainFragment[mCurrentIndex] != null && mIsErrorPageLoading[mCurrentIndex]
                        && !mIsFinishedPageLoading[mCurrentIndex]) {
                    mIsErrorPageLoading[mCurrentIndex] = false;
                    mMainFragment[mCurrentIndex].resumeFragment();
                }

                View tabView = tab.getCustomView();

                //ImageView tabDot = (ImageView) tabView.findViewById(R.id.imageview_tab_dot);
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.imageview_tab_main);
                TextView tabLabel = (TextView) tabView.findViewById(R.id.textview_tab_main);

                //tabDot.setVisibility(View.VISIBLE);
                tabIcon.setImageResource(tabIconsP[tab.getPosition()]);
                tabLabel.setTextColor(getResources().getColor(R.color.black));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View tabView = tab.getCustomView();

                //ImageView tabDot = (ImageView) tabView.findViewById(R.id.imageview_tab_dot);
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.imageview_tab_main);
                TextView tabLabel = (TextView) tabView.findViewById(R.id.textview_tab_main);

                //tabDot.setVisibility(View.INVISIBLE);
                tabIcon.setImageResource(tabIcons[tab.getPosition()]);
                tabLabel.setTextColor(getResources().getColor(R.color.color8994AA));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public synchronized void showProgress(int fragmentType) {
        Logs.e("showProgress : " + fragmentType);
        if (mMainFragment[fragmentType] != null)
            mMainFragment[fragmentType].showProgress();
    }

    public synchronized void dismissProgress(int fragmentType) {
        Logs.e("dismissProgress : " + fragmentType);
        if (mMainFragment[fragmentType] != null)
            mMainFragment[fragmentType].dismissProgress();
    }

    public void setErrorPageLoading(int fragmentType) {
        mIsErrorPageLoading[fragmentType] = true;
    }

    public void setFinishedPageLoading(int fragmentType) {
        Logs.e("setFinishedPageLoading : " + fragmentType);
        mIsFinishedPageLoading[fragmentType] = true;
    }

    public void setCurrentPage(int webViewFragmentType, String page){
        Logs.e("webViewFragmentType : " + webViewFragmentType);
        Logs.e("page : " + page);
        mCurrentWebPage[webViewFragmentType] = page;
    }

    public void selectTab(int index) {
        mCurrentIndex = index;
        mViewPager.setCurrentItem(mCurrentIndex, false);

        if (mCurrentIndex == Const.MainMenu_TYPE_Main) {
            mIsFinishedLoadingMainTab = false;
        }

        if ((mCurrentIndex == Const.MainMenu_TYPE_Main || mCurrentIndex == Const.MainMenu_TYPE_My) && mMainFragment[mCurrentIndex] != null) {
            mMainFragment[mCurrentIndex].resumeFragment();
        } else if ((mCurrentIndex == Const.MainMenu_TYPE_Goods || mCurrentIndex == Const.MainMenu_TYPE_Menu)
                && mMainFragment[mCurrentIndex] != null && mIsErrorPageLoading[mCurrentIndex]
                && !mIsFinishedPageLoading[mCurrentIndex]) {
            mIsErrorPageLoading[mCurrentIndex] = false;
            mMainFragment[mCurrentIndex].resumeFragment();
        }

        TabLayout.Tab tab = mBottomNavigationView.getTabAt(mCurrentIndex);
        tab.select();
    }

    public void showPushNotiFragment() {
        if (mCurrentIndex == Const.MainMenu_TYPE_Main) {
            mMainFragment[mCurrentIndex].showPushNotiFragment();
        }
    }

    /**
     * 메인탭 이동 여부 리턴
     * @return 로그인 후 메인탭을 이동했으면 true
     */
    public boolean isLeavedFromMainTab() {
        return mIsLeavedFromMainTab;
    }

    /**
     * 메인탭 이동 여부 셋팅
     * @param isLeavedFromMainTab 메인탭 이동한 경우 true
     */
    public void setLeavedFromMainTab(boolean isLeavedFromMainTab) {
        mIsLeavedFromMainTab = isLeavedFromMainTab;
    }

    /**
     * 메인탭 로딩 중 여부 에크
     * @param isFinishedLoading 로딩 끝난 경우 true
     */
    public void setMainTabFinishedLoading(boolean isFinishedLoading) {
        mIsFinishedLoadingMainTab = isFinishedLoading;
    }

    @Override
    public void onDateSet(long date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);

        int inputYear = calendar.get(Calendar.YEAR);
        int inputMonth = calendar.get(Calendar.MONTH) + 1;
        int inputDay = calendar.get(Calendar.DAY_OF_MONTH);

        Intent intent = new Intent();
        intent.putExtra("selected_date", date);

        if (mCurrentIndex == Const.MainMenu_TYPE_My && mMainFragment[mCurrentIndex] != null) {
            mMainFragment[mCurrentIndex].fragmentForResult(JavaScriptApi.API_250, RESULT_OK, intent);
        }
    }

    private class MainPagerFragmentApdater extends FragmentStatePagerAdapter {

        public MainPagerFragmentApdater(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case Const.MainMenu_TYPE_Main: {
                    String url = "";
                    if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL))
                        url = getIntent().getStringExtra(Const.INTENT_MAINWEB_URL);
                    mMainFragment[position] = new MainFragment(MainActivity.this, url);
                    return mMainFragment[position];
                }
                case Const.MainMenu_TYPE_Goods:
                case Const.MainMenu_TYPE_My:
                case Const.MainMenu_TYPE_Menu: {
                    mMainFragment[position] = new MainWebFragment(MainActivity.this, position);
                    return mMainFragment[position];
                }

                default: {
                    return null;
                }
            }
        }

        @Override
        public int getCount() {
            return TABMENU_COUNT;
        }
    }
}
