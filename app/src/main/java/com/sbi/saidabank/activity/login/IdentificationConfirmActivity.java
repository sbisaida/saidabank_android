package com.sbi.saidabank.activity.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.common.AnotherBankAccountVerifyActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.ShotIDFailDialog;
import com.sbi.saidabank.common.dialog.SlidingDriverAreaCodeDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomEditText;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.define.datatype.RequestCodeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: IdentificationConfirmActivity
 * Created by 950485 on 2018. 11. 08..
 * <p>
 * Description:신분증확인 완료 화면
 */

public class IdentificationConfirmActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged {
    private static final int REQ_OCR_ID = 100;
    private static final int STR_LENGTH_LICENSE_NUMBER = 10;
    private static final int STR_LENGTH_FORMAT_LICENSE_NUMBER = 12;
    private static final String ENCRYPT_KEY = "";
    private static final int SCROLL_EDITTEXT        = 0;
    private static final int SCROLL_CHECK_KEYBOARD  = 1;

    private ImageView      mImageIdentification;
    private LinearLayout   mLayoutName;
    private EditText       mEditUserName;
    private TextView       mTextNameMsg;
    private View           mViewLine01;
    private ScrollView     mSvScview;
    private TextView       mTextUserBirthday;
    private TextView       mTextUserGender;
    private View           mViewLine02;
    private LinearLayout   mLayoutLicenseNum;
    private TextView       mTextLicenseCode;
    private CustomEditText mEditLicenseNum;
    private LinearLayout   mLayoutIssuedDate;
    private EditText       mEditIssuedDate;
    private Button         mBtnConfirm;
    private Button         mBtnShoot;

    private CommonUserInfo mCommonUserInfo;
    private InputMethodManager imm = null;

    private ArrayList<RequestCodeInfo> mListArea = new ArrayList<>();
    private byte[]  mImageOCR;

    private WeakHandler mScrollHandler;
    private int mScrollOffset;
    private boolean mIsShownKeyboard;

    // 필요권한 - 카메라접근권한
    private String[] perList = new String[]{
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_identification_confirm);

        getExtra();
        initUX();
        requestDriverLicenseArea();
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
            showCancelMessage(getResources().getString(R.string.msg_cancel_pin_rereg));
            return;
        } else {
            Intent intent = new Intent(IdentificationConfirmActivity.this, IdentificationPrepareActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
        }
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showShootIdentification();
                } else {
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (!state) {
                        DialogUtil.alert(this, "권한설정", "닫기", getString(R.string.msg_permission_camera_allow),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(IdentificationConfirmActivity.this);
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                    }
                }
            }
            break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQ_OCR_ID:
                if (resultCode == CameraActivity.RETURN_OK && data != null) {
                    // 신분증 이미지 영역
                    byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
                    if (imageOCR == null)
                        return;

                    requestCheckIdentification(imageOCR);
                }
                break;

            default:
                break;
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        mImageOCR = getIntent().getByteArrayExtra(Const.INTENT_IDENTIFICATION_IMG);
    }

    /**
     * 화면 초기값
     */
    private void initUX() {
        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(this);

        findViewById(R.id.ll_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
                    imm.hideSoftInputFromWindow(mEditUserName.getWindowToken(), 0);
                    imm.hideSoftInputFromWindow(mEditLicenseNum.getWindowToken(), 0);
                } else {
                    imm.hideSoftInputFromWindow(mEditUserName.getWindowToken(), 0);
                    imm.hideSoftInputFromWindow(mEditIssuedDate.getWindowToken(), 0);
                }
            }
        });
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        //디바이스별 화면 사이즈에 따라 신분증 이미지 비율 조정
        int imgWidth = (int) Utils.dpToPixel(this, 330);
        int imgHeight = (int) Utils.dpToPixel(this, 240);
        int idWidth = outMetrics.widthPixels - (int) Utils.dpToPixel(this, 30);
        int idHeight = (imgHeight * idWidth) / idWidth;

        LinearLayout llId = findViewById(R.id.ll_id);
        ViewGroup.LayoutParams params = llId.getLayoutParams();
        params.height = idHeight;
        llId.setLayoutParams(params);
        llId.requestLayout();

        mImageIdentification = (ImageView) findViewById(R.id.imageview_identification_confirm);
        ViewGroup.LayoutParams imgParams = mImageIdentification.getLayoutParams();
        imgParams.width = idWidth - (int) Utils.dpToPixel(this, 30);
        imgParams.height = idHeight - (int) Utils.dpToPixel(this, 30);
        mImageIdentification.setLayoutParams(imgParams);
        mImageIdentification.requestLayout();

        mLayoutName = (LinearLayout) findViewById(R.id.layout_name_id_confirm);
        mEditUserName = (EditText) findViewById(R.id.edittext_uer_name_id_confirm);
        mTextNameMsg = (TextView) findViewById(R.id.textview_name_msg_id_confirm);
        mViewLine01 = (View) findViewById(R.id.view_line_01_id_confirm);
        mTextUserBirthday = (TextView) findViewById(R.id.textview_uer_birthday_id_confirm);
        mTextUserGender = (TextView) findViewById(R.id.textview_gender_id_confirm);
        mViewLine02 = (View) findViewById(R.id.view_line_02_id_confirm);
        mLayoutLicenseNum = (LinearLayout) findViewById(R.id.layout_license_number_id_confirm);
        mTextLicenseCode = (TextView) findViewById(R.id.textview_license_code);
        mEditLicenseNum = (CustomEditText) findViewById(R.id.edittext_license_num_id_confirm);
        mLayoutIssuedDate = (LinearLayout) findViewById(R.id.layout_issued_date);
        mEditIssuedDate = (EditText) findViewById(R.id.edittext_issued_date);
        mBtnConfirm = (Button) findViewById(R.id.btn_ok_identification_confirm);
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);

        mScrollHandler = new WeakHandler(this);
        mScrollOffset = (int)Utils.dpToPixel(this, (float)30f);

        mLayoutName.setBackgroundResource(R.drawable.background_box_top);
        mEditUserName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);

                String name = mEditUserName.getText().toString();
                if (!checkVaildUserName(name, hasFocus))
                    mBtnConfirm.setEnabled(false);
            }
        });

        mEditUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });

        mEditUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().matches("^[ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]*$")) {
                    mEditUserName.removeTextChangedListener(this);
                    mEditUserName.setText(editable.toString().replaceAll("[^ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]", ""));
                    mEditUserName.setSelection(mEditUserName.getText().length());
                    mEditUserName.addTextChangedListener(this);
                }
            }
        });

        mLayoutLicenseNum.setBackgroundResource(R.drawable.background_box_bottom);
        mEditLicenseNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
                    setLicenseNumberNormal();
                    mViewLine02.setBackgroundColor(getResources().getColor(R.color.color00A2B3));
                    mLayoutLicenseNum.setBackgroundResource(R.drawable.background_box_bottom_focused);
                } else {
                    if (((CustomEditText) view).getText().toString().length() == STR_LENGTH_LICENSE_NUMBER)
                        setLicenseNumberFormat();

                    mViewLine02.setBackgroundColor(getResources().getColor(R.color.colorE7E7E7));
                    mLayoutLicenseNum.setBackgroundResource(R.drawable.background_box_bottom);

                    String licenseNum = mEditLicenseNum.getText().toString();
                    if (!checkVaildLicenseNum(licenseNum))
                        mBtnConfirm.setEnabled(false);
                }
            }
        });

        mEditLicenseNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });

        mEditLicenseNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mEditLicenseNum.setOnBackPressListener(new CustomEditText.OnBackPressListener() {
            @Override
            public void onKeyboardBackPress() {

            }
        });

        mLayoutIssuedDate.setBackgroundResource(R.drawable.background_box_bottom);
        mEditIssuedDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
                    mViewLine02.setBackgroundColor(getResources().getColor(R.color.color00A2B3));
                    mLayoutIssuedDate.setBackgroundResource(R.drawable.background_box_bottom_focused);

                    if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD()))
                        return;

                    String date = mEditIssuedDate.getText().toString();
                    date = date.replaceAll("[^0-9]", "");
                    if (!checkVaildDate(date))
                        mBtnConfirm.setEnabled(false);
                    else
                        mBtnConfirm.setEnabled(true);

                    if (!TextUtils.isEmpty(date)) {
                        mEditIssuedDate.setText(date);
                    }
                } else {
                    mViewLine02.setBackgroundColor(getResources().getColor(R.color.colorE7E7E7));
                    mLayoutIssuedDate.setBackgroundResource(R.drawable.background_box_bottom);

                    if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD()))
                        return;

                    String date = mEditIssuedDate.getText().toString();
                    date = date.replaceAll("[^0-9]", "");
                    if (!checkVaildDate(date))
                        mBtnConfirm.setEnabled(false);
                    else
                        mBtnConfirm.setEnabled(true);

                    if (!TextUtils.isEmpty(date) && date.length() == 8) {
                        String dotAddDete  = date.substring(0, 4) + "." + date.substring(4, 6) + "." + date.substring(6, 8);
                        mEditIssuedDate.setText(dotAddDete);
                    }
                }
            }
        });

        mEditIssuedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });

        mEditIssuedDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD()))
                    return;

                if (editable.length() == 8) {
                    String date = editable.toString();
                    if (!checkVaildDate(date))
                        mBtnConfirm.setEnabled(false);
                    else
                        mBtnConfirm.setEnabled(true);
                } else if (editable.length() < 8) {
                    mBtnConfirm.setEnabled(false);
                }
            }
        });

        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_identification_confirm);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
                    showCancelMessage(getResources().getString(R.string.msg_cancel_pin_rereg));
                    return;
                } else {
                    goIdentificationPrepare();
                }
            }
        });

        if (mImageOCR != null) {
            Bitmap bitmapIdCard = ImgUtils.byteArrayToBitmap(mImageOCR);
            if (bitmapIdCard != null) {
                mImageIdentification.setImageBitmap(bitmapIdCard);
               paintMaskingArea(bitmapIdCard);
            }
        }

        if (!TextUtils.isEmpty(mCommonUserInfo.getName()))
            mEditUserName.setText(mCommonUserInfo.getName());

        if (!TextUtils.isEmpty(mCommonUserInfo.getIdnumber()) && mCommonUserInfo.getIdnumber().length() >= 7) {
            String day = mCommonUserInfo.getIdnumber().substring(0, 6);
            String gender = mCommonUserInfo.getIdnumber().substring(6, 7);
            mTextUserBirthday.setText(day);
            mTextUserGender.setText(gender);
        }

        if (!TextUtils.isEmpty(mCommonUserInfo.getIssueDate())) {
            String date = mCommonUserInfo.getIssueDate();

            if (Const.ID_TYPE_JUMIM.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
                if (!TextUtils.isEmpty(date) && date.length() == 8) {
                    String dotAddDete  = date.substring(0, 4) + "." + date.substring(4, 6) + "." + date.substring(6, 8);
                    mEditIssuedDate.setText(dotAddDete);
                } else {
                    mEditIssuedDate.setText(date);
                }
            }
        }

        mBtnShoot = (Button) findViewById(R.id.btn_retry_shoot_identification_confirm);
        mBtnShoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditUserName.isFocused()) {
                    Utils.hideKeyboard(IdentificationConfirmActivity.this, mEditUserName);
                }

                if (mEditIssuedDate.isFocused()) {
                    Utils.hideKeyboard(IdentificationConfirmActivity.this, mEditIssuedDate);
                }

                if (PermissionUtils.checkPermission(IdentificationConfirmActivity.this, perList, Const.REQUEST_PERMISSION_CAMERA)) {
                    showShootIdentification();
                }
            }
        });

        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                clearFocus();

                mCommonUserInfo.setName(mEditUserName.getText().toString());
                if (!TextUtils.isEmpty(mCommonUserInfo.getName()))
                    mCommonUserInfo.setName(mCommonUserInfo.getName().trim());

                if (!checkVaildUserName(mCommonUserInfo.getName(), mEditUserName.isFocused())) {
                    if (mEditUserName.isFocused()) {
                        Utils.hideKeyboard(IdentificationConfirmActivity.this, mEditUserName);
                    }
                    return;
                }

                if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
                    String code = mTextLicenseCode.getText().toString();
                    if (TextUtils.isEmpty(code)) {
                        Toast.makeText(IdentificationConfirmActivity.this, R.string.msg_no_input_driver_area_code, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                requestVerifyIdentification();
            }
        });

        LinearLayout btnCodeSelect = (LinearLayout) findViewById(R.id.layout_select_code);
        btnCodeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAreaCodeList();
            }
        });

        if (Const.ID_TYPE_JUMIM.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
            mLayoutLicenseNum.setVisibility(View.GONE);
        } else if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
            mCommonUserInfo.setDRVN_LCNS_SRNO(mCommonUserInfo.getDRVN_LCNS_SRNO().trim());
            mLayoutIssuedDate.setVisibility(View.GONE);
            mLayoutLicenseNum.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 신분증 촬영 화면 표시
     */
    private void showShootIdentification() {
        clearFocus();

        Intent intent = new Intent(IdentificationConfirmActivity.this, CameraActivity.class);
        intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
        intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
        intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
        intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
        intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, ENCRYPT_KEY);
        startActivityForResult(intent, REQ_OCR_ID);
    }

    /**
     * 이름 유효성 체크
     *
     * @param str      체크 이름값
     * @param hasfocus 포커스 여부
     * @return
     */
    private boolean checkVaildUserName(String str, boolean hasfocus) {
        int derrcode = Utils.isKorean(str);

        if (derrcode == Const.STATE_VALID_STR_NORMAL) {
            mTextNameMsg.setVisibility(View.GONE);
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_FOCUSED);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_NORMAL);
        } else {
            mTextNameMsg.setVisibility(View.VISIBLE);
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_ERROR);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_NORMAL);
            if (derrcode == Const.STATE_VALID_STR_LENGTH_ERR) {
                mTextNameMsg.setText(R.string.msg_invalid_name_length);
            } else if (derrcode == Const.STATE_VALID_STR_CHAR_ERR) {
                mTextNameMsg.setText(R.string.msg_invalid_special_char);
            }

            return false;
        }
        return true;
    }

    /**
     * 텍스트 입력 박스 background 처리
     *
     * @param boxdindex 문자 입력창 아이디
     * @param state     이름 유효성 체크 상태
     */
    private void setEditTextBackground(int boxdindex, int state) {
        switch (boxdindex) {
            case Const.INDEX_EDITBOX_NAME: {
                switch (state) {
                    case Const.STATE_EDITBOX_NORMAL: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top);
                        mViewLine01.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        break;
                    }

                    case Const.STATE_EDITBOX_FOCUSED: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top_focused);
                        mViewLine01.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        break;
                    }

                    case Const.STATE_EDITBOX_ERROR: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top_error);
                        mViewLine01.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        break;
                    }

                    default:
                        break;
                }
                break;
            }

            default:
                break;
        }
    }

    /**
     * 인증서 확인 설명창 이동
     */
    private void goIdentificationPrepare() {
        Intent intent = new Intent(IdentificationConfirmActivity.this, IdentificationPrepareActivity.class);
        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
        startActivity(intent);
        finish();
    }

    /**
     * 다음 화면(타행이체)으로 이동
     */
    private void goNextActivity() {
        Intent intent;

        intent = new Intent(IdentificationConfirmActivity.this, AnotherBankAccountVerifyActivity.class);
        intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD, "804");
        intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO, "");
        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
        startActivity(intent);
        finish();
    }

    /**
     * 운전면허지역번호
     */
    private void requestDriverLicenseArea() {
        Map param = new HashMap();
        param.put("LCCD","DRVN_LCNS_LCNO");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        RequestCodeInfo item = new RequestCodeInfo(obj);
                        mListArea.add(item);
                    }

                    if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
                        if (!TextUtils.isEmpty(mCommonUserInfo.getDRVN_LCNS_SRNO()) && mCommonUserInfo.getDRVN_LCNS_SRNO().length() == 10) {
                            String areaCode = mCommonUserInfo.getDRVN_LCNS_LCNO();
                            for (int index = 0; index < mListArea.size(); index++) {
                                RequestCodeInfo codeInfo = mListArea.get(index);
                                String code = codeInfo.getSCCD();
                                if (TextUtils.isEmpty(code))
                                    continue;

                                if (code.equalsIgnoreCase(areaCode)) {
                                    String name = codeInfo.getCD_NM();
                                    mTextLicenseCode.setText(name);
                                }
                            }

                            String licenseNum = mCommonUserInfo.getDRVN_LCNS_SRNO();
                            mEditLicenseNum.setText(licenseNum);
                            setLicenseNumberFormat();
                        }
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 신분증 확인 요청
     */
    private void requestCheckIdentification(final byte[] imageOCR) {
        String data = Base64.encodeToString(imageOCR, Base64.DEFAULT);
        if (TextUtils.isEmpty(data))
            return;

        Map param = new HashMap();
        param.put("IMG_CNTN", data);

        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
        if (TextUtils.isEmpty(mbrNo))
            mbrNo = mCommonUserInfo.getMBRnumber();
        param.put("MBR_NO", mbrNo);

        String custNo = LoginUserInfo.getInstance().getCUST_NO();
        param.put("CUST_NO", custNo);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010200A01 : " + ret);

                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getString(R.string.msg_check_identification));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_check_identification));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_check_identification));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if ("CMM0011".equals(errCode)) {
                            ShotIDFailDialog shotIDFailDialog = new ShotIDFailDialog(IdentificationConfirmActivity.this, Const.ID_SHOT_FAIL_SHOT);
                            shotIDFailDialog.setOnCofirmListener(new ShotIDFailDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    if (PermissionUtils.checkPermission(IdentificationConfirmActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                                        showShootIdentification();
                                    }
                                }
                            });
                            shotIDFailDialog.show();
                            return;
                        }
                        if (TextUtils.isEmpty(msg)) {
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        else {
                            CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    if (PermissionUtils.checkPermission(IdentificationConfirmActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                                        showShootIdentification();
                                    }
                                }
                            };
                            showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        }
                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        return;
                    }

                    // 처리 상태 ( 01 : 정상, 02 : 오류)
                    String trtmStcd = object.optString("TRTM_STCD");
                    if (Const.REQUEST_COMMON_FAIL_CODE.equalsIgnoreCase(trtmStcd)) {
                        String msg = object.optString("RESP_CNTN");
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.msg_check_identification);

                        Logs.e("msg : " + msg);
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (PermissionUtils.checkPermission(IdentificationConfirmActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                                    showShootIdentification();
                                }
                            }
                        };
                        showErrorMessage(msg, okClick);
                        return;
                    }

                    // 고객명
                    mCommonUserInfo.setName(object.optString("CUST_NM"));
                    mEditUserName.setText(mCommonUserInfo.getName());
                    checkVaildUserName(mCommonUserInfo.getName(), mEditUserName.isFocused());

                    // 주민번호
                    mCommonUserInfo.setIdnumber(object.optString("NRID"));
                    if (!TextUtils.isEmpty(mCommonUserInfo.getIdnumber()) && mCommonUserInfo.getIdnumber().length() >= 7) {
                        String day = mCommonUserInfo.getIdnumber().substring(0, 6);
                        String gender = mCommonUserInfo.getIdnumber().substring(6, 7);
                        mTextUserBirthday.setText(day);
                        mTextUserGender.setText(gender);
                    }

                    // 발행일
                    mCommonUserInfo.setIssueDate(object.optString("ISUE_DD"));
                    if (!TextUtils.isEmpty(mCommonUserInfo.getIssueDate())) {
                        mEditIssuedDate.setText(mCommonUserInfo.getIssueDate());
                    }

                    // 신분증명구분코드 (01 : 주민등록증, 02 : 운전면허증)
                    String idtPrfDvcd = object.optString("IDT_PRF_DVCD");
                    mCommonUserInfo.setIDT_PRF_DVCD(idtPrfDvcd);
                    if (Const.ID_TYPE_JUMIM.equalsIgnoreCase(idtPrfDvcd)) {
                        mLayoutLicenseNum.setVisibility(View.GONE);
                        mLayoutIssuedDate.setVisibility(View.VISIBLE);
                    } else if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(idtPrfDvcd)) {
                        // 운전면허번호전체
                        String drvnLcnsNo = object.optString("DRVN_LCNS_NO");
                        // 운전번호지역번호
                        String drvn_lcns_lcno = object.optString("DRVN_LCNS_LCNO");
                        // 운전면허일련번호
                        String drvnLcnsSrno = object.optString("DRVN_LCNS_SRNO");

                        mCommonUserInfo.setDRVN_LCNS_LCNO(drvn_lcns_lcno);
                        mCommonUserInfo.setDRVN_LCNS_SRNO(drvnLcnsSrno);
                        mCommonUserInfo.setDRVN_LCNS_SRNO(mCommonUserInfo.getDRVN_LCNS_SRNO().trim());

                        if (!TextUtils.isEmpty(mCommonUserInfo.getDRVN_LCNS_SRNO()) && mCommonUserInfo.getDRVN_LCNS_SRNO().length() == 10) {

                            String areaCode = mCommonUserInfo.getDRVN_LCNS_LCNO();
                            for (int index = 0; index < mListArea.size(); index++) {
                                RequestCodeInfo codeInfo = mListArea.get(index);
                                String code = codeInfo.getSCCD();
                                if (TextUtils.isEmpty(code))
                                    continue;

                                if (code.equalsIgnoreCase(areaCode)) {
                                    String name = codeInfo.getCD_NM();
                                    mTextLicenseCode.setText(name);
                                }
                            }

                            String licenseNum = mCommonUserInfo.getDRVN_LCNS_SRNO();
                            mEditLicenseNum.setText(licenseNum);
                            setLicenseNumberFormat();

                            if (TextUtils.isEmpty(mCommonUserInfo.getDRVN_LCNS_LCNO()))
                                mTextLicenseCode.setText("");
                        }
                        mLayoutLicenseNum.setVisibility(View.VISIBLE);
                        mLayoutIssuedDate.setVisibility(View.GONE);
                    }

                    Bitmap bitmapIdCard = ImgUtils.byteArrayToBitmap(imageOCR);
                    if (bitmapIdCard != null) {
                        mImageIdentification.setImageBitmap(bitmapIdCard);
                        mImageOCR = imageOCR;
                        paintMaskingArea(bitmapIdCard);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 모든 입력창 포커스 제거
     */
    private void clearFocus() {
        if (mEditUserName.isFocused())
            mEditUserName.clearFocus();

        if (mEditLicenseNum.isFocused())
            mEditLicenseNum.clearFocus();

        if (mEditIssuedDate.isFocused())
            mEditIssuedDate.clearFocus();
    }

    /**
     * 운전면허증 지역코드 리스트 표시
     */
    private void showAreaCodeList() {
        if (this.isFinishing())
            return;

        SlidingDriverAreaCodeDialog dialog = new SlidingDriverAreaCodeDialog(this, mListArea, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RequestCodeInfo codeInfo = mListArea.get(position);
                if (codeInfo == null)
                    return;

                String code = codeInfo.getSCCD();
                mCommonUserInfo.setDRVN_LCNS_LCNO(code);
                String name = codeInfo.getCD_NM();
                mTextLicenseCode.setText(name);
            }
        });
        dialog.show();
    }

    /**
     * 운전면허 번호 입력 완료된 상태(포멧형식 xx-xxxxxx-xx)에서 삭제하면 일반형식으로 변경
     */
    private void setLicenseNumberNormal() {
        String lincenseNum = mEditLicenseNum.getText().toString().replaceAll("[^0-9]", "");
        if (TextUtils.isEmpty(lincenseNum))
            return;

        mEditLicenseNum.setText(lincenseNum);
        mEditLicenseNum.setSelection(lincenseNum.length());

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(STR_LENGTH_LICENSE_NUMBER);
        mEditLicenseNum.setFilters(filterArray);
    }

    /**
     * 운전면허 번호 10자리 입력 시 xx-xxxxxx-xx 형식으로 변경
     */
    private void setLicenseNumberFormat() {
        String lincensestr = mEditLicenseNum.getText().toString();

        if (lincensestr.length() != STR_LENGTH_LICENSE_NUMBER)
            return;

        StringBuilder foramtstr = new StringBuilder();
        foramtstr.append(lincensestr.substring(0, 2));
        foramtstr.append("-");
        foramtstr.append(lincensestr.substring(2, 8));
        foramtstr.append("-");
        foramtstr.append(lincensestr.substring(8));

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(STR_LENGTH_FORMAT_LICENSE_NUMBER);
        mEditLicenseNum.setFilters(filterArray);

        mEditLicenseNum.setText(foramtstr);
        mEditLicenseNum.setSelection(foramtstr.length());
    }

    /**
     * 최종 확인 버튼 활성화 체크
     */
    private boolean checkDataValidation() {
        if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
            String name = mEditUserName.getText().toString();
            String licenseNum = mEditLicenseNum.getText().toString();

            if (TextUtils.isEmpty(licenseNum) || TextUtils.isEmpty(name))
                return false;
        } else {
            String name = mEditUserName.getText().toString();
            String date = mEditIssuedDate.getText().toString();

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(date))
                return false;
        }

        if (mEditUserName.isFocused()) {
            String name = mEditUserName.getText().toString();
            return checkVaildUserName(name, true);
        } else if (mEditLicenseNum.isFocused()) {
            String licenseNum = mEditLicenseNum.getText().toString();
            return checkVaildLicenseNum(licenseNum);
        } else if (mEditIssuedDate.isFocused()) {
            String date = mEditIssuedDate.getText().toString();
            date = date.replaceAll("[^0-9]", "");
            return checkVaildDate(date);
        }

        return true;
    }

    public boolean checkVaildLicenseNum(String licenseNum) {
        if (TextUtils.isEmpty(licenseNum))
            return false;

        licenseNum = licenseNum.replace("-", "");
        if (licenseNum.length() != STR_LENGTH_LICENSE_NUMBER)
            return false;

        return true;
    }

    public boolean checkVaildDate(String date) {
        //if (TextUtils.isEmpty(date) || !date.matches("^(1[0-9]|0[1-9]|3[0-1]|2[1-9]).(0[1-9]|1[0-2]).[0-9]{4}$"))
        if (TextUtils.isEmpty(date))
            return false;

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        try {
            format.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    private void paintMaskingArea(Bitmap bitmapIdCard) {
        if (bitmapIdCard == null || bitmapIdCard.getHeight() <= 0 || bitmapIdCard.getWidth() <= 0)
            return;

        float leftRatio = 78/250f;
        float topRatio = 65/159f;
        float rightRatio = 130/250f;
        float bottomRatio = 78/159f;
        float width = Utils.pixelToDp(this, bitmapIdCard.getWidth());
        float height = Utils.pixelToDp(this, bitmapIdCard.getHeight());

        if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(mCommonUserInfo.getIDT_PRF_DVCD())) {
            leftRatio = 137/250f;
            topRatio = 55/159f;
            rightRatio = 240/250f;
            bottomRatio = 68/159f;
            width = (int)Utils.pixelToDp(this, bitmapIdCard.getWidth());
            height = (int)Utils.pixelToDp(this, bitmapIdCard.getHeight());
        }

        int rrnLeft = (int) Utils.dpToPixel(this, leftRatio * width);
        int rrnTop = (int) Utils.dpToPixel(this, topRatio * height);
        int rrnRight = (int) Utils.dpToPixel(this, rightRatio * width);
        int rrnBottom = (int) Utils.dpToPixel(this, bottomRatio * height);

        Rect rrnRect = new Rect(rrnLeft, rrnTop, rrnRight, rrnBottom);

        Canvas c = new Canvas(bitmapIdCard);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        c.drawRect(rrnRect, paint);
    }

    private void requestVerifyIdentification() {
        String username = mEditUserName.getText().toString();
        String idtPrfDvcd = mCommonUserInfo.getIDT_PRF_DVCD();
        String jumin = mCommonUserInfo.getIdnumber();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(idtPrfDvcd) || TextUtils.isEmpty(jumin)) {
            return;
        }

        Map param = new HashMap();
        param.put("CUST_NM", username);
        param.put("IDT_PRF_DVCD", idtPrfDvcd);
        param.put("NRID", jumin);

        if (Const.ID_TYPE_JUMIM.equalsIgnoreCase(idtPrfDvcd)) {
            String isueDD = mEditIssuedDate.getText().toString();
            isueDD = isueDD.replaceAll("[^0-9]", "");
            if (TextUtils.isEmpty(isueDD))
                return;

            param.put("ISUE_DD", isueDD);
        } else if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(idtPrfDvcd)) {
            String areaCode = mCommonUserInfo.getDRVN_LCNS_LCNO();
            String licenseNum = mEditLicenseNum.getText().toString();

            if (TextUtils.isEmpty(areaCode) || !checkVaildLicenseNum(licenseNum))
                return;

            licenseNum = licenseNum.replaceAll("-", "");
            param.put("DRVN_LCNS_LCNO", areaCode);
            param.put("DRVN_LCNS_SRNO", licenseNum);
        }

        param.put("EDMS_YN", Const.BRIDGE_RESULT_YES);

        if (mImageOCR == null)
            return;

        param.put("IMG_CNTN", Base64.encodeToString(mImageOCR, Base64.DEFAULT));

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET)
            param.put("NFF_RLNM_ATHN_DVCD", "07");
        else
            param.put("NFF_RLNM_ATHN_DVCD", "00");

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010200A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                        return;
                    }

                    Logs.e("ret : " + ret);

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        if ("CMM0039".equals(errCode)) {
                            ShotIDFailDialog shotIDFailDialog = new ShotIDFailDialog(IdentificationConfirmActivity.this, Const.ID_SHOT_FAIL_CHECK);
                            shotIDFailDialog.setOnCofirmListener(new ShotIDFailDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                }
                            });
                            shotIDFailDialog.show();
                            return;
                        } else {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);

                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    String trtmStcd = object.optString("TRTM_STCD");
                    if (Const.REQUEST_COMMON_SUCCESS_CODE.equalsIgnoreCase(trtmStcd)) {
                        goNextActivity();
                    } else {
                        String respCntn = object.optString("RESP_CNTN");
                        if (TextUtils.isEmpty(respCntn))
                            respCntn = getString(R.string.common_msg_no_reponse_value_was);

                        showErrorMessage(respCntn);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    @Override
    public void onKeyboardShown() {
        Logs.e("onKeyboardShown");
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 500);
        mBtnConfirm.setVisibility(View.GONE);
        mBtnShoot.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        Logs.e("onKeyboardHidden");
        mIsShownKeyboard = false;
        mBtnConfirm.setEnabled(checkDataValidation());
        mBtnConfirm.setVisibility(View.VISIBLE);
        mBtnShoot.setVisibility(View.VISIBLE);
    }

    private class WeakHandler extends Handler {
        private WeakReference<IdentificationConfirmActivity> mWeakActivity;

        WeakHandler(IdentificationConfirmActivity weakactivity) {
            mWeakActivity = new WeakReference<IdentificationConfirmActivity>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            IdentificationConfirmActivity weakactivity = mWeakActivity.get();
            if (weakactivity != null) {
                switch (msg.what) {
                    case SCROLL_EDITTEXT: {
                        if (!mIsShownKeyboard)
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollOffset);
                        break;
                    }
                    case SCROLL_CHECK_KEYBOARD: {
                        mIsShownKeyboard = true;
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}
