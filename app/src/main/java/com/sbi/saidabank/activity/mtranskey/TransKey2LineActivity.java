package com.sbi.saidabank.activity.mtranskey;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Saidabank_android
 * Class: CertExportActivity
 * Created by 950469 on 2018. 9. 10..
 * <p>
 * Description:
 * 보안키패드 입력필드가 2개짜리 처리 화면
 */
public class TransKey2LineActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener {
    private static final int INPUT_COUNT = 2;

    private TransKeyCtrl[] mTKMgr;
    private int onClickIndex;
    private EditText[] mEditText;
    private int mKeypadType;
    private int mMinLength;
    private String[] mPlainTextBuf;
    private String[] mSecureTextBuf;
    private String[] mDummyTextBuf;
    private boolean mTouchFlag;
    private String mInputId1;
    private String mInputId2;
    private String mParameter;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transkey_input_two);

        initValues();

        mKeypadType = getIntent().getIntExtra(Const.INTENT_TRANSKEY_PADTYPE, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
        int mTextType = getIntent().getIntExtra(Const.INTENT_TRANSKEY_TEXTTYPE, TransKeyActivity.mTK_TYPE_TEXT_PASSWORD);
        String title = getIntent().getStringExtra(Const.INTENT_TRANSKEY_TITLE);
        String mLabel1 = getIntent().getStringExtra(Const.INTENT_TRANSKEY_LABEL1);
        String mLabel2 = getIntent().getStringExtra(Const.INTENT_TRANSKEY_LABEL2);
        int mMaxLength = getIntent().getIntExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 0);
        mMinLength = getIntent().getIntExtra(Const.INTENT_TRANSKEY_MINLENGTH,0);
        mInputId1 =  getIntent().getStringExtra(Const.INTENT_TRANSKEY_INPUTID1);
        mInputId2 =  getIntent().getStringExtra(Const.INTENT_TRANSKEY_INPUTID2);
        mParameter = getIntent().getStringExtra(Const.INTENT_TRANSKEY_PARAMETER);
        boolean mCaptionShow = getIntent().getBooleanExtra(Const.INTENT_TRANSKEY_CAPTION, false);

        //((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(title);
        ((TextView)findViewById(R.id.tv_label_1)).setText(mLabel1);
        ((TextView)findViewById(R.id.tv_label_2)).setText(mLabel2);
        if(mCaptionShow){
            ((TextView)findViewById(R.id.tv_caption)).setVisibility(View.VISIBLE);
        }


        for(int i=0;i<INPUT_COUNT;i++){

            String layoutName = "@id/inputlayout_" + (i+1);
            int layoutID = getResources().getIdentifier(layoutName,"id",this.getPackageName());
            RelativeLayout layout = (RelativeLayout)findViewById(layoutID);

            mEditText[i] = (EditText)layout.findViewById(R.id.editText);
            mEditText[i].setOnTouchListener(this);
            mEditText[i].setTag(i);

            mTKMgr[i] = TransKeyUtils.initTransKeyPad(this,i, mKeypadType,
                    mTextType,
                    "",//label
                    "",//hint
                    mMaxLength,//max length
                    getString(R.string.message_over_max_length, mMaxLength),//max length msg
                    mMinLength,
                    "",
                    5,
                    true,
                    (FrameLayout)findViewById(R.id.keypadContainer),
                    mEditText[i],
                    (HorizontalScrollView)layout.findViewById(R.id.keyscroll),
                    (LinearLayout)layout.findViewById(R.id.keylayout),
                    (ImageButton)layout.findViewById(R.id.clearall),
                    (RelativeLayout)findViewById(R.id.keypadBallon),
                    null,
                    false,
                    false);




            ((LinearLayout)layout.findViewById(R.id.keylayout)).setOnTouchListener(this);
            ((LinearLayout)layout.findViewById(R.id.keylayout)).setTag(i);

            ((HorizontalScrollView)layout.findViewById(R.id.keyscroll)).setOnTouchListener(this);
            ((HorizontalScrollView)layout.findViewById(R.id.keyscroll)).setTag(i);
        }

        showTransKey(0);
        SystemClock.sleep(500);

    }

    private void initValues(){
        mTKMgr = new TransKeyCtrl[INPUT_COUNT];
        mEditText = new EditText[INPUT_COUNT];
        mPlainTextBuf = new String[INPUT_COUNT];
        mSecureTextBuf = new String[INPUT_COUNT];
        mDummyTextBuf = new String[INPUT_COUNT];
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            /*
            case R.id.btn_back:
                finish();
                break;
            */
            default:
                break;
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(event.getAction() == MotionEvent.ACTION_DOWN){
            Logs.e("onTouch-onTouch---0");
            int id = v.getId();
            int tagValue = 0;
            Object tag = v.getTag();
            if(tag != null)
                tagValue = (Integer)tag;

            switch (id){
                case R.id.keylayout:
                    Logs.e("onTouch-keylayout");
                case R.id.keyscroll:
                    Logs.e("onTouch-keyscroll");
                case R.id.editText:
                    Logs.e("onTouch-editText");
                    mTouchFlag=true;
                    if(tagValue != onClickIndex)
                        mTKMgr[onClickIndex].finishTransKey(true);


                    showTransKey(tagValue);
                    mTouchFlag=false;
                    return true;
                default:
                    break;
            }
        }

        return false;
    }

    @Override
    public void cancel(Intent data) {
        setResult(RESULT_CANCELED, null);
        finish();
    }

    @Override
    public void done(Intent data) {

        if (data == null){
            setData(onClickIndex,null,null,null);
            return;
        }

        String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);
        String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);

        if(TextUtils.isEmpty(dummyData)||dummyData.length() < mMinLength){
            //입력완료로 불렸을 경우 최소글자체크한다.
            if(!mTouchFlag){
                showMinLengthMsg(onClickIndex,mMinLength);
            }
            setData(onClickIndex,null,null,null);
            return;
        }


        String plainData = TransKeyUtils.decryptPlainDataFromCipherData(data);

        setData(onClickIndex,secureData,plainData,dummyData);

        if(!mTouchFlag){
            checkInputData();
        }
    }

    @Override
    public void input(int type) {
        Logs.e("input");
    }

    @Override
    public void minTextSizeCallback(){
        Logs.e("minTextSizeCallback() call");
    }

    @Override
    public void maxTextSizeCallback(){
        Logs.e("maxTextSizeCallback() call");
    }

    private void setData(int idx, String secureData, String plainData, String dummyData){
        Logs.i("onClickIndex : " + idx);
        Logs.i("secureData : " + secureData);
        Logs.i("plainData : " + plainData);
        Logs.i("dummyData : " + dummyData);

        mSecureTextBuf[idx] = secureData;
        mPlainTextBuf[idx] =  plainData;
        mDummyTextBuf[idx] = dummyData;
    }

    private  void checkInputData(){

        if(!TextUtils.isEmpty(mPlainTextBuf[onClickIndex]) && mPlainTextBuf[onClickIndex].length() <mMinLength){
            showMinLengthMsg(onClickIndex,mMinLength);
            return;
        }

        for(int i=0;i<INPUT_COUNT;i++){
            if(onClickIndex != i && TextUtils.isEmpty(mPlainTextBuf[i])){
                showTransKey(i);
                return;
            }
        }

        if(!TextUtils.isEmpty(mPlainTextBuf[0]) && !TextUtils.isEmpty(mPlainTextBuf[1])){

			if(mPlainTextBuf[0].equals(mPlainTextBuf[1])){
	            Intent intent = new Intent();
	            intent.putExtra(Const.INTENT_TRANSKEY_INPUTID1,mInputId1);
	            intent.putExtra(Const.INTENT_TRANSKEY_SECURE_DATA1,mSecureTextBuf[0]);
                intent.putExtra(Const.INTENT_TRANSKEY_PLAIN_DATA1,mPlainTextBuf[0]);
	            intent.putExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1,mDummyTextBuf[0]);
	            intent.putExtra(Const.INTENT_TRANSKEY_INPUTID2,mInputId2);
	            intent.putExtra(Const.INTENT_TRANSKEY_SECURE_DATA2,mSecureTextBuf[1]);
                intent.putExtra(Const.INTENT_TRANSKEY_PLAIN_DATA2,mPlainTextBuf[1]);
	            intent.putExtra(Const.INTENT_TRANSKEY_DUMMY_DATA2,mDummyTextBuf[1]);
	            intent.putExtra(Const.INTENT_TRANSKEY_PARAMETER,mParameter);
	            setResult(RESULT_OK, intent);
	            finish();
            }else{
                String label1 = getLabelText(0);
                String label2 = getLabelText(1);
                DialogUtil.alert(this, getString(R.string.message_not_equal_cert_pass_confirm, label1, label2), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTransKey(1);
                    }
                });
            }
        }
    }

    private void showTransKey(int index){
        onClickIndex = index;
        mEditText[onClickIndex].requestFocus();
        mTKMgr[onClickIndex].showKeypad(mKeypadType);
    }

    private String getLabelText(int index){
        int resId = getResources().getIdentifier("@id/tv_label_" + (index+1),"id",this.getPackageName());
        String label = ((TextView)findViewById(resId)).getText().toString();

        return label;
    }

    private void showMinLengthMsg(int idx,int minLength){
        final int index = idx;

        String label = getLabelText(index);
        DialogUtil.alert(this, label + "의 " + getString(R.string.message_least_min_length,minLength), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTransKey(index);
            }
        });
    }
}
