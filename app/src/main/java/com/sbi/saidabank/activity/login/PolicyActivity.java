package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebTermsActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.solution.appsflyer.AppsFlyerManager;

/**
 * Saidabank_android
 * Class: PolicyActivity
 * Created by 950546
 * Date: 2018-10-17
 * Time: 오후 2:41
 * Description: 이용약관 화면
 */
public class PolicyActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private CheckBox mChboxNecessary;
    private CheckBox mChboxOption;
    private CheckBox mChboxApppush;
    private Button mBtnMarkettingAgreement;
    private CheckBox mChboxSMS;
    private Button mBtnSaidaPolicy;
    private Button mBtnPrivateInfo;
    private Button mBtnBasicPolicy;
    private Button mBtnNotiPolicy;
    private CheckBox mChboxSecurityApppush;
    private CheckBox mChboxSecuritySMS;
    private CheckBox mChboxSecurityNotAccept;
    private Button mBtnOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);

        initView();

        CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        if (commonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"2", "");
    }

    /**
     * UI 초기화
     */
    private void initView() {

        LinearLayout necessaryLayout = findViewById(R.id.ll_necessarypolicy);
        LinearLayout optionLayout = findViewById(R.id.ll_optionpolicy);
        LinearLayout apppushLayout = findViewById(R.id.ll_apppush);
        LinearLayout smsLayout = findViewById(R.id.ll_sms);
        LinearLayout appSecurityPushLayout = findViewById(R.id.ll_security_apppush);
        LinearLayout securitySMSLayout = findViewById(R.id.ll_security_sms);
        LinearLayout securityNoAcceptLayout = findViewById(R.id.ll_security_noaccept);
        necessaryLayout.setOnClickListener(this);
        optionLayout.setOnClickListener(this);
        apppushLayout.setOnClickListener(this);
        smsLayout.setOnClickListener(this);
        appSecurityPushLayout.setOnClickListener(this);
        securitySMSLayout.setOnClickListener(this);
        securityNoAcceptLayout.setOnClickListener(this);
        mChboxNecessary = findViewById(R.id.chbox_necessary);
        mChboxOption = findViewById(R.id.chbox_option);
        mBtnMarkettingAgreement = findViewById(R.id.btn_markettingagreement);
        mBtnSaidaPolicy = findViewById(R.id.btn_saidapolicy);
        mBtnPrivateInfo = findViewById(R.id.btn_privateinfo);
        mBtnBasicPolicy = findViewById(R.id.btn_basicpolicy);
        mBtnNotiPolicy = findViewById(R.id.btn_notipolicy);
        mChboxApppush = findViewById(R.id.chbox_apppush);
        mChboxSMS = findViewById(R.id.chbox_SMS);
        mChboxSecurityApppush = findViewById(R.id.chbox_security_apppush);
        mChboxSecuritySMS = findViewById(R.id.chbox_security_SMS);
        mChboxSecurityNotAccept = findViewById(R.id.chbox_security_noaccept);
        mBtnOK = findViewById(R.id.btn_confirm);


        mChboxNecessary.setOnCheckedChangeListener(this);
        mChboxOption.setOnCheckedChangeListener(this);
        mBtnMarkettingAgreement.setOnClickListener(this);
        mChboxApppush.setOnCheckedChangeListener(this);
        mChboxSMS.setOnCheckedChangeListener(this);

        findViewById(R.id.layout_basicpolicy).setOnClickListener(this);
        findViewById(R.id.layout_notipolicy).setOnClickListener(this);
        findViewById(R.id.layout_necessary).setOnClickListener(this);
        findViewById(R.id.layout_privateinfo).setOnClickListener(this);
        findViewById(R.id.layout_marketting).setOnClickListener(this);
        findViewById(R.id.ll_chbox_markettingagreement).setOnClickListener(this);
        mChboxSecurityApppush.setOnClickListener(this);
        mChboxSecuritySMS.setOnClickListener(this);
        mChboxSecurityNotAccept.setOnClickListener(this);

        mBtnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(PolicyActivity.this, PersonalInfoActivity.class);
                CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);

                if (mChboxApppush.isChecked())
                    commonUserInfo.setOptionalPolicyPush(true);
                else
                    commonUserInfo.setOptionalPolicyPush(false);

                if (mChboxSMS.isChecked())
                    commonUserInfo.setOptionalPolicySMS(true);
                else
                    commonUserInfo.setOptionalPolicySMS(false);

                if (mChboxSecurityApppush.isChecked())
                    commonUserInfo.setSecurityPolicyPush(true);
                else
                    commonUserInfo.setSecurityPolicyPush(false);

                if (mChboxSecuritySMS.isChecked())
                    commonUserInfo.setSecurityPolicySMS(true);
                else
                    commonUserInfo.setSecurityPolicySMS(false);

                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.chbox_necessary: {
                if (isChecked) {
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                    mBtnSaidaPolicy.setEnabled(true);
                    mBtnPrivateInfo.setEnabled(true);
                    mBtnBasicPolicy.setEnabled(true);
                    mBtnNotiPolicy.setEnabled(true);

                    Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "약관");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=A001");
                    startActivity(intent);
                } else {
                    mBtnSaidaPolicy.setEnabled(false);
                    mBtnPrivateInfo.setEnabled(false);
                    mBtnBasicPolicy.setEnabled(false);
                    mBtnNotiPolicy.setEnabled(false);
                    mBtnOK.setEnabled(false);
                }
                break;
            }
            case R.id.chbox_option: {
                if (isChecked) {
                    if (mBtnMarkettingAgreement.isEnabled()) {
                        break;
                    }
                    mBtnMarkettingAgreement.setEnabled(true);
                    mChboxApppush.setChecked(true);
                    mChboxSMS.setChecked(true);
                    Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "동의서");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1001");
                    startActivity(intent);
                } else {
                    mBtnMarkettingAgreement.setEnabled(false);
                    mChboxApppush.setChecked(false);
                    mChboxSMS.setChecked(false);
                }
                break;
            }
            case R.id.chbox_apppush: {
                if (isChecked) {
                    if (!mChboxSMS.isChecked()) {
                        mBtnMarkettingAgreement.setEnabled(true);
                        mChboxOption.setChecked(true);
                    }
                } else {
                    if (!mChboxSMS.isChecked()) {
                        mChboxOption.setChecked(false);
                    }
                }
                break;
            }
            case R.id.chbox_SMS: {
                if (isChecked) {
                    if (!mChboxApppush.isChecked()) {
                        mBtnMarkettingAgreement.setEnabled(true);
                        mChboxOption.setChecked(true);
                    }
                } else {
                    if (!mChboxApppush.isChecked()) {
                        mChboxOption.setChecked(false);
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_necessarypolicy: {
                mChboxNecessary.setChecked(!mChboxNecessary.isChecked());
                break;
            }
            case R.id.ll_optionpolicy: {
                mChboxOption.setChecked(!mChboxOption.isChecked());
                break;
            }
            case R.id.ll_apppush: {
                mChboxApppush.setChecked(!mChboxApppush.isChecked());
                break;
            }
            case R.id.ll_sms: {
                mChboxSMS.setChecked(!mChboxSMS.isChecked());
                break;
            }
            case R.id.layout_basicpolicy: {
                Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1008");
                startActivity(intent);
                break;
            }
            case R.id.layout_necessary: {
                Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1009");
                startActivity(intent);
                break;
            }
            case R.id.layout_notipolicy: {
                Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1024");
                startActivity(intent);
                break;
            }
            case R.id.layout_privateinfo: {
                Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1000");
                startActivity(intent);
                break;
            }
            case R.id.btn_markettingagreement:
            case R.id.ll_chbox_markettingagreement:{
                mBtnMarkettingAgreement.setEnabled(!mBtnMarkettingAgreement.isEnabled());

                if (mBtnMarkettingAgreement.isEnabled()) {
                    mChboxApppush.setChecked(true);
                    mChboxSMS.setChecked(true);
                    Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "동의서");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1001");
                    startActivity(intent);
                } else {
                    mChboxApppush.setChecked(false);
                    mChboxSMS.setChecked(false);
                }
                break;
            }
            case R.id.layout_marketting: {
                if (!mBtnMarkettingAgreement.isEnabled()) {
                    mBtnMarkettingAgreement.setEnabled(true);
                    mChboxApppush.setChecked(true);
                    mChboxSMS.setChecked(true);
                }
                Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1001");
                startActivity(intent);
                break;
            }
            case R.id.ll_security_apppush: {
                if (!mChboxSecurityApppush.isChecked()) {
                    mChboxSecurityApppush.setChecked(true);
                    mChboxSecuritySMS.setChecked(false);
                    mChboxSecurityNotAccept.setChecked(false);
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                }
                break;
            }
            case R.id.chbox_security_apppush: {
                mChboxSecurityApppush.setChecked(true);
                mChboxSecuritySMS.setChecked(false);
                mChboxSecurityNotAccept.setChecked(false);
                if (isEnabledOKBtn())
                    mBtnOK.setEnabled(true);
                break;
            }
            case R.id.ll_security_sms: {
                if (!mChboxSecuritySMS.isChecked()) {
                    mChboxSecuritySMS.setChecked(true);
                    mChboxSecurityApppush.setChecked(false);
                    mChboxSecurityNotAccept.setChecked(false);
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                }
                break;
            }
            case R.id.chbox_security_SMS: {
                mChboxSecuritySMS.setChecked(true);
                mChboxSecurityApppush.setChecked(false);
                mChboxSecurityNotAccept.setChecked(false);
                if (isEnabledOKBtn())
                    mBtnOK.setEnabled(true);
                break;
            }
            case R.id.ll_security_noaccept: {
                if (!mChboxSecurityNotAccept.isChecked()) {
                    mChboxSecurityNotAccept.setChecked(true);
                    mChboxSecurityApppush.setChecked(false);
                    mChboxSecuritySMS.setChecked(false);
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                }
                break;
            }
            case R.id.chbox_security_noaccept: {
                mChboxSecurityNotAccept.setChecked(true);
                mChboxSecurityApppush.setChecked(false);
                mChboxSecuritySMS.setChecked(false);
                if (isEnabledOKBtn())
                    mBtnOK.setEnabled(true);
                break;
            }
            default:
                break;
        }
    }

    private boolean isEnabledOKBtn() {
        if (mChboxNecessary.isChecked() && (mChboxSecurityApppush.isChecked() || mChboxSecuritySMS.isChecked() || mChboxSecurityNotAccept.isChecked())) {
            return true;
        } else {
            return false;
        }
    }
}
