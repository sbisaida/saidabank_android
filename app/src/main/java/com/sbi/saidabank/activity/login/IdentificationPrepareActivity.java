package com.sbi.saidabank.activity.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.ShotIDFailDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: IdentificationPrepareActivity
 * Created by 950485 on 2018. 11. 08..
 * <p>
 * Description:신분증확인 설명 화면
 */

public class IdentificationPrepareActivity extends BaseActivity {
    private static final int     REQUEST_OCR_ID = 20000;
    private static final String  ENCRYPT_KEY = "";

    private CommonUserInfo       mCommonUserInfo;

    // 필요권한 - 카메라접근권한
    String[] perList = new String[] {
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_identification_prepare);

        getExtra();

        TextView tvClose = (TextView) findViewById(R.id.tv_close);

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
            tvClose.setVisibility(View.VISIBLE);
            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCancelMessage(getResources().getString(R.string.msg_cancel_pin_rereg));
                    return;
                }
            });
        } else {
            tvClose.setVisibility(View.GONE);
        }

        Button btnShoot = (Button) findViewById(R.id.btn_shoot_identification_prepare);
        btnShoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (PermissionUtils.checkPermission(IdentificationPrepareActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                    showShootIdentification();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        // 신규 가입 중일 때 뒤로 가기 안됨
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
            showCancelMessage(getResources().getString(R.string.msg_cancel_pin_rereg));
            return;
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_CAMERA :
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showShootIdentification();
                } else {
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    if (!state) {
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_camera_allow),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    PermissionUtils.goAppSettingsActivity(IdentificationPrepareActivity.this);
                                }
                            }, new View.OnClickListener(){
                                @Override
                                public void onClick(View v) {
                            }
                        });
                    }
                }
            }
            break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_OCR_ID :
                if (resultCode == CameraActivity.RETURN_OK && data != null) {
                    byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
                    if (imageOCR == null)
                        return;

                    requestCheckIdentification(imageOCR);
                }
                break;

            default:
                break;
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    /**
     * 신분증 확인 요청
     */
    private void requestCheckIdentification(final byte[] imageOCR) {
        String data = Base64.encodeToString(imageOCR, Base64.DEFAULT);
        if (TextUtils.isEmpty(data))
            return;

        Map param = new HashMap();
        param.put("IMG_CNTN", data);

        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
        if (TextUtils.isEmpty(mbrNo))
            mbrNo = mCommonUserInfo.getMBRnumber();
        param.put("MBR_NO", mbrNo);

        String custNo = LoginUserInfo.getInstance().getCUST_NO();
        param.put("CUST_NO", custNo);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010200A01 : " + ret);

                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getString(R.string.msg_check_identification));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_check_identification));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_check_identification));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if ("CMM0011".equals(errCode)) {
                            ShotIDFailDialog shotIDFailDialog = new ShotIDFailDialog(IdentificationPrepareActivity.this, Const.ID_SHOT_FAIL_SHOT);
                            shotIDFailDialog.setOnCofirmListener(new ShotIDFailDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    if (PermissionUtils.checkPermission(IdentificationPrepareActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                                        showShootIdentification();
                                    }
                                }
                            });
                            shotIDFailDialog.show();
                            return;
                        }
                        if (TextUtils.isEmpty(msg)) {
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        else {
                            CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    if (PermissionUtils.checkPermission(IdentificationPrepareActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                                        showShootIdentification();
                                    }
                                }
                            };
                            showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        }
                        Logs.e(ret);
                        return;
                    }
                    Logs.e(ret);
                    // 처리 상태 ( 01 : 정상, 02 : 오류)
                    String trtmStcd = object.optString("TRTM_STCD");
                    if (Const.REQUEST_COMMON_FAIL_CODE.equalsIgnoreCase(trtmStcd)) {
                        String msg = object.optString("RESP_CNTN");
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.msg_check_identification);

                        Logs.e("msg : " + msg);
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (PermissionUtils.checkPermission(IdentificationPrepareActivity.this, perList,Const.REQUEST_PERMISSION_CAMERA)) {
                                    showShootIdentification();
                                }
                            }
                        };
                        showErrorMessage(msg, okClick);
                        return;
                    }

                    // 고객명
                    String custNm = object.optString("CUST_NM");

                    if (mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE && !TextUtils.isEmpty(mCommonUserInfo.getName())) {
                        if (!mCommonUserInfo.getName().equalsIgnoreCase(custNm)) {
                            showErrorMessage(getString(R.string.msg_check_identification_name));
                            return;
                        }
                    }
                    // 주민번호
                    String nrid = object.optString("NRID");
                    // 발행일
                    String isueDd = object.optString("ISUE_DD");
                    if (TextUtils.isEmpty(isueDd) || isueDd.length() != 8) {
                        showErrorMessage(getString(R.string.msg_check_identification));
                        return;
                    }

                    Intent intent = new Intent(IdentificationPrepareActivity.this, IdentificationConfirmActivity.class);

                    // 신분증명구분코드 (01 : 주민등록증, 02 : 운전면허증)
                    String idtPrfDvcd = object.optString("IDT_PRF_DVCD");
                    // 신분증증명구분코드 오류 예외처리
                    if (TextUtils.isEmpty(idtPrfDvcd) || (!"01".equals(idtPrfDvcd) && !"02".equals(idtPrfDvcd))) {
                        showErrorMessage(getString(R.string.msg_check_identification));
                        return;
                    }
                    mCommonUserInfo.setIDT_PRF_DVCD(idtPrfDvcd);
                    if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(idtPrfDvcd)) {
                        // 운전면허번호전체
                        String drvnLcnsNo = object.optString("DRVN_LCNS_NO");
                        mCommonUserInfo.setDRVN_LCNS_NO(drvnLcnsNo);
                        // 운전번호지역번호
                        String drvn_lcns_lcno = object.optString("DRVN_LCNS_LCNO");
                        mCommonUserInfo.setDRVN_LCNS_LCNO(drvn_lcns_lcno);
                        // 운전면허일련번호
                        String drvnLcnsSrno = object.optString("DRVN_LCNS_SRNO");
                        mCommonUserInfo.setDRVN_LCNS_SRNO(drvnLcnsSrno);
                    }

                    mCommonUserInfo.setName(custNm);
                    mCommonUserInfo.setIdnumber(nrid);
                    mCommonUserInfo.setIssueDate(isueDd);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    intent.putExtra(Const.INTENT_IDENTIFICATION_IMG, imageOCR);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 신분증 촬영 화면 표시
     */
    private void showShootIdentification() {
        Intent intent = new Intent(IdentificationPrepareActivity.this, CameraActivity.class);
        intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
        intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
        intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
        intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
        intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, ENCRYPT_KEY);
        intent.putExtra(CameraActivity.DATA_LICENSE_NUMBER_RECT, false);
        intent.putExtra(CameraActivity.DATA_RRN_RECT, 0);
        startActivityForResult(intent, REQUEST_OCR_ID);
    }
}
