package com.sbi.saidabank.activity;

import android.app.Activity;
import android.app.Application;
import android.net.Uri;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import com.appsflyer.*;
import com.netfunnel.api.LoadProperty;
import com.netfunnel.api.Property;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.solution.v3.V3Manager;
import com.tsengvn.typekit.Typekit;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Saidabank_android
 * Class: SaidaApplication
 * Created by 950469 on 2018. 8. 22..
 * <p>
 * Description:
 */
public class SaidaApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {
    private Map<String, Activity> allActivity;
    private String mTelNo;
    private Uri mProfileUri;

    @Override
    public void onCreate() {
        super.onCreate();
        allActivity = new HashMap<String, Activity>();
        registerActivityLifecycleCallbacks(this);

        /* This API enables AppsFlyer to detect installations, sessions, and updates. */
        AppsFlyerLib.getInstance().init(LicenseKey.AF_DEV_KEY , mConversionListener, this);
        //기기식별번호 수집여부 확인. 릴리즈 버전에서 IMEI 수집 막음
        if (BuildConfig.DEBUG)
            AppsFlyerLib.getInstance().setCollectIMEI(true);
        else
            AppsFlyerLib.getInstance().setCollectIMEI(false);

        AppsFlyerLib.getInstance().setCollectAndroidID(true);
        AppsFlyerLib.getInstance().startTracking(this, LicenseKey.AF_DEV_KEY);

        /* Set to true to see the debug logs. Comment out or set to false to stop the function */
        AppsFlyerLib.getInstance().setDebugLog(true);

        /* nanumbarungothic font */
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "font/nanumbarungothic.otf"))
                .addBold(Typekit.createFromAsset(this, "font/nanumbarungothicbold.otf"))
                .addItalic(Typekit.createFromAsset(this, "font/nanumbarungothiclight.otf"));

        /* NetFUNNEL의 Default Property를 수정한다.
         * 이것은 static로 작성되어 있기 때문에 전체 Application중 한번만 하면된다.
         */
        Property property =Property.getDefaultInstance();
        property.setProtocol("https");
        property.setHost("nf.sbisb.co.kr");
        property.setPort(443);
        //LoadProperty load_property = LoadProperty.getGlobalInstance();
        //load_property.setUrl("http://58.181.39.180/android/property.html");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        allActivity.put(activity.toString(), activity);
        //Logs.e("onActivityCreated " + activity.toString());
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        allActivity.remove(activity.toString());
        if (allActivity.size() <= 0 && LoginUserInfo.getInstance().isFinishedLogin()) {
            V3Manager v3Manager = V3Manager.getInstance(getApplicationContext());
            v3Manager.stopV3MobilePlus();
        }
        if (!LoginUserInfo.getInstance().isFinishedLogin())
            LoginUserInfo.getInstance().setFinishedLogin(true);
        //Logs.e("onActivityDestroyed " + activity.toString());
    }

    /**
     * 모든 떠있는 화면을 한번에 종료한다.
     * @param includeMain TRUE이면 메인화면까지 닫아버린다. FALSE이면 메인 화면은 남겨둔다.
     */
    public void allActivityFinish(boolean includeMain) {
        Iterator<String> keys = this.allActivity.keySet().iterator();
        while(keys.hasNext()) {
            String key = keys.next();
            if(key.contains(".MainActivity") && !includeMain){
                continue;
            }

            Activity activity = this.allActivity.get(key);
            if(!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    public Activity getOpenActivity(String activityName){
        Logs.i("allActivity.size() : " + allActivity.size());
        if (allActivity.size() > 0) {
            Iterator<String> keys = this.allActivity.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                Logs.e("key : " + key);
                if (key.contains(activityName)) {
                    Activity activity = this.allActivity.get(key);
                    if(!activity.isFinishing()) {
                        return activity;
                    }
                }
            }
        }

        return null;
    }

    public int getActivitySize(){
        return allActivity.size();
    }

    /**
     * 웹에서 전화걸기 시도시 전화번호 임시저장
     * @param telNo 전화번호
     */
    public void setTempTelNo(String telNo){
        mTelNo =  telNo;
    }

    public String getTempTelNo(){
        return mTelNo;
    }

    /**
     * 907번 프로필 사진 호출 자바인터페이스 시도 시 촬영 이미지 임시 Uri저장
     * @param profileUrl 프로필 이미지 Uri
     */
    public void setProfileUri(Uri profileUrl){
        mProfileUri =  profileUrl;
    }

    public Uri getProfileUri(){
        return mProfileUri;
    }

    /**
     * AppsFly 리스너
     */
    AppsFlyerConversionListener mConversionListener = new AppsFlyerConversionListener() {
        /* Returns the attribution data. Note - the same conversion data is returned every time per install */
        @Override
        public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
            for (String attrName : conversionData.keySet()) {
                Logs.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " + conversionData.get(attrName));
            }
        }

        @Override
        public void onInstallConversionFailure(String errorMessage) {
            Logs.d(AppsFlyerLib.LOG_TAG, "error getting conversion data: " + errorMessage);
        }

        /* Called only when a Deep Link is opened */
        @Override
        public void onAppOpenAttribution(Map<String, String> conversionData) {
            for (String attrName : conversionData.keySet()) {
                Logs.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " + conversionData.get(attrName));
            }
        }

        @Override
        public void onAttributionFailure(String errorMessage) {
            Logs.d(AppsFlyerLib.LOG_TAG, "error onAttributionFailure : " + errorMessage);
        }
    };
}
