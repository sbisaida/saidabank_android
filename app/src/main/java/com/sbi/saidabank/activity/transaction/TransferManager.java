package com.sbi.saidabank.activity.transaction;

import android.text.TextUtils;

import com.sbi.saidabank.define.datatype.VerifyTransferInfo;

import java.util.ArrayList;

public class TransferManager {
    private static TransferManager instance = null;

    private ArrayList<VerifyTransferInfo> listVerifyTransferInfo = new ArrayList<>();   // 수취조회 후 리스트값
    private boolean    isNotRemove = false;
    /**
     * Singleton instance 생성
     *
     * @return instance
     */
    public static TransferManager getInstance() {
        if (instance == null) {
            synchronized(TransferManager.class) {
                if (instance == null) {
                    instance = new TransferManager();
                }
            }
        }
        return instance;
    }

    /**
     * 생성자
     */
    private TransferManager() {
    }

    public void setData(ArrayList<VerifyTransferInfo> listVerifyTransferInfo) {
        this.listVerifyTransferInfo = listVerifyTransferInfo;
    }

    public ArrayList<VerifyTransferInfo> getData() {
        return listVerifyTransferInfo;
    }

    public void clear() {
        listVerifyTransferInfo.clear();
    }

    public int size() {
        return listVerifyTransferInfo.size();
    }

    public void add(VerifyTransferInfo transferInfo) {
        listVerifyTransferInfo.add(transferInfo);
    }

    public void add(int position, VerifyTransferInfo transferInfo) {
        listVerifyTransferInfo.add(position, transferInfo);
    }

    public void remove(int position) {
        listVerifyTransferInfo.remove(position);
    }

    public VerifyTransferInfo get(int position) {
        return listVerifyTransferInfo.get(position);
    }

    /**
     * 총이체액수 확인
     * @return 총이체액수
     */
    public double getTotalTransferAmount() {
        Double dAmount = 0.;
        for (int index = 0; index < listVerifyTransferInfo.size(); index++) {
            VerifyTransferInfo transferInfo = listVerifyTransferInfo.get(index);
            if (transferInfo == null)
                continue;

            String amount = transferInfo.getTRN_AMT();
            if (TextUtils.isEmpty(amount))
                continue;

            dAmount += Double.valueOf(amount);
        }
        return dAmount;
    }

    /**
     * 화면에서 세션에 있는 마지막 값 삭제 안할지 여부
     */
    public boolean getIsNotRemove() {
        return isNotRemove;
    }

    /**
     * 화면에서 세션에 있는 마지막 값 삭제 안할지 여부 설정
     * @param isNotRemove 삭제안할지 여부
     */
    public void setIsNotRemove(boolean isNotRemove) {
        this.isNotRemove = isNotRemove;
    }

    public void clearAll() {
        listVerifyTransferInfo.clear();
        isNotRemove = false;
    }
}