package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.adapter.TutorialAdapter;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;

/**
 * Saidabanking_android
 * <p>
 * Class: TutorialActivity
 * Created by 950485 on 2019. 06.05..
 * <p>
 * Description:Tutorial 화면
 */

public class TutorialActivity extends BaseActivity {
    private static final int DEVICE_STATE_NORMAL = 0;
    private static final int DEVICE_STATE_NEWUSER = 1;

    private ViewPager mViewPager;
    private ImageView mImageView01;
    private ImageView mImageView02;
    private ImageView mImageView03;
    private ImageView mImageView04;
    private ImageView mImageView05;
    private Button    mBtnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        mViewPager  = (ViewPager) findViewById(R.id.viewpager_tutorial);
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent;
                int deviceState = getIntent().getIntExtra("device_state", DEVICE_STATE_NORMAL);
                if (deviceState != DEVICE_STATE_NORMAL) {
                    Prefer.setPatternRegStatus(TutorialActivity.this, false);
                    Prefer.setPincodeRegStatus(TutorialActivity.this, false);
                    Prefer.setFidoRegStatus(TutorialActivity.this, false);
                    Prefer.setFlagFingerPrintChange(TutorialActivity.this, false);
                    intent = new Intent(TutorialActivity.this, SplashActivity.class);
                    EntryPoint curEntryPoint = (deviceState == DEVICE_STATE_NEWUSER) ? EntryPoint.SERVICE_JOIN : EntryPoint.DEVICE_CHANGE;
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.DEVICE_CHANGE, curEntryPoint);
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                } else {
                    intent = new Intent(TutorialActivity.this, PatternAuthActivity.class);
                    CommonUserInfo commonUserInfo = new CommonUserInfo();
                    if (Prefer.getFidoRegStatus(TutorialActivity.this)) {
                        commonUserInfo.setEntryStart(EntryPoint.LOGIN_BIO);
                        commonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                    } else {
                        commonUserInfo.setEntryStart(EntryPoint.LOGIN_PATTERN);
                        commonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                    }
                    commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                    commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                    if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL))
                        intent.putExtra(Const.INTENT_MAINWEB_URL, getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                }
                startActivity(intent);
                finish();
            }
        });

        mImageView01 = (ImageView) findViewById(R.id.imageview_tutorial_01);
        mImageView02 = (ImageView) findViewById(R.id.imageview_tutorial_02);
        mImageView03 = (ImageView) findViewById(R.id.imageview_tutorial_03);
        mImageView04 = (ImageView) findViewById(R.id.imageview_tutorial_04);
        mImageView05 = (ImageView) findViewById(R.id.imageview_tutorial_05);
		
        TutorialAdapter adapter = new TutorialAdapter(this);
        mViewPager.setAdapter(adapter);
        mImageView01.setImageResource(R.drawable.icon_tutorial_pageindicator_on);
        mImageView02.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
        mImageView03.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
        mImageView04.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
        mImageView05.setImageResource(R.drawable.icon_tutorial_pageindicator_off);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    mImageView01.setImageResource(R.drawable.icon_tutorial_pageindicator_on);
                    mImageView02.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView03.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView04.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView05.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                } else if (position == 1) {
                    mImageView01.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView02.setImageResource(R.drawable.icon_tutorial_pageindicator_on);
                    mImageView03.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView04.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView05.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                } else if (position == 2) {
                    mImageView01.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView02.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView03.setImageResource(R.drawable.icon_tutorial_pageindicator_on);
                    mImageView04.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView05.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                } else if (position == 3) {
                    mImageView01.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView02.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView03.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView04.setImageResource(R.drawable.icon_tutorial_pageindicator_on);
                    mImageView05.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                } else if (position == 4) {
                    mImageView01.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView02.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView03.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView04.setImageResource(R.drawable.icon_tutorial_pageindicator_off);
                    mImageView05.setImageResource(R.drawable.icon_tutorial_pageindicator_on);
                }

                if (position == 4) {
                    if (mBtnConfirm.getVisibility() == View.VISIBLE)
                        return;

                    mBtnConfirm.setVisibility(View.INVISIBLE);
                    Animation animation = AnimationUtils.loadAnimation(TutorialActivity.this, R.anim.sliding_in_bottom_confirm);
                    mBtnConfirm.clearAnimation();
                    mBtnConfirm.startAnimation(animation);
                    mBtnConfirm.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            mBtnConfirm.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    if (mBtnConfirm.getVisibility() == View.INVISIBLE)
                        return;

                    mBtnConfirm.setVisibility(View.INVISIBLE);
                    Animation animation = AnimationUtils.loadAnimation(TutorialActivity.this, R.anim.sliding_out_bottom_confirm);
                    mBtnConfirm.clearAnimation();
                    mBtnConfirm.startAnimation(animation);
                    mBtnConfirm.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            mBtnConfirm.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
     }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
