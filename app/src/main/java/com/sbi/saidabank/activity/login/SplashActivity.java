package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.test.SolutionTestActivity;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.solution.appsflyer.AppsFlyerManager;
import com.sbi.saidabank.solution.motp.MOTPManager;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        findViewById(R.id.btn_splash_next).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                goNextActivity();
            }
        });
        findViewById(R.id.ll_already_reg_user).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                goNextActivity();
            }
        });

        if (BuildConfig.DEBUG) {
            findViewById(R.id.splash_title).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SplashActivity.this, SolutionTestActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

        CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        if (commonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"1", "");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * 이용약관 화면으로 이동
     */
    public void goNextActivity() {
        final Intent intent = new Intent(this, PolicyActivity.class);

        String deviceID = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
        if (TextUtils.isEmpty(deviceID)) {
            LoginUserInfo.getInstance().setMOTPSerialNumber("");
            intent.putExtra(Const.INTENT_COMMON_INFO, getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO));
            startActivity(intent);
            finish();
        } else {
            showProgressDialog();
            MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                @Override
                public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                    dismissProgressDialog();
                    if (!"0000".equals(resultCode)) {
                        LoginUserInfo.getInstance().setMOTPSerialNumber("");
                    } else {
                        LoginUserInfo.getInstance().setMOTPSerialNumber(reqData1);
                    }
                    intent.putExtra(Const.INTENT_COMMON_INFO, getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO));
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}
