package com.sbi.saidabank.activity.certification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.initech.xsafe.cert.CertificateManager;
import com.initech.xsafe.cert.INIXSAFEException;
import com.initech.xsafe.cert.KeyPadCipher;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.solution.cert.NetResult;
import com.sbi.saidabank.solution.cert.NetServerConnector;
import com.softsecurity.transkey.TransKeyActivity;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.ProtocolException;

/**
 * Saidabank_android
 * Class: CertImportActivity
 * Created by 950469 on 2018. 9. 11..
 * <p>
 * Description:
 * 인증서 가져오기 화면
 */
public class CertImportActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged {
    private static final int REQUEST_TRANSKEYPAD = 100;

    private enum ImportStep {
        STEP_01,
        STEP_02
    }

    private EditText mEditAuthNumFront;
    private EditText mEditAuthNumEnd;
    private Button   mBtnImportCert;
    private RelativeLayout mLayoutScreen;
    private InputMethodManager imm = null;

    private static String     mPkcs12Data = "";
    private static ImportStep mImportStep = ImportStep.STEP_01;

    private ImportCertTaskV12 importTask;
    private SaveCertTaskV12   saveTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cert_import);
        initUX();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancelExportTask();
        cancelSaveTask();
        mPkcs12Data = "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_TRANSKEYPAD:
                if (resultCode == RESULT_OK) {
                    String passChiper = data.getStringExtra(Const.INTENT_TRANSKEY_CERT_CHIPER_DATA);
                    String pkcs12data = data.getStringExtra(Const.INTENT_TRANSKEY_PARAMETER);

                    String strAuthFront = mEditAuthNumFront.getText().toString();
                    String strAuthEnd = mEditAuthNumEnd.getText().toString();

                    StringBuilder authNum = new StringBuilder();
                    authNum.append(strAuthFront);
                    authNum.append(strAuthEnd);

                    cancelExportTask();
                    startExportSaveTask(pkcs12data, passChiper, authNum.toString());
                } else {
                    finish();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        ImageView btnCancel = (ImageView) findViewById(R.id.btn_cancel_import_cert);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mLayoutScreen = (RelativeLayout) findViewById(R.id.ll_root);
        mLayoutScreen.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                imm.hideSoftInputFromWindow(mEditAuthNumFront.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEditAuthNumEnd.getWindowToken(), 0);
            }
        });

        mEditAuthNumFront = (EditText) findViewById(R.id.auth_num_front);
        mEditAuthNumFront.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int len = editable.toString().length();
                if (len == 4) {
                    mEditAuthNumFront.clearFocus();
                    mEditAuthNumEnd.requestFocus();

                    String end = mEditAuthNumEnd.getText().toString();
                    if (!TextUtils.isEmpty(end))
                        mEditAuthNumEnd.setSelection(end.length());
                }
            }
        });

        mEditAuthNumEnd = (EditText) findViewById(R.id.auth_num_end);
        mEditAuthNumEnd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int len = editable.toString().length();
                if (len == 0) {
                    mEditAuthNumFront.requestFocus();
                    String front = mEditAuthNumFront.getText().toString();
                    if (!TextUtils.isEmpty(front))
                        mEditAuthNumFront.setSelection(front.length());

                    mEditAuthNumEnd.clearFocus();
                }
            }
        });

        mBtnImportCert = findViewById(R.id.btn_import_cert);
        mBtnImportCert.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String strAuthFront  = mEditAuthNumFront.getText().toString();
                String strAuthEnd = mEditAuthNumEnd.getText().toString();

                if (TextUtils.isEmpty(strAuthFront) || strAuthFront.length() < 4) {
                    DialogUtil.alert(CertImportActivity.this,"인증번호 첫번째자리 4자리를 입력하세요.",new View.OnClickListener(){

                        @Override
                        public void onClick(View v) {
                            mEditAuthNumFront.requestFocus();
                        }
                    });
                    return;
                }

                if (TextUtils.isEmpty(strAuthEnd)||strAuthEnd.length() < 4) {
                    DialogUtil.alert(CertImportActivity.this,"인증번호 두번째자리 4자리를 입력하세요.",new View.OnClickListener(){

                        @Override
                        public void onClick(View v) {
                            mEditAuthNumEnd.requestFocus();
                        }
                    });
                    return;
                }

                StringBuilder authNum = new StringBuilder();
                authNum.append(strAuthFront);
                authNum.append(strAuthEnd);

                if (mImportStep == ImportStep.STEP_01)
                    startExportTask(authNum.toString());
                if (mImportStep == ImportStep.STEP_02) {
                    int keyType = TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;

                    Intent intentKey = new Intent(CertImportActivity.this, CertAuthPWActivity.class);
                    intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
                    intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, "인증서 가져오기");
                    intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, "인증서 비밀번호를 입력하세요.");
                    intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 30);
                    intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, 2);
                    intentKey.putExtra(Const.INTENT_TRANSKEY_PARAMETER, mPkcs12Data);
                    startActivityForResult(intentKey, REQUEST_TRANSKEYPAD);
                    overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                }
            }
        });

        TextView textDesc04 = (TextView) findViewById(R.id.textview_import_cert_desc_04);
        String desc04 = getString(R.string.import_cert_desc_04);
        textDesc04.setText(desc04.replaceAll(".(?!$)", "$0\u200b"));
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(this);
    }

    public void cancelExportTask() {
        if (importTask != null) {
            importTask.cancel(true);
            importTask = null;
        }
    }

    public void startExportTask(String authNum) {
        cancelExportTask();

        if (TextUtils.isEmpty(authNum))
            return;

        importTask = new ImportCertTaskV12(CertImportActivity.this);
        importTask.execute(authNum);
    }

    /**
     * v1.2 인증서 가져오기
     */
    private static class ImportCertTaskV12 extends AsyncTask<String, Integer, Integer> {
        private WeakReference<CertImportActivity> activityReference;
        private String taskAuthCode;

        ImportCertTaskV12(CertImportActivity context){
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            CertImportActivity activity = activityReference.get();
            activity.showProgressDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {

            NetServerConnector importHttps;
            NetResult netRes;

            CertImportActivity activity = activityReference.get();
            taskAuthCode = params[0];

            try {
                // 인증서 가져오기 전송
                importHttps = new NetServerConnector(SaidaUrl.ReqUrl.URL_CERT_EXPORT_IMPORT_URL.getReqUrl());
                importHttps.setParameter("SVer", "1.2");
                importHttps.setParameter("Action", "IMPORT");
                importHttps.setParameter("AuthNum", taskAuthCode);
                netRes = importHttps.getNetResponse();

                if (netRes.isOk()) {
                    mPkcs12Data = netRes.getResMessage(0);
                } else {
                    return -1;
                }
            } catch (ProtocolException | MalformedURLException e) {
                Logs.printException(e);
                return -1;
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            CertImportActivity activity = activityReference.get();
            activity.dismissProgressDialog();

            if (result == 0) {
                int keyType = TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;

                Intent intentKey = new Intent(activity, CertAuthPWActivity.class);
                intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
                intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, "인증서 가져오기");
                intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, "인증서 비밀번호를 입력하세요.");
                intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 30);
                intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, 2);
                intentKey.putExtra(Const.INTENT_TRANSKEY_PARAMETER, mPkcs12Data);
                activity.startActivityForResult(intentKey, REQUEST_TRANSKEYPAD);
                activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                mImportStep = ImportStep.STEP_02;
            } else {
                DialogUtil.alert(activity, activity.getString(R.string.cert_message_error_import_fail));
                mImportStep = ImportStep.STEP_01;
                mPkcs12Data = "";
            }
        }
    }

    public void cancelSaveTask() {
        if (saveTask != null) {
            saveTask.cancel(true);
            saveTask = null;
        }
    }

    public void startExportSaveTask(String pkcs12data, String passChiper, String authNum) {
        cancelSaveTask();

        if (TextUtils.isEmpty(authNum))
            return;

        saveTask = new SaveCertTaskV12(CertImportActivity.this);
        saveTask.execute(pkcs12data, passChiper, authNum);
    }

    /**
     * v1.2 가져온 인증서 저장
     * @author Justin
     */
    private static class SaveCertTaskV12 extends AsyncTask<String, Integer, Integer> {
        private WeakReference<CertImportActivity> activityReference;

        private String taskPkcs12Data;
        private String taskAuthCode;
        private String resMsg = null;

        SaveCertTaskV12(CertImportActivity context){
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            CertImportActivity activity = activityReference.get();
            activity.showProgressDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {
            CertImportActivity activity = activityReference.get();
            NetServerConnector setStatusHttps;
            NetResult netRes;
            boolean doneSave;
            boolean doneFail = false;

            taskPkcs12Data = params[0];
            String password = params[1];
            taskAuthCode = params[2];

            Logs.e("taskPkcs12Data : " + taskPkcs12Data);
            Logs.e("mAuthCode : " + taskAuthCode);
            Logs.e("password : " + password);

            CertificateManager mCertMgt = new CertificateManager();
            // 가져온 인증서 저장
            try {
                // 복호화 모드(NONE : 암호화 되어있지 않음, NFILTER : NFilter 복호화, SPINPAD : SPinPad 복호화, MTRANSKEY : Raon mTransKey 복호화)
                doneSave = mCertMgt.importCertificate_v12(taskPkcs12Data, password, KeyPadCipher.MODE_MTRANSKEY);
            } catch (INIXSAFEException e1) {
                if (INIXSAFEException.NOT_MATCHED_PASSWORD.equals(e1.getErrorCode())) {
                    resMsg = activity.getString(R.string.cert_message_no_match_import);
                } else {
                    resMsg = activity.getString(R.string.cert_message_error_save_fail);
                    doneFail = true;
                }
                Logs.printException(e1);
                doneSave = false;
            }

            try {
                setStatusHttps = new NetServerConnector(SaidaUrl.ReqUrl.URL_CERT_EXPORT_IMPORT_URL.getReqUrl());
                setStatusHttps.setParameter("SVer", "1.2");
                setStatusHttps.setParameter("Action", "SET_STATUS");
                setStatusHttps.setParameter("Status", (doneSave) ? "COMPLETE" : "CANCEL");
                setStatusHttps.setParameter("AuthNum", taskAuthCode);
                netRes = setStatusHttps.getNetResponse();

                if (netRes.isOk()) {
                    if (doneFail) {
                        return -1;
                    } else if (!doneSave) {
                        return -2;
                    }
                    resMsg = activity.getString(R.string.cert_message_done_save);
                } else {
                    //res = netRes.getFullResMessage();
                    resMsg = activity.getString(R.string.cert_message_error_save_fail);
                    return -1;
                }
            } catch (ProtocolException | MalformedURLException e) {
                Logs.printException(e);
                resMsg = activity.getString(R.string.cert_message_error_save_fail);
                return -1;
            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            final CertImportActivity activity = activityReference.get();
            activity.dismissProgressDialog();

            if (result == 0) { // SUCCESS
                if(activity == null || activity.isFinishing()){
                    return;
                }

                DialogUtil.alert(activity, resMsg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent rtnIntent = new Intent();
                        activity.setResult(Activity.RESULT_OK, rtnIntent);
                        activity.finish();
                    }
                });
            } else if (result == -2) {
                DialogUtil.alert(activity, resMsg);
            } else {
                DialogUtil.alert(activity, resMsg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.finish();
                    }
                });
            }
        }
    }

    @Override
    public void onKeyboardShown() {
        mBtnImportCert.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        mBtnImportCert.setVisibility(View.VISIBLE);
    }

}
