package com.sbi.saidabank.activity.crop;

/**
 * Saidabanking_android
 *
 * Class: CropImageFragment
 * Created by 950485 on 2018. 10. 15..
 * <p>
 * Description:화면 Crop을 위한 화면
 */

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.crop.CropImagePreset;
import com.sbi.saidabank.common.crop.CropImageViewOptions;
import com.sbi.saidabank.common.util.Logs;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

public final class CropImageFragment extends Fragment implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {
    private CropImagePreset mDemoPreset;
    public CropImageView mCropImageView;

    public static CropImageFragment newInstance(CropImagePreset demoPreset) {
        CropImageFragment fragment = new CropImageFragment();
        Bundle args = new Bundle();
        args.putString("DEMO_PRESET", demoPreset.name());
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * crop된 이미지뷰에 uri 설정
     * @param imageUri 이미지 uri
     */
    public void setImageUri(Uri imageUri) {
        mCropImageView.setImageUriAsync(imageUri);
    }

    /**
     * crop image view 초기값 설정
     * @param options crop image view 옵션
     */
    public void setCropImageViewOptions(CropImageViewOptions options) {
        mCropImageView.setScaleType(options.scaleType);
        mCropImageView.setCropShape(options.cropShape);
        mCropImageView.setGuidelines(options.guidelines);
        mCropImageView.setAspectRatio(options.aspectRatio.first, options.aspectRatio.second);
        mCropImageView.setFixedAspectRatio(options.fixAspectRatio);
        mCropImageView.setMultiTouchEnabled(options.multitouch);
        mCropImageView.setShowCropOverlay(options.showCropOverlay);
        mCropImageView.setShowProgressBar(options.showProgressBar);
        mCropImageView.setAutoZoomEnabled(options.autoZoomEnabled);
        mCropImageView.setMaxZoom(options.maxZoomLevel);
        mCropImageView.setFlippedHorizontally(options.flipHorizontally);
        mCropImageView.setFlippedVertically(options.flipVertically);
    }

    /**
     * crop 사각형 화면 크기 설정
     */
    public void setInitialCropRect() {
        mCropImageView.setCropRect(new Rect(100, 300, 500, 1200));
    }

    /**
     * crop 사각형 화면 크기 초기화
     */
    public void resetCropRect() {
        mCropImageView.resetCropRect();
    }

    /**
     * crop 옵션 업데이트
     */
    public void updateCurrentCropViewOptions() {
        CropImageViewOptions options = new CropImageViewOptions();
        options.scaleType = mCropImageView.getScaleType();
        options.cropShape = mCropImageView.getCropShape();
        options.guidelines = mCropImageView.getGuidelines();
        options.aspectRatio = mCropImageView.getAspectRatio();
        options.fixAspectRatio = mCropImageView.isFixAspectRatio();
        options.showCropOverlay = mCropImageView.isShowCropOverlay();
        options.showProgressBar = mCropImageView.isShowProgressBar();
        options.autoZoomEnabled = mCropImageView.isAutoZoomEnabled();
        options.maxZoomLevel = mCropImageView.getMaxZoom();
        options.flipHorizontally = mCropImageView.isFlippedHorizontally();
        options.flipVertically = mCropImageView.isFlippedVertically();
        ((CropImageActivity) getActivity()).setCurrentOptions(options);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;
        switch (mDemoPreset) {
            case RECT:
                rootView = inflater.inflate(R.layout.fragment_crop_rect, container, false);
            break;

            case CIRCULAR:
                rootView = inflater.inflate(R.layout.fragment_crop_oval, container, false);
            break;

            case CUSTOMIZED_OVERLAY:
                rootView = inflater.inflate(R.layout.fragment_crop_customized, container, false);
            break;

            case MIN_MAX_OVERRIDE:
                rootView = inflater.inflate(R.layout.fragment_crop_min_max, container, false);
            break;

            case SCALE_CENTER_INSIDE:
                rootView = inflater.inflate(R.layout.fragment_main_scale_center, container, false);
            break;

            case CUSTOM:
                rootView = inflater.inflate(R.layout.fragment_crop_rect, container, false);
            break;

            default:
                throw new IllegalStateException("Unknown preset: " + mDemoPreset);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCropImageView = view.findViewById(R.id.cropImageView);
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);
        mCropImageView.setAutoZoomEnabled(false);

        updateCurrentCropViewOptions();

        if (savedInstanceState == null) {
            Uri targetUrl = ((CropImageActivity) getActivity()).getTargetImageUri();
            if (targetUrl != null) {
                setImageUri(targetUrl);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mDemoPreset = CropImagePreset.valueOf(getArguments().getString("DEMO_PRESET"));
        ((CropImageActivity) activity).setCurrentFragment(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCropImageView != null) {
            mCropImageView.setOnSetImageUriCompleteListener(null);
            mCropImageView.setOnCropImageCompleteListener(null);
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            //Toast.makeText(getActivity(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            handleCropResult(result);
        }
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {
            Intent intent = new Intent(getActivity(), CropImageResultActivity.class);
            if (result.getUri() != null) {
                intent.putExtra("URI", result.getUri());
            } else {
                CropImageResultActivity.mCropImage = mCropImageView.getCropShape() == CropImageView.CropShape.OVAL ? CropImage.toOvalBitmap(result.getBitmap()) : result.getBitmap();
            }
            getActivity().startActivityForResult(intent, Const.REQUEST_CROP_IMAGE);
        } else {
            Toast.makeText(getActivity(), "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_LONG) .show();
        }
    }
}
