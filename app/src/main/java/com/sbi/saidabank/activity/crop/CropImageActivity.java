package com.sbi.saidabank.activity.crop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.crop.CropImagePreset;
import com.sbi.saidabank.common.crop.CropImageViewOptions;
import com.sbi.saidabank.common.util.Logs;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Saidabanking_android
 *
 * Class: CropImageActivity
 * Created by 950485 on 2018. 10. 15..
 * <p>
 * Description:화면 Crop을 위한 화면
 */

public class CropImageActivity extends BaseActivity {
    private CropImageFragment mCurrentFragment;

    private Uri mTargetImageUri;
    private Uri mCropImageUri;

    private CropImageViewOptions mCropImageViewOptions = new CropImageViewOptions();

    public void setCurrentFragment(CropImageFragment fragment) {
        mCurrentFragment = fragment;
    }

    public void setCurrentOptions(CropImageViewOptions options) {
        mCropImageViewOptions = options;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        getExtra();
        initUX();

        if (savedInstanceState == null) {
            //setMainFragmentByPreset(CropImagePreset.RECT);
            setMainFragmentByPreset(CropImagePreset.CIRCULAR);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mCurrentFragment.updateCurrentCropViewOptions();
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                mCropImageUri = imageUri;
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                mCurrentFragment.setImageUri(imageUri);
            }
        } else if (requestCode == Const.REQUEST_CROP_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (CropImageResultActivity.mCropImage != null) {
                    Uri uri = saveBitmapToJpeg(CropImageResultActivity.mCropImage);
                    if (uri != null) {
                        Intent intent = new Intent();
                        intent.putExtra(Const.CROP_IMAGE_URI, uri.toString());
                        setResult(RESULT_OK, intent);
                    }

                    finish();
                }
            } else {
                Toast.makeText(CropImageActivity.this, "cancel, crop image", Toast.LENGTH_SHORT).show();
            }
            CropImageResultActivity.mCropImage = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mCurrentFragment.setImageUri(mCropImageUri);
            } else {

            }
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String uri = bundle.getString(Const.CROP_IMAGE_URI);
            if (!TextUtils.isEmpty(uri))
                mTargetImageUri = Uri.parse(uri);
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        Button btnOk = (Button) findViewById(R.id.btn_crop_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentFragment.mCropImageView.getCroppedImageAsync();
            }
        });

        Button btnCancel = (Button) findViewById(R.id.btn_crop_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setMainFragmentByPreset(CropImagePreset demoPreset) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.container_crop, CropImageFragment.newInstance(demoPreset))
                .commit();
    }

    /**
     * 저장될 jpeg 파일 uri
     * @return 저장될 jpeg 파일 uri
     */
    public Uri getTargetImageUri() {
        return mTargetImageUri;
    }

    /**
     * SDCard에 이미지 저장 uri 생성 (input Bitmap -> saved file JPEG)
     * @param bitmap : bitmap file
     */
    public Uri saveBitmapToJpeg(Bitmap bitmap)  {
        Logs.e("saveBitmapToJpeg");
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }

        File file = new File(storageDir, Const.CROP_IMAGE_PATH);
        try {
            FileOutputStream out = new FileOutputStream(file, false);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException exception) {
            Logs.e(exception.getMessage());
            return null;
        } catch (IOException exception) {
            Logs.e(exception.getMessage());
            return null;
        }
        Uri uri = Uri.fromFile(file);
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
        return uri;
    }
}
