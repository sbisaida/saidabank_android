package com.sbi.saidabank.activity.certification;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.solution.cert.NetResult;
import com.sbi.saidabank.solution.cert.NetServerConnector;

import java.lang.ref.WeakReference;

/**
 * Saidabank_android
 * Class: CertExportActivity
 * Created by 950469 on 2018. 9. 10..
 * <p>
 * Description:
 * 인증서 내보내기 화면
 */
public class CertExportActivity extends BaseActivity {
    private static final int INSERT_CHECK_COUNT = 1 * 60;
    private static final int COMPLETE_CHECK_COUNT = 3 * 60;

    private TextView   mTextAuthNumFront01;
    private TextView   mTextAuthNumFront02;
    private TextView   mTextAuthNumFront03;
    private TextView   mTextAuthNumFront04;
    private TextView   mTextAuthNumEnd01;
    private TextView   mTextAuthNumEnd02;
    private TextView   mTextAuthNumEnd03;
    private TextView   mTextAuthNumEnd04;

    private String       mAuthCode;
    private int          mCertIndex;
    private String       mPassChiper;
    private static int  mCheckAuthCount = -1;

    private CertExportWaitSendTask exportWaitTask = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cert_export);

        mAuthCode = getIntent().getStringExtra(Const.INTENT_CERT_AUTHCODE);
        mCertIndex = getIntent().getIntExtra(Const.INTENT_CERT_INDEX, -1);
        mPassChiper = getIntent().getStringExtra(Const.INTENT_CERT_PASSWORD);

        initUX();

        startExportWaitTask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancelExportWaitTask();
        mCheckAuthCount = -1;
    }

    private void initUX() {
        ImageView btnCancel = (ImageView) findViewById(R.id.btn_cancel_cert_export);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mTextAuthNumFront01 = (TextView) findViewById(R.id.auth_num_front_01);
        mTextAuthNumFront02 = (TextView) findViewById(R.id.auth_num_front_02);
        mTextAuthNumFront03 = (TextView) findViewById(R.id.auth_num_front_03);
        mTextAuthNumFront04 = (TextView) findViewById(R.id.auth_num_front_04);
        mTextAuthNumEnd01 = (TextView) findViewById(R.id.auth_num_end_01);
        mTextAuthNumEnd02 = (TextView) findViewById(R.id.auth_num_end_02);
        mTextAuthNumEnd03 = (TextView) findViewById(R.id.auth_num_end_03);
        mTextAuthNumEnd04 = (TextView) findViewById(R.id.auth_num_end_04);

        mTextAuthNumFront01.setText(String.valueOf(mAuthCode.charAt(0)));
        mTextAuthNumFront02.setText(String.valueOf(mAuthCode.charAt(1)));
        mTextAuthNumFront03.setText(String.valueOf(mAuthCode.charAt(2)));
        mTextAuthNumFront04.setText(String.valueOf(mAuthCode.charAt(3)));
        mTextAuthNumEnd01.setText(String.valueOf(mAuthCode.charAt(4)));
        mTextAuthNumEnd02.setText(String.valueOf(mAuthCode.charAt(5)));
        mTextAuthNumEnd03.setText(String.valueOf(mAuthCode.charAt(6)));
        mTextAuthNumEnd04.setText(String.valueOf(mAuthCode.charAt(7)));

        TextView textDesc04 = (TextView) findViewById(R.id.textview_export_cert_desc_04);
        String desc04 = getString(R.string.export_cert_desc_04);
        textDesc04.setText(desc04.replaceAll(".(?!$)", "$0\u200b"));
    }

    public void cancelExportWaitTask() {
        if (exportWaitTask != null) {
            exportWaitTask.cancel(true);
            exportWaitTask = null;
        }
    }

    public void startExportWaitTask() {
        cancelExportWaitTask();

        exportWaitTask = new CertExportWaitSendTask(this);
        exportWaitTask.execute(mAuthCode);
    }

    public static class CertExportWaitSendTask extends AsyncTask<String, Integer, Integer> {
        private WeakReference<Activity> activityReference;
        private String resMsg = "";

        private boolean mStartSend;

        public CertExportWaitSendTask(Activity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(String... params) {
            String authCode = params[0];
            Activity activity = activityReference.get();
            if (activity == null)
                return -1;

            NetServerConnector exportHttps = null;
            NetResult netRes = new NetResult();

            for (int i = 0; i < INSERT_CHECK_COUNT; i++) {
                try {
                    mCheckAuthCount++;

                    Thread.sleep(1000);

                    // PKCS12 내보내고 내보내기 시작하는지 체크
                    exportHttps = new NetServerConnector(SaidaUrl.ReqUrl.URL_CERT_EXPORT_IMPORT_URL.getReqUrl());
                    exportHttps.setParameter("SVer", "1.2");
                    exportHttps.setParameter("AuthNum", authCode);
                    exportHttps.setParameter("Action", "GET_STATUS");
                    netRes = exportHttps.getNetResponse();	// 인증코드

                    if (netRes.isOk()) {
                        String retMsg = netRes.getResMessage(0);
                        Logs.i("retMsg :" + retMsg);
                        // 내보내기 시작
                        if (retMsg.equalsIgnoreCase("SEND")) {
                            mStartSend = true;
                            break;
                        }
                    } else {
                        //resMsg = netRes.getFullResMessage();
                        Logs.e(netRes.getFullResMessage());
                        return -1;
                    }

                } catch (Exception e) {
                    Logs.printException(e);
                    return -1;
                }
            }

            // 시간이 지나서 나온경우
            if (!mStartSend) {
                resMsg = activity.getString(R.string.cert_message_expire_waiting_time);
                return -1;
            }

            exportHttps = null;
            for (int i = 0; i < COMPLETE_CHECK_COUNT - mCheckAuthCount; i++) {
                try {
                    Thread.sleep(1000);

                    // PKCS12 내보내고 인증코드 받기
                    exportHttps = new NetServerConnector(SaidaUrl.ReqUrl.URL_CERT_EXPORT_IMPORT_URL.getReqUrl());
                    exportHttps.setParameter("SVer", "1.2");
                    exportHttps.setParameter("AuthNum", authCode);
                    exportHttps.setParameter("Action", "GET_STATUS");
                    netRes = exportHttps.getNetResponse();	// 인증코드

                    if (netRes.isOk()) {
                        String retMsg = netRes.getResMessage(0);
                        Logs.i("retMsg :" + retMsg);
                        if (retMsg.equalsIgnoreCase("COMPLETE")) {
                            return 0;
                        } else if (netRes.getResMessage(0).equalsIgnoreCase("CANCEL")) {
                            resMsg = activity.getString(R.string.cert_message_error_move_to_opposite);
                            return -1;
                        }
                    } else {
                        //resMsg = netRes.getFullResMessage();
                        return -1;
                    }
                } catch (Exception e) {
                    Logs.printException(e);
                    //resMsg = e.toString();
                    return -1;
                }
            }

            resMsg = activity.getString(R.string.cert_message_expire_waiting_time);
            return -1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            mCheckAuthCount = -1;

            final Activity activity = activityReference.get();
            if(activity == null || activity.isFinishing()){
                return;
            }

            if (result == 0) {	// 다른 Entity의 인증서 이동을 대기한다
                resMsg = activity.getString(R.string.cert_message_success_export_cert);
                DialogUtil.alert(activity, resMsg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent rtnIntent = new Intent();
                        activity.setResult(Activity.RESULT_OK, rtnIntent);
                        activity.finish();
                    }
                });
            } else {			// FAIL
                if (TextUtils.isEmpty(resMsg)) {
                    resMsg = activity.getString(R.string.cert_message_error_export_cert);
                }

                DialogUtil.alert(activity, resMsg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.finish();
                    }
                });
            }
        }
    }
}
