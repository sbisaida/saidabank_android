package com.sbi.saidabank.activity.certification;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.initech.xsafe.cert.INIXSAFEException;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.solution.cert.CustomCertManager;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Saidabank_android
 * Class: CertExportActivity
 * Created by 950469 on 2018. 9. 10..
 * <p>
 * Description:
 * 보안키패드 입력필드가 1개짜리 처리 화면
 */
public class CertAuthPWActivity extends BaseActivity implements ITransKeyActionListener, ITransKeyActionListenerEx, ITransKeyCallbackListener,View.OnClickListener {
    //private static final int PASSWORD_ERR_TYPE_NULL      = 0;
    private static final int PASSWORD_ERR_TYPE_NO_MSG    = 1;
    private static final int PASSWORD_ERR_TYPE_SHORT_MSG = 2;
    private static final int PASSWORD_ERR_TYPE_NOMATCH   = 3;
    private static final long MIN_CLICK_INTERVAL 		 = 800;    // ms

    private TransKeyCtrl mTKMgr = null;


    private int mCertIndex;
    private int mKeypadType;
    private long mLastClickTime;

    private int mMinLength;
    private String mInputId;
    private String mParameter;

    private RelativeLayout mLayoutErrmsg;
    private TextView mTvErrmsg;
    private boolean mIsImportCert;
    private int mFailCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transkey_input_one);

        Intent intent = getIntent();

        mCertIndex = intent.getIntExtra(Const.INTENT_CERT_INDEX, -1);
        mKeypadType = intent.getIntExtra(Const.INTENT_TRANSKEY_PADTYPE, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
        int mTextType = intent.getIntExtra(Const.INTENT_TRANSKEY_TEXTTYPE, TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE);
        String title = intent.getStringExtra(Const.INTENT_TRANSKEY_TITLE);
        if (!TextUtils.isEmpty(title) && title.contains("가져오기"))
            mIsImportCert = true;
        else
            mIsImportCert = false;
        String label = intent.getStringExtra(Const.INTENT_TRANSKEY_LABEL1);
        int max_length = intent.getIntExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 0);
        mMinLength = intent.getIntExtra(Const.INTENT_TRANSKEY_MINLENGTH, 0);
        boolean precaution = intent.getBooleanExtra(Const.INTENT_TRANSKEY_PRECAUTION, false);
        mInputId = intent.getStringExtra(Const.INTENT_TRANSKEY_INPUTID1);
        mParameter = intent.getStringExtra(Const.INTENT_TRANSKEY_PARAMETER);
        if (TextUtils.isEmpty(mParameter)) mParameter = "";
        boolean mCationShow = intent.getBooleanExtra(Const.INTENT_TRANSKEY_CAPTION, false);

        mLayoutErrmsg = findViewById(R.id.ll_alert_msg);
        mTvErrmsg = findViewById(R.id.tv_errmsg);

        ((TextView) findViewById(R.id.tv_title)).setText(title);
        ((TextView) findViewById(R.id.tv_label)).setText(label);

        if (mCationShow) {
            ((LinearLayout) findViewById(R.id.tv_caption_title)).setVisibility(View.VISIBLE);
            LinearLayout mCaptionExpLayout = ((LinearLayout) findViewById(R.id.tv_caption_explain));
            mCaptionExpLayout.setVisibility(View.VISIBLE);
        }

        RelativeLayout Reinputlayout01 = findViewById(R.id.inputlayout01);

        EditText mEditText = Reinputlayout01.findViewById(R.id.editText);

        mTKMgr = TransKeyUtils.initTransKeyPad(this, 0, mKeypadType,
                mTextType,
                label,//label
                "",//hint
                max_length,//max length
                "",//getString(R.string.message_over_max_length,max_length),//max length msg
                mMinLength,//min length
                "",//min length msg
                5,
                true,
                (FrameLayout) findViewById(R.id.keypadContainer),
                mEditText,
                (HorizontalScrollView) Reinputlayout01.findViewById(R.id.keyscroll),
                (LinearLayout) Reinputlayout01.findViewById(R.id.keylayout),
                (ImageButton) Reinputlayout01.findViewById(R.id.clearall),
                (RelativeLayout) findViewById(R.id.keypadBallon),
                null,
                false,
        false);


        if (mTKMgr != null) {
            mTKMgr.showKeypad(mKeypadType);
        }

        findViewById(R.id.btn_cancel).setOnClickListener(this);

        mFailCount = 0;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
    }

    private void setAlertMsg(int errtype, boolean setvisible) {
        if (!setvisible) {
            if (mLayoutErrmsg.getVisibility() != View.INVISIBLE)
                mLayoutErrmsg.setVisibility(View.INVISIBLE);
            return;
        }

        switch (errtype) {
            case PASSWORD_ERR_TYPE_NO_MSG:
            case PASSWORD_ERR_TYPE_SHORT_MSG: {
                mTvErrmsg.setText(getString(R.string.message_least_min_length, mMinLength));
                break;
            }
            case PASSWORD_ERR_TYPE_NOMATCH: {
                mFailCount++;
                if (mFailCount < 5) {
                    mTvErrmsg.setText(getString(R.string.cert_message_not_matched_password) + "(" + mFailCount + "/5)");
                } else {
                    mLayoutErrmsg.setVisibility(View.INVISIBLE);
                    DialogUtil.alert(this, getString(R.string.cert_message_retry_password), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                    return;
                }
                break;
            }
            default:
                break;
        }

        if (mLayoutErrmsg.getVisibility() != View.VISIBLE)
            mLayoutErrmsg.setVisibility(View.VISIBLE);

        mTKMgr.setReArrangeKeapad(true);
        mTKMgr.showKeypad(mKeypadType);
    }

    @Override
    public void cancel(Intent data) {
        setResult(RESULT_CANCELED, null);
        finish();
        overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
    }

    @Override
    public void done(Intent data) {
        if (data == null) {
            setAlertMsg(PASSWORD_ERR_TYPE_NO_MSG, true);
            return;
        }

        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복클릭 방지
            return;
        }

        String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
        if (TextUtils.isEmpty(dummyData) || dummyData.length() < mMinLength) {
            setAlertMsg(PASSWORD_ERR_TYPE_SHORT_MSG, true);
            return;
        }

        String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);

        String plainData = TransKeyUtils.decryptPlainDataFromCipherData(data);
        String passChiperData = TransKeyUtils.getTransKeyPassCipherData(data);

        Logs.i("secureData : " + secureData);
        Logs.i("passChiperData : " + passChiperData);
        Logs.i("dummyData : " + dummyData);

        if (mCertIndex >= 0) {
            try {
                Boolean result = CustomCertManager.getInstance().checkPassword(this, mCertIndex, passChiperData);
                if (!result) {
                    setAlertMsg(PASSWORD_ERR_TYPE_NOMATCH, true);
                    return;
                } else {
                    final Intent intent = new Intent();
                    intent.putExtra(Const.INTENT_TRANSKEY_INPUTID1, mInputId);
                    intent.putExtra(Const.INTENT_TRANSKEY_SECURE_DATA1, secureData);
                    intent.putExtra(Const.INTENT_TRANSKEY_CERT_CHIPER_DATA, passChiperData);
                    intent.putExtra(Const.INTENT_TRANSKEY_PLAIN_DATA1, plainData.toString());
                    intent.putExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1, dummyData);
                    intent.putExtra(Const.INTENT_TRANSKEY_PARAMETER, mParameter);

                    if (mIsImportCert) {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                setResult(RESULT_OK, intent);
                                finish();
                                overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
                            }
                        };
                        showErrorMessage("인증서 가져오기를 성공하였습니다.", okClick);
                    } else {
                        setResult(RESULT_OK, intent);
                        finish();
                        overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
                    }
                }
            } catch (INIXSAFEException e) {
                Logs.printException(e);
                setResult(RESULT_CANCELED);
                finish();
                overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
            }
        } else {
            Intent intent = new Intent();
            intent.putExtra(Const.INTENT_TRANSKEY_INPUTID1, mInputId);
            intent.putExtra(Const.INTENT_TRANSKEY_SECURE_DATA1, secureData);
            intent.putExtra(Const.INTENT_TRANSKEY_CERT_CHIPER_DATA, passChiperData);
            intent.putExtra(Const.INTENT_TRANSKEY_PLAIN_DATA1, plainData.toString());
            intent.putExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1, dummyData);
            intent.putExtra(Const.INTENT_TRANSKEY_PARAMETER, mParameter);
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
        }
    }

    @Override
    public void input(int type) {
        Logs.e("input");
    }

    @Override
    public void minTextSizeCallback() {
        Logs.e("minTextSizeCallback() call");
    }

    @Override
    public void maxTextSizeCallback() {
        Logs.e("maxTextSizeCallback() call");
    }

    @Override
    public void onClick(View v) {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
    }
}
