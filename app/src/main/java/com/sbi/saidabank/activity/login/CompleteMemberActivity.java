package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.solution.appsflyer.AppsFlyerManager;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;

/**
 * Saidabanking_android
 *
 * Class: CompleteMemberActivity
 * Created by 950485 on 2018. 10. 25..
 * <p>
 * Description:신규재등록 완료를 위한 화면
 */

public class CompleteMemberActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_complete_member);

        initUX();
    }

    @Override
    public void onBackPressed() {
	    //super.onBackPressed();
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        final CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);

        if (!commonUserInfo.getName().isEmpty()) {
            if (!TextUtils.isEmpty(commonUserInfo.getName())) {
                TextView textDesc = (TextView) findViewById(R.id.textview_complete_member_desc);
                String desc01 = String.format(getString(R.string.account_customer_desc_01), commonUserInfo.getName());
                textDesc.setText(desc01);
            }
        }

        LinearLayout layoutFingerprint = (LinearLayout) findViewById(R.id.layout_use_fingerprint_complete_member);

        Boolean isFingerpringt = Prefer.getFidoRegStatus(CompleteMemberActivity.this);
        if (isFingerpringt) {
            layoutFingerprint.setVisibility(View.VISIBLE);
        } else {
            if (StonePassUtils.hasFingerprintDevice(this) == 0) {
                ImageView imageView = findViewById(R.id.imageview_check_finger);
                imageView.setImageResource(R.drawable.btn_check_off);
                TextView textCheckFinger = findViewById(R.id.textview_check_finger);
                textCheckFinger.setTextColor(getResources().getColor(R.color.color888888));
                textCheckFinger.setText(R.string.msg_no_use_fingerprint);
                layoutFingerprint.setVisibility(View.VISIBLE);
            }
        }

        Button btnOK = (Button) findViewById(R.id.btn_ok_complete_member);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CompleteMemberActivity.this, PatternAuthActivity.class);
                if (Prefer.getFidoRegStatus(CompleteMemberActivity.this))
                    commonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                else
                    commonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
                finish();
            }
        });

        if (commonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"fin", "");
    }
}