package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;

import java.util.HashMap;
import java.util.Map;

/**
 * siadabank_android
 * Class: LogoutActivity
 * Created by 950546
 * Date: 2019-02-18
 * Time: 오후 9:02
 * Description: 로그아웃 화면
 */
public class LogoutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        int logoutType = Const.LOGOUT_TYPE_USER;
        if (getIntent().hasExtra(Const.INTENT_LOGOUT_TYPE)) {
            logoutType = getIntent().getIntExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
        }

        TextView tvMsg = findViewById(R.id.tv_msg);

        if (logoutType == Const.LOGOUT_TYPE_TIMEOUT)
            tvMsg.setText("장시간 미사용으로\n자동 로그아웃 되었습니다.");

        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogoutActivity.this, IntroActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });

        LogoutTimeChecker.clearInstance();

        if (logoutType != Const.LOGOUT_TYPE_FORCE)
            sendLogout();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void sendLogout() {
        Map param = new HashMap();
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
            }
        });
    }
}
