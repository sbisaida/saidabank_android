package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.FingerprintActivity;
import com.sbi.saidabank.define.Const;

/**
 * siadabank_android
 * Class: ReregChangeFingerPrintActivity
 * Created by 950546
 * Date: 2019-01-04
 * Time: 오후 1:36
 * Description: ReregChangeFingerPrintActivity
 */
public class ReregChangeFingerPrintActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint_change);

        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        /*
        Intent intent = new Intent(this, FingerprintActivity.class);
        intent.putExtra(Const.INTENT_COMMON_INFO, getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO));
        startActivity(intent);
        */
        finish();
    }
}
