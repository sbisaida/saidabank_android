package com.sbi.saidabank.activity.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptApi;
import com.sbi.saidabank.web.JavaScriptBridge;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

@SuppressLint("ValidFragment")
public class MainWebFragment extends BaseFragment {
    private Context             mContext;
    private FrameLayout         mContainer;
    private BaseWebView         mWebView;
    private JavaScriptBridge    mJsBridge;
    private LinearLayout        mlayout_porgress;
    private ImageView           mLoadingImageView;
    private int                 mFragmentType;
    String urlGood = "";

    @SuppressLint("ValidFragment")
    public MainWebFragment(Context context, int fragmentType) {
        this.mContext = context;
        this.mFragmentType = fragmentType;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_web, container, false);

        //return super.onCreateView(inflater, container, savedInstanceState);

        mContainer = rootView.findViewById(R.id.container_webview);
        mWebView = (BaseWebView) mContainer.getChildAt(mContainer.getChildCount() - 1);
        mWebView.setTag(1);
        BaseWebClient client = new BaseWebClient((Activity)mContext, mContainer, mFragmentType);
        mWebView.setWebViewClient(client);
        mJsBridge = new JavaScriptBridge((Activity)mContext, mContainer, mFragmentType);
        mWebView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);
        BaseWebChromeClient chromeClient = new BaseWebChromeClient((Activity)mContext, mContainer, mJsBridge);
        mWebView.setWebChromeClient(chromeClient);

        mlayout_porgress = rootView.findViewById(R.id.ll_progress);
        mLoadingImageView = rootView.findViewById(R.id.img_progress_webview);

        urlGood = "";

        switch (mFragmentType) {
            case Const.MainMenu_TYPE_Goods: {
                urlGood = WasServiceUrl.MAI0030100.getServiceUrl();
                break;
            }
            case Const.MainMenu_TYPE_My: {
                urlGood = WasServiceUrl.MAI0040100.getServiceUrl();
                break;
            }
            case Const.MainMenu_TYPE_Menu: {
                urlGood = WasServiceUrl.MAI0020100.getServiceUrl();
                break;
            }
            default: {
                Logs.e("url param is null.");
                return rootView;
            }
        }
        //showProgress();
        mWebView.loadUrl(urlGood);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void resumeFragment() {
        super.resumeFragment();
        if (mlayout_porgress.getVisibility() != View.VISIBLE) {
            //showProgress();
            mWebView.setVisibility(View.GONE);
            mWebView.loadUrl("about:blank");
            Handler delayHandler1 = new Handler();
            delayHandler1.postDelayed(new Runnable() {
                @Override
                public void run() {

                    mWebView.loadUrl(urlGood);
                }
            }, 300);
        }
    }

    @Override
    public void pauseFragment() {
        super.pauseFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mJsBridge.destroyJavaScriptBrigdge();
    }

    @Override
    public void showProgress() {
        if (mlayout_porgress != null && mlayout_porgress.getVisibility() != View.VISIBLE) {
            mlayout_porgress.setVisibility(View.VISIBLE);
            ((AnimationDrawable) mLoadingImageView.getBackground()).start();
        }
    }

    @Override
    public void dismissProgress() {
        if (mlayout_porgress != null) {
            mlayout_porgress.setVisibility(View.GONE);
        }
        if (mWebView.getVisibility() != View.VISIBLE)
            mWebView.setVisibility(View.VISIBLE);
    }

    @Override
    public void callGlobalBackPressed() {
        mJsBridge.callJavascriptFunc("co.app.from.back", null);
        super.callGlobalBackPressed();
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);


        Logs.e("fragmentForResult - requestCode : " + requestCode);
        Logs.e("fragmentForResult - resultCode : " + resultCode);

        /**
         * 로시스 라이브러리 업체의 카메라 화면의 리턴값이 1로 되어 있다.
         */
        if (requestCode == JavaScriptApi.API_800) {
            if (resultCode == 1) {
                // 신분증 이미지 영역
                byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);

                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("encode_image", Base64.encodeToString(imageOCR, Base64.DEFAULT));
                } catch (JSONException e) {
                    Logs.printException(e);
                }
                Logs.e(jsonObj.toString());
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), jsonObj);
            }else{
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), null);
            }
        } else if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case JavaScriptApi.API_200: {//1줄 보안키패드
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("input_id", data.getStringExtra(Const.INTENT_TRANSKEY_INPUTID1));
                        jsonObj.put("secure_data", data.getStringExtra(Const.INTENT_TRANSKEY_SECURE_DATA1));
                        jsonObj.put("input_length", data.getStringExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1).length());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }

                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_201:  //dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:  //dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:  //타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:  //타기관OTP 비밀번호 인증
                case JavaScriptApi.API_205:  //모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:  //모바일OTP 비밀번호 인증
                {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                        jsonObj.put("secure_data", commonUserInfo.getSecureData());
                        if(requestCode == JavaScriptApi.API_202 || requestCode == JavaScriptApi.API_206)
                            jsonObj.put("forget_auth_no", commonUserInfo.isOtpForgetAuthNo());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_250: {
                    final Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(data.getLongExtra("selected_date", 0));

                    int inputYear = calendar.get(Calendar.YEAR);
                    int inputMonth = calendar.get(Calendar.MONTH) + 1;
                    int inputDay = calendar.get(Calendar.DAY_OF_MONTH);

                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("date", String.format("%04d", inputYear) + "" + String.format("%02d", inputMonth) + "" + String.format("%02d", inputDay));
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                    break;
                }

                case JavaScriptApi.API_500: { // PIN 전자서명
                    if(data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String result = commonUserInfo.getResultMsg();
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                            jsonObj.put("elec_sgnr_srno", commonUserInfo.getSignData());
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    }
                    break;
                }

                case JavaScriptApi.API_501: { // 지문 전자서명
                    break;
                }

                case JavaScriptApi.API_502: { // 휴대폰 본인인증

                    try {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("ci", "");
                        jsonObj.put("di", "");
                        jsonObj.put("cmcm_dvcd", "");
                        jsonObj.put("cpno", "");
                        if (data != null) {
                            String diNo = null;
                            String ciNo = null;

                            if (data.hasExtra("DI_NO")) {
                                diNo = data.getStringExtra("DI_NO");
                                jsonObj.put("di", diNo);
                            }
                            if (data.hasExtra("CI_NO")) {
                                ciNo = data.getStringExtra("CI_NO");
                                jsonObj.put("ci", ciNo);
                            }
                            if (data.hasExtra("CMCM_DVCD")) {
                                ciNo = data.getStringExtra("CMCM_DVCD");
                                jsonObj.put("cmcm_dvcd", ciNo);
                            }
                            if (data.hasExtra("CPNO")) {
                                ciNo = data.getStringExtra("CPNO");
                                jsonObj.put("cpno", ciNo);
                            }
                        } else {
                            Logs.shwoToast(mContext, "본인인증 실패. data null");
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    break;
                }

                case JavaScriptApi.API_503 : { // 타행계좌인증
                    try {
                        JSONObject jsonObj = new JSONObject();
                        if (data != null) {
                            String accountNum = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_NUMBER);
                            String bankCode = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_BANK_CODE);
                            String custName = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_CUST_NAME);
                            jsonObj.put(Const.BRIDGE_RESULT_KEY, data.getStringExtra(Const.BRIDGE_RESULT_KEY));
                            jsonObj.put("account_num", accountNum);
                            jsonObj.put("bank_cd", bankCode);
                            jsonObj.put("deposit_cust_name", custName);
                            mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                        }
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
                break;

                case JavaScriptApi.API_903: { // PDF약관보기
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_TRUE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                default:
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case JavaScriptApi.API_201:          // dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:          // dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:          // 타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:          // 타기관OTP 비밀번호 인증
                case JavaScriptApi.API_205:          // 모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:          // 모바일OTP 비밀번호 인증
                case JavaScriptApi.API_500:          // Pincode 인증
                case JavaScriptApi.API_501:          // 지문 인증
                case JavaScriptApi.API_502:          // 휴대폰 본인인증
                case JavaScriptApi.API_503:          // 타행계좌인증
                case JavaScriptApi.API_903:          // PDF약관보기
                {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                default:
                    break;
            }
        }
    }
}
