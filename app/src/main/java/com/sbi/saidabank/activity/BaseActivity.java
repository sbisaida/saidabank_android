package com.sbi.saidabank.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.ProgressExDialog;
import com.sbi.saidabank.common.dialog.ReadyLogoutDialog;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONObject;

public class BaseActivity extends AppCompatActivity implements LogoutTimeChecker.LogoutTimeCheckerListener{

    private ProgressExDialog   mProgressExDialog;
    private ReadyLogoutDialog  mReadyLogoutDialog;
    private CommonErrorDialog  mCommonErrorDialog;
    private AlertDialog        mAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 시스템 killprocess 등의 이유로 앱이 강제 중단되었을 때 재시작 여부 체크
        boolean isRecreated = false;
        if (savedInstanceState != null) {
            savedInstanceState = null;
            isRecreated = true;
        }
        super.onCreate(savedInstanceState);

        // 릴리즈 버전일 때만 캡쳐 방지
        if (!BuildConfig.DEBUG)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);

        // 재시작되었을 경우 기존 앱을 종료하고 인트로부터 다시 시작
        if (isRecreated) {
            Logs.e("savedInstanceState is not null");
            LoginUserInfo.clearInstance();
            LoginUserInfo.getInstance().setLogin(false);
            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(this);
            System.exit(0);
            return;
        }
        //setContentView(R.layout.activity_base);
    }

    @Override
    protected void onResume() {
        Logs.e("Base onResume");
        super.onResume();
        if (LogoutTimeChecker.getInstance(this).ismIsBackground()) {
            LogoutTimeChecker.getInstance(this).autoLogoutStop();
            LogoutTimeChecker.getInstance(this).setmIsBackground(false);
            LoginUserInfo.clearInstance();
            LoginUserInfo.getInstance().setLogin(false);
            SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
            if (mApplicationClass != null)
                mApplicationClass.allActivityFinish(true);
            else
                finish();

            Intent intent = new Intent(this, LogoutActivity.class);
            intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_TIMEOUT);
            startActivity(intent);
            return;
        } else {
            LogoutTimeChecker.getInstance(this).autoLogoutUserInteraction((mReadyLogoutDialog != null) ? true:false);
        }
    }

    /* nanumbarungothic font */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    /**
     * 프로그레스바 출력
     */
    public void showProgressDialog() {
        //dismissProgressDialog();
        dismissReadyLogoutDialog();
        LogoutTimeChecker.getInstance(this).autoLogoutUserInteraction(false);
        if (isFinishing()) return;

        if (mProgressExDialog == null) {
            mProgressExDialog = new ProgressExDialog(this);
            mProgressExDialog.show();
        }
    }

    /**
     *  프로그레스바 종료
     */
    public void dismissProgressDialog() {
        if (mProgressExDialog != null && mProgressExDialog.isShowing()) {
            mProgressExDialog.dismiss();
            mProgressExDialog = null;
        }
    }

    /**
     * 로그아웃 연장 알림창 출력
     */
    private void showReadyLogoutDialog(long remainTime) {
        dismissReadyLogoutDialog();
        if (isFinishing()) return;

        if (mReadyLogoutDialog == null) {
            mReadyLogoutDialog = new ReadyLogoutDialog(this);
            mReadyLogoutDialog.setCancelable(false);
            mReadyLogoutDialog.show();
            mReadyLogoutDialog.updateTime(remainTime);
        }
    }

    /**
     *  로그아웃 연장 알림창 종료
     */
    public void dismissReadyLogoutDialog() {
        if (mReadyLogoutDialog != null && mReadyLogoutDialog.isShowing()) {
            mReadyLogoutDialog.dismiss();
            mReadyLogoutDialog = null;
        }
    }

    /**
     * 로그아웃 연장 알림창 출력
     */
    private void updateReadyLogoutDialog(long remainTime) {
        if (mReadyLogoutDialog != null)
            mReadyLogoutDialog.updateTime(remainTime);
    }

    @Override
    public void OnReadyLogoutTime(long remainTime) {
        if (mReadyLogoutDialog == null) {
            showReadyLogoutDialog(remainTime);
            return;
        }

        updateReadyLogoutDialog(remainTime);
    }

    @Override
    public void OnEndLogoutTime(boolean isRestart) {
        dismissReadyLogoutDialog();

        if (isRestart) {
            mReadyLogoutDialog = null;
        } else {
            if (Utils.isAppOnForeground(this)) {
                LogoutTimeChecker.getInstance(this).setmIsBackground(false);
                LoginUserInfo.clearInstance();
                LoginUserInfo.getInstance().setLogin(false);
                SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
                if (mApplicationClass != null)
                    mApplicationClass.allActivityFinish(true);
                else
                    finish();

                Intent intent = new Intent(this, LogoutActivity.class);
                intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_TIMEOUT);
                startActivity(intent);
            } else {
                LoginUserInfo.getInstance().setLogin(false);
                LogoutTimeChecker.getInstance(this).setmIsBackground(true);
            }
        }
    }

    /**
     * 전문 오류 메세지 표시
     * @param msg 오류 메세지
     * @param codeRet 오류 코드
     * @param scrnId 스크린 아이디
     * @param object 전문 오류 헤더 jsonobject
     * @param isShownRetryText "잠시후 다시시도해주세요" 문구 표시 여부
     */
    public void showCommonErrorDialog(String msg, String codeRet, String scrnId, JSONObject object, boolean isShownRetryText) {
        msg = msg.replaceAll("\\\\n", "\n");
        if (mCommonErrorDialog != null) {
            mCommonErrorDialog.dismiss();
            mCommonErrorDialog = null;
        }
        mCommonErrorDialog = new CommonErrorDialog(BaseActivity.this, msg, codeRet, scrnId, object, isShownRetryText);
        mCommonErrorDialog.setOnDismissListener(new CommonErrorDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mCommonErrorDialog = null;
            }
        });
        mCommonErrorDialog.show();
    }

    /**
     * 전문 오류 메세지 표시
     * @param msg 오류 메세지
     * @param codeRet 오류 코드
     */
    public void showCommonErrorDialog(String msg, String codeRet, String scrnId, JSONObject object, boolean isShownRetryText, CommonErrorDialog.OnConfirmListener okClick) {
        msg = msg.replaceAll("\\\\n", "\n");
        if (mCommonErrorDialog != null) {
            mCommonErrorDialog.dismiss();
            mCommonErrorDialog = null;
        }
        mCommonErrorDialog = new CommonErrorDialog(BaseActivity.this, msg, codeRet, scrnId, object, isShownRetryText);
        mCommonErrorDialog.setOnConfirmListener(okClick);
        mCommonErrorDialog.setOnDismissListener(new CommonErrorDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mCommonErrorDialog = null;
            }
        });
        mCommonErrorDialog.show();
    }

    /**
     * 현재 떠 있는 CommonError/Error/Cancel message 팝업 모두 닫음
     */
    public void dismissAllPopup() {
        if (mCommonErrorDialog != null) {
            mCommonErrorDialog.dismiss();
            mCommonErrorDialog = null;
        }

        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }
    }

    /**
     * 오류 메세지 표시
     * @param msg 오류 메세지
     */
    public void showErrorMessage(String msg) {
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        msg = msg.replaceAll("\\\\n", "\n");

        mAlertDialog = new AlertDialog(BaseActivity.this);
        mAlertDialog.mPListener = okClick;
        mAlertDialog.msg = msg;
        mAlertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mAlertDialog = null;
            }
        });
        mAlertDialog.show();
    }

    /**
     * 오류 메세지 표시
     * @param msg 오류 메세지
     */
    public void showErrorMessage(String msg, View.OnClickListener okClick) {
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        mAlertDialog = new AlertDialog(BaseActivity.this);
        msg = msg.replaceAll("\\\\n", "\n");
        mAlertDialog.mPListener = okClick;
        mAlertDialog.msg = msg;
        mAlertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mAlertDialog = null;
            }
        });
        mAlertDialog.show();
    }

    /**
     * 취소 메세지 표시
     * @param msg 취소 메세지
     */
    public void showCancelMessage(String msg) {
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        mAlertDialog = new AlertDialog(BaseActivity.this);
        msg = msg.replaceAll("\\\\n", "\n");
        View.OnClickListener okClick = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        };

        View.OnClickListener cancelClick = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            }
        };

        mAlertDialog.mPListener = okClick;
        mAlertDialog.mNListener = cancelClick;
        mAlertDialog.msg = msg;
        mAlertDialog.mPBtText = "나가기";
        mAlertDialog.mNBtText = "취소";
        mAlertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mAlertDialog = null;
            }
        });
        mAlertDialog.show();
        /*
        DialogUtil.alert(BaseActivity.this,
            "나가기",
            "취소",
            msg,

            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            },

            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            */
    }

    public void showLogoutDialog() {
        DialogUtil.alert(this,
            "로그아웃",
            "취소",
            "로그아웃 하시겠습니까?",

            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginUserInfo.clearInstance();
                    LoginUserInfo.getInstance().setLogin(false);
                    LogoutTimeChecker.getInstance(BaseActivity.this).autoLogoutStop();
                    SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
                    if (mApplicationClass != null)
                        mApplicationClass.allActivityFinish(true);
                    else
                        finish();
                    Intent intent = new Intent(BaseActivity.this, LogoutActivity.class);
                    intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
                    startActivity(intent);
                    return;
                }
            },

            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
    }

   /**
     * 화면에서 사용자가 화면을 터치하면 호출되는 함수.

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }
    */
}
