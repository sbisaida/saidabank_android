package com.sbi.saidabank.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.FrameLayout;

import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.info.DeviceResult;
import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.crop.CropImageActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PathToUriUtil;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.ContactsInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.solution.espider.EspiderManager;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptApi;
import com.sbi.saidabank.web.JavaScriptBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Saidabanking_android
 * Class: WebMainActivity
 * Created by 950469 on 2018. 8. 17..
 * <p>
 * Description:하이브리드 웹 처리를 위한 화면.
 */
public class WebMainActivity extends BaseActivity implements /*KeyboardDetectorRelativeLayout.IKeyboardChanged,*/ I3GAsyncResponse, EspiderManager.EspiderEventListener, DatePickerCallback {

    private JavaScriptBridge mJsBridge;

    private FrameLayout mWebViewContainer;
    private String mCurrentWebPage;
    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_main);
        Logs.e("WebMainActivity - onCreate");

        mWebViewContainer = (FrameLayout) findViewById(R.id.webview_container);
        BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
        webView.setTag(1);

        BaseWebClient client = new BaseWebClient(this, mWebViewContainer);
        webView.setWebViewClient(client);

        mJsBridge = new JavaScriptBridge(this, mWebViewContainer);
        webView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);

        BaseWebChromeClient chromeClient = new BaseWebChromeClient(this, mWebViewContainer, mJsBridge);
        webView.setWebChromeClient(chromeClient);

        Intent intent = getIntent();
        String url = intent.getStringExtra(Const.INTENT_MAINWEB_URL);
        mUrl = url;
        if (TextUtils.isEmpty(url)) {
//            url = AcuonUrl.getBaseWebUrl();
        }

        String param = "";
        if (intent.hasExtra(Const.INTENT_PARAM))
            param = intent.getStringExtra(Const.INTENT_PARAM);

        if (TextUtils.isEmpty(param)) {
            webView.loadUrl(url);
        }
        else {
            webView.postUrl(url, param.getBytes());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Logs.e("WebMainActivity - onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Logs.e("WebMainActivity - onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logs.e("WebMainActivity - onDestroy");
        //인증서 내보내기의 Pending상태이면 취소 시키고 빠져 나간다.

        mJsBridge.destroyJavaScriptBrigdge();
        // motpmanager 리스너 해제
        //MOTPManager.getInstance(getApplication()).removeListener();
    }

    @Override
    public void onBackPressed() {
        Logs.e("mCurrentWebPage : " + mCurrentWebPage);

        // back key noti
        mJsBridge.callJavascriptFunc("co.app.from.back", null);

        if (TextUtils.isEmpty(mCurrentWebPage)) {
                finish();
            //}else if(mCurrentWebPage.contains(AcuonUrl.URL_MAIN_PAGE)){
            //    showLogoutExitDialog();
        } else {
            int webViewCnt = mWebViewContainer.getChildCount();
            if (webViewCnt > 1) {
                mWebViewContainer.removeViewAt(webViewCnt - 1);
            } else {
                //여기서 JS 히스토리 백 함수를 호출한다.
                //BaseWebView webView = (BaseWebView)mWebViewContainer.getChildAt(0);
                //webView.loadUrl("javascript:deviceBackHistory()");
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPerDeny = false;

        switch (requestCode) {
            case Const.REQUEST_PERMISSION_CALL:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }

                if (isPerDeny) {
                    DialogUtil.alert(this, R.string.common_msg_call_permission);
                } else {
                    String telNo = ((SaidaApplication) getApplication()).getTempTelNo();
                    if (!TextUtils.isEmpty(telNo)) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telNo));
                        startActivity(intent);
                    }
                }
                break;

            case JavaScriptApi.API_800:

                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }

                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.i("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_camera_allow), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                        ;
                                    }
                                });
                    }

                } else {
                    JSONObject jsonObject = null;
                    try {
                        String param = mJsBridge.getJsonObjecParam(JavaScriptApi.API_800);
                        if(!TextUtils.isEmpty(param)){
                            jsonObject = new JSONObject(param);
                        }
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }

                    Intent intent = new Intent(this, CameraActivity.class);

                    if (jsonObject != null) {
                        try {
                            JSONObject body = jsonObject.getJSONObject("body");
                            String docType =  body.optString("doc_type");
                            if (Const.IDENTIFICATION_TYPE_ID.equalsIgnoreCase(docType)) {
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
                            } else if (Const.IDENTIFICATION_TYPE_DOC.equalsIgnoreCase(docType)) {
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_DOCUMENT_A4);
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_PORTRAIT);
                            }
                        } catch (JSONException e) {
                            Logs.d(e.getMessage());
                        }
                        //파라미터를 꺼내오면 자동으로 삭제한다.
                        //mJsBridge.setJsonObjecParam(null);
                    } else {
                        intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
                        intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
                    }

                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
                    intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, "");
                    startActivityForResult(intent, JavaScriptApi.API_800);
                }

                break;
            case JavaScriptApi.API_900:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }

                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_900,true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.i("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_storage_allow), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {

                                    }
                                });
                    }
                } else {
                    JSONObject jsonObj = new JSONObject();
                    String fileName = mJsBridge.getJsonObjecParam(JavaScriptApi.API_900);
                    if (!"webreturn".equals(fileName)) {
                        try {
                            BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);

                            if(TextUtils.isEmpty(fileName))
                                fileName = Utils.getCurrentDate("") + "_" + Utils.getCurrentTime("")+".jpg";

                            boolean ret =  ImgUtils.captureSave(this,webView,fileName);
                            jsonObj.put("result", ret);
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                    } else {
                        try {
                            BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);

                            byte[] imgData =  ImgUtils.captureSave(webView);
                            if (imgData != null && imgData.length > 0) {
                                jsonObj.put("result", true);
                                jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                            } else {
                                jsonObj.put("result", false);
                            }
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_900,true), jsonObj);
                }
                break;

            case Const.REQUEST_PROFILE_ALBUM: {
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }

                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907,true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.i("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", "저장소 권한동의를 하셔야 앨범에서 사진을 가져올 수 있습니다.", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                    startActivityForResult(intent, Const.REQUEST_PROFILE_ALBUM);
                }
                break;
            }

            case Const.REQUEST_PROFILE_IMAGE: {
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }

                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907,true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.i("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", "저장소 권한동의를 하셔야 프로필 사진을 가져올 수 있습니다.", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                            }
                        },
                        new View.OnClickListener(){
                            @Override
                            public void onClick(View v) {
                            }
                        });
                    }
                } else {
                    sendProfileImage();
                }
                break;
            }

            case Const.REQUEST_PROFILE_CAMERA: {
                int i = 0;
                for (i = 0 ; i < permissions.length ; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }

                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907,true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[i]);
                    Logs.i("state : " + state);
                    String msg = "";
                    if(!state){
                        msg = "앱 설정에서 카메라 및 저장소 권한을 모두 허용하셔야 프로필 사진을 촬영하실 수 있습니다.";
                    } else {
                        if (Manifest.permission.CAMERA.equals(permissions[i])) {
                            msg = getString(R.string.msg_permission_camera_allow);
                        } else {
                            msg = getString(R.string.msg_permission_storage_allow);
                        }
                    }

                    DialogUtil.alert(this,"권한설정","닫기", msg, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                }
                            },
                            new View.OnClickListener(){
                                @Override
                                public void onClick(View v) {
                                }
                            });
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            String imageFileName = "sbi_" + timeStamp;
                            //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                            File storageDir = getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
                            photoFile = File.createTempFile(
                                    imageFileName,
                                    ".jpg",
                                    storageDir
                            );
                        } catch (IOException ex) {
                            Logs.printException(ex);
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri mTargetPhotoURI;
                            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentapiVersion < Build.VERSION_CODES.N) {
                                mTargetPhotoURI =  Uri.fromFile(photoFile);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                                startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
                            } else {
                                ContentValues contentValues = new ContentValues(1);
                                contentValues.put(MediaStore.Images.Media.DATA, photoFile.getAbsolutePath());
                                mTargetPhotoURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                                startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
                            }
                        }
                    }
                }
                break;
            }

            case Const.REQUEST_PERMISSION_SYNC_CONTACTS: {
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }

                if (isPerDeny) {
                    final JSONObject jsonObj = new JSONObject();
                    DialogUtil.alert(this, getString(R.string.msg_permission_read_contacts), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        jsonObj.put("result", Const.BRIDGE_RESULT_FALSE);
                                    } catch (JSONException e) {
                                        //e.printStackTrace();
                                    }
                                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_930), jsonObj);
                                }
                            });
                } else {
                    JSONObject jsonObj = new JSONObject();
                    ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();
                    ContactsInfo contactsInfo;
                    Cursor contactCursor = null;
                    String name;
                    String phonenumber;
                    JSONArray jsonArray = new JSONArray();
                    try {
                        Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

                        String[] projection = new String[] {
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.Contacts.PHOTO_ID};

                        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

                        contactCursor = getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
                        if (contactCursor.moveToFirst()) {
                            do {
                                phonenumber = contactCursor.getString(1);
                                if (phonenumber.length() <= 0)
                                    continue;

                                name = contactCursor.getString(2);
                                phonenumber = phonenumber.replaceAll("-", "");
                                if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                                    continue;
                                }

                                // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 핸드폰 번호인 경우
                                if (!TextUtils.isEmpty(name) && listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                                    contactsInfo = new ContactsInfo();
                                    contactsInfo.setTLNO(phonenumber);
                                    contactsInfo.setFLNM(name);
                                    listContactsInPhonebook.add(contactsInfo);

                                    JSONObject itemObject = new JSONObject();
                                    itemObject.put("TLNO", phonenumber);
                                    itemObject.put("FLNM", name);
                                    jsonArray.put(itemObject);
                                }
                            } while (contactCursor.moveToNext());
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    } finally {
                        if (contactCursor != null) {
                            contactCursor.close();
                        }

                        try {
                            jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                            jsonObj.put("addr_data", jsonArray);
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_930), jsonObj);
                    }
                }
                break;
            }

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Logs.e("onActivityResult - requestCode : " + requestCode);
        Logs.e("onActivityResult - resultCode : " + resultCode);

        /**
         * 로시스 라이브러리 업체의 카메라 화면의 리턴값이 1로 되어 있다.
         */
        if (requestCode == JavaScriptApi.API_800) {
            if (resultCode == 1) {
                // 신분증 이미지 영역
                byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);

                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("encode_image", Base64.encodeToString(imageOCR, Base64.DEFAULT));
                } catch (JSONException e) {
                    Logs.printException(e);
                }
                Logs.e(jsonObj.toString());
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), jsonObj);
            }else{
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), null);
            }
        } else if (requestCode == JavaScriptApi.API_301 || requestCode == JavaScriptApi.API_302 || requestCode == JavaScriptApi.API_400) {
            mJsBridge.callJavascriptFunc("co.app.from.webviewClose", null);
        } if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case JavaScriptApi.API_200: {//1줄 보안키패드
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("input_id", data.getStringExtra(Const.INTENT_TRANSKEY_INPUTID1));
                        jsonObj.put("secure_data", data.getStringExtra(Const.INTENT_TRANSKEY_SECURE_DATA1));
                        jsonObj.put("input_length", data.getStringExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1).length());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }

                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                /*
                case JavaScriptApi.API_201: {//2줄 보안키패드
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("input_id1", data.getStringExtra(Const.INTENT_TRANSKEY_INPUTID1));
                        jsonObj.put("secure_data1", data.getStringExtra(Const.INTENT_TRANSKEY_SECURE_DATA1));
                        jsonObj.put("input_length1", data.getStringExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1).length());
                        jsonObj.put("input_id2", data.getStringExtra(Const.INTENT_TRANSKEY_INPUTID2));
                        jsonObj.put("secure_data2", data.getStringExtra(Const.INTENT_TRANSKEY_SECURE_DATA2));
                        jsonObj.put("input_length2", data.getStringExtra(Const.INTENT_TRANSKEY_DUMMY_DATA2).length());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }
                */

                case JavaScriptApi.API_201:  //dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:  //dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:  //타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:  //타기관OTP 비밀번호 인증
                case JavaScriptApi.API_205:  //모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:  //모바일OTP 비밀번호 인증
                    {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                        jsonObj.put("secure_data", commonUserInfo.getSecureData());
                        if(requestCode == JavaScriptApi.API_202 || requestCode == JavaScriptApi.API_206)
                            jsonObj.put("forget_auth_no", commonUserInfo.isOtpForgetAuthNo());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_500: { // PIN 전자서명
                    if(data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String result = commonUserInfo.getResultMsg();
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                            jsonObj.put("elec_sgnr_srno", commonUserInfo.getSignData());
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    }
                    break;
                }

                case JavaScriptApi.API_501: { // 지문 전자서명
                    break;
                }

                case JavaScriptApi.API_502: { // 휴대폰 본인인증

                    try {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("ci", "");
                        jsonObj.put("di", "");
                        jsonObj.put("cmcm_dvcd", "");
                        jsonObj.put("cpno", "");
                        if (data != null) {
                            String diNo = null;
                            String ciNo = null;

                            if (data.hasExtra("DI_NO")) {
                                diNo = data.getStringExtra("DI_NO");
                                jsonObj.put("di", diNo);
                            }
                            if (data.hasExtra("CI_NO")) {
                                ciNo = data.getStringExtra("CI_NO");
                                jsonObj.put("ci", ciNo);
                            }
                            if (data.hasExtra("CMCM_DVCD")) {
                                ciNo = data.getStringExtra("CMCM_DVCD");
                                jsonObj.put("cmcm_dvcd", ciNo);
                            }
                            if (data.hasExtra("CPNO")) {
                                ciNo = data.getStringExtra("CPNO");
                                jsonObj.put("cpno", ciNo);
                            }
                        } else {
                            Logs.shwoToast(this, "본인인증 실패. data null");
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    break;
                }

                case JavaScriptApi.API_503 : { // 타행계좌인증
                    try {
                        JSONObject jsonObj = new JSONObject();
                        if (data != null) {
                            String accountNum = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_NUMBER);
                            String bankCode = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_BANK_CODE);
                            String custName = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_CUST_NAME);
                            jsonObj.put(Const.BRIDGE_RESULT_KEY, data.getStringExtra(Const.BRIDGE_RESULT_KEY));
                            jsonObj.put("account_num", accountNum);
                            jsonObj.put("bank_cd", bankCode);
                            jsonObj.put("deposit_cust_name", custName);
                            mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                        }
                    } catch (JSONException e) {
                            Logs.printException(e);
                   }
                }
                break;

                case JavaScriptApi.API_600:
                case JavaScriptApi.API_601: { // 스크래핑 : 건강보험납부내역, 건강보험 자격득실
                    boolean isCertOnly = data.getBooleanExtra(Const.INTENT_CERT_NO_SCRAPING, false);
                    if (isCertOnly) {
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_COMPLETE);
                            jsonObj.put("cn", data.getStringExtra(Const.INTENT_CERT_ISSUER_DN));
                            jsonObj.put("dn", data.getStringExtra(Const.INTENT_CERT_SUBJECT_DN));
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    } else {
                        String pass = data.getStringExtra(Const.INTENT_CERT_PASSWORD);
                        String path = data.getStringExtra(Const.INTENT_CERT_PATH);
                        String id = data.getStringExtra(Const.INTENT_CERT_IDDATA);
                        String svcdata = data.getStringExtra(Const.INTENT_CERT_SERVICEDATA);

                        JSONObject jsonObj = new JSONObject();

                        EspiderManager spiderManager = EspiderManager.getInstance(getApplicationContext());
                        spiderManager.setEspiderEventListener(this);
                        String callBackName = mJsBridge.getCallBackFunc(requestCode,true);
                        if (spiderManager.startEspider(requestCode, callBackName, pass, path, id, svcdata, data.getStringExtra(Const.INTENT_CERT_ISSUER_DN), data.getStringExtra(Const.INTENT_CERT_SUBJECT_DN)) == Const.ESPIDER_TYPE_RETURN_SUCCESS) {
                            // scraping start
                            try {
                                jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_START);
                            } catch (JSONException e) {
                                Logs.printException(e);
                            }

                        } else {
                            // scraping fail
                            try {
                                jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                            } catch (JSONException e) {
                                Logs.printException(e);
                            }

                        }
                        mJsBridge.callJavascriptFunc(callBackName, jsonObj);
                    }
                    break;
                }
/*
                case JavaScriptApi.API_601: { // 스크래핑 : 건강보험 납부확인, 국민연금 정산용 가입내역, 국세청 소득금액증명
                    String pass = data.getStringExtra(Const.INTENT_CERT_PASSWORD);
                    String path = data.getStringExtra(Const.INTENT_CERT_PATH);
                    String id = data.getStringExtra(Const.INTENT_CERT_IDDATA);
                    String svcdata = data.getStringExtra(Const.INTENT_CERT_SERVICEDATA);

                    JSONObject jsonObj = new JSONObject();
                    EspiderManager spiderManager = EspiderManager.getInstance(getApplicationContext());
                    spiderManager.setEspiderEventListener(this);

                    if (spiderManager.startEspider_API601(pass, path, id, svcdata) == Const.ESPIDER_TYPE_RETURN_SUCCESS) {
                        // scraping start
                        try {
                            jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_START);
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                    } else {
                        // scraping start
                        try {
                            jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(), jsonObj);
                    break;
                }
*/
                case JavaScriptApi.API_903: { // PDF약관보기
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_TRUE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case Const.REQUEST_CROP_TO_IMAGE: {
                    if (data != null) {
                        Bundle bundle = data.getExtras();
                        String uri = bundle.getString(Const.CROP_IMAGE_URI);
                        if (!TextUtils.isEmpty(uri)) {
                            Uri mCropURI = Uri.parse(uri);
                            if (mCropURI != null) {
                                sendProfileImage();
                            }
                        }
                    }
                    break;
                }

                case Const.REQUEST_PROFILE_ALBUM: {
                    if (data != null) {
                        Uri photoUri = data.getData();
                        if (photoUri == null)
                            return;

                        Intent intent = new Intent(WebMainActivity.this, CropImageActivity.class);
                        intent.putExtra(Const.CROP_IMAGE_URI, photoUri.toString());
                        startActivityForResult(intent, Const.REQUEST_CROP_TO_IMAGE);
                    }
                    break;
                }

                case Const.REQUEST_PROFILE_CAMERA: {
                    Uri profileUri = ((SaidaApplication) getApplicationContext()).getProfileUri();

                    if (profileUri == null)
                        return;

                    Intent intent = new Intent(WebMainActivity.this, CropImageActivity.class);
                    intent.putExtra(Const.CROP_IMAGE_URI, profileUri.toString());
                    startActivityForResult(intent, Const.REQUEST_CROP_TO_IMAGE);
                    break;
                }

                default:
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case JavaScriptApi.API_201:          // dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:          // dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:          // 타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:          // 타기관OTP 비밀번호 인증
                case JavaScriptApi.API_205:          // 모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:          // 모바일OTP 비밀번호 인증
                case JavaScriptApi.API_500:          // Pincode 인증
                case JavaScriptApi.API_501:          // 지문 인증
                case JavaScriptApi.API_502:          // 휴대폰 본인인증
                case JavaScriptApi.API_503:          // 타행계좌인증
                case JavaScriptApi.API_903:          // PDF약관보기
                case Const.REQUEST_PROFILE_ALBUM:    // API_907 프로필 사진 가져오기
                case Const.REQUEST_PROFILE_IMAGE:    // API_907 프로필 사진 가져오기
                case Const.REQUEST_CROP_TO_IMAGE:    // API_907 프로필 사진 가져오기
                case Const.REQUEST_PROFILE_CAMERA: { // API_907 프로필 사진 가져오기
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    if (requestCode == Const.REQUEST_PROFILE_CAMERA) {
                        deleteTmpProfileImage();
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_600:
                case JavaScriptApi.API_601: { // Javainterface 에서 600, 601번 호출하여 인증서 리스트 취소 시 콜백
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                default:
                    break;
            }
        }
    }

    /*
     * Javainterface 에서 801번 호출되면 작업 후 값을 만들어 넘겨준다.
     * */
    @Override
    public void i3GProcessFinish(DeviceResult deviceResult) {
        String szNATIP = deviceResult.getNatip();
        String szWDATA = deviceResult.getResultStr();

        Logs.i("i3GProcessFinish - szNATIP : " + szNATIP);
        Logs.i("i3GProcessFinish - szWDATA : " + szWDATA);

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("natip", szNATIP);
            jsonObj.put("wdata", szWDATA);
        } catch (JSONException e) {
            Logs.printException(e);
        }
        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_801,true), jsonObj);
    }
/*
    public  void setCaptureFileName(String fileName){
        mCaptureFileName = fileName;
    }
*/
    /*
     * Javainterface 에서 600, 601번 호출되면 작업 후 값을 만들어 넘겨준다.
     * */
    @Override
    public void onFinishEspider(String callBackName,JSONObject returnjsonobj) {
        Logs.i("onFinishEspider");
        //Logs.li(returnjsonobj.toString());
        if(EspiderManager.isEngineEnabled()){
            mJsBridge.callJavascriptFunc(callBackName, returnjsonobj);
        }

    }

    @Override
    public void onStatusEspider(String callBackName,int status, String msg) {
        Logs.i("onStatusEspider: status[" + status + "] msg[" + msg + "]");
    }

    /*
    @Override
    public void onKeyboardShown() {
    }

    @Override
    public void onKeyboardHidden() {
    }
    */

    public void setCurrentPage(String page){
        mCurrentWebPage = page;
    }

    private void sendProfileImage() {
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }

        deleteTmpProfileImage();

        File file = new File(storageDir, Const.CROP_IMAGE_PATH);
        if (file.exists()) {
            int size = (int) file.length();
            byte[] imgData = new byte[size];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                buf.read(imgData, 0, imgData.length);
                buf.close();
            } catch (FileNotFoundException e) {
                //e.printStackTrace();
            } catch (IOException e) {
                //e.printStackTrace();
            }
            JSONObject jsonObj = new JSONObject();
            try {
                if (imgData != null && imgData.length > 0) {
                    jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                } else {
                    jsonObj.put("result", false);
                }
            } catch (JSONException e) {
                Logs.printException(e);
            }
            Logs.e("이미지 전달");
            mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
        } else {
            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put("result", false);
            } catch (JSONException e) {
                //e.printStackTrace();
            }
            Logs.e("이미지 전달 실패");
            mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
        }
    }

    private void deleteTmpProfileImage() {
        Uri profileUri = ((SaidaApplication) getApplicationContext()).getProfileUri();
        if (profileUri != null) {
            String filePath  = PathToUriUtil.getRealPath(WebMainActivity.this, profileUri);
            File file = new File(filePath);
            if (file.exists())
                file.delete();
            getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MediaStore.MediaColumns.DATA + "='" + file.getAbsolutePath() + "'", null);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, profileUri));
        }
        ((SaidaApplication) getApplicationContext()).setProfileUri(null);
    }

    @Override
    public void onDateSet(long l) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(l);

        int inputYear = calendar.get(Calendar.YEAR);
        int inputMonth = calendar.get(Calendar.MONTH) + 1;
        int inputDay = calendar.get(Calendar.DAY_OF_MONTH);

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("date", String.format("%04d", inputYear) + "" + String.format("%02d", inputMonth) + "" + String.format("%02d", inputDay));
        } catch (JSONException e) {
            Logs.printException(e);
        }
        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_250), jsonObj);
    }
}
