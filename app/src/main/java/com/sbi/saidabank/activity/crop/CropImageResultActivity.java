package com.sbi.saidabank.activity.crop;

/**
 * Saidabanking_android
 *
 * Class: CropImageResultActivity
 * Created by 950485 on 2018. 10. 15..
 * <p>
 * Description:화면 Crop한 결과를 보이기 위한 화면
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

public final class CropImageResultActivity extends BaseActivity {
    /** The image to show in the activity. */
    public static Bitmap mCropImage;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_crop_result);

        initUX();

        Intent intent = getIntent();
        if (mCropImage != null) {
            mImageView.setImageBitmap(mCropImage);
        } else {
            Uri imageUri = intent.getParcelableExtra("URI");
            if (imageUri != null) {
                mImageView.setImageURI(imageUri);
            } else {
                Toast.makeText(this, "No image is set to show", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        releaseBitmap();
        super.onBackPressed();
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mImageView = ((ImageView) findViewById(R.id.imageview_crop_result));

        Button btnCancel = (Button) findViewById(R.id.btn_crop_result_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED, null);
                finish();
            }
        });

        Button btnOK = (Button) findViewById(R.id.btn_crop_result_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, null);
                finish();
            }
        });
    }

    /**
     * 표시 비트맵 릴리즈
     */
    private void releaseBitmap() {
        if (mCropImage != null) {
            mCropImage.recycle();
            mCropImage = null;
        }
    }
}
