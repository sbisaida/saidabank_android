package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.CommonUserInfo;

/**
 * siadabank_android
 * Class: ReregLostPatternActivity
 * Created by 950546
 * Date: 2018-12-18
 * Time: 오전 10:33
 * Description: ReregLostPatternActivity
 */
public class ReregLostPatternActivity extends BaseActivity implements View.OnClickListener {
    CommonUserInfo mCommonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rereg_lost_pattern);

        getExtra();

        findViewById(R.id.btn_confirm).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            Intent intent = new Intent(this, PatternAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm: {
                Intent intent = new Intent(this, CertifyPhonePersonalInfoActivity.class);
                mCommonUserInfo.setEntryPoint(EntryPoint.PATTERN_FORGET);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                finish();
                break;
            }

            case R.id.btn_cancel: {
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
                    Intent intent = new Intent(this, PatternAuthActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                }
                finish();
                break;
            }

            default:
                break;
        }
    }


    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }
}
