package com.sbi.saidabank.activity.ssenstone;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.info.DeviceResult;
import com.netfunnel.api.ContinueData;
import com.netfunnel.api.Netfunnel;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.login.ReregChangeFingerPrintActivity;
import com.sbi.saidabank.activity.login.ReregLostPatternActivity;
import com.sbi.saidabank.activity.main.MainActivity;
import com.sbi.saidabank.activity.test.SolutionTestActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.NetfunnelDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.patternlockview.PatternLockView;
import com.sbi.saidabank.customview.patternlockview.listener.PatternLockViewListener;
import com.sbi.saidabank.customview.patternlockview.utils.PatternLockUtils;
import com.sbi.saidabank.customview.patternlockview.utils.ResourceUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.push.FLKPushAgentSender;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: PatternAuthActivity
 * Created by 950485 on 2018. 10. 18..
 * <p>
 * Description:패턴 인증을 위한 화면
 */

public class PatternAuthActivity extends BaseActivity implements StonePassManager.FingerPrintDlgListener, StonePassManager.StonePassListener, View.OnClickListener, I3GAsyncResponse {
    private LinearLayout                    mLayoutMsg;
    private TextView                        mTextMsg;
    private PatternLockView                 mPatternLockView;

    private CommonUserInfo                  mCommonUserInfo;

    private String                          mBioType = "PATTERN";

    private boolean                         mIsProgress = false;
    private String                          mAuthPattern;
    private int                             mCountFail = 0;

    // 지문 로그인
    private FidoDialog mFingerprintDlg;
    private boolean            mIsFingerprintLogin;

    //필요권한 - 전화접근권한
    /*
    String[] perList = new String[]{
            Manifest.permission.READ_PHONE_STATE,
    };
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_auth);

        getExtra();
        initUX();

        // 진입점이 로그인이고 지문등록 상태일 때 체크
        mIsFingerprintLogin = false;
        mCommonUserInfo.setChangedFinger(false);
        /*
        if(!Prefer.getFlagFingerPrintChange(this) && (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) && Prefer.getFidoRegStatus(this)) {
            mIsFingerprintLogin = true;
            showProgressDialog();
            StonePassManager.getInstance(getApplication()).ssenstoneFIDO(PatternAuthActivity.this, mCommonUserInfo.getOperation(), mCommonUserInfo.getSignData(), mBioTypeF, mBioTypeF);
        }
        */
        //PermissionUtils.checkPermission(this,perList,Const.REQUEST_READ_PHONE_STATE_PERMISSIONS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logs.e("onResume");
        mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        mCountFail = Prefer.getPatternErrorCount(this);
        Logs.e("mCountFail : " + mCountFail);

        if((mFingerprintDlg == null || (mFingerprintDlg != null && !mFingerprintDlg.isShowing()))
                && !Prefer.getFlagFingerPrintChange(this) && (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) && Prefer.getFidoRegStatus(this)) {
            mIsFingerprintLogin = true;
            showProgressDialog();
            StonePassManager.getInstance(getApplication()).ssenstoneFIDO(PatternAuthActivity.this, mCommonUserInfo.getOperation(), mCommonUserInfo.getSignData(), "FINGERPRINT", "FINGERPRINT");
        }
    }

    @Override
    public void onBackPressed() {
        // 로그인 진입 시에는 뒤로가기 없음.
        //if (mCommonUserInfo.getEntryPoint() != EntryPoint.LOGIN_BIO && mCommonUserInfo.getEntryPoint() != EntryPoint.LOGIN_PATTERN)
                super.onBackPressed();
    }

    @Override
    public void showFingerPrintDialog() {
        dismissProgressDialog();
        if(mFingerprintDlg == null) {
            mFingerprintDlg = DialogUtil.fido(this, true,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mIsFingerprintLogin = false;
                            StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                            mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                            mFingerprintDlg.dismiss();
                        }
                    },
                    null
            );
            mFingerprintDlg.mBackKeyListener = new Dialog.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mIsFingerprintLogin = false;
                    StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                }
            };
            mFingerprintDlg.setCanceledOnTouchOutside(false);
        }

        if (mFingerprintDlg != null) {
            mFingerprintDlg.showFidoCautionMsg(false);
            mFingerprintDlg.show();
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    /*
     * 화면 초기화
     */
    private void initUX() {
        mPatternLockView = (PatternLockView) findViewById(R.id.pattern_lockview_auth);
        mPatternLockView.setDotCount(3);
        mPatternLockView.setDotNormalSize((int) ResourceUtils.getDimensionInPx(this, R.dimen.pattern_dot_size));
        mPatternLockView.setDotSelectedSize((int) ResourceUtils.getDimensionInPx(this, R.dimen.pattern_selected_dot_size));
        mPatternLockView.setPathWidth((int) ResourceUtils.getDimensionInPx(this, R.dimen.pattern_path_width));
        mPatternLockView.setAspectRatioEnabled(true);
        //mPatternLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS);
        mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
        mPatternLockView.setDotAnimationDuration(150);
        mPatternLockView.setPathEndAnimationDuration(100);
        mPatternLockView.setBackgroundColor(getResources().getColor(R.color.color3F444D));
        mPatternLockView.setNormalStateColor(Color.WHITE);
        mPatternLockView.setCorrectStateColor(Color.WHITE);
        mPatternLockView.setWrongStateColor(getResources().getColor(R.color.colorErr));
        mPatternLockView.setDrawStateColor(getResources().getColor(R.color.color00EBFF));
        mPatternLockView.setInStealthMode(Prefer.getUsePatternStealth(this));
        mPatternLockView.setTactileFeedbackEnabled(true);
        mPatternLockView.setInputEnabled(true);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);

        RelativeLayout layoutLostPattern = (RelativeLayout) findViewById(R.id.layout_lost_pattern);
        layoutLostPattern.setOnClickListener(this);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        float density  = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        float rate = dpHeight / dpWidth;
        if (rate < 1.70) {
            ViewGroup.LayoutParams p = layoutLostPattern.getLayoutParams();
            float magin = Utils.dpToPixel(PatternAuthActivity.this, 10);
            if (p instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutLostPattern.setLayoutParams(lp);
            } else if (p instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutLostPattern.setLayoutParams(lp);
            } else if (p instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutLostPattern.requestLayout();
            }
        }

        TextView textDesc = (TextView) findViewById(R.id.textview_pattern_auth_desc);
        ImageView imageProfile = (ImageView) findViewById(R.id.imageview_profile_pattern_auth);
        ImageView imageBG = (ImageView) findViewById(R.id.imageview_profile_bg);
        mLayoutMsg = (LinearLayout) findViewById(R.id.layout_pattern_msg_auth);
        mTextMsg = (TextView) findViewById(R.id.textview_pattern_msg_auth);

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            textDesc.setVisibility(View.VISIBLE);
            imageProfile.setVisibility(View.GONE);
            imageBG.setVisibility(View.GONE);
            TextView textClose = findViewById(R.id.tv_close);
            textClose.setVisibility(View.VISIBLE);
            textClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }

        File file = new File(storageDir, Const.CROP_IMAGE_PATH);
        if (file.exists()) {
            imageProfile.setImageURI(Uri.fromFile(file));
        }

        if (BuildConfig.DEBUG) {
            //테스트 코드. 로그인 불가 시 프로필이미지 클릭하여 테스트 메뉴 진입
            imageProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PatternAuthActivity.this, SolutionTestActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

        LoginUserInfo.getInstance().setCompletedReg(false);
    }

    /**
     * 패턴 화면 이벤트 리스너
     */
    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
            if (mLayoutMsg.isShown()) {
                mLayoutMsg.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
            mIsProgress = true;
        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            mIsProgress = false;
            if (pattern.size() < Const.SSENSTONE_MIN_PATTERN) {
                mTextMsg.setText(R.string.msg_min_length_reg_pattern);
                mLayoutMsg.setVisibility(View.VISIBLE);
                mPatternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!mIsProgress)
                            mPatternLockView.clearPattern();
                    }
                }, 500);
                AniUtils.shakeView(mLayoutMsg);
                return;
            }

            Logs.e("pattern compete fail count : " + mCountFail);
            if (mCountFail >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
                mPatternLockView.clearPattern();
                String msg;

                if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN)
                    msg = String.format(getString(R.string.msg_err_over_match_pattern_login), Const.SSENSTONE_AUTH_COUNT_FAIL);
                else
                    msg = String.format(getString(R.string.msg_err_over_match_pattern), Const.SSENSTONE_AUTH_COUNT_FAIL);

                DialogUtil.alert(PatternAuthActivity.this,
                        getResources().getString(R.string.rereg),
                        getResources().getString(R.string.common_cancel),
                        msg,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(PatternAuthActivity.this, ReregLostPatternActivity.class);
                                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                mLayoutMsg.setVisibility(View.INVISIBLE);
                                startActivity(intent);

                                if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN)
                                    finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //setResult(RESULT_CANCELED);
                                finish();
                            }
                        });
                return;
            }

            showProgressDialog();
            mAuthPattern = PatternLockUtils.patternToString(mPatternLockView, pattern);
            StonePassManager.getInstance(getApplication()).ssenstoneFIDO(
                    PatternAuthActivity.this, mCommonUserInfo.getOperation(), null, mBioType, PatternLockUtils.patternToString(mPatternLockView, pattern));
            mPatternLockView.clearPattern();
        }

        @Override
        public void onCleared() {
        }
    };

    @Override
    public void stonePassResult(String op, int errorCode, String errorMsg) {
        //dismissProgressDialog();
        Logs.e("Pattern - stonePassResult : errorCode[" + errorCode + "], errorMsg["+errorMsg + "]");
        String msg ="";

        if(mIsFingerprintLogin) {
            switch (errorCode) {
                case 1:
                case 2:
                case 4:
                case 8:
                case 11:
                case 1001:
                    dismissProgressDialog();
                    if (mFingerprintDlg != null) {
                        mFingerprintDlg.showFidoCautionMsg(true);
                    }
                    return;

                default:
                    break;
            }

            if (mFingerprintDlg != null) {
                mFingerprintDlg.dismiss();
            }

            switch (errorCode) {
                // 사용자 취소
                case 3: {
                    dismissProgressDialog();
                    mIsFingerprintLogin = false;
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                    return;
                }
                // 지문 추가 시 재등록 처리
                case 200: {
                    dismissProgressDialog();
                    Prefer.setFlagFingerPrintChange(this, true);
                    Prefer.setFidoRegStatus(this, false);
                    Intent intent = new Intent(this, ReregChangeFingerPrintActivity.class);
                    mCommonUserInfo.setChangedFinger(true);
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                    mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mLayoutMsg != null)
                                mLayoutMsg.setVisibility(View.INVISIBLE);
                        }
                    });
                    startActivity(intent);
                    //finish();
                    return;
                }

                case 1200: {
                    break;
                }

                default: {
                    dismissProgressDialog();
                    mIsFingerprintLogin = false;
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                    if (errorCode == 10) {
                        msg = "지문인증 시도횟수가 너무 많습니다. 나중에 다시 시도하세요.";
                    } else if (errorCode == 9) {
                        msg = "시도 횟수가 너무 많습니다. 지문 센서가 사용 중지되었습니다.";
                    } else {
                        msg = "지문 인증 실패했습니다.";
                    }
                    showErrorMessage(msg);
                    return;
                }
            }
        }

        switch (errorCode) {
            case 100: {
                mCountFail++;
                if (mCountFail < Const.SSENSTONE_AUTH_COUNT_FAIL) {
                    msg = String.format(getString(R.string.msg_err_no_match_pattern), mCountFail, Const.SSENSTONE_AUTH_COUNT_FAIL);
                    Prefer.setPatternErrorCount(this, mCountFail);
                } else {
                    Prefer.setPatternErrorCount(this, Const.SSENSTONE_AUTH_COUNT_FAIL);
                    dismissProgressDialog();

                    if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN)
                        msg = String.format(getString(R.string.msg_err_over_match_pattern_login), Const.SSENSTONE_AUTH_COUNT_FAIL);
                    else
                        msg = String.format(getString(R.string.msg_err_over_match_pattern), Const.SSENSTONE_AUTH_COUNT_FAIL);
                    DialogUtil.alert(PatternAuthActivity.this,
                            getResources().getString(R.string.rereg),
                            getResources().getString(R.string.common_cancel),
                            msg,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(PatternAuthActivity.this, ReregLostPatternActivity.class);
                                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                    mLayoutMsg.setVisibility(View.INVISIBLE);
                                    startActivity(intent);

                                    if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN)
                                        finish();
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //setResult(RESULT_CANCELED);
                                    finish();
                                }
                            });
                    return;
                }
                break;
            }
            case 1200:
                if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    msg = "인증했습니다.";
                    if (mCommonUserInfo.getEntryPoint() != EntryPoint.LOGIN_BIO)
                        Prefer.setPatternErrorCount(this, 0);
                } else {
                    msg = "해제했습니다.";
                    Prefer.setPatternRegStatus(this,false);
                }
                break;

            case 1404:
                //msg = "사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.";
                msg = "인증을 실패했습니다.";
                break;

            case 1491:
                if(op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    if (Prefer.getPatternRegStatus(this)) {
                        msg = "인증을 실패했습니다.";
                    } else {
                        msg = "사용자 정보가 없습니다. 먼저 등록하시기 바랍니다.";
                    }
                }
                break;

            case 0://패턴일때만 리턴되는 값인듯.
                //msg = "해제했습니다.";
                msg = "인증을 실패했습니다.";
                break;

            case 3:
                dismissProgressDialog();
                msg = "취소하셨습니다.";
                mIsFingerprintLogin = false;
                mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                return;

            case 9998:
                //msg = "서버로 부터 리턴 값이 널(Null)입니다..";
                msg = "인증을 실패했습니다.";
                break;

            case 9999:
                //msg = "작업중 json에러가 발생했습니다.";
                msg = "인증을 실패했습니다.";
                break;

            default:
                if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    msg = "인증을 실패했습니다.";
                } else {
                    msg = "해제를 실패했습니다.";
                }
                break;
        }

         if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE) && errorCode != 1200) {
                dismissProgressDialog();
                List<PatternLockView.Dot> list = mPatternLockView.getPattern();
                mPatternLockView.setPattern(PatternLockView.PatternViewMode.WRONG, list);
                mTextMsg.setText(msg);
                mLayoutMsg.setVisibility(View.VISIBLE);

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPatternLockView.clearPattern();
                        if (mCountFail >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
                            mPatternLockView.setEnabled(false);
                        }
                    }
                }, 500);
                AniUtils.shakeView(mLayoutMsg);
                return;
            }
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
         } else {
            mPatternLockView.clearPattern();

            if (errorCode != 1200) {
                dismissProgressDialog();
                Intent intent = new Intent();
                mCommonUserInfo.setResultMsg(msg);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                //setResult(RESULT_CANCELED, intent);
                finish();
                return;
            }
        }

        mPatternLockView.clearPattern();
        mCountFail = 0;
        mPatternLockView.setEnabled(true);

       if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            dismissProgressDialog();
            Intent intent = new Intent(this, PatternRegActivity.class);
            mCommonUserInfo.setSignData(mAuthPattern);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
            finish();
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            Intent intent = new Intent();
            if(mIsFingerprintLogin)
                mCommonUserInfo.setLoginPattern(false);
            else
                mCommonUserInfo.setLoginPattern(true);

           if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO)
               showProgressDialog();

           Netfunnel.BEGIN("service_1","login_app"
                   ,NetfunnelDialog.Show(this)
                   ,new Netfunnel.Listener() {
                       @Override
                       public void netfunnelMessage(Netfunnel netfunnel, Netfunnel.EvnetCode code) {
                           try {
                               Logs.d("netfunnel", "netfunnelMessage -> code: " + code + " / "+netfunnel.getResponse().getCode() + " ttl=" + netfunnel.getResponse().getTTL());
                               if (code.isContinue()) {
                                   ContinueData continue_data = netfunnel.getContinueData();
                                   if (code == Netfunnel.EvnetCode.ContinueInterval) {
                                   } else {  // Netfunnel.EvnetCode.Continue
                                       String continue_msg = "    예상대기시간:" + continue_data.getCurrentWaitTimeSecond() + "초\n"
                                                                     + "    진   행   율:" + continue_data.getCurrentWaitPercent() + "%\n"
                                                                     + "    대기자수(앞):" + continue_data.getCurrentWaitCount() + "명\n"
                                                                     + "    대기자수(뒤):" + continue_data.getCurrentNextCount() + "명\n"
                                                                     + " 초당처리량(TPS):" + netfunnel.getResponse().getTPS() + "\n"
                                                                     + "   확인주기(TTL):" + netfunnel.getResponse().getTTL() + "초\n"
                                                                     + "   notice acount:" + continue_data.getAcountNotice() + "\n"
                                                                     + "   update acount:" + continue_data.getUpdateAcount() + "\n"
                                                                     + "UI     표시대기시간:" + netfunnel.getProperty().getUiWaitTimeLimit() + "초\n"
                                                                     + "UI 표시대기자수(앞):" + netfunnel.getProperty().getUiWaitCountLimit() + "명\n"
                                                                     + "UI 표시대기자수(뒤):" + netfunnel.getProperty().getUiNextCountLimit() + "명";



                                       Logs.d("netfunnel", "-------------------------\n" + continue_msg);
                                   }
                                   return;
                               }

                               NetfunnelDialog.Close();
                               if (code.isSuccess()) {
                                   FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this,PatternAuthActivity.this);
                                   if(code == Netfunnel.EvnetCode.Success) {
                                       // 주의) 여기에만 AliveNotice()을 사용할수 있는 곳
                                   }else if(code == Netfunnel.EvnetCode.NotUsed){
                                   }else if(code == Netfunnel.EvnetCode.Bypass){
                                   }else if(code == Netfunnel.EvnetCode.ErrorBypass){
                                   }else if(code == Netfunnel.EvnetCode.ExpressNumber){
                                   }
                               } else if (code.isBlocking()) {
                                   FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this,PatternAuthActivity.this);
                                   if(code == Netfunnel.EvnetCode.Block) {
                                       // 서비스 차단
                                   }else if(code == Netfunnel.EvnetCode.IpBlock){
                                       // 사용자 사용패턴에 의한 차단
                                   }
                               } else if (code.isStop()) {
                                   // TODO: 자체처리영역
                                   dismissProgressDialog();
                                   Netfunnel.END();
                               } else { // }else if(code.isError()){
                                   // TODO: 자체처리영역
                                   //dismissProgressDialog();
                                   FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this,PatternAuthActivity.this);
                               }
                           } catch (Exception e) {
                               e.printStackTrace();
                               FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this,PatternAuthActivity.this);
                           }
                       }
                   });

        }
        else {
            dismissProgressDialog();
            //setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.layout_lost_pattern) {
            Intent intent = new Intent(this, ReregLostPatternActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            mLayoutMsg.setVisibility(View.INVISIBLE);
            startActivity(intent);

            if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN)
                finish();
        }
    }

    private void loginProcess(String IMEI, String UUID, String NDATA, String WDATA) {
        final Map param = new HashMap();

        param.put("MBR_NO", LoginUserInfo.getInstance().getMBR_NO()); // 회원번호
        if(mCommonUserInfo != null) {
            if(mCommonUserInfo.isLoginPattern())
                param.put("LOIN_DVCD", "01"); // 패턴
            else
                param.put("LOIN_DVCD", "02"); // 지문
        } else {
            param.put("LOIN_DVCD", "01"); // 패턴
        }

        param.put("DEVICE_OS", "android"); // 802 os
        param.put("DEVICE_INFO1", IMEI); // 802 IMEI
        param.put("DEVICE_INFO2", UUID); // 802 UUID
        param.put("N_DATA", NDATA); // 801 NDATA
        param.put("W_DATA", WDATA); // 801 WDATA
        param.put("PUSH_APSF_ID", FLKPushAgentSender.mAppName); // app id
        param.put("PUSH_REG_ID", Prefer.getPushRegID(PatternAuthActivity.this)); // push id
        param.put("PUSH_GCM_TKN", Prefer.getPushGCMToken(PatternAuthActivity.this)); // gcm id
        param.put("APSF_VRSN", Utils.getVersionName(PatternAuthActivity.this)); // app version

        mIsFingerprintLogin = false;
        mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
        mCommonUserInfo.setLoginPattern(true);
        //showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A05.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Netfunnel.END();
                if (TextUtils.isEmpty(ret)) {
                    dismissProgressDialog();
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                } else {
                    try {
                        JSONObject object = new JSONObject(ret);
                        if (object == null) {
                            dismissProgressDialog();
                            showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                            Logs.e(getResources().getString(R.string.msg_debug_err_response));
                            Logs.e(ret);
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            dismissProgressDialog();

                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                            Logs.e(ret);
                            return;
                        }

                        Logs.e("ret : " + ret);
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().syncLoginSession(ret);
                        MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                            @Override
                            public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                                if ("0000".equals(resultCode)) {
                                    LoginUserInfo.getInstance().setMOTPSerialNumber(reqData1);
                                }
                                return;
                            }
                        });
                        dismissProgressDialog();
                        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();

                        loginUserInfo.setLogin(true);
                        LogoutTimeChecker.clearInstance();
                        LogoutTimeChecker.getInstance(PatternAuthActivity.this).autoLogoutStart();

                        // 신분증보완 체크
                        String strIdConfirm = loginUserInfo.getIDNF_CNFR_PRGS_STCD();

                        // 신분증확인상태코드가 2 또는 3이면 해당 페이지 이동
                        if (!TextUtils.isEmpty(strIdConfirm) && ("2".equals(strIdConfirm) || "3".equals(strIdConfirm))) {
                            Intent intent = new Intent(PatternAuthActivity.this, WebMainActivity.class);
                            intent.putExtra("url", WasServiceUrl.LGN0020100.getServiceUrl());
                            startActivity(intent);
                            finish();
                            return;
                        }

                        // cdd/edd 고객재확인평가일자 체크
                        String strCddElpsCount = loginUserInfo.getCDD_EDD_CUST_CNFR_RESS_ELPS_DCNT();
                        int cddElpsCount = TextUtils.isEmpty(strCddElpsCount) ? 99999 : Integer.parseInt(strCddElpsCount);
                        boolean needOpenCDD = false;
                        String curServerTime = loginUserInfo.getSYS_DTTM();

                        // 이전에 7일간 열지않기를 선택했을 때 저장된 서버 시간 체크
                        if (!TextUtils.isEmpty(curServerTime) && !TextUtils.isEmpty(Prefer.getCDDEDDNotOpenFor1Week(PatternAuthActivity.this))) {
                            // 현재 서버 시간과 이전에 앱에 저장된 서버 시간을 모두 가지고 있는 경우
                            String savedServerTime = Prefer.getCDDEDDNotOpenFor1Week(PatternAuthActivity.this);
                            Calendar calendar = Calendar.getInstance();
                            Calendar savedCalendar = Calendar.getInstance();
                            calendar.set(Integer.parseInt(curServerTime.substring(0, 4)), Integer.parseInt(curServerTime.substring(4, 6)) - 1, Integer.parseInt(curServerTime.substring(6, 8)));
                            savedCalendar.set(Integer.parseInt(savedServerTime.substring(0, 4)), Integer.parseInt(savedServerTime.substring(4, 6)) - 1, Integer.parseInt(savedServerTime.substring(6, 8)));
                            if (((long) (60 * 60 * 24 * 7 * 1000.0)) < (calendar.getTimeInMillis() - savedCalendar.getTimeInMillis())) {
                                needOpenCDD = true;
                            }

                        } else if (TextUtils.isEmpty(Prefer.getCDDEDDNotOpenFor1Week(PatternAuthActivity.this))) {
                            // 이전에 앱에 저장된 서버 시간이 없는 경우
                            needOpenCDD = true;
                        } else {
                            // 현재 서버 시간이 없거나 못 가져온 경우 디바이스 시간으로 서버 시간 설정하고 비교
                            String savedServerTime = Prefer.getCDDEDDNotOpenFor1Week(PatternAuthActivity.this);
                            Calendar calendar = Calendar.getInstance();
                            Calendar savedCalendar = Calendar.getInstance();
                            savedCalendar.set(Integer.parseInt(savedServerTime.substring(0, 4)), Integer.parseInt(savedServerTime.substring(4, 6)) - 1, Integer.parseInt(savedServerTime.substring(6, 8)));
                            if (((long) (60 * 60 * 24 * 7 * 1000.0)) < (calendar.getTimeInMillis() - savedCalendar.getTimeInMillis())) {
                                needOpenCDD = true;
                            }
                        }
                        Logs.i("needOpenCDD : " + needOpenCDD);
                        // cdd/edd 남은 기간이 0보다 같거나 작으면 무조건 안내 페이지 접속
                        // cdd/edd 남은 기간이 0-30일이면 일주일간 보지않기 선택했는지 여부에 따라 안내 페이지 접속
                        if ((cddElpsCount <= 0)
                            || ((cddElpsCount > 0 && cddElpsCount <=30) && needOpenCDD)) {
                            // cdd/edd 로그인
                            Intent intent = new Intent(PatternAuthActivity.this, WebMainActivity.class);
                            intent.putExtra("url", WasServiceUrl.LGN0010100.getServiceUrl());
                            startActivity(intent);
                        } else {
                            //로그인 성공 후 메인 페이지 이동
                            Intent intent;
                            if (!mCommonUserInfo.isChangedFinger()) {
                                intent = new Intent(PatternAuthActivity.this, MainActivity.class);
                                if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL))
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
                            }
                            else {
                                intent = new Intent(PatternAuthActivity.this, FingerprintActivity.class);
                                mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                                mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                            }

                            startActivity(intent);
                        }
                        loginUserInfo.setFinishedLogin(false);
                        finish();
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
            }
        });
    }

    @Override
    public void i3GProcessFinish(final DeviceResult deviceResult) {
        final String[] item1 = new String[4];
        new Thread(new Runnable() {
            @Override
            public void run() {
                item1[0] = FDSManager.getInstance().getEncIMEI(PatternAuthActivity.this);
                item1[1] = FDSManager.getInstance().getEncUUID(PatternAuthActivity.this);
                item1[2] = deviceResult.getNatip();
                item1[3] = deviceResult.getResultStr();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loginProcess(item1[0], item1[1], item1[2], item1[3]);
                    }
                });
            }
        }).start();
    }
}
