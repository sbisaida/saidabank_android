package com.sbi.saidabank.activity.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.test.SolutionTestActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.adapter.AccountAdapter;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.JoinGoodDialog;
import com.sbi.saidabank.common.dialog.MainEventDialog;
import com.sbi.saidabank.common.dialog.MainNoticeDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.DrawEvent;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.customview.RollingTextView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.AccountInfo;
import com.sbi.saidabank.define.datatype.AdsItemInfo;
import com.sbi.saidabank.define.datatype.BankGoodInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.define.datatype.MainPopupInfo;
import com.sbi.saidabank.define.datatype.MyAccountInfo;
import com.sbi.saidabank.define.datatype.SlidingItemInfo;
import com.sbi.saidabank.define.datatype.TagItemInfo;
import com.sbi.saidabank.push.FLKPushAgentReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class MainFragment extends BaseFragment implements RollingTextView.IRollingTextSelected {
    private Context                  mContext;

    private RelativeLayout           mLayoutFragmentMain;
    private LinearLayout             mMainTitle;
    private ImageView                mImageProfileSmallBg;
    private ImageView                mImageLogo;
    private ImageView                mImageAlram;
    private ImageView                mImageAlramNoti;
    private ImageView                mImageProfileSmall;

    private RecyclerView             mRecyclerViewAccount;
    private RelativeLayout           mLayoutDrawEvent;
    private View                     mEventView;
    private LinearLayout             mlayoutProgress;
    private ImageView                mLoadingImageView;
    private LinearLayout             mLayoutTrick;
    private RelativeLayout           mLayoutCircleMain;
    private RelativeLayout           mLayoutLine01;
    private RelativeLayout           mLayoutLine02;

    private ArrayList<AccountInfo>   mListAccountInfo = new ArrayList<>();
    private ArrayList<SlidingItemInfo> mListSlidingItem = new ArrayList<>();
    private ArrayList<TagItemInfo>   mListTagItem = new ArrayList<>();
    private ArrayList<BankGoodInfo>  mListGoodItem = new ArrayList<>();
    private ArrayList<AdsItemInfo>   mListAdsItem = new ArrayList<>();
    private ArrayList<MainPopupInfo> mListMainPopupItem = new ArrayList<>();

    private AccountAdapter           mAccountAdapter;

    private boolean                  mFirstEntry = true;
    private boolean                  mIsExistCard = false;

    private MainNoticeDialog         mNoticeDialog;
    private MainEventDialog          mEventDialog;
    private String                   mUrl;
    private int                      mShowAdsIndex = -1;

    @SuppressLint("ValidFragment")
    public MainFragment(Context context, String url) {
        this.mContext = context;
        mUrl = url;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main_main, container, false);
        //return super.onCreateView(inflater, container, savedInstanceState);

        initUX(rootView);

        if (TextUtils.isEmpty(mUrl)) {
            resumeFragment();
        }
        else {
            Intent intent = new Intent(mContext, WebMainActivity.class);
            if (WasServiceUrl.MAI0060100.getServiceUrl().equals(mUrl)) {
                Prefer.setFlagShowFirstNotiMsgBox(mContext.getApplicationContext(), false);
                FLKPushAgentReceiver.updateBadge(mContext.getApplicationContext(), 0);
            }
            intent.putExtra(Const.INTENT_MAINWEB_URL, mUrl);
            mContext.startActivity(intent);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }

        if (mEventDialog != null) {
            mEventDialog.dismiss();
            mEventDialog = null;
        }

        if (mEventView != null) {
            mLayoutDrawEvent.removeView(mEventView);
            mEventView = null;
        }
    }

    private void initUX(View rootView) {
        mLayoutFragmentMain = (RelativeLayout) rootView.findViewById(R.id.layout_fragment_main);
        mMainTitle = (LinearLayout) rootView.findViewById(R.id.layout_main_title);
        mImageProfileSmallBg = (ImageView) rootView.findViewById(R.id.imageview_profile_small_bg);
        mImageLogo = (ImageView) rootView.findViewById(R.id.imageview_logo);
        mImageAlram = (ImageView) rootView.findViewById(R.id.imageview_alram_title);
        mImageAlramNoti = (ImageView) rootView.findViewById(R.id.imageview_alram_noti);
        mImageProfileSmall = (ImageView) rootView.findViewById(R.id.imageview_profile_small);

        mImageProfileSmall.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(mContext, WebMainActivity.class);
                String fullUrl = WasServiceUrl.MYP0010100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, fullUrl);
                mContext.startActivity(intent);
            }
        });

        mRecyclerViewAccount = (RecyclerView) rootView.findViewById(R.id.recyclerview_account_list);
        mlayoutProgress = (LinearLayout) rootView.findViewById(R.id.ll_progress);
        mLoadingImageView = (ImageView) rootView.findViewById(R.id.img_progress_webview);
        mLayoutTrick = (LinearLayout) rootView.findViewById(R.id.layout_trick_title);
        mLayoutCircleMain = (RelativeLayout) rootView.findViewById(R.id.background_circle_main);
        mLayoutLine01 = (RelativeLayout) rootView.findViewById(R.id.layout_trick_title_line_01);
        mLayoutLine02 = (RelativeLayout) rootView.findViewById(R.id.layout_trick_title_line_02);

        /*if (BuildConfig.DEBUG) {
            mImageLogo.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    Intent intent = new Intent(mContext, SolutionTestActivity.class);
                    mContext.startActivity(intent);
                }
            });
        }*/

        mImageAlram.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Prefer.setFlagShowFirstNotiMsgBox(mContext.getApplicationContext(), false);
                FLKPushAgentReceiver.updateBadge(mContext.getApplicationContext(), 0);
                mImageAlramNoti.setVisibility(View.GONE);

                Intent intent = new Intent(mContext, WebMainActivity.class);
                String fullUrl = WasServiceUrl.MAI0060100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, fullUrl);
                mContext.startActivity(intent);
            }
        });

        mLayoutDrawEvent = (RelativeLayout) rootView.findViewById(R.id.layout_draw_event);
    }

    @Override
    public void resumeFragment() {
        super.resumeFragment();
        showTimeMode();

        mMainTitle.setVisibility(View.INVISIBLE);
        mRecyclerViewAccount.scrollToPosition(0);

        Logs.e("isLeavedFromMainTab : " + ((MainActivity) mContext).isLeavedFromMainTab());

        if (((MainActivity) mContext).isLeavedFromMainTab()) {
            if (!mFirstEntry) {
                if (mEventView != null) {
                    mLayoutDrawEvent.removeView(mEventView);
                    mEventView = null;
                }
            }
        }

        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        boolean isLogin = userInfo.IsLogin();
        if (isLogin)
            requestMainInfo();

        File storageDir = mContext.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
        File file = new File(storageDir, Const.CROP_IMAGE_PATH);
        if (file.exists()) {
            mImageProfileSmall.setImageResource(0);
            mImageProfileSmall.setImageURI(Uri.fromFile(file));
        } else {
            mImageProfileSmall.setImageResource(0);
            mImageProfileSmall.setImageResource(R.drawable.ico_profile);
        }

        boolean isPushData = Prefer.getFlagShowFirstNotiMsgBox(mContext);
        if (isPushData) {
            if (!mImageAlramNoti.isShown())
                mImageAlramNoti.setVisibility(View.VISIBLE);
        } else {
            mImageAlramNoti.setVisibility(View.GONE);
        }
    }

    @Override
    public void pauseFragment() {
        super.pauseFragment();
    }

    @Override
    public void showPushNotiFragment() {
        super.showPushNotiFragment();

        boolean isPushData = Prefer.getFlagShowFirstNotiMsgBox(mContext);
        if (isPushData) {
            if (!mImageAlramNoti.isShown())
                mImageAlramNoti.setVisibility(View.VISIBLE);
        } else {
            mImageAlramNoti.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        ((BaseActivity) mContext).dismissReadyLogoutDialog();
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        ((MainActivity) mContext).setMainTabFinishedLoading(false);
        if (mlayoutProgress != null && mlayoutProgress.getVisibility() != View.VISIBLE) {
            mlayoutProgress.setVisibility(View.VISIBLE);
            ((AnimationDrawable) mLoadingImageView.getBackground()).start();
        }
    }

    @Override
    public void dismissProgress() {
        ((MainActivity) mContext).setMainTabFinishedLoading(true);
        if (mlayoutProgress != null) {
            mlayoutProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRollingTextSelect(int index) {
        Logs.e("onRollingTextSelect : " + index);
        SlidingItemInfo slidingInfo = mListSlidingItem.get(index);
        if (slidingInfo == null)
            return;

        requestGoOn(slidingInfo);
    }

    /**
     * 메인정보조회
     */
    private void requestMainInfo() {
        showProgress();
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                Logs.i("MAI0010100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    //showErrorMessage("정상적으로 통신이 이루어지지 않았습니다.\n잠시후 다시 시도해 주세요.");
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        //showErrorMessage("정상적으로 통신이 이루어지지 않았습니다.\n잠시후 다시 시도해 주세요.");
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        if ("CMM0037".equals(errCode)) {
                            //로그인 중 분실신고기기 체크
                            if (Utils.isAppOnForeground(mContext)) {
                                LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
                                SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
                                mApplicationClass.allActivityFinish(true);
                                LoginUserInfo.clearInstance();
                                LoginUserInfo.getInstance().setLogin(false);
                                Intent intent = new Intent(mContext, LogoutActivity.class);
                                intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
                                mContext.startActivity(intent);
                            } else {
                                LoginUserInfo.getInstance().setLogin(false);
                                LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
                                LogoutTimeChecker.getInstance(mContext).setmIsBackground(true);
                            }
                        } else {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                            ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    mIsExistCard = false;
                    mListSlidingItem.clear();
                    mListTagItem.clear();
                    mListAccountInfo.clear();
                    mListAdsItem.clear();
                    mListMainPopupItem.clear();

                    ArrayList<AccountInfo> listAccountInfo = new ArrayList<>();
                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        AccountInfo accountInfo = new AccountInfo(jsonObject);
                        String DSCT_CD = accountInfo.getDSCT_CD();
                        if (TextUtils.isEmpty(DSCT_CD))
                            continue;

                        if (Const.SLIDING_EXTEND.equalsIgnoreCase(DSCT_CD) ||
                            Const.SLIDING_SMALL_EXTEND.equalsIgnoreCase(DSCT_CD) ||
                            Const.SLIDING_ADD_LIMIT.equalsIgnoreCase(DSCT_CD) ||
                            Const.SLIDING_DOWN_INTEREST.equalsIgnoreCase(DSCT_CD)) {
                            SlidingItemInfo slidingItemInfo = new SlidingItemInfo(jsonObject);
                            mListSlidingItem.add(slidingItemInfo);

                        } else if (Const.ACCOUNT_TAG.equalsIgnoreCase(DSCT_CD)) {
                            TagItemInfo tagInfo = new TagItemInfo(jsonObject);
                            mListTagItem.add(tagInfo);

                        } else {
                            listAccountInfo.add(accountInfo);
                        }
                    }

                    AccountInfo topAccountInfoTop = new AccountInfo();
                    topAccountInfoTop.setDSCT_CD(Const.ACCOUNT_TOP);
                    if (mListSlidingItem != null && mListSlidingItem.size() > 0) {
                        topAccountInfoTop.setListSlidings(mListSlidingItem);
                    }

                    if (mListTagItem != null && mListTagItem.size() > 0) {
                        topAccountInfoTop.setListTags(mListTagItem);
                    }
                    mListAccountInfo.add(topAccountInfoTop);

                    int countRepNomal = setNomalRepAccount(listAccountInfo);

                    int countGoOn = 0;

                    countGoOn = setGoOnNomal(listAccountInfo);
                    countGoOn += setGoOnLoan(listAccountInfo);
                    countGoOn += setGoOnMinus(listAccountInfo);
                    countGoOn += setGoOnSmallMinus(listAccountInfo);

                    int countNomal = setNomalAccount(listAccountInfo);

                    if (countRepNomal == 0 && countNomal == 0) {
                        AccountInfo accountInfo = new AccountInfo();
                        accountInfo.setDSCT_CD(Const.ACCOUNT_NO);
                        accountInfo.setADD_ACCOUNT_MODE(Const.ADD_ACCOUNT_MODE.NOMAL_MODE);
                        mListAccountInfo.add(1, accountInfo);
                    }

                    int countMinus = setMinusAccount(listAccountInfo);

                    int countSaving = setSavingsAccount(listAccountInfo);
                    int countInstallSavings = setInstallmentSavingsAccount(listAccountInfo);
                    if ((countRepNomal > 0 || countNomal > 0) && (countInstallSavings == 0 && countSaving == 0)) {
                        AccountInfo accountInfo = new AccountInfo();
                        accountInfo.setDSCT_CD(Const.ACCOUNT_NO);
                        accountInfo.setADD_ACCOUNT_MODE(Const.ADD_ACCOUNT_MODE.SAVINGS_MODE);
                        mListAccountInfo.add(mListAccountInfo.size(), accountInfo);
                    }

                    int countLoad = setLoanAccount(listAccountInfo);

                    int countAccount = countRepNomal + countNomal + countMinus +
                                        countSaving + countInstallSavings + countLoad;
                    if (mListAccountInfo != null &&  countAccount > 0)  {
                        setListReportCredit();
                    }

                    JSONArray arrayREC_BNR = object.optJSONArray("REC_BNR");
                    if (arrayREC_BNR != null) {
                        for (int index = 0; index < arrayREC_BNR.length(); index++) {
                            JSONObject jsonObject = arrayREC_BNR.getJSONObject(index);
                            if (jsonObject == null)
                                continue;

                            AdsItemInfo adsItemInfo = new AdsItemInfo(jsonObject);
                            mListAdsItem.add(adsItemInfo);
                        }
                    }

                    if (mListAdsItem.size() > 0)
                        setListAds(mListAdsItem);

                    setListAccount();

                    if (mShowAdsIndex < 0)
                        mShowAdsIndex = 0;
                    else if (mShowAdsIndex >= mListAdsItem.size() - 1) {
                        mShowAdsIndex = 0;
                    } else {
                        mShowAdsIndex++;
                    }

                    if (mAccountAdapter != null) {
                        mAccountAdapter.setAdsIndex(mShowAdsIndex);
                    }

                    JSONArray arrayREC_POPUP = object.optJSONArray("REC_POPUP");
                    if (arrayREC_POPUP != null) {
                        for (int index = 0; index < arrayREC_POPUP.length(); index++) {
                            JSONObject jsonObject = arrayREC_POPUP.getJSONObject(index);
                            if (jsonObject == null)
                                continue;

                            MainPopupInfo mainPopupInfo = new MainPopupInfo(jsonObject);
                            mListMainPopupItem.add(mainPopupInfo);
                        }
                    }

                    setListMainPopup(mListMainPopupItem);

                    String MAIN_EVNT_PRTP_POSB_YN = object.optString("MAIN_EVNT_PRTP_POSB_YN");
                    String EVNT_ID = object.optString("EVNT_ID");
                    if (mFirstEntry) {
                        showInterActionProfile(MAIN_EVNT_PRTP_POSB_YN, EVNT_ID);
                        mFirstEntry = false;
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void showInterActionProfile(final String PRTP_POSB_YN, final String EVNT_ID) {
        mLayoutTrick.setVisibility(View.INVISIBLE);

        Animation animSlideTrick = AnimationUtils.loadAnimation(mContext, R.anim.sliding_out_trick);
        mLayoutTrick.clearAnimation();
        mLayoutTrick.startAnimation(animSlideTrick);
        mLayoutTrick.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTrick.setVisibility(View.GONE);

                if (Const.REQUEST_WAS_YES.equalsIgnoreCase(PRTP_POSB_YN)) {
                    if (TextUtils.isEmpty(EVNT_ID))
                        return;

                    showMainEvent(EVNT_ID);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mRecyclerViewAccount.setVisibility(View.INVISIBLE);

        Animation animSlideAccount = AnimationUtils.loadAnimation(mContext, R.anim.sliding_in_account_list);
        mRecyclerViewAccount.clearAnimation();
        mRecyclerViewAccount.startAnimation(animSlideAccount);
        mRecyclerViewAccount.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mRecyclerViewAccount.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void showInterActionAccount() {
        mRecyclerViewAccount.setVisibility(View.INVISIBLE);

        Animation animSlideAccount = AnimationUtils.loadAnimation(mContext, R.anim.sliding_in_account_list);
        mRecyclerViewAccount.clearAnimation();
        mRecyclerViewAccount.startAnimation(animSlideAccount);
        mRecyclerViewAccount.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mRecyclerViewAccount.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 대표계좌 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setNomalRepAccount(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;
        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_NOMAL_REP.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);

                String CANO = accountInfo.getCANO();
                if (!TextUtils.isEmpty(CANO))
                    mIsExistCard = true;

                count = 1;
                break;
            }
        }
        return count;
    }

    /**
     * 이어하기 보통예금 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setGoOnNomal(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;
        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_GO_ON_NOMAL.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    /**
     * 이어하기 신용대출 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setGoOnLoan(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;
        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_GO_ON_LOAN.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    /**
     * 이어하기 마이너스대출 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setGoOnMinus(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;
        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_GO_ON_MINUS.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    /**
     * 이어하기 소액마이너스대출 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setGoOnSmallMinus(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;
        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_GO_ON_SMALL_MINUS.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    /**
     * 입출금계좌 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setNomalAccount(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;
        
        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            String CMPH_BNKB_LOAN_YN = accountInfo.getCMPH_BNKB_LOAN_YN();
            if (Const.ACCOUNT_NOMAL.equalsIgnoreCase(DSCT_CD) &&
                !Const.REQUEST_WAS_YES.equalsIgnoreCase(CMPH_BNKB_LOAN_YN)) {
                mListAccountInfo.add(accountInfo);

                String CANO = accountInfo.getCANO();
                if (!TextUtils.isEmpty(CANO))
                    mIsExistCard = true;

                count++;
            }
        }
        return count;
    }

    /**
     * 마이너스 계좌 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setMinusAccount(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;

         for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

             String DSCT_CD = accountInfo.getDSCT_CD();
            String CMPH_BNKB_LOAN_YN = accountInfo.getCMPH_BNKB_LOAN_YN();
            if (!Const.ACCOUNT_NOMAL_REP.equalsIgnoreCase(DSCT_CD) && Const.REQUEST_WAS_YES.equalsIgnoreCase(CMPH_BNKB_LOAN_YN)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
         return count;
    }

    /**
     * 정기예금 계좌 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setSavingsAccount(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;

        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    /**
     * 적금 계좌 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setInstallmentSavingsAccount(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;

        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_INSTALL_SAVINGS.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    /**
     * 대출 계좌 추가
     * @param listAccountInfo 전문 포함 계좌 리스트
     */
    private int setLoanAccount(ArrayList<AccountInfo> listAccountInfo) {
        int count = 0;

        for (int index = 0; index < listAccountInfo.size(); index++) {
            AccountInfo accountInfo = listAccountInfo.get(index);
            if (accountInfo == null)
                continue;

            String DSCT_CD = accountInfo.getDSCT_CD();
            if (Const.ACCOUNT_LOAN.equalsIgnoreCase(DSCT_CD)) {
                mListAccountInfo.add(accountInfo);
                count++;
            }
        }
        return count;
    }

    private void setListReportCredit() {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setDSCT_CD(Const.ACCOUNT_REPORT);
        mListAccountInfo.add(accountInfo);
    }

    private void setListAds(ArrayList<AdsItemInfo> listAdsInfo) {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setDSCT_CD(Const.ACCOUNT_ADS);
        accountInfo.setListAds(listAdsInfo);
        mListAccountInfo.add(accountInfo);
    }

    private void setListAccount() {
        if (mAccountAdapter == null) {
            final LinearLayoutManager llm = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            mRecyclerViewAccount.setLayoutManager(llm);
            mRecyclerViewAccount.setHasFixedSize(true);
            mRecyclerViewAccount.setNestedScrollingEnabled(true);

            mRecyclerViewAccount.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    int position = llm.findFirstVisibleItemPosition();
                    if (position == 0) {
                        mMainTitle.animate().translationY(-500).withLayer().setDuration(200);
                    } else {
                        mMainTitle.setVisibility(View.VISIBLE);
                        mMainTitle.animate().translationY(0).withLayer().setDuration(200);
                    }
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {

                    } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                    } else {

                    }
                }
            });

            mAccountAdapter = new AccountAdapter((BaseActivity) mContext, mListAccountInfo, mIsExistCard, new AccountAdapter.ClickListener() {
                @Override
                public void OnAlramClickListener() {
                    Prefer.setFlagShowFirstNotiMsgBox(mContext.getApplicationContext(), false);
                    FLKPushAgentReceiver.updateBadge(mContext.getApplicationContext(), 0);
                    mImageAlramNoti.setVisibility(View.GONE);

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String fullUrl = WasServiceUrl.MAI0060100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, fullUrl);
                    mContext.startActivity(intent);
                }

                @Override
                public void OnRollingTextSelect(int position) {
                    SlidingItemInfo slidingInfo = mListSlidingItem.get(position);
                    if (slidingInfo == null)
                        return;

                    requestGoOn(slidingInfo);
                }

                @Override
                public void OnAddClickListener() {
                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = WasServiceUrl.UNT0030100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    startActivity(intent);
                }

                @Override
                public void OnJoinClickListener() {
                    ((MainActivity)mContext).selectTab(1);
                    //requestGoodInfo();
                }

                @Override
                public void OnItemClickListener(int position) {

                }

                @Override
                public void OnJoinCancelClickListener() {
                    requestMainInfo();
                }

                @Override
                public void OnGetSelectTag(int position, TagItemInfo tagInfo) {
                    if (tagInfo == null)
                        return;

                    ArrayList<MyAccountInfo> listAccountInfo = LoginUserInfo.getInstance().getMyAccountInfo();
                    if (listAccountInfo == null || listAccountInfo.size() <= 0) {
                        showErrorMessage(getString(R.string.msg_no_exist_trasferable));
                        return;
                    }

                    String TRNF_BANK_CD = tagInfo.getTRNF_BANK_CD();
                    String TRNF_ACNO = tagInfo.getTRNF_ACNO();
                    if (TextUtils.isEmpty(TRNF_BANK_CD) || TextUtils.isEmpty(TRNF_ACNO))
                        return;

                    Intent intent = new Intent(mContext, TransferAccountActivity.class);
                    intent.putExtra(Const.INTENT_TAG_BANK_CODE, TRNF_BANK_CD);
                    intent.putExtra(Const.INTENT_TAG_ACCOUNT, TRNF_ACNO);
                    mContext.startActivity(intent);
                }

                @Override
                public void OnAdsItemClickListener(int position) {
                    if (mListAdsItem == null)
                        return;

                    int size = mListAdsItem.size();
                    if (size <= 0)
                        return;

                    AdsItemInfo adsItemInfo = mListAdsItem.get(position);
                    if (adsItemInfo == null)
                        return;

                    String LINK_URL_ADDR = adsItemInfo.getLINK_URL_ADDR();
                    if (TextUtils.isEmpty(LINK_URL_ADDR))
                        return;

                    String url = WasServiceUrl.getUrl();
                    url += "/";
                    url += LINK_URL_ADDR;
                    url += ".act";

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    startActivity(intent);
                }
            });
            mRecyclerViewAccount.setAdapter(mAccountAdapter);
        } else {
            mAccountAdapter.setListAccountInfo(mListAccountInfo);
        }
    }

    private void setListMainPopup(ArrayList<MainPopupInfo> listMainPopup) {
        if (listMainPopup == null || listMainPopup.size() <= 0)
            return;

        showMainNoticeDialog();
    }

    private void showTimeMode() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
            (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            mLayoutFragmentMain.setBackgroundColor(getResources().getColor(R.color.color39434E));
            mMainTitle.setBackgroundColor(getResources().getColor(R.color.color39434E));
            mImageProfileSmallBg.setImageResource(R.drawable.img_user_circle_small_n);
            mImageLogo.setImageResource(R.drawable.img_logo_n);
            mImageAlram.setImageResource(R.drawable.ico_bell_n);
            mRecyclerViewAccount.setBackgroundColor(getResources().getColor(R.color.color39434E));
            mLayoutTrick.setBackgroundColor(getResources().getColor(R.color.color39434E));
            mLayoutCircleMain.setBackgroundResource(R.drawable.background_circle_main_n);
            mLayoutLine01.setBackgroundColor(getResources().getColor(R.color.color4D5660));
            mLayoutLine02.setBackgroundColor(getResources().getColor(R.color.color4D5660));
        } else {
            mLayoutFragmentMain.setBackgroundColor(getResources().getColor(R.color.white));
            mMainTitle.setBackgroundColor(getResources().getColor(R.color.white));
            mImageProfileSmallBg.setImageResource(R.drawable.img_user_circle_small);
            mImageLogo.setImageResource(R.drawable.img_logo_main);
            mImageAlram.setImageResource(R.drawable.ico_bell);
            mRecyclerViewAccount.setBackgroundColor(getResources().getColor(R.color.white));
            mLayoutTrick.setBackgroundColor(getResources().getColor(R.color.white));
            mLayoutCircleMain.setBackgroundResource(R.drawable.background_circle_main);
            mLayoutLine01.setBackgroundColor(getResources().getColor(R.color.colorF2F2F2));
            mLayoutLine02.setBackgroundColor(getResources().getColor(R.color.colorF2F2F2));
        }
    }

    /**
     * 상품목록조회
     */
    private void requestGoodInfo() {
        showProgress();
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                Logs.i("MAI0010200A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage("정상적으로 통신이 이루어지지 않았습니다.\n잠시후 다시 시도해 주세요.");
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        showErrorMessage("정상적으로 통신이 이루어지지 않았습니다.\n잠시후 다시 시도해 주세요.");
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage("정상적으로 통신이 이루어지지 않았습니다.\n잠시후 다시 시도해 주세요.");
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    mListGoodItem.clear();

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        BankGoodInfo goodInfo = new BankGoodInfo(jsonObject);
                        mListGoodItem.add(goodInfo);
                    }
                    showJoinGood();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 예적금 상품 표시
     */
    private void showJoinGood() {
        if (mListGoodItem == null || mListGoodItem.size() <= 0)
            return;

        JoinGoodDialog td = new JoinGoodDialog(mContext, mListGoodItem);
        td.show();
    }

    public void showErrorMessage(String msg) {
        msg = msg.replaceAll("\\\\n", "\n");

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getActivity().finish();
            }
        };

        final AlertDialog alertDialog = new AlertDialog(mContext);
        alertDialog.mPListener = okClick;
        alertDialog.msg = msg;
        alertDialog.show();
    }

    /**
     * 이어하기
     */
    private void requestGoOn(SlidingItemInfo slidingInfo) {
        String DSCT_CD = slidingInfo.getDSCT_CD();
        String ACNO = slidingInfo.getACNO();
        String ACCO_IDNO = slidingInfo.getACCO_IDNO();
        String TRNF_BANK_CD = slidingInfo.getTRNF_BANK_CD();
        final String PROP_STEP_CD = slidingInfo.getPROP_STEP_CD();
        final String CANO = slidingInfo.getCANO();

        if (TextUtils.isEmpty(DSCT_CD) || TextUtils.isEmpty(ACNO) || TextUtils.isEmpty(ACCO_IDNO) ||
            TextUtils.isEmpty(TRNF_BANK_CD) || TextUtils.isEmpty(PROP_STEP_CD) || TextUtils.isEmpty(CANO))
            return;

        Map param = new HashMap();
        param.put("ACNO", ACNO);
        param.put("ACCO_IDNO", ACCO_IDNO);
        param.put("SUBJ_CD", TRNF_BANK_CD);
        param.put("LOAN_LNKN_PRGS_STEP_DVCD", PROP_STEP_CD);
        param.put("PROP_NO", CANO);

        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String SCRN_ID = object.optString("SCRN_ID");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = WasServiceUrl.getUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param =  "PROP_NO=" + CANO + "&INTR_RDCN_DEMD_PROP_NO=" + CANO + "&LOAN_LNKN_PRGS_STEP_DVCD=" + PROP_STEP_CD;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void showMainNoticeDialog() {
        if (mNoticeDialog != null && mNoticeDialog.isShowing()) {
            return;
        }

        if (mListMainPopupItem == null || mListMainPopupItem.size() <= 0)
            return;

        final MainPopupInfo mainPopupInfo = mListMainPopupItem.get(0);
        if (mainPopupInfo == null)
            return;

        mNoticeDialog = new MainNoticeDialog(mContext, mainPopupInfo, new MainNoticeDialog.OnListener() {
            @Override
            public void onImageClick() {
                if (mContext instanceof Activity && ((Activity) mContext).isFinishing())
                    return;

                String LINK_URL_ADDR = mainPopupInfo.getLINK_URL_ADDR();
                if (TextUtils.isEmpty(LINK_URL_ADDR))
                    return;

                Intent intent = new Intent(mContext, WebMainActivity.class);
                String url = WasServiceUrl.getUrl() + "/" + LINK_URL_ADDR + ".act";
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                mContext.startActivity(intent);
            }

            @Override
            public void onNoShowClick() {
                requestNoShowPopup(mainPopupInfo);
            }
        });

        mNoticeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mNoticeDialog = null;
            }
        });
        mNoticeDialog.show();
    }

    /**
     * 메인 팝업 다시보지 않기
     */
    private void requestNoShowPopup(MainPopupInfo mainPopupInfo) {
        String PUP_SRNO = mainPopupInfo.getPUP_SRNO();
        if (TextUtils.isEmpty(PUP_SRNO))
            return;

        Map param = new HashMap();
        param.put("PUP_SRNO", PUP_SRNO);

        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void showMainEvent(final String EVNT_ID) {
        if (mEventView != null)
            return;

        mEventView = new DrawEvent(mContext, new DrawEvent.TouchViewListener() {
            @Override
            public void OnTouchView() {
                if (mContext instanceof Activity && ((Activity) mContext).isFinishing())
                    return;

                if (mEventDialog != null) {
                    mEventDialog.dismiss();
                    mEventDialog = null;
                }

                showMainEventDialog(EVNT_ID);

                mLayoutDrawEvent.removeView(mEventView);
                mEventView = null;
            }
        });
        mLayoutDrawEvent.addView(mEventView);
    }

    private void showMainEventDialog(String EVNT_ID) {
        if (mEventDialog != null && mEventDialog.isShowing()) {
            return;
        }

        mEventDialog = new MainEventDialog(mContext, EVNT_ID, new MainEventDialog.OnListener() {
            @Override
            public void onFinishEvent(String moveUrl) {
                if (mContext instanceof Activity && ((Activity) mContext).isFinishing())
                    return;

                mEventDialog.dismiss();
                mEventDialog = null;
            }
        });
        mEventDialog.show();
    }
}
