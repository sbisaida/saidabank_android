package com.sbi.saidabank.activity.certification;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.initech.xsafe.cert.INIXSAFEException;
import com.initech.xsafe.util.PasswordChecker;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.solution.cert.CustomCertManager;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Saidabank_android
 * Class: CertExportActivity
 * Created by 950469 on 2018. 9. 10..
 * <p>
 * Description:
 * 보안키패드 입력필드가 1개짜리 처리 화면
 */
public class CertChangePWActivity extends BaseActivity implements ITransKeyActionListener, ITransKeyActionListenerEx, ITransKeyCallbackListener {

    // password error type
    private static final int PASSWORD_ERR_TYPE_NULL = 0;
    private static final int PASSWORD_ERR_TYPE_NOMATCH = 1;
    private static final int PASSWORD_ERR_TYPE_DUPLICATE_CHAR = 2;
    private static final int PASSWORD_ERR_TYPE_CONTINUE_CHAR = 3;
    private static final int PASSWORD_ERR_TYPE_LENGTH = 4;
    private static final int PASSWORD_ERR_TYPE_NOMATCH_NEW = 5;
    private static final int PASSWORD_ERR_TYPE_SAME_CHAR = 6;

    // password change step
    private static final int PASSWORD_CHANGE_STEP_CURRENT = 1;
    private static final int PASSWORD_CHANGE_STEP_NEW = 2;
    private static final int PASSWORD_CHANGE_STEP_NEW_CONFIRM = 3;

    // password min/max length
    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final int PASSWORD_MAX_LENGTH = 49;

    private TransKeyCtrl mTKMgr = null;

    private RelativeLayout mLayoutErrmsg;
    private TextView       mTvErrmsg;
    private TextView       mTvTitle;
    private TextView       mTvLabel;

    private int mCertIndex;
    private int mPasswordChangeStep;

    private String mOldpassword;
    private String mNewpassword;
    private Bundle mCertbundledata;
    private int    mFailCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transkey_input_one);

        getExtra();
        initUX();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void cancel(Intent data) {

    }

    @Override
    public void done(Intent data) {
        if (data == null) {
            setAlertMsg(PASSWORD_ERR_TYPE_NOMATCH, true);
            mTKMgr.setReArrangeKeapad(true);
            mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
            return;
        }

        String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
        if (TextUtils.isEmpty(dummyData)) {
            setAlertMsg(PASSWORD_ERR_TYPE_NOMATCH, true);
            mTKMgr.setReArrangeKeapad(true);
            mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
            return;
        }

        String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);
        String plainData = TransKeyUtils.decryptPlainDataFromCipherData(data);
        String passChiperData = TransKeyUtils.getTransKeyPassCipherData(data);

        Logs.i("secureData : " + secureData);
        Logs.i("passChiperData : " + passChiperData);
        Logs.i("dummyData : " + dummyData);

        if (mCertIndex >= 0) {
            try {
                switch (mPasswordChangeStep) {
                    case PASSWORD_CHANGE_STEP_CURRENT: {
                        Boolean result = CustomCertManager.getInstance().checkPassword(this, mCertIndex, passChiperData);
                        if (!result) {
                            setAlertMsg(PASSWORD_ERR_TYPE_NOMATCH, true);
                            mTKMgr.setReArrangeKeapad(true);
                            mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                        } else {
                            mPasswordChangeStep = PASSWORD_CHANGE_STEP_NEW;
                            mOldpassword = passChiperData;
                            setAlertMsg(PASSWORD_ERR_TYPE_NULL, false);
                            mTKMgr.setReArrangeKeapad(true);
                            mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);

                            mTvLabel.setText(getString(R.string.cert_message_input_new_password));
                        }
                        break;
                    }

                    case PASSWORD_CHANGE_STEP_NEW: {
                        PasswordChecker checker = new PasswordChecker(plainData);

                        if (!checker.checkRepeatedLetter()) {
                            setAlertMsg(PASSWORD_ERR_TYPE_DUPLICATE_CHAR, true);
                            break;
                        }

                        if (!checker.checkContinousLetter()) {
                            setAlertMsg(PASSWORD_ERR_TYPE_CONTINUE_CHAR, true);
                            break;
                        }

                        checker.setMinLength(PASSWORD_MIN_LENGTH);
                        checker.setMaxLength(PASSWORD_MAX_LENGTH);
                        if (!checker.checkMinLength() || !checker.checkMaxLength()) {
                            setAlertMsg(PASSWORD_ERR_TYPE_LENGTH, true);
                            break;
                        }

                        if (mOldpassword.equals(passChiperData)) {
                            setAlertMsg(PASSWORD_ERR_TYPE_SAME_CHAR, true);
                            break;
                        }

                        mPasswordChangeStep = PASSWORD_CHANGE_STEP_NEW_CONFIRM;
                        mNewpassword = passChiperData;
                        setAlertMsg(PASSWORD_ERR_TYPE_NULL, false);
                        mTKMgr.setReArrangeKeapad(true);
                        mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);

                        mTvLabel.setText(getString(R.string.cert_message_input_new_password_again));
                        break;
                    }

                    case PASSWORD_CHANGE_STEP_NEW_CONFIRM: {
                        if (mOldpassword != null && mNewpassword != null && mNewpassword.equals(passChiperData)) {
                            Boolean result = CustomCertManager.getInstance().changePassword(mCertIndex, mOldpassword, mNewpassword);

                            if (!result) {
                                setAlertMsg(PASSWORD_ERR_TYPE_NOMATCH_NEW, true);
                                mTKMgr.setReArrangeKeapad(true);
                                mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                            } else {
                                setAlertMsg(PASSWORD_ERR_TYPE_NULL, false);
                                DialogUtil.alert(this, getString(R.string.cert_message_complete_change_password), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });
                            }
                        } else {
                            setAlertMsg(PASSWORD_ERR_TYPE_NOMATCH_NEW, true);
                            mTKMgr.setReArrangeKeapad(true);
                            mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                        }
                        break;
                    }
                    default:
                        break;
                }
            } catch (INIXSAFEException e) {
                Logs.printException(e);
                finish();
            }
        } else {
            finish();
        }
    }

    @Override
    public void input(int type) {
        Logs.e("input");
    }

    @Override
    public void minTextSizeCallback() {
        Logs.e("minTextSizeCallback() call");
    }

    @Override
    public void maxTextSizeCallback() {
        Logs.e("maxTextSizeCallback() call");
    }


    private void getExtra() {
        Intent intent = getIntent();
        mCertIndex = intent.getIntExtra(Const.INTENT_CERT_INDEX, -1);
        mCertbundledata = intent.getBundleExtra(Const.INTENT_CERT_BUNDLEDATA);
    }

    private void initUX() {
        String label = getString(R.string.cert_message_input_current_password);

        mLayoutErrmsg = (RelativeLayout) findViewById(R.id.ll_alert_msg);
        mTvErrmsg = (TextView) findViewById(R.id.tv_errmsg);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvLabel = (TextView) findViewById(R.id.tv_label);

        mTvTitle.setText("");
        mTvLabel.setText(label);

        Button btnClose = (Button) findViewById(R.id.btn_cancel);
        btnClose.setVisibility(View.VISIBLE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        RelativeLayout layoutInput = findViewById(R.id.inputlayout01);
        EditText editInput = layoutInput.findViewById(R.id.editText);

        mTKMgr = TransKeyUtils.initTransKeyPad(this, 0, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER,
                TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE,
                label,
                "",
                30,
                "",
                2,
                "",
                5,
                true,
                (FrameLayout) findViewById(R.id.keypadContainer),
                editInput,
                (HorizontalScrollView) layoutInput.findViewById(R.id.keyscroll),
                (LinearLayout) layoutInput.findViewById(R.id.keylayout),
                (ImageButton) layoutInput.findViewById(R.id.clearall),
                (RelativeLayout) findViewById(R.id.keypadBallon),
                null,
                false,
                false);

        if (mTKMgr != null) {
            mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
        }

        mPasswordChangeStep = PASSWORD_CHANGE_STEP_CURRENT;

        mFailCount = 0;
    }

    private void setAlertMsg(int errtype, boolean setvisible) {
        if (!setvisible) {
            if (mLayoutErrmsg.getVisibility() != View.GONE)
                mLayoutErrmsg.setVisibility(View.GONE);
            return;
        }

        switch (errtype) {
            case PASSWORD_ERR_TYPE_NOMATCH: {
                mFailCount++;
                if (mFailCount < 5) {
                    mTvErrmsg.setText(getString(R.string.cert_message_not_matched_password) + "(" + mFailCount + "/5)");
                } else {
                    mLayoutErrmsg.setVisibility(View.INVISIBLE);
                    DialogUtil.alert(this, getString(R.string.cert_message_retry_password), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    return;
                }
                break;
            }
            case PASSWORD_ERR_TYPE_DUPLICATE_CHAR: {
                mTvErrmsg.setText(getString(R.string.cert_message_error_continue_duplcate_str));
                break;
            }
            case PASSWORD_ERR_TYPE_CONTINUE_CHAR: {
                mTvErrmsg.setText(getString(R.string.cert_message_error_continue_str));
                break;
            }
            case PASSWORD_ERR_TYPE_LENGTH: {
                mTvErrmsg.setText(getString(R.string.cert_message_error_str_length));
                break;
            }
            case PASSWORD_ERR_TYPE_NOMATCH_NEW: {
                mTvErrmsg.setText(getString(R.string.cert_message_not_matched_new_password));
                break;
            }

            case PASSWORD_ERR_TYPE_SAME_CHAR: {
                mTvErrmsg.setText(getString(R.string.cert_message_same_password));
                break;
            }
            default:
                break;
        }

        if (mLayoutErrmsg.getVisibility() != View.VISIBLE)
            mLayoutErrmsg.setVisibility(View.VISIBLE);

        mTKMgr.setReArrangeKeapad(true);
        mTKMgr.showKeypad(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
    }
}
