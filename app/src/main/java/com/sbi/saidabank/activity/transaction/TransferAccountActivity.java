package com.sbi.saidabank.activity.transaction;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.login.ReregLostPincodeActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.common.adapter.BankStockAdapter;
import com.sbi.saidabank.common.adapter.MyAccountAdapter;
import com.sbi.saidabank.common.adapter.TransferRecentlyAdapter;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.AlertTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmMultiTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmSingleTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.dialog.SlidingInputTextDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.define.datatype.MyAccountInfo;
import com.sbi.saidabank.define.datatype.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.TransferAccountInfo;
import com.sbi.saidabank.define.datatype.TransferReceiptInfo;
import com.sbi.saidabank.define.datatype.VerifyTransferInfo;
import com.sbi.saidabank.solution.motp.MOTPManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Saidabanking_android
 * <p>
 * Class: TransferAccountActivity
 * Created by 950485 on 2018. 11. 21..
 * <p>
 * Description: 계좌번호로 이체를 위한 화면
 */

public class TransferAccountActivity extends BaseActivity implements DatePickerCallback {
    private static final int REQUEST_SSENSTONE_AUTH   = 20000;
    private static final int REQUEST_OTP_AUTH         = 20001;
    private static final float ANIMATION_SHAKE_OFFSET = 20f;

    public static final int MAX_OTP_AMOUNT      = 5000000;

    /**
     * 계좌번호 이체 단계
     */
    private enum TRANSFER_ACCOUNT_STEP {
        STEP_01,
        STEP_02,
        STEP_03
    }

    private LinearLayout   mLayoutTotalTransfer;          // 다건 바로이체
    private TextView       mTextSendNum;                  // 다건 바로이체 건수
    private TextView       mTextSendAmount;               // 다건 바로이체 총금액
    private RelativeLayout mLayoutMyAccountList;
    private ListView       mListViewMyAccount;            // 상단 계좌 리스트
    private RelativeLayout mBtnCloseMyAccountList;        // 상단 계좌 리스트 닫힘
    private RelativeLayout mBtnWithdrawAccount;           // 상단 출금계좌 표시 (출금 계좌 변경 기능)
    private TextView       mTextAccountName;              // 출금 계좌명 (별칭->상품명순)
    private TextView       mTextAccount;                  // 계좌번호 (['계좌번호 뒤4자리'])
    private ImageView      mImageClock;                   // 지연이체 서비스 가입여부 표시
    private TextView       mTextAccountBalance;           // 계좌 출금 가능 금액
    private ImageView      mBtnBalanceDropdown;           // 출금 계좌 변경 버튼
    private ImageView      mBtnLockDropdown;              // 계좌 리스트 사용불가 버튼 (다건)
    private LinearLayout   mLayoutStep01;                 // 이체 금액 입력 단계
    private TextView       mTextHangulAmount;             // 이체 금액 한글 표시
    private LinearLayout   mLayoutStep01Body;             // 실제 금액 입력란 표시 (interaction 위해 구분)
    private View           mViewEditAmountLine;           // 금액 입력란 하단 바
    private ProgressBar    mProgressLine;                 // 금액 입력란 하단 바 (interaction 위해 추가)
    private RelativeLayout mLayoutStep01Bottom;          // 금액 입력란 하단 (interaction 위해 구분)
    private EditText       mEditAmount;                   // 금액 입력란
    private RelativeLayout mBtnEraseAmount;               // 금액 입력 삭제 버튼
    private TextView       mTextErrMsg;                   // 금액 입력 오류 메세지
    private LinearLayout   mLayoutLimitTransfer;          // 한도
    private TextView       mTextLimitOneTime;             // 1회 이체한도
    private TextView       mTextLimitOneDay;              // 1일 이체한도
    private LinearLayout   mLayoutStep02;                 // 이체 계좌 입력 단계
    private TextView       mTextDisplayTransferAmount;    // 이체 금액 표시 (선택시, 1단계로 이동)
    private TextView       mTextBankName;                 // 은행명 표시
    private LinearLayout   mBtnSelectBank;                // 은행 선택창 표시 버튼
    private EditText       mEditAccountNum;               // 이체할 계좌번호
    private RelativeLayout mBtnEraseAccount;              // 이체할 계좌번호 삭제 버튼
    private View           mViewStep02Bar;
    private LinearLayout   mLayoutStep03;                 // 수취 조회 후 단계
    private ScrollView     mScrollviewStep03;
    private TextView       mTextDisplayRecipient;         // 보낸 계좌 표시 문장
    private TextView       mTextDisplaySending;           // 받을 계좌 표시 문장
    private RelativeLayout mLayoutOpenCloseInput;
    private ToggleButton   mBtnOpenCloseInput;            // 시간 및 메모 입력창 표시 버튼
    private RelativeLayout mLayoutAddInpuntItem;          // 시간 및 메모 입력창
    private TextView       mTextInputDate;                // 시간 표시
    private TextView       mTextInputMemo;                // 메모 표시
    private LinearLayout   mLayoutSendDate;               // 시간 설정
    private RelativeLayout mBtnDropdownSendDate;          // 시간 설정창 표시 버튼
    private RelativeLayout mBtnEraseSendDate;             // 시간 삭제 버튼
    private LinearLayout   mLayoutHelp;                   // 시간 설정 도움 버튼
    private LinearLayout   mLayoutDelayDesc;              // 지연이체 설명창
    private Button         mBtnConfirm;                   // 하단 확인 버튼
    private Button         mBtnSendTransfer;              // 하단 이체하기 버튼
    private LinearLayout   mLayoutTransferRecentlyList;   // 최근 이체 내역 표시
    private LinearLayout   mLayoutNoTransferRecently;     // 최근 이체 내역 없을 시 표시
    private SwipeMenuListView mListViewTransferRecently;  // 최근 이체 내역 리스트
    private LinearLayout   mLayoutHelpDelayAccount;       // 지연이체 도움 설명창

    private String         mJsonAccount = "";             // 타메뉴에서 전달된 값
    private String         mTagBankCode = "";            // 메인 최근 이체 태그 은행코드
    private String         mTagAccount = "";             // 메인 최근 이체 태그 계좌
    private String         mInputAmount = "";             // 이체 금액
    private int            mInputYear = 0;                // 날짜선택값
    private int            mInputMonth = 0;               // 날짜선택값
    private int            mInputDay = 0;                 // 날짜선택값
    private int            mInputHour = 0;                // 날짜선택값
    private Double         mBalanceAmount = 0.0;          // 계좌 잔액
    private Double         mTransferOneTime = 0.0;        // 1회 이체한도
    private Double         mTransferOneDay = 0.0;         // 일일 이체한도
    private int            mCount = 0;                    // 계좌입력 라인 인터액션을 위한 카운트
    private int            mSendAccountIndex = -1;        // 나의 계좌 리스트에서 선택된 계좌의 인덱스
    private String         mSendBankStockCode;            // 은행선택창에서 선택된 은행 코드값
    private boolean        mbDLY_TRNF_SVC_ENTR;           // 지연이체 서비스 가입 여부
    private boolean        mbDSGT_MNRC_ACCO_SVC_ENTR_YN;  // 입금계좌지정 서비스 가입 여부
    private String         mMOTPSerialNumber;
    private String         mMOTPTAVersion;

    //++ DatePicker values
    private boolean        mIstoday;
    private boolean        mIsDelayService;
    private int            mCurYear;
    private int            mCurMon;
    private int            mCurDay;
    //--

    private ArrayList<MyAccountInfo>        mListMyAccount = new ArrayList<>();            // 나의 계좌 리스트값
    private ArrayList<TransferAccountInfo>  mListTransferRecently = new ArrayList<>();     // 나의 최근 이체계좌 리스트값
    private ArrayList<RequestCodeInfo>      mListBank = new ArrayList<>();                 // 은행 리스트값
    private ArrayList<RequestCodeInfo>      mListStock = new ArrayList<>();                // 증권사 리스트값

    private ArrayList<TransferReceiptInfo>  mListTransferReceiptInfo = new ArrayList<>();  // 이체완료 후 결과 리스트값

    private TRANSFER_ACCOUNT_STEP mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_01;           // 이체 수행 단계 (1단계 - 금액입력, 2단계 - 이체계좌 입력, 3단계 - 수취조회후)

    private MyAccountAdapter       mMyAccountAdapter;                // 나의 계좌 리스트 어댑터
    private TransferRecentlyAdapter mTransferRecentlyAdapter;       // 최근 이체 리스트 어댑터

    private VerifyTransferInfo     mLastTransferInfo;                 // 수취조회 후 '아니오' 선택 처리를 위해 추가

    private DialogPlus                         dialogBankPlus;         // 은행선택창
    private SlidingConfirmSingleTransferDialog singleTransferDialog;  // 단건확인창
    private SlidingConfirmMultiTransferDialog  multiTransferDialog;   // 다건확인창

    private static final long MIN_CLICK_INTERVAL = 600;             // 연속 클릭 불가 간격
    private long mLastClickTime = 0;                               // 연속 클릭값

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transfer_account);

        getExtra();
        initUX();
        //interActionStep01();

        initMyAccount();
        requestSessionSync();
        requestBankList(false);
        requestStockList(false);
    }

    @Override
    protected void onDestroy() {
        TransferManager.getInstance().clearAll();

        if (dialogBankPlus!= null && dialogBankPlus.isShowing()) {
            dialogBankPlus.dismiss();
            dialogBankPlus = null;
        }

        if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
            hideSingleTransferDialog();
        }

        if (multiTransferDialog != null && multiTransferDialog.isShowing()) {
            hideMultiTransferDialog();
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (dialogBankPlus!= null && dialogBankPlus.isShowing()) {
            dialogBankPlus.dismiss();
            dialogBankPlus = null;
            return;
        }

        if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
            hideSingleTransferDialog();
            return;
        }

        if (multiTransferDialog != null && multiTransferDialog.isShowing()) {
            hideMultiTransferDialog();
            return;
        }

        if (mLayoutHelpDelayAccount.isShown()) {
            hideHelpDelay();
            return;
        }

        if (mLayoutHelp.isShown()) {
            mLayoutHelp.setVisibility(View.GONE);
            mLayoutSendDate.setVisibility(View.VISIBLE);
            return;
        }

        if (mLayoutMyAccountList.isShown()) {
            hideMyAccountList();
            return;
        }

        /*
        if (mLayoutTransferRecentlyList.isShown()) {
            hideTransferRecentlyList(false);
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String bankName = mTextBankName.getText().toString();
                        String accountNum = mEditAccountNum.getText().toString();
                        if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum)) {
                            mBtnConfirm.setEnabled(false);
                        }

                        mBtnConfirm.setVisibility(View.VISIBLE);
                        mBtnSendTransfer.setVisibility(View.GONE);
                    }
                }, 250);
            }

            return;
        }
        */
        showCancelMessage();
        //super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect rectHelpDelay = new Rect();
        mLayoutHelpDelayAccount.getGlobalVisibleRect(rectHelpDelay);
        if (mLayoutHelpDelayAccount.getVisibility() == View.VISIBLE && !rectHelpDelay.contains((int) ev.getRawX(), (int) ev.getRawY())) {
            hideHelpDelay();
            return true;
        } else if (mLayoutHelpDelayAccount.getVisibility() == View.INVISIBLE) {
            return true;
        }

        Rect rectMyAccountList = new Rect();
        mLayoutMyAccountList.getGlobalVisibleRect(rectMyAccountList);
        if (mLayoutMyAccountList.getVisibility() == View.VISIBLE && !rectMyAccountList.contains((int) ev.getRawX(), (int) ev.getRawY())) {
            hideMyAccountList();
            return true;
        } else if (mLayoutMyAccountList.getVisibility() == View.INVISIBLE) {
            return true;
        }

        Rect rectEditAccountNum = new Rect();
        mEditAccountNum.getGlobalVisibleRect(rectEditAccountNum);
        if (mEditAccountNum.getVisibility() == View.VISIBLE) {
            if (!mLayoutMyAccountList.isShown() &&
                rectEditAccountNum.contains((int) ev.getRawX(), (int) ev.getRawY())) {
                if (mLayoutHelpDelayAccount.isShown()) {
                    hideHelpDelay();
                }

                if (mLayoutHelp.isShown()) {
                    mLayoutHelp.setVisibility(View.GONE);
                    mLayoutSendDate.setVisibility(View.VISIBLE);
                }

                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                    if (!mLayoutTransferRecentlyList.isShown()) {
                        showTransferRecentlyList();
                    }
                }
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SSENSTONE_AUTH : {
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String result = commonUserInfo.getResultMsg();
                    if ("P000".equalsIgnoreCase(result)) {
                        requestTransfer(commonUserInfo.getSignData());
                    } else {
                        Logs.shwoToast(this, "인증에 실패했습니다.");
                        if (!TransferManager.getInstance().getIsNotRemove()) {
                            requestRemoveTransfer();
                        }
                    }
                } else {
                    if (!TransferManager.getInstance().getIsNotRemove()) {
                        requestRemoveTransfer();
                    }

                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                        updateSendMultiTransfer(true);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 50);
                    }
                    TransferManager.getInstance().setIsNotRemove(false);
                }
                break;
            }

            case REQUEST_OTP_AUTH: {
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String otpAuthTools = data.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
                    if ("3".equalsIgnoreCase(otpAuthTools) || "2".equalsIgnoreCase(otpAuthTools)) {
                        if (data.hasExtra(Const.INTENT_OTP_AUTH_SIGN)) {
                            String otpSignData = data.getStringExtra(Const.INTENT_OTP_AUTH_SIGN);
                            Intent intent = new Intent(this, PincodeAuthActivity.class);
                            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                            //TODO:sign data, service id 추가
                            commonUserInfo.setSignData(otpSignData);
                            intent.putExtra(Const.INTENT_SERVICE_ID, "");
                            intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                            intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                            commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                            startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
                            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                            return;
                        }
                    }
                } else {
                    if (!TransferManager.getInstance().getIsNotRemove()) {
                        requestRemoveTransfer();
                    }

                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                        updateSendMultiTransfer(true);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 50);
                    }
                    TransferManager.getInstance().setIsNotRemove(false);
                }
                break;
            }

            default:
                break;
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mJsonAccount = getIntent().getStringExtra(Const.INTENT_MAIN_TRANSFER_SAVINGS);
        mTagBankCode = getIntent().getStringExtra(Const.INTENT_TAG_BANK_CODE);
       if ("000".equalsIgnoreCase(mTagBankCode))
           mTagBankCode = "028";
        mTagAccount = getIntent().getStringExtra(Const.INTENT_TAG_ACCOUNT);

        String DLY_TRNF_SVC_ENTR_YN = LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DLY_TRNF_SVC_ENTR_YN))
            mbDLY_TRNF_SVC_ENTR = true;
        else
            mbDLY_TRNF_SVC_ENTR = false;

        String DSGT_MNRC_ACCO_SVC_ENTR_YN = LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DSGT_MNRC_ACCO_SVC_ENTR_YN))
            mbDSGT_MNRC_ACCO_SVC_ENTR_YN = true;
        else
            mbDSGT_MNRC_ACCO_SVC_ENTR_YN = false;
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mLayoutTotalTransfer = (LinearLayout) findViewById(R.id.layout_total_transfer_account);
        mTextSendNum = (TextView) findViewById(R.id.textview_send_num_multi_transfer);
        mTextSendAmount = (TextView) findViewById(R.id.textview_send_amount_multi_transfer);
        mLayoutMyAccountList =  (RelativeLayout) findViewById(R.id.layout_my_account_list);
        mListViewMyAccount = (ListView) findViewById(R.id.listview_my_account_list);
        mBtnCloseMyAccountList = (RelativeLayout) findViewById(R.id.layout_close_my_account_list);
        mBtnWithdrawAccount = (RelativeLayout) findViewById(R.id.layout_account_transfer_account);
        mTextAccountName = (TextView) findViewById(R.id.textview_account_name_transfer_account);
        mTextAccount = (TextView) findViewById(R.id.textview_account_transfer_account);
        mImageClock = (ImageView) findViewById(R.id.imageview_clock);
        mTextAccountBalance = (TextView) findViewById(R.id.textview_account_balance_transfer_account);
        mBtnBalanceDropdown = (ImageView) findViewById(R.id.imageview_account_balance_dropdown);
        mBtnLockDropdown = (ImageView) findViewById(R.id.imageview_lock_balance_dropdown);
        mLayoutStep01 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_01);
        mTextHangulAmount = (TextView) findViewById(R.id.textview_hangul_amount_transfer_account);
        mLayoutStep01Body = (LinearLayout) findViewById(R.id.layout_transfer_account_step_01_body);
        mViewEditAmountLine = (View)  findViewById(R.id.view_edit_amount_bar);
        mProgressLine = (ProgressBar) findViewById(R.id.progress_edit_amount_bar);
        mLayoutStep01Bottom = (RelativeLayout) findViewById(R.id.layout_transfer_account_step_01_bottom);
        mEditAmount = (EditText) findViewById(R.id.edittext_amount_transfer_account);
        mBtnEraseAmount = (RelativeLayout) findViewById(R.id.imageview_erase_transfer_amount);
        mLayoutLimitTransfer = (LinearLayout) findViewById(R.id.layout_limit_transfer);
        mTextLimitOneTime = (TextView) findViewById(R.id.textview_limit_one_time);
        mTextLimitOneDay = (TextView) findViewById(R.id.textview_limit_one_day);
        mTextErrMsg = (TextView) findViewById(R.id.textview_err_msg_transfer_account);
        mLayoutStep02 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_02);
        mTextDisplayTransferAmount = (TextView) findViewById(R.id.textview_display_transfer_amount);
        mTextBankName = (TextView) findViewById(R.id.textview_bank_transfer_account);
        mBtnSelectBank = (LinearLayout) findViewById(R.id.layout_select_bank_transfer_account);
        mEditAccountNum = (EditText) findViewById(R.id.edittext_account_num_transfer_account);
        mBtnEraseAccount = (RelativeLayout) findViewById(R.id.imageview_erase_transfer_account);
        mViewStep02Bar = (View) findViewById(R.id.view_step02_bar);
        mLayoutStep03 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_03);
        mScrollviewStep03 = (ScrollView) findViewById(R.id.scrollview_step_03);
        mTextDisplayRecipient = (TextView) findViewById(R.id.textview_display_recipient);
        mTextDisplaySending = (TextView) findViewById(R.id.textview_display_sending);
        mLayoutOpenCloseInput = (RelativeLayout)  findViewById(R.id.layout_open_close_input);
        mBtnOpenCloseInput = (ToggleButton) findViewById(R.id.btn_open_close_input);
        mLayoutAddInpuntItem = (RelativeLayout) findViewById(R.id.layout_add_input_item);
        mTextInputDate = (TextView) findViewById(R.id.textview_input_date);
        mTextInputMemo = (TextView) findViewById(R.id.textview_input_memo);
        mLayoutDelayDesc = (LinearLayout) findViewById(R.id.layout_delay_desc);
        mBtnConfirm = (Button) findViewById(R.id.btn_ok_transfer_account);
        mBtnSendTransfer = (Button) findViewById(R.id.btn_send_transfer_account);
        mLayoutTransferRecentlyList = (LinearLayout) findViewById(R.id.layout_transfer_recently_list);
        mLayoutNoTransferRecently = (LinearLayout) findViewById(R.id.layout_no_transfer_recently_list);
        mListViewTransferRecently = (SwipeMenuListView) findViewById(R.id.listview_transfer_recently_list);
        mLayoutHelpDelayAccount = (LinearLayout) findViewById(R.id.layout_help_delay_account);

        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_transfer_account);
        btnCancel.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showCancelMessage();
            }
        });

        mListViewMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TransferManager.getInstance().clear();
                mLastTransferInfo = null;

                MyAccountInfo accountInfo = mListMyAccount.get(position);
                if (accountInfo == null)
                    return;

                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }

                String ACNO = accountInfo.getACNO();
                String partACNO = "";
                if (!TextUtils.isEmpty(ACNO)) {
                    int lenACNO = ACNO.length();
                    if (lenACNO > 4)
                        partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                }

                mTextAccountName.setText(ACCO_ALS);
                mTextAccount.setText(" [" + partACNO + "]");

                String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                    mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    mTextAccountBalance.setText(amount + getString(R.string.won));
                }

                if (mLayoutTransferRecentlyList.isShown()) {
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);
                }

                requestInit(ACNO, position, true);
            }
        });

        mBtnCloseMyAccountList.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideMyAccountList();
            }
        });

        LinearLayout btnDirectTransfer = (LinearLayout) findViewById(R.id.layout_send_multi_transfer_account);
        btnDirectTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (TransferManager.getInstance().size() > 0 && mLayoutTotalTransfer.getVisibility() == View.VISIBLE) {
                    TransferManager.getInstance().setIsNotRemove(true);
                    showVerifyTransferAccount();
                }
            }
        });

        initStep01();
        initStep02();
        initStep03();

        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                    if (!checkVaildAmount()) {
                        AniUtils.shakeView(mEditAmount, ANIMATION_SHAKE_OFFSET);
                        mViewEditAmountLine.setBackgroundResource(R.color.red);
                        return;
                    }

                    if (mEditAmount.isFocused()) {
                        mEditAmount.clearFocus();
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAmount);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                requestTransferAccountList(false);
                            }
                        }, 250);
                    } else {
                        requestTransferAccountList(false);
                    }
                } else if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();
                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum))
                        return;

                    if (mEditAccountNum.isFocused()) {
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                    }

                    String amount = mEditAmount.getText().toString();
                    if (TextUtils.isEmpty(amount))
                        return;

                    amount = amount.replaceAll("[^0-9]", "");
                    requestVerifyTransferAccount(mSendBankStockCode, accountNum, amount);
                }
            }
        });

        mBtnSendTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mTransferStep != TRANSFER_ACCOUNT_STEP.STEP_03)
                    return;

                if (mLayoutTransferRecentlyList.isShown())
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);

                if (mLastTransferInfo == null)
                    return;

                requestConfirmTransferAccount(mLastTransferInfo);
            }
        });
    }

    /**
     * 타이틀, 이체금액 입력 화면
     */
    private void initStep01() {
        mBtnWithdrawAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLayoutHelpDelayAccount.isShown()) {
                    return;
                }

                if (mEditAmount.isFocused()) {
                    mEditAmount.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAmount);
                }

                int sendNum = TransferManager.getInstance().size();
                if (sendNum > 0) {
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    };

                    final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                    alertDialog.mPListener = okClick;
                    alertDialog.msg = getString(R.string.msg_no_select_account_multi_transfer);
                    alertDialog.show();
                    return;
                }

                //if (mLayoutTransferRecentlyList.isShown()) {
                //    mLayoutTransferRecentlyList.setVisibility(View.GONE);
                //}

                mBtnWithdrawAccount.animate().translationY(-500).withLayer();
                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showMyAccountList();
                    }
                }, 250);
            }
        });

        mEditAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (!TextUtils.isEmpty(charSequence.toString()) && !charSequence.toString().equals(mInputAmount)) {
                    mInputAmount = Utils.moneyFormatToWon(Double.parseDouble(charSequence.toString().replaceAll(",", "")));
                    mEditAmount.setText(mInputAmount);
                    mEditAmount.setSelection(mInputAmount.length());

                    String hangul = Utils.convertHangul(mInputAmount);
                    if (!TextUtils.isEmpty(hangul)) {
                        hangul += getString(R.string.won);
                        mTextHangulAmount.setText(hangul);
                    }
                } else {
                    if (TextUtils.isEmpty(charSequence.toString())) {
                        mTextHangulAmount.setText("");
                        mInputAmount = "";
                        mBtnConfirm.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String amount = editable.toString();
                amount = amount.replaceAll("[^0-9]", "");
                if (!TextUtils.isEmpty(amount) && Double.valueOf(amount) > 0) {
                    if (!mTextHangulAmount.isShown())
                        mTextHangulAmount.setVisibility(View.VISIBLE);

                    if (!checkVaildAmount()) {
                        mViewEditAmountLine.setBackgroundResource(R.color.red);
                        mLayoutLimitTransfer.setVisibility(View.GONE);
                        mBtnConfirm.setEnabled(false);
                        AniUtils.shakeView(mEditAmount, ANIMATION_SHAKE_OFFSET);
                        return;
                    }

                    if (mTextErrMsg.isShown()) {
                        mTextErrMsg.setText("");
                        mTextErrMsg.setVisibility(View.GONE);
                    }

                    if (!mLayoutLimitTransfer.isShown())
                        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

                    if (!mBtnEraseAmount.isShown())
                        mBtnEraseAmount.setVisibility(View.VISIBLE);

                    mViewEditAmountLine.setBackgroundResource(R.color.color00D5E7);
                } else {
                    mBtnConfirm.setEnabled(false);
                    mBtnEraseAmount.setVisibility(View.GONE);

                    if (mTextErrMsg.isShown()) {
                        mTextErrMsg.setText("");
                        mTextErrMsg.setVisibility(View.GONE);
                    }

                    if (!mLayoutLimitTransfer.isShown())
                        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

                    mViewEditAmountLine.setBackgroundResource(R.color.color00D5E7);
                }
            }
        });

        mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    String amount = mEditAmount.getText().toString();
                    if (!TextUtils.isEmpty(amount)) {
                        if (!mBtnEraseAmount.isShown())
                            mBtnEraseAmount.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (mBtnEraseAmount.isShown())
                        mBtnEraseAmount.setVisibility(View.GONE);
                }
            }
        });

        mBtnEraseAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditAmount.setText("");
            }
        });

        if (mbDLY_TRNF_SVC_ENTR || mbDSGT_MNRC_ACCO_SVC_ENTR_YN) {
            if (mbDLY_TRNF_SVC_ENTR)
                mImageClock.setImageResource(R.drawable.ico_clock);

            if (mbDSGT_MNRC_ACCO_SVC_ENTR_YN)
                mImageClock.setImageResource(R.drawable.ico_check_fill);

            mImageClock.setVisibility(View.VISIBLE);
        }

        mImageClock.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mbDSGT_MNRC_ACCO_SVC_ENTR_YN)
                    return;

                showHelpDelay();
            }
        });

        RelativeLayout btnHideHelpDelay = (RelativeLayout) findViewById(R.id.layout_close_help_delay);
        btnHideHelpDelay.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideHelpDelay();
            }
        });
    }

    /**
     * 이체계좌 선택 화면
     */
    private void initStep02() {
        mTextDisplayTransferAmount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                //mLastTransferInfo = null;
                if (mLastTransferInfo != null) {
                    String CUST_NM = mTextDisplaySending.getText().toString();
                    mLastTransferInfo.setCUST_NM(CUST_NM);

                    String RECV_NM = mTextDisplayRecipient.getText().toString();
                    mLastTransferInfo.setRECV_NM(RECV_NM);
                }

                if (mLayoutTransferRecentlyList.isShown())
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);

                mLayoutStep02.setVisibility(View.INVISIBLE);
                Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_out_bottom_sbi);
                mLayoutStep02.clearAnimation();
                mLayoutStep02.startAnimation(animation);
                mLayoutStep02.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        hideStep03(false);
                        hideStep02(false);
                        showStep01(false);

                        mViewEditAmountLine.setVisibility(View.GONE);
                        showProgressLine();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        mBtnSelectBank.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mListBank.size() <= 0) {
                    requestBankList(false);
                    return;
                }

                if (mListStock.size() <= 0) {
                    requestStockList(false);
                    return;
                }

                showBankStockList();
            }
        });

        mEditAccountNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                        if (!mBtnConfirm.isShown())
                            mBtnConfirm.setVisibility(View.VISIBLE);

                        if(mBtnSendTransfer.isShown())
                            mBtnSendTransfer.setVisibility(View.GONE);

                        hideStep03(false);
                    } if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_03) {
                        hideStep03(false);
                        if (!mBtnConfirm.isShown())
                            mBtnConfirm.setVisibility(View.VISIBLE);

                        if (mBtnSendTransfer.isShown())
                            mBtnSendTransfer.setVisibility(View.GONE);
                    }

                    String amount = mEditAmount.getText().toString();
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();

                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum)) {
                        mBtnConfirm.setEnabled(false);
                    } else {
                        if (TextUtils.isEmpty(amount))
                            mBtnConfirm.setEnabled(false);
                        else
                            mBtnConfirm.setEnabled(true);
                    }

                    if (!mLayoutTransferRecentlyList.isShown()) {
                        showTransferRecentlyList();

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAccountNum.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                            }
                        }, 500);
                    }
                }
            }
        });

        mEditAccountNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(mTextBankName.getText().toString())) {
                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_03) {
                        hideStep03( true);
                    }

                    if (editable.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        mBtnConfirm.setEnabled(true);
                    } else
                        mBtnConfirm.setEnabled(false);

                    mBtnEraseAccount.setVisibility(View.VISIBLE);

                    if (!mBtnConfirm.isShown())
                        mBtnConfirm.setVisibility(View.VISIBLE);

                    if(mBtnSendTransfer.isShown())
                        mBtnSendTransfer.setVisibility(View.GONE);
                } else {
                    hideStep03(true);
                    mBtnConfirm.setEnabled(false);
                }
            }
        });

        mEditAccountNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();
                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum))
                        return false;

                    if (mEditAccountNum.isFocused()) {
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                    }

                    if (accountNum.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        String amount = mEditAmount.getText().toString();
                        if (TextUtils.isEmpty(amount))
                            return false;

                        amount = amount.replaceAll("[^0-9]", "");
                        requestVerifyTransferAccount(mSendBankStockCode, accountNum, amount);
                    } else {
                        showErrorMessage(String.format(getString(R.string.msg_input_limit_length_account), Const.MAX_TRANSFER_ACOOUNT_LENGTH));
                    }
                }
                return false;
            }
        });

        mBtnEraseAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideStep03( true);

                mBtnConfirm.setEnabled(false);
                mBtnConfirm.setVisibility(View.VISIBLE);
                mBtnSendTransfer.setVisibility(View.GONE);
                mEditAccountNum.setText("");
                mBtnEraseAccount.setVisibility(View.GONE);

                if (!mLayoutTransferRecentlyList.isShown()) {
                    showTransferRecentlyList();

                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mEditAccountNum.requestFocus();
                            Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                        }
                    }, 500);
                } else {
                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mEditAccountNum.requestFocus();
                            Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                        }
                    }, 100);
                }
            }
        });

        mListViewTransferRecently.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long currTime = System.currentTimeMillis();
                if (currTime - mLastClickTime < MIN_CLICK_INTERVAL) {
                    return;
                }
                mLastClickTime = currTime;

                TransferAccountInfo accountInfo = mListTransferRecently.get(position);
                if (accountInfo == null)
                    return;

                String bankStockCode = accountInfo.getMNRC_BANK_CD();
                if ("000".equalsIgnoreCase(bankStockCode))
                    bankStockCode = "028";

                if (!TextUtils.isEmpty(bankStockCode)) {
                    String nameBankStock = "";
                    for (int index = 0; index < mListBank.size(); index++) {
                        RequestCodeInfo codeInfo = (RequestCodeInfo) mListBank.get(index);
                        if (codeInfo == null)
                            continue;

                        if (bankStockCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                            nameBankStock = codeInfo.getCD_NM();
                            mSendBankStockCode = codeInfo.getSCCD();
                        }
                    }

                    for (int index = 0; index < mListStock.size(); index++) {
                        RequestCodeInfo codeInfo = (RequestCodeInfo) mListStock.get(index);
                        if (codeInfo == null)
                            continue;

                        if (bankStockCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                            nameBankStock = codeInfo.getCD_NM();
                            mSendBankStockCode = codeInfo.getSCCD();
                        }
                    }

                    if (!TextUtils.isEmpty(nameBankStock))
                        mTextBankName.setText(nameBankStock);
                }

                String accountNum = accountInfo.getMNRC_ACNO();
                if (TextUtils.isEmpty(accountNum))
                    return;

                mEditAccountNum.setText(accountNum);
                if (mEditAccountNum.isFocused()) {
                    mEditAccountNum.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                }

                String amount = mEditAmount.getText().toString();
                if (TextUtils.isEmpty(amount))
                    return;

                amount = amount.replaceAll("[^0-9]", "");
                requestVerifyTransferAccount(mSendBankStockCode, accountNum, amount);
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem editItem = new SwipeMenuItem(getApplicationContext());
                editItem.setBackground(new ColorDrawable(Color.rgb(0x8C,0x97, 0xAC)));
                editItem.setWidth((int) Utils.dpToPixel(TransferAccountActivity.this, 66));
                editItem.setIcon(R.drawable.btn_write_w);
                menu.addMenuItem(editItem);

                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xE0, 0x25, 0x0C)));
                deleteItem.setWidth((int) Utils.dpToPixel(TransferAccountActivity.this, 66));
                deleteItem.setIcon(R.drawable.btn_delete_w);
                menu.addMenuItem(deleteItem);
            }
        };

        mListViewTransferRecently.setMenuCreator(creator);
        mListViewTransferRecently.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        mListViewTransferRecently.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        showEditTransferAccount(position);
                        break;
                    case 1:
                        showRemoveTransferAccount(position);
                        break;
                    default:
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    /**
     * 이체 정보 입력 화면
     */
    private void initStep03() {
        String hintRecipient = String.format(getString(R.string.msg_input_limit_length), 10);
        mTextDisplayRecipient.setHint(hintRecipient);
        RelativeLayout layoutDisplayRecipient = (RelativeLayout) findViewById(R.id.layout_display_recipient);
        layoutDisplayRecipient.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditAccountNum.isFocused()) {
                    mEditAccountNum.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                }

                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferAccountActivity.this,
                        getString(R.string.display_transfer_sending), getString(R.string.hint_display_recipient),
                        10, mTextDisplayRecipient.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextDisplayRecipient.setText(inputString);
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        final RelativeLayout btnEraseRecipient = (RelativeLayout) findViewById(R.id.imageview_erase_display_recipient);
        mTextDisplayRecipient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseRecipient.setVisibility(View.GONE);
                } else {
                    btnEraseRecipient.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseRecipient.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextDisplayRecipient.setText("");
            }
        });

        final String hintSending = String.format(getString(R.string.msg_input_limit_length), 10);
        mTextDisplaySending.setHint(hintSending);
        RelativeLayout layoutDisplaySending =  (RelativeLayout) findViewById(R.id.layout_display_sending);
        layoutDisplaySending.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditAccountNum.isFocused()) {
                    mEditAccountNum.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                }

                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferAccountActivity.this,
                        getString(R.string.display_transfer_recipient), hintSending, 10, mTextDisplaySending.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextDisplaySending.setText(inputString);
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        final RelativeLayout btnEraseSending = (RelativeLayout) findViewById(R.id.imageview_erase_display_sending);
        mTextDisplaySending.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseSending.setVisibility(View.GONE);
                } else {
                    btnEraseSending.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseSending.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextDisplaySending.setText("");
            }
        });

        mLayoutOpenCloseInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtnOpenCloseInput.setChecked(!mBtnOpenCloseInput.isChecked());
            }
        });

        mBtnOpenCloseInput.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mLayoutAddInpuntItem.setVisibility(View.VISIBLE);
                    mLayoutSendDate.setVisibility(View.VISIBLE);
                    mLayoutHelp.setVisibility(View.GONE);
                    mBtnOpenCloseInput.setBackgroundResource(R.drawable.btn_acco_u);
                } else {
                    mLayoutAddInpuntItem.setVisibility(View.GONE);
                    mLayoutSendDate.setVisibility(View.VISIBLE);
                    mLayoutHelp.setVisibility(View.GONE);
                    mBtnOpenCloseInput.setBackgroundResource(R.drawable.btn_acco_d);
                }
            }
        });

        final RelativeLayout btnEraseMemo = (RelativeLayout) findViewById(R.id.imageview_erase_memo);
        final String hintMemo = String.format(getString(R.string.msg_input_limit_length), 20);
        mTextInputMemo.setHint(hintMemo);
        RelativeLayout layoutInputMemo = (RelativeLayout) findViewById(R.id.layout_input_memo);
        layoutInputMemo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferAccountActivity.this,
                        getString(R.string.memo), hintMemo, 20, mTextInputMemo.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextInputMemo.setText(inputString);

                                if (mEditAccountNum.isFocused())
                                    mEditAccountNum.clearFocus();
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        mTextInputMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseMemo.setVisibility(View.GONE);
                } else {
                    btnEraseMemo.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseMemo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextInputMemo.setText("");
            }
        });

        RelativeLayout layoutDisplaySendDate = (RelativeLayout) findViewById(R.id.layout_display_send_date);
        layoutDisplaySendDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLastTransferInfo == null)
                    return;

                mIsDelayService = false;
                // 수취인 조회 결과가 서비스구분코드 '지연3시간이후'로 전달될 시는 시간이 3시간 이후부터 설정 가능
                if (mbDLY_TRNF_SVC_ENTR) {
                    String SVC_DVCD = mLastTransferInfo.getSVC_DVCD();
                    if ("2".equalsIgnoreCase(SVC_DVCD))
                        mIsDelayService = true;
                }

                showDatePicker(mIsDelayService);
            }
        });

        mBtnDropdownSendDate = (RelativeLayout) findViewById(R.id.imageview_dropdown_send_date);
        mBtnEraseSendDate = (RelativeLayout) findViewById(R.id.imageview_erase_send_date);
        mBtnEraseSendDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLastTransferInfo != null) {
                    String TRNF_DVCD = mLastTransferInfo.getTRNF_DVCD();
                    if ("2".equalsIgnoreCase(TRNF_DVCD)) {
                        mTextInputDate.setText(getString(R.string.msg_after_3_hour));
                    } else
                        mTextInputDate.setText(getString(R.string.Immediately));
                } else {
                    mTextInputDate.setText(getString(R.string.Immediately));
                }

                mInputYear = 0;
                mInputMonth = 0;
                mInputDay = 0;
                mInputHour = 0;

                mLastTransferInfo.setTRNF_DVCD("1");
                mLastTransferInfo.setTRNF_DMND_DT("");
                mLastTransferInfo.setTRNF_DMND_TM("");

                mBtnDropdownSendDate.setVisibility(View.VISIBLE);
                mBtnEraseSendDate.setVisibility(View.GONE);
            }
        });

        mLayoutSendDate = (LinearLayout) findViewById(R.id.layout_send_date);
        mLayoutHelp = (LinearLayout) findViewById(R.id.layout_help_send_date);
        RelativeLayout btnHelpOn = (RelativeLayout) findViewById(R.id.imageview_hint_on_send_date);
        btnHelpOn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutSendDate.setVisibility(View.GONE);
                mLayoutHelp.setVisibility(View.VISIBLE);
            }
        });

        RelativeLayout btnHelpOff = (RelativeLayout) findViewById(R.id.imageview_hint_off_send_date);
        btnHelpOff.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutSendDate.setVisibility(View.VISIBLE);
                mLayoutHelp.setVisibility(View.GONE);
            }
        });

        RelativeLayout btnCloseHelp = (RelativeLayout) findViewById(R.id.imageview_close_help);
        btnCloseHelp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutSendDate.setVisibility(View.VISIBLE);
                mLayoutHelp.setVisibility(View.GONE);
            }
        });
    }

    /**
     * 최초 화면 진입 인터액션
     * 금액입력란이 하단에서 위로 올라옴
     */
    private void interActionStep01() {
        Animation animSlideTopBody = AnimationUtils.loadAnimation(this, R.anim.sliding_in_transfer_amount);
        mLayoutStep01Body.clearAnimation();
        mLayoutStep01Body.startAnimation(animSlideTopBody);
        mLayoutStep01Body.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01Body.setVisibility(View.VISIBLE);
                showProgressLine();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최초 화면 진입 금액입력 하단바 인터액션
     * 하단바가 좌에서 우로 이동, 금액입력란은 fade in
     */
    private void showProgressLine() {
        mProgressLine.setVisibility(View.VISIBLE);

        showFadeInLimitAmount();

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mCount >= 100) {
                    timer.cancel();
                    mCount = 0;
                    showAmountLine();
                } else {
                    updateProgressLine();
                }
            }
        }, 0, 2);
    }

    /**
     * 하단바가 좌에서 우로 이동 완료 후 실제 입력란 표시 및 포커스
     */
    private void showAmountLine() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressLine.setVisibility(View.INVISIBLE);
                mViewEditAmountLine.setVisibility(View.VISIBLE);
                mEditAmount.requestFocus();
                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
            }
        });
    }

    /**
     * 금액입력 하단바 하단바가 좌에서 우로 이동
     */
    private void updateProgressLine() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCount++;
                mProgressLine.setProgress(mCount);
            }
        });
    }

    /**
     * 금액입력란 하단 이체 한도 fade in
     */
    private void showFadeInLimitAmount() {
        mLayoutStep01Bottom.setVisibility(View.INVISIBLE);
        Animation animSlideTopBody = AnimationUtils.loadAnimation(this, R.anim.fade_in_transfer_limit);
        mLayoutStep01Bottom.clearAnimation();
        mLayoutStep01Bottom.startAnimation(animSlideTopBody);
        mLayoutStep01Bottom.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01Bottom.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 나의 계좌 중 대표 및 진입 화면 선택 계좌 정보 표시
     */
    private void initMyAccount() {
        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mListMyAccount = userInfo.getMyAccountInfo();
        if (mListMyAccount.size() <= 0)
            return;

        MyAccountInfo accountInfo = null;
        if (TextUtils.isEmpty(mJsonAccount)) {
            accountInfo = mListMyAccount.get(0);
        } else {
            for (int index = 0; index < mListMyAccount.size(); index++) {
                MyAccountInfo account = (MyAccountInfo) mListMyAccount.get(index);
                if (account == null)
                    continue;

                String ACNO = account.getACNO();
                if (mJsonAccount.equalsIgnoreCase(ACNO)) {
                    accountInfo = account;
                    break;
                }
            }
        }

        if (accountInfo == null)
            accountInfo = mListMyAccount.get(0);

        String ACCO_ALS = accountInfo.getACCO_ALS();
        if (TextUtils.isEmpty(ACCO_ALS)) {
            ACCO_ALS = accountInfo.getPROD_NM();
        }

        String ACNO = accountInfo.getACNO();
        String partACNO = "";
        if (!TextUtils.isEmpty(ACNO)) {
            int lenACNO = ACNO.length();
            if (lenACNO > 4)
                partACNO = ACNO.substring(lenACNO - 4, lenACNO);
        }

        mTextAccountName.setText(ACCO_ALS);
        mTextAccount.setText(" [" + partACNO + "]");

        String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
        if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
            mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
            String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
            mTextAccountBalance.setText(amount + getString(R.string.won));
        }
    }

    /**
     * 전체 계좌 리스트 생성
     */
    private void checkMyAccount() {
        if (mListMyAccount.size() > 0)
            mListMyAccount.clear();

        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mListMyAccount = userInfo.getMyAccountInfo();
        if (mListMyAccount.size() <= 0) {
            dismissProgressDialog();
            interActionStep01();
            return;
        }

        if (mMyAccountAdapter != null)
            mMyAccountAdapter = null;

        mMyAccountAdapter = new MyAccountAdapter(TransferAccountActivity.this, mListMyAccount);
        mListViewMyAccount.setAdapter(mMyAccountAdapter);

        int size = mListMyAccount.size();
        if (size >= 6) {
            int height = 0;
            for (int i = 0; i < 6; i++) {
                View childView = mMyAccountAdapter.getView(i, null, mListViewMyAccount);
                childView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                height+= childView.getMeasuredHeight();
            }
            height += mListViewMyAccount.getDividerHeight() * 6;

            ViewGroup.LayoutParams params = mListViewMyAccount.getLayoutParams();
            params.height = height;
            mListViewMyAccount.setLayoutParams(params);
        }

        if (TextUtils.isEmpty(mJsonAccount)) {
            showMyAccount(null);
        } else {
            showMyAccount(mJsonAccount);
        }
    }

    /**
     * 전체 계좌 중 대표 및 진입 시 계좌 표시
     */
    private void showMyAccount(String accountNum) {
        int size = mListMyAccount.size();

        boolean isExistRPRS_ACCO_YN = false;
        if (TextUtils.isEmpty(accountNum)) {
            for (int index = 0; index < size; index++) {
                MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(index);
                if (accountInfo == null)
                    continue;

                String RPRS_ACCO_YN = accountInfo.getRPRS_ACCO_YN();
                if (!TextUtils.isEmpty(RPRS_ACCO_YN) && Const.REQUEST_WAS_YES.equalsIgnoreCase(RPRS_ACCO_YN)) {
                    String ACCO_ALS = accountInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(ACCO_ALS)) {
                        ACCO_ALS = accountInfo.getPROD_NM();
                    }

                    String ACNO = accountInfo.getACNO();
                    String partACNO = "";
                    if (!TextUtils.isEmpty(ACNO)) {
                        int lenACNO = ACNO.length();
                        if (lenACNO > 4)
                            partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                    }

                    mTextAccountName.setText(ACCO_ALS);
                    mTextAccount.setText(" [" + partACNO + "]");

                    String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                    if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                        mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                        String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                        mTextAccountBalance.setText(amount + getString(R.string.won));
                    }

                    requestInit(ACNO, index, false);
                    isExistRPRS_ACCO_YN = true;
                    break;
                }
            }

            if (!isExistRPRS_ACCO_YN) {
                MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(0);
                if (accountInfo == null)
                    return;

                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }

                String ACNO = accountInfo.getACNO();
                String partACNO = "";
                if (!TextUtils.isEmpty(ACNO)) {
                    int lenACNO = ACNO.length();
                    if (lenACNO > 4)
                        partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                }

                mTextAccountName.setText(ACCO_ALS);
                mTextAccount.setText(" [" + partACNO + "]");

                String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                    mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    mTextAccountBalance.setText(amount + getString(R.string.won));
                }
                requestInit(ACNO, 0, false);
            }
        } else {
            for (int index = 0; index < size; index++) {
                MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(index);
                if (accountInfo == null)
                    continue;

                String ACNO = accountInfo.getACNO();
                if (accountNum.equalsIgnoreCase(ACNO)) {
                    String ACCO_ALS = accountInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(ACCO_ALS)) {
                        ACCO_ALS = accountInfo.getPROD_NM();
                    }

                    String partACNO = "";
                    if (!TextUtils.isEmpty(ACNO)) {
                        int lenACNO = ACNO.length();
                        if (lenACNO > 4)
                            partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                    }

                    mTextAccountName.setText(ACCO_ALS);
                    mTextAccount.setText(" [" + partACNO + "]");

                    String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                    if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                        mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                        String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                        mTextAccountBalance.setText(amount + getString(R.string.won));
                    }

                    requestInit(ACNO, index, false);
                    break;
                }
            }
        }
    }

    /**
     * 이체세션클리어 요청
     */
    private void requestClearSession() {
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0010100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 전체 입출금계좌 조회
     */
    private void requestMyAccountList() {
        if (mListMyAccount.size() > 0)
            mListMyAccount.clear();

        Map param = new HashMap();
        param.put("DMNB_ACCO_YN", Const.REQUEST_WAS_YES);      // 요구불계좌여부
        param.put("ISST_ACCO_YN", Const.REQUEST_WAS_NO);       // 적립식계좌여부
        param.put("DFST_ACCO_YN", Const.REQUEST_WAS_NO);       // 거치식계좌여부
        param.put("ACCO_STCD", "1");                           // 계좌상태코드 (1 - 등록), 4 - 해지)

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        MyAccountInfo item = new MyAccountInfo(obj);
                        mListMyAccount.add(item);
                    }
                    showMyAccountList();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 전체 입출금계좌 리스트 표시
     * fade in 및 alpha 조정으로 처리
     */
    private void showMyAccountList() {
        if (this.isFinishing())
            return;

        mLayoutMyAccountList.setVisibility(View.INVISIBLE);
        mListViewMyAccount.setVisibility(View.VISIBLE);
        mBtnCloseMyAccountList.setVisibility(View.VISIBLE);

        Animation animSlideDown = AnimationUtils.loadAnimation(this, R.anim.sliding_list_down);
        mLayoutMyAccountList.clearAnimation();
        mLayoutMyAccountList.startAnimation(animSlideDown);
        mLayoutMyAccountList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutMyAccountList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 전체 입출금계좌 리스트 감추기
     * fade out 및 alpha 조정으로 처리
     */
    private void hideMyAccountList() {
        if (this.isFinishing())
            return;

        Animation animSlideUp = AnimationUtils.loadAnimation(this, R.anim.sliding_list_up);
        mLayoutMyAccountList.clearAnimation();
        mLayoutMyAccountList.startAnimation(animSlideUp);
        mLayoutMyAccountList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutMyAccountList.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        if (mMyAccountAdapter != null)
            mMyAccountAdapter.setIsNotClick(true);

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mListViewMyAccount.setVisibility(View.GONE);
                mBtnCloseMyAccountList.setVisibility(View.GONE);
                if (mMyAccountAdapter != null)
                    mMyAccountAdapter.setIsNotClick(false);
                mBtnWithdrawAccount.animate().translationY(0).withLayer();
            }
        }, 200);
    }

    private void showStep02Bar() {
        Animation animSlideDown = AnimationUtils.loadAnimation(this, R.anim.fade_in_step02_bar);
        mViewStep02Bar.clearAnimation();
        mViewStep02Bar.startAnimation(animSlideDown);
        mViewStep02Bar.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewStep02Bar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 이체초기정보처리
     * @param account 출금계좌
     * @param position 전체계좌 리스트에서 선택된 계좌 위치,
     * @param isUpdate 다른 스텝 화면 정보 갱신 여부
     */
    private void requestInit(String account, final int position, final boolean isUpdate) {
        Map param = new HashMap();
        param.put("WTCH_ACNO", account);
        param.put("TRN_TYCD", "1");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if (isUpdate) {
                    hideMyAccountList();
                    hideStep03(true);
                    hideStep02(true);
                    showStep01(true);
                    mBtnWithdrawAccount.setClickable(true);
                } else {
                    interActionStep01();
                }

                Logs.i("TRA0010100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    showErrorMessage(getString(R.string.msg_debug_no_response), okClick);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        showErrorMessage(getString(R.string.msg_debug_err_response), okClick);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                finish();
                            }
                        };
                        showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        return;
                    }

                    String TI1_TRNF_LMIT_AMT = object.optString("TI1_TRNF_LMIT_AMT");
                    Logs.e("TI1_TRNF_LMIT_AMT : " + TI1_TRNF_LMIT_AMT);
                    if (!TextUtils.isEmpty(TI1_TRNF_LMIT_AMT))
                        mTransferOneTime = Double.valueOf(TI1_TRNF_LMIT_AMT);

                    if (mTransferOneTime >= 0)
                        mTextLimitOneTime.setText(Utils.moneyFormatToWon(mTransferOneTime) + getString(R.string.won));

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                    } else {
                        String D1_TRNF_LMIT_AMT = object.optString("D1_TRNF_LMIT_AMT");
                        if (!TextUtils.isEmpty(D1_TRNF_LMIT_AMT))
                            mTransferOneDay = Double.valueOf(D1_TRNF_LMIT_AMT);
                    }

                    if (mTransferOneDay >= 0)
                        mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));

                    String WTCH_TRN_POSB_AMT = object.optString("WTCH_TRN_POSB_AMT");
                    mBalanceAmount = Double.valueOf(WTCH_TRN_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(mBalanceAmount));
                    mTextAccountBalance.setText(amount + getString(R.string.won));

                    mSendAccountIndex = position;

                    if (mMyAccountAdapter != null) {
                        if (mSendAccountIndex >= 0) {
                            mMyAccountAdapter.setSelectedIndex(mSendAccountIndex);
                            mMyAccountAdapter.notifyDataSetChanged();
                        }
                    }

                    if (mEditAmount.isShown()) {
                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 100);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    showErrorMessage(getString(R.string.msg_debug_no_response), okClick);
                }
            }
        });
    }

    /**
     * 통장 한도 조회
     * @param account 통장번호
     */
    private void requestInqueryLimit(String account) {
        Map param = new HashMap();
        param.put("TRN_DVCD", "0");
        param.put("ACNO", account);
        param.put("TRN_TYCD", "1");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0010300A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {

                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이체 가능 금액 존재 여부 체크
     * @return boolean
     */
    private boolean checkUnderAmount() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount) || mBalanceAmount <= 0) {
            mBtnConfirm.setEnabled(false);
            return true;
        }

        return false;
    }

    /**
     * 잔액 이체 가능 금액 체크
     * @return boolean
     */
    private boolean checkOverAmount() {
        String inputAmount = mEditAmount.getText().toString();
        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        double dInputAmount = Double.valueOf(inputAmount);
        if (dInputAmount > mBalanceAmount) {
            return true;
        }

        return false;
    }

    /**
     * 1회 이체 가능 금액 체크
     * @return boolean
     */
    private boolean checkOverOneTime() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount)) {
            mBtnConfirm.setEnabled(false);
            return false;
        }

        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        double dInputAmount = Double.valueOf(inputAmount);
        if (dInputAmount > mTransferOneTime)
            return true;

        return false;
    }

    /**
     * 일일이체 가능 금액 체크
     * @return boolean
     */
    private boolean checkOverOneDay() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount)) {
            mBtnConfirm.setEnabled(false);
            return false;
        }

        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        double dInputAmount = Double.valueOf(inputAmount);
        if (dInputAmount > mTransferOneDay) {
            return true;
        }

        return false;
    }

    /**
     * 다건이체 시 잔액보다 많이 이체 금액 여부
     * @return boolean
     */
    private boolean checkOverTotalAmount() {
        if (!mLayoutTotalTransfer.isShown())
            return false;

        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount))
            return true;

        String sendAmount = mTextSendAmount.getText().toString();
        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        sendAmount = sendAmount.replaceAll("[^0-9]", "");
        if (!TextUtils.isEmpty(sendAmount)) {
            double dSendAmount = Double.valueOf(sendAmount);
            double dInputAmount = Double.valueOf(inputAmount);
            dInputAmount = dInputAmount + dSendAmount;
            if (dInputAmount > mBalanceAmount) {
                return true;
            }
        }

        return false;
    }

    /**
     * 이체금액 유효성 체크
     * @return boolean
     */
    private boolean checkVaildAmount() {
        if (checkUnderAmount()) {
            String msg = getString(R.string.msg_err_trnasfer_over_balance);
            mTextErrMsg.setText(msg);
            mTextErrMsg.setVisibility(View.VISIBLE);
            mBtnConfirm.setEnabled(false);
            return false;
        }

        if (checkOverAmount()) {
            String msg = getString(R.string.msg_err_trnasfer_over_balance);
            mTextErrMsg.setText(msg);
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverOneTime()) {
            String msg = getString(R.string.msg_err_trnasfer_over_one_time);
            mTextErrMsg.setText(msg);
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverOneDay()) {
            String msg = getString(R.string.msg_err_trnasfer_over_one_day);
            mTextErrMsg.setText(msg);
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverTotalAmount()) {
            String msg = getString(R.string.msg_err_trnasfer_over_balance);
            mTextErrMsg.setText(msg);
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else {
            mBtnConfirm.setEnabled(true);
            mTextHangulAmount.setVisibility(View.VISIBLE);
            mTextErrMsg.setVisibility(View.GONE);
        }

        return true;
    }

    /**
     * 취소 메세지 출력
     */
    private void showCancelMessage() {
        View.OnClickListener okClick = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        };

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
        alertDialog.mPListener = okClick;
        alertDialog.mNListener = cancelClick;

        if (TransferManager.getInstance().size() > 1) {
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                alertDialog.msg = getString(R.string.msg_cancel_multi_trnasfer_account);
            }
            else {
                alertDialog.msg = getString(R.string.msg_cancel_transfer);
                alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
            }
        } else {
            alertDialog.msg = getString(R.string.msg_cancel_transfer);
            alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
        }
        alertDialog.show();
    }

    /**
     * 입금할 금액 입력 화면 표시
     * @param isClearAmount 입력 금액 삭제 여부
     */
    private void showStep01(boolean isClearAmount) {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_01;

        mTextErrMsg.setVisibility(View.GONE);
        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

        mBtnConfirm.setVisibility(View.VISIBLE);
        mBtnSendTransfer.setVisibility(View.GONE);

        if (mLayoutTransferRecentlyList.isShown())
            mLayoutTransferRecentlyList.setVisibility(View.GONE);

        mLayoutStep01.setVisibility(View.VISIBLE);

        if (isClearAmount) {
            mEditAmount.setText("");
        } else {
            mBtnConfirm.setEnabled(true);
        }

        TransferManager.getInstance().setIsNotRemove(false);

        updateSendMultiTransfer(true);
    }

    /**
     * 금액 입력란 사라지는 interaction
     * 상단에서 하단으로 이동, fade out
     */
    private void interActionHideStep01() {
        Animation animSlideOut = AnimationUtils.loadAnimation(this, R.anim.sliding_out_transfer_amount);
        mLayoutStep01.clearAnimation();
        mLayoutStep01.startAnimation(animSlideOut);
        mLayoutStep01.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTotalTransfer.setVisibility(View.GONE);
                mLayoutStep01.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mLayoutStep02.setVisibility(View.INVISIBLE);
                interActionShowStep02();
            }
        }, 100);
    }

    /**
     * 입금할 계좌 입력 화면 표시
     */
    private void showStep02() {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;
        mLayoutTotalTransfer.setVisibility(View.INVISIBLE);

        String bankName = mTextBankName.getText().toString();
        String accountNum  = mEditAccountNum.getText().toString();
        if (TextUtils.isEmpty(bankName) && TextUtils.isEmpty(accountNum)) {
            mBtnConfirm.setEnabled(false);
        } else {
            if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum)) {
                mBtnConfirm.setEnabled(false);
            } else
                mBtnConfirm.setEnabled(true);
        }

        interActionHideStep01();

        String amount = mEditAmount.getText().toString();
        if (!TextUtils.isEmpty(amount)) {
            mTextDisplayTransferAmount.setText(amount);

            if (mLastTransferInfo != null) {
                amount = amount.replaceAll(",", "");
                mLastTransferInfo.setTRN_AMT(amount);
            }
        }

        if (!TextUtils.isEmpty(mTagBankCode)) {
            String nameBankStock = "";
            String codeBankStock = "";

            for (int index = 0; index < mListBank.size(); index++) {
                RequestCodeInfo codeInfo = (RequestCodeInfo) mListBank.get(index);
                if (codeInfo == null)
                    continue;

                if (mTagBankCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                    nameBankStock = codeInfo.getCD_NM();
                    codeBankStock = codeInfo.getSCCD();
                    break;
                }
            }

            if (TextUtils.isEmpty(codeBankStock)) {
                for (int index = 0; index < mListStock.size(); index++) {
                    RequestCodeInfo codeInfo = (RequestCodeInfo) mListBank.get(index);
                    if (codeInfo == null)
                        continue;

                    if (mTagBankCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                        nameBankStock = codeInfo.getCD_NM();
                        codeBankStock = codeInfo.getSCCD();
                        break;
                    }
                }
            }

            if (!TextUtils.isEmpty(codeBankStock)) {
                mTextBankName.setText(nameBankStock);
                mSendBankStockCode = codeBankStock;
            }
            mTagBankCode = "";
        }

        if (!TextUtils.isEmpty(mTagAccount)) {
            mEditAccountNum.setText(mTagAccount);
            mTagAccount = "";
        }
    }

    /**
     * 이체 계좌 입력 화면 interaction
     * 하단에서 상단으로 이동, fade in 처리
     */
    private void interActionShowStep02() {
        Animation animSlideIn = AnimationUtils.loadAnimation(this, R.anim.sliding_in_transfer_verify);
        mLayoutStep02.clearAnimation();
        mLayoutStep02.startAnimation(animSlideIn);
        mLayoutStep02.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep02.setVisibility(View.VISIBLE);
                if (!mLayoutTransferRecentlyList.isShown())
                    showTransferRecentlyList();
                else {
                    String recipient = mTextDisplayRecipient.getText().toString();
                    String sending = mTextDisplaySending.getText().toString();
                    String memo = mTextInputMemo.getText().toString();
                    if (!TextUtils.isEmpty(recipient) || !TextUtils.isEmpty(sending) ||
                        !TextUtils.isEmpty(memo) ||
                        (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0)) {
                        showStep03();
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 이체계좌 입력화면 숨기기
     * @param isClear 입력값 삭제 여부
     */
    private void hideStep02(boolean isClear) {
        mLayoutStep02.setVisibility(View.GONE);
        mViewStep02Bar.setVisibility(View.INVISIBLE);

        clearStep02(isClear);
    }

    /**
     * 이체계좌 입력값 삭제
     * @param isClear 입력값 삭제 여부
     */
    private void clearStep02(boolean isClear) {
        mTextDisplayTransferAmount.setText("");

        if (isClear) {
            mTextBankName.setText("");
            mEditAccountNum.setText("");
        }

        if (TransferManager.getInstance().size() <= 0) {
            mBtnBalanceDropdown.setVisibility(View.VISIBLE);
            mBtnLockDropdown.setVisibility(View.GONE);
            mBtnWithdrawAccount.setClickable(true);
        }
    }

    /**
     * 보낼시간과 메모 입력화면 표시
     */
    private void showStep03() {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_03;
        mLayoutStep03.setVisibility(View.INVISIBLE);

        if (mLastTransferInfo != null) {
            String RECV_NM = mLastTransferInfo.getRECV_NM();
            if (!TextUtils.isEmpty(RECV_NM))
                mTextDisplayRecipient.setText(RECV_NM);

            String CUST_NM = mLastTransferInfo.getCUST_NM();
            if (!TextUtils.isEmpty(CUST_NM))
                mTextDisplaySending.setText(CUST_NM);

            String TRNF_DVCD = mLastTransferInfo.getTRNF_DVCD();
            if ("1".equalsIgnoreCase(TRNF_DVCD)) {
                if (mbDLY_TRNF_SVC_ENTR)
                    mLayoutDelayDesc.setVisibility(View.VISIBLE);
                else
                    mLayoutDelayDesc.setVisibility(View.GONE);

                mTextInputDate.setText(getString(R.string.Immediately));
            } else if ("2".equalsIgnoreCase(TRNF_DVCD)) {
                mLayoutDelayDesc.setVisibility(View.VISIBLE);
                mTextInputDate.setText(getString(R.string.msg_after_3_hour));
            }
        } else {
            if (mbDLY_TRNF_SVC_ENTR) {
                mLayoutDelayDesc.setVisibility(View.VISIBLE);
                mTextInputDate.setText(getString(R.string.msg_after_3_hour));
            } else {
                mLayoutDelayDesc.setVisibility(View.GONE);
                mTextInputDate.setText(getString(R.string.Immediately));
            }
        }

        interActionShowStep03();
    }

    /**
     * 보낼시간과 메모 입력화면 interaction
     * 하단에서 상단이동, fain in 처리
     */
    private void interActionShowStep03() {
        Animation animSlideIn = AnimationUtils.loadAnimation(this, R.anim.sliding_in_transfer_verify);
        mLayoutStep03.clearAnimation();
        mLayoutStep03.startAnimation(animSlideIn);
        mLayoutStep03.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep03.setVisibility(View.VISIBLE);
                mBtnConfirm.setVisibility(View.GONE);
                mBtnSendTransfer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 보낼시간과 메모 입력화면 숨기기
     */
    private void hideStep03(boolean isClear) {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;
        mLayoutStep03.setVisibility(View.GONE);
        mLayoutHelp.setVisibility(View.GONE);
        mLayoutSendDate.setVisibility(View.GONE);
        mLayoutAddInpuntItem.setVisibility(View.GONE);
        mBtnSendTransfer.setVisibility(View.GONE);

        mBtnOpenCloseInput.setSelected(false);
        mBtnOpenCloseInput.setBackgroundResource(R.drawable.btn_acco_d);

        mScrollviewStep03.scrollTo(0, 0);

        clearStep03(isClear);
    }

    /**
     * 보낼시간과 메모 입력값 삭제
     */
    private void clearStep03(boolean isClear) {
        if (isClear) {
            mTextDisplayRecipient.setText("");
            mTextDisplaySending.setText("");
            //mTextInputMemo.setText("");

            if (mbDLY_TRNF_SVC_ENTR)
                mTextInputDate.setText(getString(R.string.msg_after_3_hour));
            else
                mTextInputDate.setText(getString(R.string.Immediately));

            mInputYear = 0;
            mInputMonth = 0;
            mInputDay = 0;
            mInputHour = 0;
        }

        mBtnDropdownSendDate.setVisibility(View.VISIBLE);
        mBtnEraseSendDate.setVisibility(View.GONE);
    }

    /**
     * 최근/자주 이체 계좌 조회
     * @param isUpdate 조회결과만 update 여부
     */
    private void requestTransferAccountList(final boolean isUpdate) {
        if (mListTransferRecently != null && mListTransferRecently.size() > 0)
            mListTransferRecently.clear();

        Map param = new HashMap();
        param.put("INQ_DVCD", "1");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0050100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (!isUpdate)
                    showStep02();

                Logs.i("CMM0050100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        TransferAccountInfo item = new TransferAccountInfo(obj);
                        mListTransferRecently.add(item);
                    }

                    if (isUpdate) {
                        updateTransferRecentlyList(mListTransferRecently);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 최근/자주 이체 계좌 리스트 설정 및 표시
     * 상단으로 이동, fade in 처리
     */
    private void showTransferRecentlyList() {
        if (this.isFinishing())
            return;

        if (mTransferRecentlyAdapter != null)
            mTransferRecentlyAdapter = null;

        mTransferRecentlyAdapter = new TransferRecentlyAdapter(TransferAccountActivity.this, 0, mListTransferRecently,
                new TransferRecentlyAdapter.OnItemClickListener() {
            @Override
            public void OnFavirteListener(int position) {
                requestFavirteAccount(position);
            }
        });
        mListViewTransferRecently.setAdapter(mTransferRecentlyAdapter);

        if (mTransferRecentlyAdapter.getCount() == 0) {
            mLayoutNoTransferRecently.setVisibility(View.VISIBLE);
            mListViewTransferRecently.setVisibility(View.GONE);
        } else {
            mLayoutNoTransferRecently.setVisibility(View.GONE);
            mListViewTransferRecently.setVisibility(View.VISIBLE);
        }

        mLayoutTransferRecentlyList.setVisibility(View.INVISIBLE);
        Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_in_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                String bankName = mTextBankName.getText().toString();
                String accountNum = mEditAccountNum.getText().toString();
                if (!TextUtils.isEmpty(bankName) || !TextUtils.isEmpty(accountNum)) {
                    mBtnConfirm.setVisibility(View.VISIBLE);
                }

                mLayoutTransferRecentlyList.setVisibility(View.VISIBLE);

                // iOS 무조건 금액 변경 후 수취인 조회한다고 하여 sync 맞춤
                /*String recipient = mTextDisplayRecipient.getText().toString();
                String sending = mTextDisplaySending.getText().toString();
                String memo = mTextInputMemo.getText().toString();
                if (!TextUtils.isEmpty(recipient) || !TextUtils.isEmpty(sending) ||
                    !TextUtils.isEmpty(memo) ||
                    (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0)) {
                    showStep03();
                }*/

                if (mViewStep02Bar.getVisibility() == View.INVISIBLE) {
                    showStep02Bar();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최근/자주 이체 계좌 리스트 감추기
     * @param isShowStep03 보낼시간, 메모 입력란 표시 여부
     */
    private void hideTransferRecentlyList(final boolean isShowStep03) {
        if (this.isFinishing())
            return;

        if (!mLayoutTransferRecentlyList.isShown()) {
            if (isShowStep03) {
                showStep03();

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBtnConfirm.setVisibility(View.GONE);
                        mBtnSendTransfer.setVisibility(View.VISIBLE);
                    }
                }, 250);
            }

            return;
        }

        Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_out_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTransferRecentlyList.setVisibility(View.GONE);
                if (isShowStep03) {
                    showStep03();

                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBtnConfirm.setVisibility(View.GONE);
                            mBtnSendTransfer.setVisibility(View.VISIBLE);
                        }
                    }, 250);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 수취 조회 후 최근/자주 이체 계좌 리스트 감추기
     * 상단에서 하단으로 이동, fade out 처리
     */
    private void hideTransferRecentlyListAfter() {
        if (this.isFinishing())
            return;

        Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_out_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTransferRecentlyList.setVisibility(View.GONE);
                showStep03();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최근/자주 이체 계좌 리스트 업데이트
     * @param dataList 업데이트할 리스트
     */
    private void updateTransferRecentlyList(final ArrayList<TransferAccountInfo> dataList) {
        if (mTransferRecentlyAdapter == null)
            return;

        mTransferRecentlyAdapter.updateDataList(dataList);
    }

    /**
     * 은행 조회
     * @param isShowProgress 진행바 노출여부 (최초 진입시 선행으로 실행 위해 포함)
     */
    private void requestBankList(final boolean isShowProgress) {
        Map param = new HashMap();
        param.put("LCCD", "BANK_CD");
        if (isShowProgress)
            showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("CMM0010100A00 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    if (isShowProgress) {
                        dismissProgressDialog();
                        showErrorMessage(getString(R.string.msg_debug_no_response));
                    }
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);

                        if (isShowProgress) {
                            dismissProgressDialog();
                            showErrorMessage(getString(R.string.msg_debug_err_response));
                        }
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {

                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        /*
                        if (isShowProgress) {
                            dismissProgressDialog();
                            showErrorMessage(msg);
                        }
                        */
                        if (isShowProgress) {
                            dismissProgressDialog();
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        RequestCodeInfo item = new RequestCodeInfo(jsonObject);
                        mListBank.add(item);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 증권사 조회
     * @param isShowProgress 진행바 노출여부 (최초 진입시 선행으로 실행 위해 포함)
     */
    private void requestStockList(final boolean isShowProgress) {
        Map param = new HashMap();
        param.put("LCCD", "SCCM_CD");
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if (isShowProgress)
                    dismissProgressDialog();

                Logs.i("CMM0010100A00 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    if (isShowProgress)
                        showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        if (isShowProgress)
                            showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        /*
                        if (isShowProgress)
                            showErrorMessage(msg);
                        */
                        if (isShowProgress) {
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        RequestCodeInfo item = new RequestCodeInfo(jsonObject);
                        mListStock.add(item);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 은행/증권사 리스트 표시
     */
    private void showBankStockList() {
        if (this.isFinishing())
            return;

        if (mLayoutMyAccountList.isShown()) {
            hideMyAccountList();
        }

        if (dialogBankPlus != null && dialogBankPlus.isShowing()) {
            return;
        }

        if (mEditAccountNum.isFocused()) {
            mEditAccountNum.clearFocus();
            Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
        }

        final BankStockAdapter adapter = new BankStockAdapter(this, mListBank, mListStock);

        Holder holder = new ListHolder();

        View viewHeader = getLayoutInflater().inflate(R.layout.dialog_backstock_header, null);
        TextView textTitle = viewHeader.findViewById(R.id.textview_dialog_slide_header);
        textTitle.setText(getString(R.string.title_bank_stock));

        OnItemClickListener itemClickListener = new OnItemClickListener() {
            @Override
            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                if (position < 1) return;

                RequestCodeInfo codeInfo = null;
                if (adapter.getTabState() == Const.BANK_STOCK_MODE.BANK_MODE)
                    codeInfo = (RequestCodeInfo) mListBank.get(position - 1);
                else if (adapter.getTabState() == Const.BANK_STOCK_MODE.STOCK_MODE)
                    codeInfo = (RequestCodeInfo) mListStock.get(position - 1);

                if (codeInfo == null)
                    return;

                String bankName = codeInfo.getCD_NM();
                mTextBankName.setText(bankName);
                mSendBankStockCode = codeInfo.getSCCD();

                mEditAccountNum.setText("");
                if (mBtnEraseAccount.isShown())
                    mBtnEraseAccount.setVisibility(View.GONE);

                mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;

                dialog.dismiss();

                if (!mLayoutTransferRecentlyList.isShown()) {
                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showTransferRecentlyList();
                        }
                    }, 500);
                }
            }
        };

        OnDismissListener dismissListener = new OnDismissListener() {
            @Override
            public void onDismiss(DialogPlus dialog) {
                if (mEditAccountNum.isFocused()) {
                    Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                }
                dialogBankPlus = null;
            }
        };

        float pixel = Utils.dpToPixel(this, 245 + 68 + 45 + 14);

        dialogBankPlus = DialogPlus.newDialog(this)
                                        .setContentHolder(holder)
                                        .setAdapter(adapter)
                                        .setCancelable(true)
                                        .setExpanded(true, (int) pixel)
                                        .setHeader(viewHeader)
                                        .setOnItemClickListener(itemClickListener)
                                        .setOnDismissListener(dismissListener)
                                        .create();
        dialogBankPlus.show();
    }

    /**
     * 수취인 조회
     * @param bankStockCode 이체계좌 은행코드
     * @param transferAccount 이체계좌
     * @param transferAmount 이체금액
     */
    private void requestVerifyTransferAccount(final String bankStockCode, final String transferAccount, final String transferAmount) {
        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo == null)
            return;

        String WTCH_ACNO = accountInfo.getACNO();

        Map param = new HashMap();
        param.put("TRTM_DVCD", "1");
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("CNTP_FIN_INST_CD", bankStockCode);
        param.put("CNTP_BANK_ACNO", transferAccount);
        param.put("MNRC_CPNO", "");
        param.put("TRN_AMT", transferAmount);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0010400A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));

                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!mLayoutTransferRecentlyList.isShown())
                                showTransferRecentlyList();
                        }
                    };

                    showErrorMessage(String.format(getString(R.string.msg_fail_timeout_request), getString(R.string.inquire_deposit_account)), okClick);
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList();
                            }
                        };

                        showErrorMessage(String.format(getString(R.string.msg_fail_timeout_request), getString(R.string.inquire_deposit_account)), okClick);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        String msg = getString(R.string.common_msg_no_reponse_value_was);
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList();
                            }
                        };

                        showErrorMessage(msg, okClick);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        /*
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList(true);
                            }
                        };

                        showErrorMessage(msg, okClick);
                        */
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if ("EEFN0306".equalsIgnoreCase(errCode) ||
                            "EEFN0307".equalsIgnoreCase(errCode) ||
                            "EEIF0516".equalsIgnoreCase(errCode)) {
                            Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                            String url = WasServiceUrl.ERR0030100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                            String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode +
                                           "&" +  Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                            intent.putExtra(Const.INTENT_PARAM, param);

                            startActivity(intent);
                            finish();
                            return;
                        }

                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList();
                            }
                        };
                        showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        return;
                    }

                    String SAME_AMT_TRNF_YN = object.optString("SAME_AMT_TRNF_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SAME_AMT_TRNF_YN)) {
                        View.OnClickListener cancelClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        };

                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDlgVerifyTransferAccount(bankStockCode, transferAccount, transferAmount, object);
                            }
                        };

                        final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                        alertDialog.mPListener = okClick;
                        alertDialog.mNListener = cancelClick;
                        alertDialog.msg = "오늘 같은 분에게 동일한 금액을\n이체하셨습니다.\n정말 이체하시겠습니까?";
                        alertDialog.show();
                        return;
                    }

                    showDlgVerifyTransferAccount(bankStockCode, transferAccount, transferAmount, object);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 수취인 조회 결과 화면 표시
     * @param bankStockCode 이체계좌 은행코드
     * @param transferAccount 이체계좌
     * @param transferAmount 이체금액
     * @param object 수취인 조회 결과값 (JSON object)
     */
    private void showDlgVerifyTransferAccount(final String bankStockCode, final String transferAccount, final String transferAmount, JSONObject object) {
        final String RECV_NM = object.optString("RECV_NM");
        final String FEE = object.optString("FEE");
        final String CUST_NM = object.optString("CUST_NM");
        final String SVC_DVCD = object.optString("SVC_DVCD");

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerifyTransferInfo verifyTransferInfo = new VerifyTransferInfo();
                verifyTransferInfo.setCNTP_FIN_INST_CD(bankStockCode);
                verifyTransferInfo.setCNTP_BANK_ACNO(transferAccount);
                verifyTransferInfo.setTRN_AMT(transferAmount);
                verifyTransferInfo.setRECV_NM(RECV_NM);
                verifyTransferInfo.setFEE(FEE);
                verifyTransferInfo.setCUST_NM(CUST_NM);
                verifyTransferInfo.setSVC_DVCD(SVC_DVCD);

                if (!mbDLY_TRNF_SVC_ENTR && !mbDSGT_MNRC_ACCO_SVC_ENTR_YN) {
                    if (mInputYear == 0 || mInputMonth == 0 || mInputDay == 0 || mInputHour == 0) {
                        verifyTransferInfo.setTRNF_DVCD("1");
                        verifyTransferInfo.setTRNF_DMND_DT("");
                        verifyTransferInfo.setTRNF_DMND_TM("");
                    } else {
                        verifyTransferInfo.setTRNF_DVCD("3");
                        String TRNF_DMND_DT = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                        String TRNF_DMND_TM = String.format("%02d0000", mInputHour);
                        verifyTransferInfo.setTRNF_DMND_DT(TRNF_DMND_DT);
                        verifyTransferInfo.setTRNF_DMND_TM(TRNF_DMND_TM);
                    }
                } else if (mbDLY_TRNF_SVC_ENTR) {
                    if ("1".equalsIgnoreCase(SVC_DVCD)) {            // 즉시
                        mTextInputDate.setText(getString(R.string.Immediately));
                        verifyTransferInfo.setTRNF_DVCD("1");
                    } else if ("2".equalsIgnoreCase(SVC_DVCD)) {    // 지연3시간이후
                        verifyTransferInfo.setTRNF_DVCD("2");
                        mTextInputDate.setText(getString(R.string.msg_after_3_hour));
                    }
                } else if (mbDSGT_MNRC_ACCO_SVC_ENTR_YN) {
                    verifyTransferInfo.setTRNF_DVCD("5");
                }

                if (mLastTransferInfo != null)
                    mLastTransferInfo = null;

                mLastTransferInfo = verifyTransferInfo;

                if (mLayoutTransferRecentlyList.isShown()) {
                    hideTransferRecentlyListAfter();
                } else {
                    showStep03();
                }
            }
        };

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();
                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum)) {
                        mBtnConfirm.setEnabled(false);
                    }

                    mBtnConfirm.setVisibility(View.VISIBLE);
                    mBtnSendTransfer.setVisibility(View.GONE);

                    if (!mLayoutTransferRecentlyList.isShown())
                        showTransferRecentlyList();
                }
            }
        };

        if (mEditAccountNum.hasFocus())
            mEditAccountNum.clearFocus();

        final AlertTransferDialog alertDialog = new AlertTransferDialog(TransferAccountActivity.this);
        alertDialog.mPListener = okClick;
        alertDialog.mNListener = cancelClick;
        alertDialog.msg = String.format(getString(R.string.msg_confirm_received_account_01), RECV_NM);
        alertDialog.show();
    }

    /**
     * 업무서버에 이체목록 등록
     * @param verifyTransferInfo 이체정보
     */
    private void requestConfirmTransferAccount(final VerifyTransferInfo verifyTransferInfo) {
        if (verifyTransferInfo == null)
            return;

        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo == null)
            return;

        showProgressDialog();

        String WTCH_ACNO = accountInfo.getACNO();

        String CNTP_FIN_INST_CD = verifyTransferInfo.getCNTP_FIN_INST_CD();
        String CNTP_BANK_ACNO = verifyTransferInfo.getCNTP_BANK_ACNO();
        String RECV_NM = verifyTransferInfo.getRECV_NM();
        String FEE = verifyTransferInfo.getFEE();
        String TRN_AMT = verifyTransferInfo.getTRN_AMT();
        String TRNF_DVCD  = verifyTransferInfo.getTRNF_DVCD();

        Map param = new HashMap();
        param.put("UUID_NO", "");
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("CNTP_FIN_INST_CD", CNTP_FIN_INST_CD);
        param.put("CNTP_BANK_ACNO", CNTP_BANK_ACNO);
        param.put("CNTP_ACCO_DEPR_NM", RECV_NM);

        String displaySending = mTextDisplaySending.getText().toString();
        String CUST_NM = verifyTransferInfo.getCUST_NM();
        if (TextUtils.isEmpty(displaySending)) {
            if (TextUtils.isEmpty(CUST_NM))
                param.put("DEPO_BNKB_MRK_NM", "");
            else {

                param.put("DEPO_BNKB_MRK_NM", CUST_NM);
            }
        } else {
            param.put("DEPO_BNKB_MRK_NM", displaySending);
        }

        String displayRecipient = mTextDisplayRecipient.getText().toString();
        if (TextUtils.isEmpty(displayRecipient)) {
            if (TextUtils.isEmpty(RECV_NM))
                param.put("TRAN_BNKB_MRK_NM", "");
            else
                param.put("TRAN_BNKB_MRK_NM", RECV_NM);
        } else {
            param.put("TRAN_BNKB_MRK_NM", displayRecipient);
        }

        param.put("FEE", FEE);
        param.put("TRN_AMT", TRN_AMT);
        param.put("TRNF_DVCD", TRNF_DVCD);

        if ("3".equalsIgnoreCase(TRNF_DVCD)) {
            String TRNF_DMND_DT = verifyTransferInfo.getTRNF_DMND_DT();
            String TRNF_DMND_TM =  verifyTransferInfo.getTRNF_DMND_TM();
            param.put("TRNF_DMND_DT", TRNF_DMND_DT);
            param.put("TRNF_DMND_TM", TRNF_DMND_TM);
        } else if ("2".equalsIgnoreCase(TRNF_DVCD)) {
            String SVC_DVCD  = verifyTransferInfo.getSVC_DVCD();
            if ("1".equalsIgnoreCase(SVC_DVCD)) {            // 즉시
                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                    String TRNF_DMND_DT = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                    String TRNF_DMND_TM = String.format("%02d0000", mInputHour);
                    verifyTransferInfo.setTRNF_DMND_DT(TRNF_DMND_DT);
                    verifyTransferInfo.setTRNF_DMND_TM(TRNF_DMND_TM);
                    param.put("TRNF_DMND_DT", TRNF_DMND_DT);
                    param.put("TRNF_DMND_TM", TRNF_DMND_TM);
                }
            } else {
                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                    String TRNF_DMND_DT = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                    String TRNF_DMND_TM = String.format("%02d0000", mInputHour);
                    verifyTransferInfo.setTRNF_DMND_DT(TRNF_DMND_DT);
                    verifyTransferInfo.setTRNF_DMND_TM(TRNF_DMND_TM);
                    param.put("TRNF_DMND_DT", TRNF_DMND_DT);
                    param.put("TRNF_DMND_TM", TRNF_DMND_TM);
                }
            }
        } else if ("5".equalsIgnoreCase(TRNF_DVCD)) {
            if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                String TRNF_DMND_DT = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                String TRNF_DMND_TM = String.format("%02d0000", mInputHour);
                verifyTransferInfo.setTRNF_DMND_DT(TRNF_DMND_DT);
                verifyTransferInfo.setTRNF_DMND_TM(TRNF_DMND_TM);
                param.put("TRNF_DMND_DT", TRNF_DMND_DT);
                param.put("TRNF_DMND_TM", TRNF_DMND_TM);
            }
        }

        String TRN_MEMO_CNTN = mTextInputMemo.getText().toString();
        if (!TextUtils.isEmpty(TRN_MEMO_CNTN))
            param.put("TRN_MEMO_CNTN", TRN_MEMO_CNTN);
        else
            param.put("TRN_MEMO_CNTN", "");

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0010400A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    String msg = getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        String msg = getString(R.string.msg_debug_err_response);
                        showErrorMessage(msg);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        /*
                        showErrorMessage(msg);
                        return;
                        */
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    // 다계좌일 때, 이체 등록된 정보에서 사용
                    String SUM_AMT = object.optString("SUM_AMT");

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                        if (mTransferOneDay >= 0)
                            mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                    }

                    JSONArray jsonArray = object.optJSONArray("REC");
                    if (jsonArray == null) return;

                    int sizeArray = jsonArray.length();
                    if (sizeArray < 1)
                        return;

                    TransferManager.getInstance().clear();
                    for (int index = 0; index < sizeArray; index++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        String UUID_NO = jsonObject.optString("UUID_NO");
                        String WTCH_ACNO = jsonObject.optString("WTCH_ACNO");
                        String CNTP_FIN_INST_CD = jsonObject.optString("CNTP_FIN_INST_CD");
                        if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                            CNTP_FIN_INST_CD = "028";
                        }
                        String CNTP_BANK_ACNO = jsonObject.optString("CNTP_BANK_ACNO");
                        String CNTP_ACCO_DEPR_NM = jsonObject.optString("CNTP_ACCO_DEPR_NM");
                        String DEPO_BNKB_MRK_NM = jsonObject.optString("DEPO_BNKB_MRK_NM");
                        String TRAN_BNKB_MRK_NM = jsonObject.optString("TRAN_BNKB_MRK_NM");
                        String TRN_AMT = jsonObject.optString("TRN_AMT");
                        String FEE = jsonObject.optString("FEE");
                        String TRNF_DMND_DT = "";
                        if (jsonObject.has("TRNF_DMND_DT"))
                            TRNF_DMND_DT = jsonObject.optString("TRNF_DMND_DT");

                        String TRNF_DMND_TM = "";
                        if (jsonObject.has("TRNF_DMND_TM"))
                            TRNF_DMND_TM = jsonObject.optString("TRNF_DMND_TM");

                        String TRN_MEMO_CNTN = "";
                        if (jsonObject.has("TRN_MEMO_CNTN"))
                            TRN_MEMO_CNTN = jsonObject.optString("TRN_MEMO_CNTN");

                        String TRNF_DVCD = jsonObject.optString("TRNF_DVCD");
                        String MNRC_BANK_NM  = jsonObject.optString("MNRC_BANK_NM");
                        String BANK_NM = jsonObject.optString("BANK_NM");

                        VerifyTransferInfo transerInfo = new VerifyTransferInfo();

                        transerInfo.setUUID_NO(UUID_NO);
                        transerInfo.setCNTP_FIN_INST_CD(CNTP_FIN_INST_CD);
                        transerInfo.setCNTP_BANK_ACNO(CNTP_BANK_ACNO);
                        transerInfo.setRECV_NM(CNTP_ACCO_DEPR_NM);
                        transerInfo.setDEPO_BNKB_MRK_NM(DEPO_BNKB_MRK_NM);
                        transerInfo.setTRAN_BNKB_MRK_NM(TRAN_BNKB_MRK_NM);
                        transerInfo.setTRN_AMT(TRN_AMT);
                        transerInfo.setFEE(FEE);
                        if (!TextUtils.isEmpty(TRNF_DMND_DT))
                            transerInfo.setTRNF_DMND_DT(TRNF_DMND_DT);
                        if (!TextUtils.isEmpty(TRNF_DMND_TM))
                            transerInfo.setTRNF_DMND_TM(TRNF_DMND_TM);
                        if (!TextUtils.isEmpty(TRN_MEMO_CNTN))
                            transerInfo.setTRN_MEMO_CNTN(TRN_MEMO_CNTN);
                        transerInfo.setTRNF_DVCD(TRNF_DVCD);
                        transerInfo.setMNRC_BANK_NM(MNRC_BANK_NM);
                        transerInfo.setBANK_NM(BANK_NM);
                        TransferManager.getInstance().add(transerInfo);
                    }
                    showVerifyTransferAccount();
                } catch (JSONException e) {

                }
            }
        });
    }

    /**
     * 수취인조회 결과 표시
     */
    private void showVerifyTransferAccount() {
        int sendTransferNum = TransferManager.getInstance().size();
        if (sendTransferNum == 1) {
            hideSingleTransferDialog();
            showConfirmSingleFransfer();
        } else if (sendTransferNum > 1) {
            showConfirmMultiTransfer();
        }
    }

    /**
     * 수취인조회 단건 결과 표시
     */
    private void showConfirmSingleFransfer() {
        //String withdrawAccount = mTextAccountName.getText().toString() + " " + mTextAccount.getText().toString();
        String withdrawAccount = "";
        String alias = "";
        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo != null) {
            withdrawAccount = accountInfo.getACNO();
            alias = accountInfo.getACCO_ALS();
        }

        if (singleTransferDialog != null && singleTransferDialog.isShowing())
            hideSingleTransferDialog();

        singleTransferDialog = new SlidingConfirmSingleTransferDialog(this, withdrawAccount, alias,
                new SlidingConfirmSingleTransferDialog.FinishListener() {
                    @Override
                    public void OnOKListener() {
                        if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
                            hideSingleTransferDialog();
                        }

                        if (multiTransferDialog != null && multiTransferDialog.isShowing()) {
                            hideMultiTransferDialog();
                        }

                        double dAmount = TransferManager.getInstance().getTotalTransferAmount();
                        if (dAmount >= Const.SHOW_VOICE_PHISHING_AMOUNT) {
                            if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
                                hideSingleTransferDialog();
                            }
                            transferSignData(true);
                        } else {
                            transferSignData(false);
                        }
                    }

                    @Override
                    public void OnAddTransfer() {
                        clearAll();
                        updateSendMultiTransfer(true);

                        mBtnBalanceDropdown.setVisibility(View.GONE);
                        mBtnLockDropdown.setVisibility(View.VISIBLE);
                        mBtnWithdrawAccount.setClickable(false);

                        hideStep03(true);
                        hideStep02(true);
                        showStep01(true);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 50);
                    }
                });
        singleTransferDialog.setCancelable(false);
        singleTransferDialog.show();
    }

    /**
     * 수취인조회 다건 결과 표시
     */
    private void showConfirmMultiTransfer() {
        String withdrawAccount = mTextAccountName.getText().toString() + " " + mTextAccount.getText().toString();
        multiTransferDialog = new SlidingConfirmMultiTransferDialog(this, withdrawAccount, new SlidingConfirmMultiTransferDialog.FinishListener() {
            @Override
            public void OnCancelListener(boolean isLastRemove, Double dLimitOneDay) {
                if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
                    hideSingleTransferDialog();
                }

                if (multiTransferDialog != null && multiTransferDialog.isShowing()) {
                    hideMultiTransferDialog();
                }

                if (!TransferManager.getInstance().getIsNotRemove() && !isLastRemove) {
                    requestRemoveTransfer();
                } else if (TransferManager.getInstance().getIsNotRemove()) {
                    updateSendMultiTransfer(false);
                }

                TransferManager.getInstance().setIsNotRemove(false);

                if (dLimitOneDay >= 0) {
                    mTransferOneDay = dLimitOneDay;
                    mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                }
            }

            @Override
            public void OnOKListener(boolean isLastRemove) {
                if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
                    hideSingleTransferDialog();
                }

                if (multiTransferDialog != null && multiTransferDialog.isShowing()) {
                    hideMultiTransferDialog();
                }

                TransferManager.getInstance().setIsNotRemove(isLastRemove);
                transferSignData(false);
            }

            @Override
            public void OnRemoveAllListener() {
                TransferManager.getInstance().clear();

                hideStep03(true);
                hideStep02(true);
                showStep01(true);

                mViewEditAmountLine.setVisibility(View.GONE);
                showProgressLine();

                mBtnWithdrawAccount.setClickable(true);
                mBtnBalanceDropdown.setVisibility(View.VISIBLE);
                mBtnLockDropdown.setVisibility(View.GONE);
            }

            @Override
            public void OnAddTransfer() {
                clearAll();
                updateSendMultiTransfer(true);

                mBtnBalanceDropdown.setVisibility(View.GONE);
                mBtnLockDropdown.setVisibility(View.VISIBLE);
                mBtnWithdrawAccount.setClickable(false);

                hideMultiTransferDialog();

                hideStep03(true);
                hideStep02(true);
                showStep01(true);

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mEditAmount.requestFocus();
                        Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                    }
                }, 50);
            }

            @Override
            public void OnChangeLimit(double dLimitOneDay) {
                mTransferOneDay = dLimitOneDay;
                mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
            }

            @Override
            public void OnShowVoicePhishing() {
                showVoicePhishing();
            }
        });
        multiTransferDialog.setCancelable(false);
        multiTransferDialog.show();
    }

    /**
     * 모든 입력값 초기화
     */
    private void clearAll() {
        mEditAmount.setText("");
        mTextBankName.setText("");
        mEditAccountNum.setText("");
        mTextDisplayRecipient.setText("");
        mTextDisplaySending.setText("");
        mTextInputMemo.setText("");

        mInputAmount = "";
    }

    /**
     * 수취인조회 다건 결과 업데이트
     */
    private void updateSendMultiTransfer(boolean isShow) {
        int sendNum = TransferManager.getInstance().size();
        if (sendNum <= 0) {
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                mLayoutTotalTransfer.setVisibility(View.INVISIBLE);
            } else if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                mLayoutTotalTransfer.setVisibility(View.GONE);
            }

            return;
        }

        double totalAmount = 0;
        for (int index = 0; index < sendNum; index++) {
            VerifyTransferInfo transerInfo = TransferManager.getInstance().get(index);
            String amount = transerInfo.getTRN_AMT();
            if (!TextUtils.isEmpty(amount)) {
                double dAmount = Double.valueOf(amount);
                totalAmount += dAmount;
            }
        }

        String totalSendNum = String.format(getString(R.string.msg_total_send_num), sendNum);
        mTextSendNum.setText(totalSendNum);
        String totalSendAmount = String.format(getString(R.string.msg_total_send_amount), Utils.moneyFormatToWon(totalAmount));
        mTextSendAmount.setText(totalSendAmount);

        if (isShow)
            mLayoutTotalTransfer.setVisibility(View.VISIBLE);
    }

    /**
     * 1회 이체 금액 초과 메세지 표시
     */
    private void showOverMaxAmountAtOneTime() {
        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };

        final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
        alertDialog.mPListener = okClick;
        alertDialog.msg = getString(R.string.msg_transfer_over_at_once);
        alertDialog.show();
    }

    /**
     * 보이스피싱 체크 알림창 표시
     * @param signData 전자서명된 이체정보
     */
    private void showAdvanceVoicePhishing(final String signData) {
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBtnConfirm.setVisibility(View.GONE);
                mBtnSendTransfer.setVisibility(View.VISIBLE);

                showOTPActivity(signData);
            }
        };

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSingleTransferDialog();
                showVoicePhishing();
            }
        };

        SlidingVoicePhishingDialog dialog = new SlidingVoicePhishingDialog(this, cancelClick, okClick);
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * 보이스피싱 체크 알림창 '예' 선택 후 알림창 표시
     */
    private void showVoicePhishing() {
        final Dialog dialog = new Dialog(this, R.style.AppThemeNoTitle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.dialog_alert_voice_phishing);

        Button btnOK = dialog.findViewById(R.id.btn_ok_alert_voice_phishing);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                clearAll();

                TransferManager.getInstance().clear();
                mListTransferReceiptInfo.clear();
                mLastTransferInfo = null;

                if (mMyAccountAdapter != null) {
                    MyAccountInfo accountInfo = mListMyAccount.get(0);
                    String ACNO = accountInfo.getACNO();
                    requestInit(ACNO, 0, true);
                }
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * mOTP, 타행 OTP, 핀코드 입력창 표시
     * @param signData 전자서명된 이체정보
     */
    private void showOTPActivity(String signData) {
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);

        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (TextUtils.isEmpty(SECU_MEDI_DVCD)) {
            Intent intent = new Intent(this, PincodeAuthActivity.class);
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            //TODO:sign data, service id 추가
            commonUserInfo.setSignData(signData);
            intent.putExtra(Const.INTENT_SERVICE_ID, "");
            intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
            intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
            commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        } else {
            Intent intent;
            if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                intent = new Intent(this, OtherOtpAuthActivity.class);
               /*
                String OTP_STCD = loginUserInfo.getOTP_STCD();
                String OTP_VNDR_CD = loginUserInfo.getOTP_VNDR_CD();
                if (TextUtils.isEmpty(OTP_STCD) || TextUtils.isEmpty(OTP_VNDR_CD)) {
                    return;
                }
                */

                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = 0;
                    for (int index = 0; index < TransferManager.getInstance().size(); index++) {
                        VerifyTransferInfo transerInfo = TransferManager.getInstance().get(index);
                        String amount = transerInfo.getTRN_AMT();
                        if (!TextUtils.isEmpty(amount)) {
                            double dAmount = Double.valueOf(amount);
                            totalAmount += dAmount;
                        }
                    }

                    if (totalAmount <= MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //TODO:sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, "");
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = 0;
                    for (int index = 0; index < TransferManager.getInstance().size(); index++) {
                        VerifyTransferInfo transerInfo = TransferManager.getInstance().get(index);
                        String amount = transerInfo.getTRN_AMT();
                        if (!TextUtils.isEmpty(amount)) {
                            double dAmount = Double.valueOf(amount);
                            totalAmount += dAmount;
                        }
                    }
                    if (totalAmount <= MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //TODO:sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, "");
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }

                intent = new Intent(this, OtpPwAuthActivity.class);
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP);
                intent.putExtra(Const.INTENT_OTP_SERIAL_NUMBER, mMOTPSerialNumber);
                intent.putExtra(Const.INTENT_OTP_TA_VERSION, mMOTPTAVersion);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else {
                intent = new Intent(this, PincodeAuthActivity.class);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                //TODO:sign data, service id 추가
                commonUserInfo.setSignData(signData);
                intent.putExtra(Const.INTENT_SERVICE_ID, "");
                intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
                overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                return;
            }
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQUEST_OTP_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 이체 요청
     * @param elecSrno 전자서명된 이체정보
     */
    private void requestTransfer(String elecSrno) {
        mListTransferReceiptInfo.clear();

        Map param = new HashMap();
        // TODO 센스톤, OTP, mOTP 구분 필요, 전문 미완성
        param.put("ELEC_SGNR_SRNO", elecSrno);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0010500A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    showFailTransfer(0, "");
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showFailTransfer(0, "");
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {

                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        /*
                        if (TransferManager.getInstance().size() > 1) {
                            mLastTransferInfo = null;
                            showErrorMessage(msg);
                        } else
                            showFailTransfer(0, ret);
                        */
                        if (TransferManager.getInstance().size() > 1) {
                            mLastTransferInfo = null;
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        } else {
                            showFailTransfer(0, ret);
                        }
                        return;
                    }

                    String AFTR_BLNC = object.optString("AFTR_BLNC");
                    String WTCH_TRN_POSB_AMT = object.optString("WTCH_TRN_POSB_AMT");
                    String PROD_NM = object.optString("PROD_NM");
                    String ACCO_ALS = object.optString("ACCO_ALS");
                    String bankName = object.optString("BANK_NM");

                    if (TextUtils.isEmpty(bankName))
                        bankName ="SBI저축(사이다뱅크)";

                    JSONArray array = object.optJSONArray("REC_OUT");
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        TransferReceiptInfo item = new TransferReceiptInfo(jsonObject);
                        mListTransferReceiptInfo.add(item);
                    }

                    String mWithdrawName = "";
                    MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
                    if (accountInfo != null) {
                        if (TextUtils.isEmpty(ACCO_ALS)) {
                            ACCO_ALS = PROD_NM;
                        }

                        String ACNO = accountInfo.getACNO();
                        if (!TextUtils.isEmpty(ACNO)) {
                            int lenACNO = ACNO.length();
                            if (lenACNO > 4)
                                ACNO = ACNO.substring(lenACNO - 4, lenACNO);

                            ACNO = " [" + ACNO + "]";
                        }

                        mWithdrawName = ACCO_ALS + ACNO;
                    }

                    if (mListTransferReceiptInfo.size() == 1) {
                        TransferReceiptInfo receiptInfo = mListTransferReceiptInfo.get(0);
                        String RESP_CD = receiptInfo.getRESP_CD();
                        if ("XEEL0140".equalsIgnoreCase(RESP_CD) || "XEEL0141".equalsIgnoreCase(RESP_CD) ||
                            "XEEL0142".equalsIgnoreCase(RESP_CD) || "XEEL0143".equalsIgnoreCase(RESP_CD) ||
                            "XEKM0089".equalsIgnoreCase(RESP_CD) || "XEKM0091".equalsIgnoreCase(RESP_CD) ||
                            "XEKM0096".equalsIgnoreCase(RESP_CD)) {
                            showFailTransfer(0, "");
                            return;
                        }
                    }

                    Intent intent = new Intent(TransferAccountActivity.this, TransferCompleteActivity.class);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO, mListTransferReceiptInfo);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_NAME, LoginUserInfo.getInstance().getCUST_NM());
                    intent.putExtra(Const.INTENT_TRANSFER_BALANCE, AFTR_BLNC);
                    intent.putExtra(Const.INTENT_WITHDRAW_NAME, mWithdrawName);
                    intent.putExtra(Const.INTENT_PROD_NAME, PROD_NM);
                    intent.putExtra(Const.INTENT_ACCO_ALS, ACCO_ALS);
                    intent.putExtra(Const.INTENT_BANK_NM, bankName);
                    intent.putExtra(Const.INTENT_WITHDRAWABLE_AMOUNT, WTCH_TRN_POSB_AMT);
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 계좌 별칭 수정 화면 표시
     * @param position 선택된 리스트 아이템 위치
     */
    void showEditTransferAccount(final int position) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_transfer_account);
        View view = dialog.getWindow().getDecorView();
        view.setBackgroundResource(android.R.color.transparent);

        final EditText editAlias = (EditText) dialog.findViewById(R.id.edittext_edit_transfer_account);

        Button btnCancel = (Button) dialog.findViewById(R.id.btn_negative);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TransferAccountActivity.this.isFinishing())
                    return;

                dialog.dismiss();
            }
        });

        Button btnOK = (Button) dialog.findViewById(R.id.btn_positive);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (TransferAccountActivity.this.isFinishing())
                    return;

                String alias = editAlias.getText().toString();
                if (TextUtils.isEmpty(alias)) {
                    Toast.makeText(TransferAccountActivity.this, getString(R.string.msg_no_alias_input), Toast.LENGTH_SHORT).show();
                    return;
                }

                dialog.dismiss();
                requestRegisterAlias(position, alias);
            }
        });

        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.width = size.x;
        dialog.getWindow().setAttributes(lp);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        //dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
    }

    /**
     * 계좌별칭 등록/변경 요청
     * @param position 선택위치
     */
    private void requestRegisterAlias(final int position, final String alias) {
        TransferAccountInfo accountInfo = (TransferAccountInfo) mListTransferRecently.get(position);
        if (accountInfo == null)
            return;

        String MNRC_BANK_CD = accountInfo.getMNRC_BANK_CD();
        String ACNO = accountInfo.getMNRC_ACNO();

        Map param = new HashMap();
        param.put("CHNG_DVCD","1");
        param.put("FIN_INST_CD", MNRC_BANK_CD);
        param.put("BANK_ACNO", ACNO);
        param.put("ACCO_ALS", alias);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0010200A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {

                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String result = object.optString("TRTM_RSLT_CD");
                    if (!Const.REQUEST_WAS_YES.equalsIgnoreCase(result)) {
                        showErrorMessage(getString(R.string.msg_fail_edit_alias));
                        return;
                    }

                    TransferAccountInfo accountInfo = (TransferAccountInfo) mListTransferRecently.get(position);
                    accountInfo.setMNRC_ACCO_ALS(alias);
                    mListTransferRecently.set(position, accountInfo);
                    mTransferRecentlyAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이체계좌 삭제메세지 화면 표시
     * @param position 선택된 리스트 아이템 위치
     */
    void showRemoveTransferAccount(final int position) {
        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestRemoveTransferAccount(position);
            }
        };

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
        alertDialog.mPListener = okClick;
        alertDialog.mPBtText = getString(R.string.common_confirm);
        alertDialog.mNListener = cancelClick;
        alertDialog.mNBtText = getString(R.string.common_cancel);
        alertDialog.msg = getString(R.string.msg_ask_remove);
        alertDialog.show();
    }

    /**
     * 선택된 이체목록 제외처리 요청
     * @param position 선택된 리스트 아이템 위치
     */
    private void requestRemoveTransferAccount(final int position) {
        Map param = new HashMap();

        TransferAccountInfo accountInfo = mListTransferRecently.get(position);
        String MNRC_BANK_CD = accountInfo.getMNRC_BANK_CD();
        String MNRC_ACNO = accountInfo.getMNRC_ACNO();

        if (TextUtils.isEmpty(MNRC_BANK_CD) || TextUtils.isEmpty(MNRC_ACNO))
            return;

        param.put("REG_STCD", "0");
        param.put("CNTP_FIN_INST_CD", MNRC_BANK_CD);
        param.put("CNTP_BANK_ACNO", MNRC_ACNO);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0050100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0050100A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    mListTransferRecently.remove(position);
                    mTransferRecentlyAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

     /**
      * 지연이체계좌 설명 보이기
      * fade in 처리
      */
    private void showHelpDelay() {
        if (this.isFinishing())
            return;

        mLayoutHelpDelayAccount.setVisibility(View.INVISIBLE);

        Animation animSlideTopBody = AnimationUtils.loadAnimation(this, R.anim.sliding_list_down);
        mLayoutHelpDelayAccount.clearAnimation();
        mLayoutHelpDelayAccount.startAnimation(animSlideTopBody);
        mLayoutHelpDelayAccount.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutHelpDelayAccount.setVisibility(View.VISIBLE);

                if (TransferAccountActivity.this.isFinishing())
                    return;

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mLayoutHelpDelayAccount.isShown()) {
                            hideHelpDelay();
                        }
                    }
                }, 5000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 지연이체계좌 설명 감추기
     */
    private void hideHelpDelay() {
        if (this.isFinishing())
            return;

        mLayoutHelpDelayAccount.setVisibility(View.INVISIBLE);

        Animation animSlideTopBody = AnimationUtils.loadAnimation(this, R.anim.sliding_list_up);
        mLayoutHelpDelayAccount.clearAnimation();
        mLayoutHelpDelayAccount.startAnimation(animSlideTopBody);
        mLayoutHelpDelayAccount.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutHelpDelayAccount.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 단건이체 확인창 닫기
     */
    private void hideSingleTransferDialog() {
        if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
            singleTransferDialog.dismiss();
            singleTransferDialog = null;
        }
    }

    /**
     * 다건이체 확인창 닫기
     */
    private void hideMultiTransferDialog() {
        if (multiTransferDialog != null && multiTransferDialog.isShowing()) {
            multiTransferDialog.dismiss();
            multiTransferDialog = null;
        }
    }

    /**
     * 즐겨찾기계좌관리 요청
     * @param position 최근 이체계좌 리스트에서 선택된 계좌 위치치
     */
    private void requestFavirteAccount(int position) {
        TransferAccountInfo accountInfo = (TransferAccountInfo) mListTransferRecently.get(position);
        if (accountInfo == null)
            return;

        String FAVO_ACCO_YN = accountInfo.getFAVO_ACCO_YN();
        String MNRC_BANK_CD = accountInfo.getMNRC_BANK_CD();
        String MNRC_ACNO = accountInfo.getMNRC_ACNO();
        String MNRC_ACCO_DEPR_NM = accountInfo.getMNRC_ACCO_DEPR_NM();
        String MNRC_ACCO_ALS = accountInfo.getMNRC_ACCO_ALS();

        // TODO 현재 임시 통장이라 예금주가 없음
        if (TextUtils.isEmpty(FAVO_ACCO_YN) || TextUtils.isEmpty(MNRC_BANK_CD) ||
            TextUtils.isEmpty(MNRC_ACNO))// || TextUtils.isEmpty(MNRC_ACCO_DEPR_NM))
            return;

        Map param = new HashMap();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN))
            param.put("REG_STCD", "1");
        else
            param.put("REG_STCD", "0");

        param.put("CNTP_FIN_INST_CD", MNRC_BANK_CD);
        param.put("CNTP_BANK_ACNO", MNRC_ACNO);
        param.put("CNTP_ACCO_DEPR_NM", MNRC_ACCO_DEPR_NM);
        if (!TextUtils.isEmpty(MNRC_ACCO_ALS)) {
            param.put("CNTP_ACCO_ALS", MNRC_ACCO_ALS);
        }

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    requestTransferAccountList(true);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이체실패에 따른 메세지 화면 표시
     * @param failType 오류 타입
     */
    private void showFailTransfer(int failType, String ret) {
        Intent intent = new Intent(TransferAccountActivity.this, TransferFailActivity.class);
        intent.putExtra(Const.INTENT_TRANSFER_ENTRY_TYPE, 0);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_TYPE, failType);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_RET, ret);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    /**
     * 세션동기화
     */
    private void requestSessionSync() {
        Map param = new HashMap();
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    showErrorMessage(getString(R.string.msg_debug_no_response), okClick);
                    //checkVaildAuthMethod();
                    return;
                } else {
                    try {
                        JSONObject object = new JSONObject(ret);
                        if (object == null) {
                            Logs.e(getString(R.string.msg_debug_err_response));
                            Logs.e(ret);
                            View.OnClickListener okClick = new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            };
                            showErrorMessage(getString(R.string.msg_debug_err_response), okClick);
                            //checkVaildAuthMethod();
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            Logs.e(ret);
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    finish();
                                }
                            };
                            showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                            return;
                        }

                        Logs.e("ret : " + ret);
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().syncLoginSession(ret);
                        checkVaildAuthMethod();
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
            }
        });
    }

    /**
     * 인증 수단별 입력 횟수 오류 체크
     */
    private void checkVaildAuthMethod() {
        // 핀코드 오류 횟수 체크
        if (Prefer.getPinErrorCount(this) >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
            dismissProgressDialog();
            View.OnClickListener cancelClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TransferAccountActivity.this, ReregLostPincodeActivity.class);
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.PINCODE_FORGET);
                    String accn = LoginUserInfo.getInstance().getODDP_ACCN();
                    String mbrno = LoginUserInfo.getInstance().getMBR_NO();

                    Logs.e("accn : " + accn);
                    Logs.e("mbrno : " + mbrno);

                    if (TextUtils.isEmpty(mbrno))
                        mbrno = "";
                    commonUserInfo.setMBRnumber(mbrno);
                    if (TextUtils.isEmpty(accn) || Integer.parseInt(accn) < 1)
                        commonUserInfo.setHasAccount(false);
                    else
                        commonUserInfo.setHasAccount(true);

                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivity(intent);
                    finish();
                }
            };

            final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
            alertDialog.mNListener = cancelClick;
            alertDialog.mPListener = okClick;
            alertDialog.msg = String.format(getString(R.string.msg_err_over_match_pin), Const.SSENSTONE_AUTH_COUNT_FAIL);
            alertDialog.mPBtText = getString(R.string.rereg);
            alertDialog.show();
            return;
        }

        final LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (TextUtils.isEmpty(SECU_MEDI_DVCD)) {
            checkMyAccount();
            return;
        }

        if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
            String OTP_STCD = loginUserInfo.getOTP_STCD();
            final String MOTP_EROR_TCNT = loginUserInfo.getMOTP_EROR_TCNT();
            if ("120".equalsIgnoreCase(OTP_STCD)) { // 사고 분실
                dismissProgressDialog();
                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CUS0030101.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };

                View.OnClickListener onLinkClock = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0040101.getServiceUrl();
                        String param = null;
                        if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                            param = "TRTM_DVCD=ACDTLOCK";
                        } else {
                            param = "TRTM_DVCD=ACDT";
                        }
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        intent.putExtra(Const.INTENT_PARAM, param);
                        startActivity(intent);
                        finish();
                    }
                };

                final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
                alertDialog.mLinkListener = onLinkClock;
                alertDialog.msg = getString(R.string.msg_otp_accident);
                alertDialog.mPBtText = getString(R.string.msg_report_otp);
                alertDialog.mLinkText = getString(R.string.msg_motp_issue);
                alertDialog.show();
                return;
            } else if ("140".equalsIgnoreCase(OTP_STCD)) { // 오류 횟수 초과
                dismissProgressDialog();
                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };

                final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
                alertDialog.msg = getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = getString(R.string.msg_init_otp);
                alertDialog.show();
                return;
            } else if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                dismissProgressDialog();

                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };

                final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
                alertDialog.msg = getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = getString(R.string.msg_init_otp);
                alertDialog.show();
                return;
            } else {
                checkMyAccount();
            }
        } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
            final View.OnClickListener cancelClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            final View.OnClickListener okClickDiffDevice = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                    String url = WasServiceUrl.CRT0040401.getServiceUrl();
                    String param = "TRTM_DVCD=" + 1;
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    intent.putExtra(Const.INTENT_PARAM, param);
                    startActivity(intent);
                    finish();
                }
            };

            final View.OnClickListener okClickSameDevice = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                    String url = WasServiceUrl.CRT0040501.getServiceUrl();
                    intent.putExtra("url", url);
                    startActivity(intent);
                    finish();
                }
            };

            String MOTP_END_YN = loginUserInfo.getMOTP_END_YN();
            if (Const.BRIDGE_RESULT_YES.equals(MOTP_END_YN)) {
                dismissProgressDialog();
                final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClickDiffDevice;
                alertDialog.msg = getString(R.string.msg_motp_expired);
                alertDialog.mPBtText = getString(R.string.common_reissue);
                alertDialog.show();
                return;
            }

            String MOTP_EROR_TCNT = loginUserInfo.getMOTP_EROR_TCNT();
            if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                dismissProgressDialog();
                final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClickDiffDevice;
                alertDialog.msg = getString(R.string.msg_motp_lost);
                alertDialog.mPBtText = getString(R.string.common_reissue);
                alertDialog.show();
                return;
            }

            showProgressDialog();
            MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                @Override
                public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                    if (!"0000".equals(resultCode)) {
                        dismissProgressDialog();
                        final AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                        alertDialog.mNListener = cancelClick;
                        String deviceId = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
                        String motpDeviceId = LoginUserInfo.getInstance().getMOTP_SMPH_DEVICE_ID();
                        if (TextUtils.isEmpty(deviceId) || TextUtils.isEmpty(motpDeviceId)) {
                            alertDialog.mPListener = okClickDiffDevice;
                        } else {
                            if (deviceId.equals(motpDeviceId)) {
                                alertDialog.mPListener = okClickSameDevice;
                            } else {
                                alertDialog.mPListener = okClickDiffDevice;
                            }
                        }
                        alertDialog.msg = getString(R.string.msg_motp_change_device);
                        alertDialog.mPBtText = getString(R.string.common_reissue);
                        alertDialog.show();
                    } else {
                        mMOTPSerialNumber = reqData1;
                        mMOTPTAVersion = reqData4;
                        checkMyAccount();
                    }
                    return;
                }
            });
        } else {
            checkMyAccount();
        }
    }

    /**
     * key가 존재하는지 체크
     * @param json
     * @param key
     * @return
     */
    public String getJsonString(JSONObject json, String key){
        String outValue = null;

        if (json.has(key)) {
            try {
                outValue = json.getString(key);
            } catch (JSONException e) {
                //e.printStackTrace();
            }
        }

        return outValue;
    }

    /**
     * 수취인 조회 결과 화면에서 '아니오' 선택시 미지막 수취인 조회 결과 삭제
     */
    public void requestRemoveTransfer() {
        int size = TransferManager.getInstance().size();
        if (size <= 0)
            return;

        VerifyTransferInfo transferInfo = TransferManager.getInstance().get(size - 1);
        if (transferInfo == null)
            return;

        String UUID_NO = transferInfo.getUUID_NO();
        if (TextUtils.isEmpty(UUID_NO)) {
            showErrorMessage(getString(R.string.msg_debug_err_response));
            return;
        }

        Map param = new HashMap();
        param.put("UUID_NO", UUID_NO);
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0010400A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    String msg = getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        String msg = getString(R.string.msg_debug_err_response);
                        showErrorMessage(msg);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                        if (mTransferOneDay >= 0)
                            mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                    }

                    TransferManager.getInstance().clear();
                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;

                        VerifyTransferInfo item = new VerifyTransferInfo();

                        String CNTP_FIN_INST_CD = jsonObject.optString("CNTP_FIN_INST_CD");
                        if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                            CNTP_FIN_INST_CD = "028";
                        }
                        item.setCNTP_FIN_INST_CD(CNTP_FIN_INST_CD);

                        String CNTP_BANK_ACNO = jsonObject.optString("CNTP_BANK_ACNO");
                        item.setCNTP_BANK_ACNO(CNTP_BANK_ACNO);

                        String CNTP_ACCO_DEPR_NM = jsonObject.optString("CNTP_ACCO_DEPR_NM");
                        item.setRECV_NM(CNTP_ACCO_DEPR_NM);

                        String DEPO_BNKB_MRK_NM = jsonObject.optString("DEPO_BNKB_MRK_NM");
                        item.setDEPO_BNKB_MRK_NM(DEPO_BNKB_MRK_NM);

                        String TRAN_BNKB_MRK_NM = jsonObject.optString("TRAN_BNKB_MRK_NM");
                        item.setTRAN_BNKB_MRK_NM(TRAN_BNKB_MRK_NM);

                        String TRN_AMT = jsonObject.optString("TRN_AMT");
                        item.setTRN_AMT(TRN_AMT);

                        String FEE = jsonObject.optString("FEE");
                        item.setFEE(FEE);

                        String TRNF_DMND_DT = getJsonString(jsonObject, "TRNF_DMND_DT");
                        if (!TextUtils.isEmpty(TRNF_DMND_DT)) {
                            item.setTRNF_DMND_DT(TRNF_DMND_DT);
                        }

                        String TRNF_DMND_TM = getJsonString(jsonObject, "TRNF_DMND_TM");
                        if (!TextUtils.isEmpty(TRNF_DMND_TM)) {
                            item.setTRNF_DMND_TM(TRNF_DMND_TM);
                        }

                        String TRN_MEMO_CNTN = getJsonString(jsonObject, "TRN_MEMO_CNTN");
                        if (!TextUtils.isEmpty(TRN_MEMO_CNTN)) {
                            item.setTRN_MEMO_CNTN(TRN_MEMO_CNTN);
                        }

                        String TRNF_DVCD = jsonObject.optString("TRNF_DVCD");
                        item.setTRNF_DVCD(TRNF_DVCD);

                        String MNRC_BANK_NM = jsonObject.optString("MNRC_BANK_NM");
                        item.setMNRC_BANK_NM(MNRC_BANK_NM);

                        String BANK_NM = jsonObject.optString("BANK_NM");
                        item.setBANK_NM(BANK_NM);

                        String UUID_NO = jsonObject.optString("UUID_NO");
                        item.setUUID_NO(UUID_NO);

                        TransferManager.getInstance().add(index, item);
                    }

                    updateSendMultiTransfer(false);
                } catch (JSONException e) {

                }
            }
        });
    }

    /**
     * 이체정보 전자 서명값 요청
     * @param isShowPhishing 보이스 피싱 체크 알림창에서 접근 여부
     */
    private void transferSignData(final boolean isShowPhishing) {
        Map param = new HashMap();
        param.put("TRN_TYCD", "1");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0019900A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    String msg = getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        String msg = getString(R.string.msg_debug_err_response);
                        showErrorMessage(msg);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String ELEC_SGNR_VAL_CNTN = object.optString("ELEC_SGNR_VAL_CNTN");
                    if (!TextUtils.isEmpty(ELEC_SGNR_VAL_CNTN)) {
                        if (isShowPhishing)
                            showAdvanceVoicePhishing(ELEC_SGNR_VAL_CNTN);
                        else {
                            showOTPActivity(ELEC_SGNR_VAL_CNTN);
                        }
                    }
                } catch (JSONException e) {

                }
            }
        });
    }

    @Override
    public void onDateSet(long date) {
        Logs.e("date : " + date);

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);

        mInputYear = calendar.get(Calendar.YEAR);
        mInputMonth = calendar.get(Calendar.MONTH) + 1;
        mInputDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (mIstoday) {
            if (mCurYear != mInputYear || mInputMonth != mInputMonth || mCurDay != mInputDay)
                mIstoday = false;
        }

        SlidingDateTimerPickerDialog timepickerslidingDialog = new SlidingDateTimerPickerDialog(TransferAccountActivity.this, Const.PICKER_TYPE_TIME, mIstoday, mIsDelayService);
        timepickerslidingDialog.setOnConfirmListener(new SlidingDateTimerPickerDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress(int year, int month, int day, int time, boolean needtimeselect, boolean istoday, boolean isDelayService) {
                String inputDate = String.valueOf(mInputYear) + "년 " + String.valueOf(mInputMonth) + "월 " +
                        String.valueOf(mInputDay) + "일 " + String.format("%02d", time) + ":00";
                mTextInputDate.setText(inputDate);
                mInputHour = time;

                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                    mLastTransferInfo.setTRNF_DVCD("3");
                    String TRNF_DMND_DT = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                    String TRNF_DMND_TM = String.format("%02d0000", mInputHour);
                    mLastTransferInfo.setTRNF_DMND_DT(TRNF_DMND_DT);
                    mLastTransferInfo.setTRNF_DMND_TM(TRNF_DMND_TM);
                }

                mBtnDropdownSendDate.setVisibility(View.GONE);
                mBtnEraseSendDate.setVisibility(View.VISIBLE);
            }
        });
        timepickerslidingDialog.show();
    }

    private void showDatePicker(final boolean isDelayService) {
        Map param = new HashMap();
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0011500A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                }

                try {
                    Calendar calendar = Calendar.getInstance();
                    Calendar minDate = Calendar.getInstance();
                    if (!TextUtils.isEmpty(ret)) {
                        JSONObject object = new JSONObject(ret);
                        if (object == null) {
                            Logs.e(getResources().getString(R.string.msg_debug_err_response));
                            Logs.e(ret);
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        if (objectHead == null) {
                            return;
                        }

                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getResources().getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                        }

                        String result = object.optString("SYS_DTTM");
                        Logs.i("result : " + result);

                        if (!TextUtils.isEmpty(result)) {
                            calendar.set(Integer.parseInt(result.substring(0, 4)), Integer.parseInt(result.substring(4, 6)) - 1, Integer.parseInt(result.substring(6, 8)));
                        }
                    }

                    // 22시 30분이 넘으면 당일 예약 이체가 불가하므로 다음날부터 이체되도록 함
                    if (!isDelayService) {
                        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 && calendar.get(Calendar.MINUTE) > 29) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    } else {
                        if (calendar.get(Calendar.HOUR_OF_DAY) + 3 >= 23) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    }

                    mCurYear = calendar.get(Calendar.YEAR);
                    mCurMon = calendar.get(Calendar.MONTH);
                    mCurDay = calendar.get(Calendar.DAY_OF_MONTH);

                    minDate.set(mCurYear, mCurMon, mCurDay);
                    long tmp = calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 90 * 1000.0);
                    DatePickerFragmentDialog.newInstance(DateTimeBuilder.get().withMinDate(minDate.getTimeInMillis()).withMaxDate(tmp).withTheme(R.style.datepickerCustom)).show(getSupportFragmentManager(), "DatePickerFragmentDialog");
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}