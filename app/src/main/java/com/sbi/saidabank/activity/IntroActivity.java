package com.sbi.saidabank.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.ahnlab.enginesdk.rc.RootCheckInfo;
import com.ahnlab.enginesdk.up.UpdateResult;
import com.ahnlab.v3mobileplus.interfaces.V3MobilePlusCtl;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.netfunnel.api.ContinueData;
import com.netfunnel.api.Netfunnel;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.common.ReportLostGuideActivity;
import com.sbi.saidabank.activity.login.BlockIPActivity;
import com.sbi.saidabank.activity.login.SplashActivity;
import com.sbi.saidabank.activity.login.TutorialActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.NetfunnelDialog;
import com.sbi.saidabank.common.dialog.PermissionDialog;
import com.sbi.saidabank.common.dialog.SelectDevDialog;
import com.sbi.saidabank.common.dialog.SlidingDevModeCheckDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.push.FLKPushAgentSender;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.solution.v3.RootCheckerManager;
import com.sbi.saidabank.solution.v3.V3Manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*
 *
 * Intro순서
 * 1.쿠키정리
 * 2.퍼미션
 * 3.네트웍 상태 체크
 * 4.루팅/후킹/엔진엡데이트 체크
 * 5.앱버전체크/메뉴버전체크
 * 5-1.메뉴 다운로드
 * 6.백신실행
 * 7.Tutorial실행
 */
public class IntroActivity extends BaseActivity implements RootCheckerManager.RootCheckerEventListener, V3Manager.V3CompleteListener {

    private static final int REQUEST_DEBUGINGMODE = 1000;
    private static final int DEVICE_STATE_NORMAL = 0;
    private static final int DEVICE_STATE_NEWUSER = 1;
    private static final int DEVICE_STATE_CHANGE = 2;
    //private String mPushJson;
    //private String mPushCategory;
    private boolean mNeedV3Install;
    private int mDeviceState = DEVICE_STATE_NORMAL;

    private String[] mPermissionList = new String[]{
            Manifest.permission.READ_PHONE_STATE,
    };

    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_intro);
        Intent intent = getIntent();

        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }

        //mPushJson = intent.getStringExtra(Const.INTENT_PUSH_DATA);
        //mPushCategory = intent.getStringExtra(Const.INTENT_PUSH_CATEGORY);


        //ImageView imageview = (ImageView)findViewById(R.id.img_progress);
        //((AnimationDrawable)imageview.getBackground()).start();

        //세션및 쿠기 Clear
        HttpUtils.removeCookie();

        // 로그인 정보 Clear
        LoginUserInfo.clearInstance();

        //sbi://saidabank?{"header":{"api":"125"},"body":{"url":"/mm/MM010000.do"}}
        /*
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();

            try {
                String urlStr = URLDecoder.decode(uri.toString(), "utf-8");
                String param = HttpUtils.getMobileWebParam(urlStr);
                if(!TextUtils.isEmpty(param)){
                    Logs.e("ACTION_VIEW : " + param);
                    AcuonApplication.getInstance().setMobileWebParam(param);
                }
            } catch (UnsupportedEncodingException e) {
                Logs.printException(e);
            }
        }
        */

        if (!Prefer.getPermissionGuideShowStatus(IntroActivity.this)) {
            showPermissionGuide();
        } else {
            startIntroGif();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * V3설치 화면으로 이동했다가 설치하지 않고 취소해서 돌아왔을 경우
         * Intro 종료한다. 무조건 V3는 설치해야 한다.
         *
         */
        if (mNeedV3Install) {
            if (!V3Manager.getInstance(getApplicationContext()).isRunV3MobilePlus()) {
                finish();
                return;
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // 백키 처리 안할 것인지 ?
        // 루팅 동작중 처리가 필요 ?
        // 루팅 동작 종료 unloadAll 처리
        // V3 설치중 처리가 필요 ?
        // V3 동작중 처리가 필요 ?
        /*V3Interface interf = V3Interface.getInstance(getApplicationContext());
        if (interf.isRunV3MobilePlus()) {
            interf.stopV3MobilePlus();
        }*/

        //super.onBackPressed();
    }

    public void startPlayIntro() {
        //++ 로고 gif 재생
        RequestOptions reqOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true);
        Glide.with(this).load(R.drawable.img_intro).listener(requestListener).apply(reqOptions).into((ImageView) findViewById(R.id.iv_logo)).clearOnDetach();
        //--
    }

    /**
     * 퍼미션 요청
     */
    public void showPermissionGuide() {

        PermissionDialog dialog = new PermissionDialog(this);
        dialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefer.setPermissionGuideShowStatus(IntroActivity.this, true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    PermissionUtils.checkPermission(IntroActivity.this, mPermissionList, Const.REQUEST_PERMISSION_READ_PHONE_STATE);
                } else {
                    // 마쉬멜로우 미만 버전은 권한요청 하지 않고 다음 프로세스 진행
                    startIntroGif();
                }
            }
        };
        dialog.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        };
        dialog.show();

    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logs.i("onRequestPermissionsResult");
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_READ_PHONE_STATE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startIntroGif();
                } else {
                    final boolean showFlag = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    DialogUtil.alert(this, "권한설정", "종료", getString(R.string.msg_permission_phone_state_allow),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (showFlag)
                                        PermissionUtils.checkPermission(IntroActivity.this, mPermissionList, Const.REQUEST_PERMISSION_READ_PHONE_STATE);
                                    else {
                                        PermissionUtils.goAppSettingsActivity(IntroActivity.this);
                                        finish();
                                    }
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Utils.finishAffinity(IntroActivity.this);
                                }
                            });
                }
            }
            break;

            default:
                break;
        }

    }

    private void checkNetStatus() {
        showProgress();
        Logs.e("checkNetStatus");
        HttpUtils.NetState state = HttpUtils.checkNetworkState(this);
        //인터넷 상태 체크
        Logs.e("network result : " + state);
        if (state == HttpUtils.NetState.NET_STATE_OFFLINE || state == HttpUtils.NetState.NET_STATE_NOT_SUPPORT) {
            dismissProgress();
            DialogUtil.alert(this, "종료", "재시도", getResources().getString(R.string.msg_error_network_status), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 네트워크 연결 재시도
                    checkNetStatus();
                }
            });
            return;
        } else {
            /**
             * USIM 확인
             */
            if (!BuildConfig.DEBUG && !Utils.isExistSimCard(this)) {
                DialogUtil.alert(this, "종료", getResources().getString(R.string.msg_error_no_usim), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                return;
            }

            /**
             * 루팅/후킹체크
             */
            //checkRootChecker();
            if (BuildConfig.DEBUG) {
                checkTestVersion();
            } else {
                initCheckRootChecker();
            }
        }
    }

    private void checkTestVersion() {
        Logs.e("checkTestVersion - 0");
        if (BuildConfig.DEBUG && SaidaUrl.serverIndex == Const.DEBUGING_SERVER_TEST) {
            Map param = new HashMap();

            String url = "https://testm.saidabank.co.kr/app/aos/aos_version.txt";
            HttpUtils.sendHttpTask(url, param, new HttpSenderTask.HttpRequestListener() {
                @Override
                public void endHttpRequest(String ret) {
                    Logs.e("checkTestVersion - endHttpRequest: " + ret);
                    if (TextUtils.isEmpty(ret)) {
                        initCheckRootChecker();
                        return;
                    }

                    int serverVersion = Integer.parseInt(ret);
                    int appVersion = Utils.getVersionCode(mContext);

                    Logs.e("checkTestVersion : " + serverVersion + " - " + appVersion);

                    if (serverVersion > appVersion) {
                        dismissProgress();
                        String msg = "현재버전 : " + appVersion + "\n신규버전 : " + serverVersion + "\n\n신규버전이 생성되었습니다.\n다운로드 하시겠습니까?";
                        DialogUtil.alert(mContext, "예", "아니요", msg, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //여기에서 암묵적 인텐트 날린다.
                                Intent intent =
                                        new Intent(Intent.ACTION_VIEW, Uri.parse("https://testm.saidabank.co.kr/test0000100.act"));

                                startActivity(intent);
                                finish();
                            }
                        }, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 네트워크 연결 재시도
                                showProgress();
                                initCheckRootChecker();
                            }
                        });
                    } else {
                        initCheckRootChecker();
                    }
                }
            });
        } else {
            initCheckRootChecker();
        }
    }

    private void initCheckRootChecker() {
        //V3 RootChecker 제외. 적용 요청 있을 때 추가예정
        /*
        if (BuildConfig.DEBUG) {
            final RootCheckerManager rootCheckerManager = RootCheckerManager.getInstance(getApplicationContext());
            rootCheckerManager.setRootCheckEventListener(this);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final boolean bRet = rootCheckerManager.initAhnLabMobileRootChecker();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkRootChecker(rootCheckerManager, bRet);
                        }
                    });
                }
            }).start();
        } else*/ {
            Netfunnel.BEGIN("service_1","intro_app"
                , NetfunnelDialog.Show(this)
                , new Netfunnel.Listener() {
                    @Override
                    public void netfunnelMessage(Netfunnel netfunnel, Netfunnel.EvnetCode code) {
                        try {
                            Logs.d("netfunnel", "netfunnelMessage -> code: " + code + " / "+netfunnel.getResponse().getCode() + " ttl=" + netfunnel.getResponse().getTTL());
                            if (code.isContinue()) {
                                ContinueData continue_data = netfunnel.getContinueData();
                                if (code == Netfunnel.EvnetCode.ContinueInterval) {
                                } else {  // Netfunnel.EvnetCode.Continue
                                    String continue_msg = "    예상대기시간:" + continue_data.getCurrentWaitTimeSecond() + "초\n"
                                                                  + "    진   행   율:" + continue_data.getCurrentWaitPercent() + "%\n"
                                                                  + "    대기자수(앞):" + continue_data.getCurrentWaitCount() + "명\n"
                                                                  + "    대기자수(뒤):" + continue_data.getCurrentNextCount() + "명\n"
                                                                  + " 초당처리량(TPS):" + netfunnel.getResponse().getTPS() + "\n"
                                                                  + "   확인주기(TTL):" + netfunnel.getResponse().getTTL() + "초\n"
                                                                  + "   notice acount:" + continue_data.getAcountNotice() + "\n"
                                                                  + "   update acount:" + continue_data.getUpdateAcount() + "\n"
                                                                  + "UI     표시대기시간:" + netfunnel.getProperty().getUiWaitTimeLimit() + "초\n"
                                                                  + "UI 표시대기자수(앞):" + netfunnel.getProperty().getUiWaitCountLimit() + "명\n"
                                                                  + "UI 표시대기자수(뒤):" + netfunnel.getProperty().getUiNextCountLimit() + "명";

                                    Logs.d("netfunnel", "-------------------------\n" + continue_msg);
                                }
                                return;
                            }

                            NetfunnelDialog.Close();
                            if (code.isSuccess()) {
                                sendCheckDeviceInfo();
                                if(code == Netfunnel.EvnetCode.Success) {
                                        // 주의) 여기에만 AliveNotice()을 사용할수 있는 곳
                                }else if(code == Netfunnel.EvnetCode.NotUsed){
                                }else if(code == Netfunnel.EvnetCode.Bypass){
                                }else if(code == Netfunnel.EvnetCode.ErrorBypass){
                                }else if(code == Netfunnel.EvnetCode.ExpressNumber){
                                }
                            } else if (code.isBlocking()) {
                                sendCheckDeviceInfo();
                                if(code == Netfunnel.EvnetCode.Block) {
                                        // 서비스 차단
                                }else if(code == Netfunnel.EvnetCode.IpBlock){
                                        // 사용자 사용패턴에 의한 차단
                                }
                            } else if (code.isStop()) {
                                // TODO: 자체처리영역
                                dismissProgressDialog();
                                Netfunnel.END();
                                finish();
                            } else { // }else if(code.isError()){
                                // TODO: 자체처리영역
                                sendCheckDeviceInfo();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            sendCheckDeviceInfo();
                        }
                    }
                });
        }
    }

    private void sendCheckDeviceInfo() {
        final String[] item1 = new String[2];
        new Thread(new Runnable() {
            @Override
            public void run() {
                item1[0] = FDSManager.getInstance().getEncIMEI(IntroActivity.this);
                item1[1] = FDSManager.getInstance().getEncUUID(IntroActivity.this);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        checkDeviceInfo(item1[0], item1[1]);
                    }
                });
            }
        }).start();
    }

    private void checkRootChecker(RootCheckerManager rootCheckerManager, boolean initRet) {
        if (initRet == false) {
            // RootChecker 초기화 실패
            dismissProgress();
            Logs.i("check root 1");
            DialogUtil.alert(this, R.string.msg_error_check_rooting, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            return;
        }

        // 루팅 솔루션 업데이트 후 루팅 검사할지
        // 루팅 검사 후  루팅 솔루션 업데이트
        Logs.i("IntroActivity - checkRootChecker");
        boolean cRet = rootCheckerManager.checkAhnLabMobileRootChecker();
        if (cRet == false) {
            dismissProgress();
            // RootChecker 초기화 실패
            Logs.i("check root 2");
            DialogUtil.alert(this, R.string.msg_error_check_rooting, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            return;
        }

        rootCheckerManager.updateAhnLabMobileRootChecker();
    }


    @Override
    public void onCheckRootingResult(String code, RootCheckInfo rootCheckInfo) {
        if (code.equals(RootCheckerManager.RC_CHECK_COMPLETE)) {
            StonePassManager stonePassManager = StonePassManager.getInstance(getApplication());
            StonePassManager.StonePassInitListener stonePassInitListener = new StonePassManager.StonePassInitListener() {
                @Override
                public void stonePassInitResult() {
                    sendCheckDeviceInfo();
                }
            };
            stonePassManager.StonePassInit(stonePassInitListener);
        } else {
            dismissProgress();
            DialogUtil.alert(this, R.string.msg_error_check_rooting, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    @Override
    public void onUpdateResult(String code, UpdateResult updateResult) {
        RootCheckerManager.getInstance(getApplicationContext()).stopEngineSDK();
    }

    /**
     * 기기정보 확인
     */
    private void checkDeviceInfo(String item1, String item2) {
        //showProgressDialog();
        Map param = new HashMap();

        param.put("DEVICE_OS", "android");
        param.put("DEVICE_INFO1", item1);
        param.put("DEVICE_INFO2", item2);

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Netfunnel.END();
                // return data가 없을 경우 종료 처리
                if (TextUtils.isEmpty(ret)) {
                    dismissProgress();
                    String msg = getResources().getString(R.string.msg_debug_no_response);
                    Logs.e(msg);
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    showErrorMessage(msg, okClick);
                    return;
                } else {
                    Logs.e("ret : " + ret);
                    try {
                        final JSONObject object = new JSONObject(ret);

                        if (object == null) {
                            dismissProgress();
                            String msg = getString(R.string.msg_debug_err_response);
                            View.OnClickListener okClick = new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            };
                            showErrorMessage(msg, okClick);
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            dismissProgress();
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    finish();
                                }
                            };
                            showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                            return;
                        }

                        StonePassManager stonePassManager = StonePassManager.getInstance(getApplication());
                        StonePassManager.StonePassInitListener stonePassInitListener = new StonePassManager.StonePassInitListener() {
                            @Override
                            public void stonePassInitResult() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        dismissProgress();
                                        //회원번호
                                        String strValue = object.optString("MBR_NO", "");
                                        LoginUserInfo.getInstance().setMBR_NO(strValue);

                                        //고객번호
                                        strValue = object.optString("CUST_NO", "");
                                        LoginUserInfo.getInstance().setCUST_NO(strValue);

                                        //Device ID
                                        strValue = object.optString("SMPH_DEVICE_ID", "");
                                        LoginUserInfo.getInstance().setSMPH_DEVICE_ID(strValue);

                                        // App 버전 체크
                                        strValue = object.optString("APSF_VRSN", "");
                                        if (TextUtils.isEmpty(strValue)) {
                                            // 앱버전 체크 실패 시 앱 실행
                                            /*
                                            View.OnClickListener okClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                }
                                            };
                                            showErrorMessage("기기정보 확인(앱버전체크) 오류입니다.", okClick);
                                            return;
                                            */
                                        } else {
                                            String appVersionName = Utils.getVersionName(IntroActivity.this);
                                            appVersionName = appVersionName.replaceAll("[^0-9]", "");
                                            String serverVersionName = strValue.replaceAll("[^0-9]", "");
                                            int serverVersion = Integer.parseInt(serverVersionName);
                                            int appVersion = Integer.parseInt(appVersionName);
                                            //App Store : 서버버전이 앱버전보다 클 때만 업데이트 수행
                                            if (!BuildConfig.DEBUG && serverVersion > appVersion) {
                                                Logs.e("goto App Store");
                                                DialogUtil.alert(IntroActivity.this, "업데이트", getResources().getString(R.string.msg_app_update), "업데이트", new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                                intent.setData(Uri.parse(
                                                                        "https://play.google.com/store/apps/details?id=" + getPackageName()));
                                                                intent.setPackage("com.android.vending");
                                                                startActivity(intent);
                                                                finish();
                                                            }
                                                        });
                                                return;
                                            }
                                        }

                                        // 해외 IP 차단 여부
                                        strValue = object.optString("AB_IP_BLCK_YN", "");
                                        if (TextUtils.isEmpty(strValue)) {
                                            View.OnClickListener okClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                }
                                            };
                                            showErrorMessage("기기정보 확인(해외IP차단여부) 오류입니다.", okClick);
                                            return;
                                        }
                                        String strIPBlockValue = object.optString("AB_IP_YN", "");
                                        if (TextUtils.isEmpty(strIPBlockValue)) {
                                            View.OnClickListener okClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                }
                                            };
                                            showErrorMessage("기기정보 확인(해외IP차단) 오류입니다.", okClick);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(strValue) && Const.BRIDGE_RESULT_YES.equalsIgnoreCase(strIPBlockValue)) {
                                            Intent intent = new Intent(IntroActivity.this, BlockIPActivity.class);
                                            startActivity(intent);
                                            finish();
                                            return;
                                        }

                                        // 분실신고 기기여부
                                        strValue = object.optString("EQMT_LOS_DCL_YN", "");
                                        if (TextUtils.isEmpty(strValue)) {
                                            View.OnClickListener okClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                }
                                            };
                                            showErrorMessage("기기정보 확인(분실기기확인) 오류입니다.", okClick);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(strValue)) {
                                            Intent intent = new Intent(IntroActivity.this, ReportLostGuideActivity.class);
                                            startActivity(intent);
                                            finish();
                                            return;
                                        }

                                        // 신규가입 여부
                                        strValue = object.optString("EQMT_NEW_YN", "");
                                        if (TextUtils.isEmpty(strValue)) {
                                            View.OnClickListener okClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                }
                                            };
                                            showErrorMessage("기기정보 확인(신규가입여부) 오류입니다.", okClick);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(strValue)) {
                                            //회원가입 진행
                                            mDeviceState = DEVICE_STATE_NEWUSER;
                                            checkV3MobilePlus();
                                            return;
                                        }

                                        // 타기기 등록 여부
                                        strValue = object.optString("EQMT_CHNG_YN", "");
                                        if (TextUtils.isEmpty(strValue)) {
                                            View.OnClickListener okClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                }
                                            };
                                            showErrorMessage("기기정보 확인(기기변경확인) 오류입니다.", okClick);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(strValue)) {
                                            mDeviceState = DEVICE_STATE_CHANGE;
                                            // 기기변경이라도 앱 삭제 후 재설치된 상태면 기기변견 알림 팝업 띄우지 않음
                                            if (!Prefer.getPincodeRegStatus(IntroActivity.this)) {
                                                checkV3MobilePlus();
                                                return;
                                            }
                                            DialogUtil.alert(IntroActivity.this, "기기변경", "앱종료", getResources().getString(R.string.msg_error_change_device), new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            //기기변경 진행
                                                            checkV3MobilePlus();
                                                        }
                                                    },
                                                    new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            finish();
                                                        }
                                                    });
                                            return;
                                        }

                                        // 긴급공지 팝업
                                        try {
                                            JSONArray jsonArray = object.getJSONArray("REC_POPUP");
                                            if (jsonArray.length() > 0) {
                                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                                String popupMsg = jsonObject.optString("PUP_CNTN");
                                                View.OnClickListener okClick = new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        finish();
                                                    }
                                                };
                                                showErrorMessage(popupMsg, okClick);
                                                return;
                                            }
                                        } catch (JSONException e) {
                                            //e.printStackTrace();
                                        }
                                        checkV3MobilePlus();
                                    }
                                });
                            }
                        };
                        stonePassManager.StonePassInit(stonePassInitListener);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
            }
        });
    }

    /**
     * 관리자페이지에 등록된 앱의 버전을 체크한다.
     */
    /*
    private void checkAppVersion() {
        if (BuildConfig.DEBUG) {
            if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV) {
                checkDeveloperMode();
            } else {
                checkV3MobilePlus();
            }
        } else {
            checkV3MobilePlus();
        }
    }
    */

    /**
     * V3 실행
     */
    private void checkV3MobilePlus() {
        if (BuildConfig.DEBUG) {
            if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV) {
                checkDeveloperMode();
                return;
            }
        }
        V3Manager v3Manager = V3Manager.getInstance(getApplicationContext());
        v3Manager.setV3CompleteListener(this);

        if (!v3Manager.isRunV3MobilePlus()) {
            int ret = v3Manager.startV3MobilePlus(LicenseKey.V3_LICENSE_KEY);
            Logs.i("checkV3MobilePlus : " + ret);
            if (ret == V3MobilePlusCtl.ERR_FAILURE) {
                DialogUtil.alert(this, R.string.msg_error_run_v3, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            } else if (ret == V3MobilePlusCtl.ERR_NOT_INSTALLED) {

                DialogUtil.alert(this, getString(R.string.common_market), getString(R.string.common_close), getString(R.string.msg_not_install_v3),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mNeedV3Install = true;
                                V3Manager.getInstance(getApplicationContext()).goProductMarket();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
            } else if (ret == V3MobilePlusCtl.ERR_NOT_CONNECTABLE) {
                DialogUtil.alert(this, R.string.msg_not_setting_v3, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            } else if (ret == V3MobilePlusCtl.ERR_SUCCESS) {
                Logs.e("V3 start success -------");
            } else {
                Logs.e("V3 start failed -------");
                DialogUtil.alert(this, R.string.msg_error_run_v3, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        } else {
            Logs.e("V3 is already running");
            checkDeveloperMode();
        }
    }

    @Override
    public void onCompleteV3() {
        //임시 코드
        checkDeveloperMode();
        //goNextActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_DEBUGINGMODE: {
                goNextActivity();
                break;
            }

            default:
                break;
        }
    }

    /**
     * 개발자 모드 디버깅 모드 활성화 체크
     * 활성화 true면 팝업 띄우고 해제할 것인지 선택하게 함
     * 활성화 false면 다음 intro 순서 진행
     */
    private void checkDeveloperMode() {
        if (Utils.isUsbDebuggingEnable(this) && !Prefer.getFlagDontShowDevModeCheckDialog(this)) {
            SlidingDevModeCheckDialog dialog = new SlidingDevModeCheckDialog(this);
            dialog.mPListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                    startActivityForResult(intent, REQUEST_DEBUGINGMODE);
                }
            };
            dialog.mNListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goNextActivity();
                }
            };
            dialog.show();
        } else {
            goNextActivity();
        }
    }

    public void goNextActivity() {
        //푸시 Agent시작
        Logs.e("FLKPushAgentSender.sendToStart :" + Prefer.getPushRegID(this));
        FLKPushAgentSender.sendToStart(this,false);
        /*
        if (BuildConfig.DEBUG) {
            FLKPushAgentSender.sendToStart(this, SaidaUrl.ReqUrl.URL_FLKPUSH.getReqUrl());
        } else {
            FLKPushAgentSender.sendToStart(this, SaidaUrl.ReqUrl.URL_FLKPUSH.getReqUrl());
            //FLKPushAgentSender.sendToStart(this,false);
        }
        */
        Intent intent;

        Drawable drawable = ((ImageView) findViewById(R.id.iv_logo)).getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).stop();
        }

        if (!Prefer.getTutorialStatus(this)) {
            Prefer.setTutorialStatus(this, true);
            intent = new Intent(IntroActivity.this, TutorialActivity.class);
            intent.putExtra("device_state", mDeviceState);
            startActivity(intent);
            finish();
            return;
        }

        if (mDeviceState != DEVICE_STATE_NORMAL) {
            Prefer.setPatternRegStatus(this, false);
            Prefer.setPincodeRegStatus(this, false);
            Prefer.setFidoRegStatus(this, false);
            Prefer.setFlagFingerPrintChange(this, false);
            intent = new Intent(IntroActivity.this, SplashActivity.class);
            EntryPoint curEntryPoint = (mDeviceState == DEVICE_STATE_NEWUSER) ? EntryPoint.SERVICE_JOIN : EntryPoint.DEVICE_CHANGE;
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.DEVICE_CHANGE, curEntryPoint);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        } else {
            intent = new Intent(IntroActivity.this, PatternAuthActivity.class);
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            if (Prefer.getFidoRegStatus(this)) {
                commonUserInfo.setEntryStart(EntryPoint.LOGIN_BIO);
                commonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
            } else {
                commonUserInfo.setEntryStart(EntryPoint.LOGIN_PATTERN);
                commonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
            }
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
            if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL))
                intent.putExtra(Const.INTENT_MAINWEB_URL, getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        }

        startActivity(intent);
        finish();
    }

    /**
     * 로고 gif requestListener
     */
    RequestListener<Drawable> requestListener = new RequestListener<Drawable>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
            return false;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
            final GifDrawable gifDrawable = (GifDrawable) resource;

            // 로고 gif 1회만 재생
            gifDrawable.setLoopCount(1);

            Logs.i("frame count : " + gifDrawable.getFrameCount());
            Logs.i("frame index : " + gifDrawable.getFrameIndex());

            // 로고 gif 재생 종료 후 APP 시작 프로세스 진행
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        if (gifDrawable.getFrameIndex() == (gifDrawable.getFrameCount() - 1)) {
                            if (PermissionUtils.checkPermission(IntroActivity.this, mPermissionList, Const.REQUEST_PERMISSION_READ_PHONE_STATE)) {
                                IntroActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        checkNetStatus();
                                    }
                                });
                            }
                            break;
                        }
                    }
                }
            }).start();

            return false;
        }
    };

    private void startIntroGif() {
        if (BuildConfig.DEBUG) {
            SelectDevDialog selectDevDialog = new SelectDevDialog(this);
            selectDevDialog.setOnCofirmListener(new SelectDevDialog.OnConfirmListener() {
                @Override
                public void onConfirmPress(int selectIndex) {
                    if (selectIndex == Const.DEBUGING_SERVER_NONE) {
                        finish();
                        return;
                    }
                    SaidaUrl.serverIndex = selectIndex;
                    Logs.e("index : " + selectIndex);
                    startPlayIntro();
                }
            });
            selectDevDialog.show();
        } else {
            startPlayIntro();
        }
    }

    private void showProgress() {
        IntroActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showProgressDialog();
            }
        });
    }

    private void dismissProgress() {
        IntroActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
            }
        });
    }

    /*
    // Test Code
    private void downloadAPK() {
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.SMP0030101A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if (TextUtils.isEmpty(ret)) {
                    Logs.e("서버응답이 없습니다.");
                    checkAppVersion();
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        checkAppVersion();
                        return;
                    }

                    Logs.e("ret : " + ret);

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        checkAppVersion();
                        return;
                    }

                    String serverVersion = object.optString("TRMN_APSF_VRSN");
                    String appVersion = Utils.getVersionName(IntroActivity.this);
                    Logs.e("serverVersion : " + serverVersion);
                    Logs.e("appVersion : " + appVersion);
                    if (!TextUtils.isEmpty(serverVersion) && !appVersion.equals(serverVersion)) {
                        DialogUtil.alert(IntroActivity.this,
                                "업데이트",
                                "취소",
                                "새로운 버전이 업데이트되었습니다. 다운로드하시겠습니까?\n(" + appVersion + " --> " + serverVersion +")",

                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(IntroActivity.this, NewApkDownloadActivity.class);
                                        startActivity(intent);
                                    }
                                },

                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkAppVersion();
                                    }
                                });
                    } else {
                        checkAppVersion();
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                    checkAppVersion();
                }
            }
        });
    }
    */
}
