package com.sbi.saidabank.activity.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.ContactsInfo;

import java.util.ArrayList;

/**
 * Saidabanking_android
 * <p>
 * Class: TransferPhoneActivity
 * Created by 950485 on 2019. 02. 21..
 * <p>
 * Description: 휴대폰번호로 이체 완료 화면
 */

public class TransferFailActivity extends BaseActivity {
    private int mEntryType = 0;
    private int mFailType = 0;
    private String mFailRet = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_fail);

        getExtra();
        initUX();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        Intent intent = getIntent();
        mEntryType = intent.getIntExtra(Const.INTENT_TRANSFER_ENTRY_TYPE, 0);
        mFailType = intent.getIntExtra(Const.INTENT_TRANSFER_FAIL_TYPE, 0);
        mFailRet = intent.getStringExtra(Const.INTENT_TRANSFER_FAIL_RET);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        TextView textMsgFail = (TextView) findViewById(R.id.textview_msg_fail);
        RelativeLayout layoutFailNomal = (RelativeLayout) findViewById(R.id.layout_fail_nomal);
        LinearLayout layoutFailBalance = (LinearLayout) findViewById(R.id.layout_fail_low_balance);
        if (mFailType != 0) {
            layoutFailNomal.setVisibility(View.GONE);
            layoutFailBalance.setVisibility(View.VISIBLE);
        }

        if (mFailType == 0) {
            /*if (!TextUtils.isEmpty(mFailRet)) {
                textMsgFail.setText(mFailRet);
            }*/
        } else if (mEntryType == 1)
            textMsgFail.setText(R.string.msg_transfer_fail_phone_desc);

        Button btnConfirm = (Button) findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEntryType == 0) {
                    Intent intent = new Intent(TransferFailActivity.this, TransferAccountActivity.class);
                    startActivity(intent);
                }

                finish();
            }
        });
    }
}
