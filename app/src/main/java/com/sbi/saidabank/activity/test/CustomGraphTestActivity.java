package com.sbi.saidabank.activity.test;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.customview.CircleProgressView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * siadabank_android
 * Class: CustomGraphTestActivity
 * Created by 950546
 * Date: 2018-12-12
 * Time: 오후 5:22
 * Description: CustomGraphTestActivity
 */
public class CustomGraphTestActivity extends BaseActivity implements View.OnClickListener {

    CircleProgressView mCircleView;
    EditText mEditTextSetSpeed;
    EditText mEditTextSetWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customgraph_test);
        mCircleView = findViewById(R.id.mycircleview);
        mEditTextSetSpeed = findViewById(R.id.et_speed);
        mEditTextSetWidth = findViewById(R.id.et_width);

        findViewById(R.id.btn_start).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start: {
                mCircleView.setPercent(80);
                mCircleView.setStartAngle(270.0F);
                mCircleView.setSpeed(Integer.parseInt(mEditTextSetSpeed.getText().toString()));
                mCircleView.setStorkeWidth(Integer.parseInt(mEditTextSetWidth.getText().toString()));
                mCircleView.startDraw();
                break;
            }

            default:
                break;
        }
    }
}
