package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


public class SchemeEntryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
        Logs.e("mApplicationClass.getActivitySize() : " + mApplicationClass.getActivitySize());

        if (mApplicationClass.getActivitySize() < 2) {
            Uri uri = getIntent().getData();
            String urlStr = "";
            Logs.e("data : " + uri);

            // Sheme 호출일 때 url parsing
            if (uri != null) {
                try {
                    urlStr = URLDecoder.decode(uri.toString(), "utf-8");
                    urlStr = HttpUtils.getMobileWebParam(urlStr);
                    StringBuilder param = new StringBuilder();
                    try {
                        JSONObject jsonObject = new JSONObject(urlStr);
                        if (jsonObject.has("url")) {
                            urlStr = jsonObject.optString("url");
                            for ( int i = 0 ; i < jsonObject.length() ; i++) {
                                if (i != 0)
                                    param.append("&");
                                try {
                                    param.append(jsonObject.names().getString(i) + "=" + URLEncoder.encode(jsonObject.getString(jsonObject.names().getString(i)), "EUC-KR"));
                                } catch (UnsupportedEncodingException e) {
                                    //e.printStackTrace();
                                }
                            }
                            urlStr = urlStr + "?" + param.toString();
                            Logs.e("urlStr : " + urlStr);
                        } else {
                            urlStr = "";
                        }
                    } catch (JSONException e) {
                        urlStr = "";
                        //e.printStackTrace();
                    }
                } catch (UnsupportedEncodingException e) {
                    urlStr = "";
                    //e.printStackTrace();
                }
            }

            Intent intent = new Intent(this, IntroActivity.class);
            if (!TextUtils.isEmpty(urlStr)) {
                urlStr = SaidaUrl.getBaseWebUrl() + "/" + urlStr;
                intent.putExtra(Const.INTENT_MAINWEB_URL, urlStr);
            } else {
                // push url parsing
                if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL))
                    urlStr = getIntent().getStringExtra(Const.INTENT_MAINWEB_URL);
                if (!TextUtils.isEmpty(urlStr))
                    intent.putExtra(Const.INTENT_MAINWEB_URL, urlStr);
            }
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
