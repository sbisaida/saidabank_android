package com.sbi.saidabank.push;


import java.io.Serializable;

public class FLKPushMessage implements Serializable {
	public String mLegacyIp;
	public String mMessage;
	public String mData;
	public String mSubTime;
	public String mPushId;
	
	
	public boolean mIsRead;

	public int mLegacyPort;
	
	public long mRowID;
	public String mCode;

	public int mReplyType;
}
