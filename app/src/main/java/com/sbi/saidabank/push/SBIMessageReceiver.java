package com.sbi.saidabank.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.SchemeEntryActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.LoginUserInfo;

import java.io.UnsupportedEncodingException;

public class SBIMessageReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Logs.e("SBIMessageReceiver - intent.getAction() : " + intent.getAction());

        if (intent.getAction().equals(context.getPackageName() + Const.ACTION_PUSH_MESSAGE)) {
            if (intent.hasExtra(Const.INTENT_PUSH_REGID) && intent.hasExtra(Const.INTENT_PUSH_DATA)) {
                String RegID = intent.getStringExtra(Const.INTENT_PUSH_REGID);
                FLKPushMessage flkPushMessage = (FLKPushMessage) intent.getSerializableExtra(Const.INTENT_PUSH_DATA);
                try {
                    FLKPushAgentSender.sendToReadMsgPushAgent(context, context.getPackageName(), FLKPushAgentSender.mHostName, RegID, flkPushMessage);
                } catch (UnsupportedEncodingException e) {
                    //e.printStackTrace();
                }
            }
            if (LoginUserInfo.getInstance().isLogin()) {
                // 로그인 상태면 푸쉬 노티 클릭 시 알림함 페이지로 이동
                /*
                Prefer.setFlagShowFirstNotiMsgBox(context.getApplicationContext(), false);
                FLKPushAgentReceiver.updateBadge(context.getApplicationContext(), 0);
                Intent webMainIntent = new Intent(context, WebMainActivity.class);
                String fullUrl = WasServiceUrl.MAI0060100.getServiceUrl();
                webMainIntent.putExtra(Const.INTENT_MAINWEB_URL, fullUrl);
                context.startActivity(webMainIntent);
                */
                // 로그인 상태면 현재 앱 상태 그대로 띄움
                Intent webMainIntent = new Intent(context, SchemeEntryActivity.class);
                webMainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(webMainIntent);
            } else {
                // 로그인 상태가 아니고 앱이 실행 중이 아니면 시작페이지로 이동 후 로그인하면 알림함으로 이동
                SaidaApplication mApplicationClass = (SaidaApplication) context.getApplicationContext();
                if (mApplicationClass != null) {
                    Logs.e("mApplicationClass.getActivitySize() : " + mApplicationClass.getActivitySize());
                    if (mApplicationClass.getActivitySize() > 0) {
                        Intent loginIntent = new Intent(context, SchemeEntryActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(loginIntent);
                    } else {
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().setLogin(false);
                        Intent loginIntent = new Intent(context, IntroActivity.class);
                        loginIntent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0060100.getServiceUrl());
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(loginIntent);
                    }
                } else {
                    Logs.e("application is null");
                    LoginUserInfo.clearInstance();
                    LoginUserInfo.getInstance().setLogin(false);
                    Intent loginIntent = new Intent(context, IntroActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(loginIntent);
                }
            }
        } else if (intent.getAction().equals(context.getPackageName() + Const.ACTION_REFRESH_FCM_TOKEN)) {

        }
    }
}
