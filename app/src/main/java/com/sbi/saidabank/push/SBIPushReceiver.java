package com.sbi.saidabank.push;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.main.MainActivity;
import com.sbi.saidabank.define.Const;

import java.util.List;

public class SBIPushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(context.getPackageName() + Const.ACTION_PUSH_RECEIVE)) {
            ActivityManager actMng = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> list = actMng.getRunningAppProcesses();
            String packageName = "";
            for (ActivityManager.RunningAppProcessInfo rap : list) {
                packageName = rap.processName;
                if (packageName.equals(context.getPackageName())) {
                    //현재 앱이 실행중이다.
                    SaidaApplication mApplicationClass = (SaidaApplication) context.getApplicationContext();

                    if (mApplicationClass.getActivitySize() > 0) {
                        final Activity activity = mApplicationClass.getOpenActivity(".MainActivity");
                        if (activity != null) {
                            ((MainActivity) activity).runOnUiThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                    ((MainActivity) activity).showPushNotiFragment();
                                }
                            });
                        }
                    }
                    return;
                }
            }
        }
    }
}
