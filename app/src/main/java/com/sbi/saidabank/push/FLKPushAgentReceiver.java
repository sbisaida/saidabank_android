package com.sbi.saidabank.push;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Random;

/**
 * @파일이름   : FLKPushAgentReceiver.java
 * @파일설명 : 3rd Party App의 AgentReceiver 클레스
 */
public class FLKPushAgentReceiver extends BroadcastReceiver {
	public static int badgeCount = 0;
	
	@Override
	public void onReceive(final Context context, Intent intent) {
		Logs.e("FLKPushAgentReceiver - intent.getAction() : " + intent.getAction());
		//Registration ID를 받음.
		if(intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_APP_REG_ID")){
			Uri revUri = Uri.parse(intent.getDataString());
			final String regID = revUri.getQueryParameter("regid");
			
			////////////////////////////////////////////////////////
			///3rd 개발자 자체 수정코드
			String oldRegID = Prefer.getPushRegID(context);
			Prefer.setPushRegID(context,regID);

			if(oldRegID.length() != 0 && !oldRegID.equals(regID)) {
				///기존에 RegID가 있음에도 AID복구에 실패해서 RID를 재발급 받았을 경우 처리.
				Logs.e("AID복구 실패로 인해 RID재발급!");
			}

			if(BuildConfig.DEBUG){
				Logs.e("regidReceived " + regID);
				//Logs.shwoToast(context,"regidReceived " + regID);
			}

			Logs.e("RECEIVED_APP_REG_ID - regID : " + regID);

			//ID를 새로 받으면 서버에 전송해준다.
			Intent pushintent = new Intent(context.getPackageName() + Const.ACTION_REFRESH_FCM_TOKEN);
			context.sendBroadcast(pushintent);
			////////////////////////////////////////////////////////
		}
		///Push Message를 받음.
		else if(intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_APP_MSG_INFO")) {
			try {
				FLKPushMessage pushMessage = new FLKPushMessage();
				pushMessage.mSubTime = intent.getStringExtra("subtime");
				pushMessage.mReplyType = intent.getIntExtra("replytype", 0);
				pushMessage.mPushId = intent.getStringExtra("pushid");
				pushMessage.mLegacyPort = intent.getIntExtra("legacyport", 0);
				
				final byte[] revLegacyIP = intent.getByteArrayExtra("legacyip");
				final byte[] revMessage = intent.getByteArrayExtra("msg");
				final byte[] revData = intent.getByteArrayExtra("data");
				
				pushMessage.mLegacyIp = new String( revLegacyIP , "UTF-8");
				pushMessage.mMessage = new String( revMessage , "UTF-8");
				pushMessage.mData = new String( revData , "UTF-8");

				Logs.e("push msg : " + pushMessage.mMessage);
				Logs.e("push data : " + pushMessage.mData);

				String notiMsg = "";
				String channelId = "";
				try {
					JSONObject object = new JSONObject(pushMessage.mData);
					if (object == null) {
						notiMsg = pushMessage.mMessage;
						channelId = object.optString("channelId");
					} else {
						if (object.has("dataContent"))
							notiMsg = object.optString("dataContent");
						if (object.has("channelId"))
							channelId = object.optString("channelId");
					}
				} catch (JSONException e) {
					//e.printStackTrace();
				}

				if (TextUtils.isEmpty(channelId))
					channelId = "SBK";

				//노티를 눌렀을 경우 이동할 Intent를 만든다.
				Intent pendingIntent = new Intent(context, SBIMessageReceiver.class);
				pendingIntent.setAction(context.getPackageName() + Const.ACTION_PUSH_MESSAGE);
				pendingIntent.putExtra(Const.INTENT_PUSH_DATA, (Serializable) pushMessage);
				pendingIntent.putExtra(Const.INTENT_PUSH_REGID, Prefer.getPushRegID(context));

				//여기서 Noti를 출력해준다.
				sendNotification(context,pendingIntent,context.getString(R.string.app_name) + " 알림", notiMsg, channelId);

				badgeCount++;
				updateBadge(context, badgeCount);
				Prefer.setFlagShowFirstNotiMsgBox(context.getApplicationContext(), true);
			} catch (UnsupportedEncodingException e) {
				//e.printStackTrace();
			}			
		}
		///Push Message 읽음확인 Ack를 받음.
		else if(intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_APP_READ_MSG_ACK")) {
			Uri revUri = Uri.parse(intent.getDataString());
			final String pushID = revUri.getQueryParameter("pushid");

			////////////////////////////////////////////////////////
			///3rd message 읽음 확인 완료 처리
			/*
			if(FushIncomingActivity.self != null) {
				FushIncomingActivity.self.readItem(pushID);
			}
			*/
			////////////////////////////////////////////////////////
		}
		///Agent로 부터 Registration ID를 요청받을 준비가 되었음을 받음.
		else if(intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".REQUEST_READY_FOR_AGENT")){
			///일단 무조건 RID를 재발급 받는다.
			try {
				FLKPushAgentSender.sendToRegistration(context, context.getPackageName(), FLKPushAgentSender.mHostName, FLKPushAgentSender.mAppName);
			} catch (UnsupportedEncodingException e) {
				Logs.printException(e);
			}
		}		
		///Registration ID등록 시 parameter 에러.
		else if(intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_REG_PARAM_ERROR")){
			Uri revUri = Uri.parse(intent.getDataString());
			String errorCode = revUri.getQueryParameter("errorcode");

			Logs.e("receivedRegParamError " + errorCode);
			//Toast.makeText(context, "receivedRegParamError " + errorCode, Toast.LENGTH_SHORT).show();
		}
		///Registration ID발급 실패 에러.
		else if(intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_REG_RESULT_ERROR")){
			Uri revUri = Uri.parse(intent.getDataString());
			String errorCode = revUri.getQueryParameter("errorcode");
			Logs.e("receivedRegResultError " + errorCode);
			//Toast.makeText(context, "receivedRegResultError " + errorCode, Toast.LENGTH_SHORT).show();
		}
	}

	/** Notification을 보낸다.
	 * @param context
	 * @param intent : Noti 메세지 탭 시, 실행 할 intent
	 * @param title : 타이틀
	 * @param msg : 메세지
	 */
	public void sendNotification(Context context, Intent intent, String title, String msg, String channelId){
		// 오레오 이상 버전 noti 구분처리
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
			notificationChannel.setDescription(channelId);
			notificationChannel.enableLights(true);
			notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.enableVibration(false);
			//notificationChannel.enableVibration(true);
			//notificationChannel.setVibrationPattern(new long[]{100, 200, 100, 200});
			notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
			nm.createNotificationChannel(notificationChannel);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
					//.setContentTitle(title)
					.setContentText(msg)
					//아이콘 컬러
					.setSmallIcon(R.drawable.flk_service_icon)
					.setColor(0xff009beb)
					.setColorized(true)
					.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
					.setContentIntent(pendingIntent)
					.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
					.setAutoCancel(true);

			Random random = new Random();
			int mLastId = random.nextInt(1000000) + 1;
			nm.notify(mLastId, mBuilder.build());
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			Notification.Builder mBuilder = new Notification.Builder(context);
			//아이콘 컬러
			mBuilder.setSmallIcon(R.drawable.flk_service_icon);
			mBuilder.setColor(0xff009beb);
			//mBuilder.setTicker("Notification.Builder");
			mBuilder.setWhen(System.currentTimeMillis());
			//mBuilder.setNumber(10);
			//mBuilder.setContentTitle(title);
			mBuilder.setContentText(msg);
			mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
			mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
			mBuilder.setContentIntent(pendingIntent);
			mBuilder.setStyle(new Notification.BigTextStyle().bigText(msg));
			mBuilder.setAutoCancel(true);

			mBuilder.setPriority(Notification.PRIORITY_MAX);

			Random random = new Random();
			int mLastId = random.nextInt(1000000) + 1;

			// Notification 발생
			nm.notify(mLastId, mBuilder.build());
		} else {
			NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			Notification.Builder mBuilder = new Notification.Builder(context);
			//아이콘 컬러
			mBuilder.setSmallIcon(R.drawable.flk_service_icon);
			mBuilder.setColor(0xff009beb);
			//mBuilder.setTicker("Notification.Builder");
			mBuilder.setWhen(System.currentTimeMillis());
			//mBuilder.setNumber(10);
			mBuilder.setContentTitle(title);
			mBuilder.setContentText(msg);
			mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
			mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
			mBuilder.setContentIntent(pendingIntent);
			mBuilder.setStyle(new Notification.BigTextStyle().bigText(msg));
			mBuilder.setAutoCancel(true);

			mBuilder.setPriority(Notification.PRIORITY_MAX);

			Random random = new Random();
			int mLastId = random.nextInt(1000000) + 1;

			// Notification 발생
			nm.notify(mLastId, mBuilder.build());
		}
	}

	public static void updateBadge(Context context, int count) {
		///이 기능을 사용하려면 badge_count_package_name, badge_count_class_name 값 수정필요.
		Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
		intent.putExtra("badge_count", count);		
		intent.putExtra("badge_count_package_name", context.getPackageName());
		//intent.putExtra("badge_count_class_name", context.getPackageName() + ".gui.FushTabActivity");
		intent.putExtra("badge_count_class_name", getLauncherClassName(context));
		context.sendBroadcast(intent);

		Intent intentReecive = new Intent(context.getPackageName() + Const.ACTION_PUSH_RECEIVE);
		context.sendBroadcast(intentReecive);
	}

	public static String getLauncherClassName(Context context) {
		PackageManager pm = context.getPackageManager();

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);

		List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
		for (ResolveInfo resolveInfo : resolveInfos) {
			String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
			if (pkgName.equalsIgnoreCase(context.getPackageName())) {
				String className = resolveInfo.activityInfo.name;
				return className;
			}
		}
		return null;
	}
}

