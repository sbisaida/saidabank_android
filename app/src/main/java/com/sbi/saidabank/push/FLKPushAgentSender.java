package com.sbi.saidabank.push;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import com.feelingk.pushagent.core.constants.ModelConstant;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.SaidaUrl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @파일이름   : FLKPushAgentSender.java
 * @파일설명 : 3rd Party App의 Sender 클레스
 */
public class FLKPushAgentSender {
    public static final String KEY_APP_ID = "SBK0000001";
	public static final String PHONE_NUMBER_ID = "phone_number_id";
    //public static final String DEFAULT_APP_ID = "0000000001";
	//public static final String DEFAULT_APP_ID = "SBK0000001";

    ///sendBroadcast시 필요한 3rd앱의 식별자 manifest의 receiver 설정과 같아야한다.
	public static final String mHostName = "sbi_pushagent_uri";
	///3rd앱을 식별하기 위한 AppID, 10자 고정으로 hex값으로 설정한다.(0~9, a~f)
	public static final String mAppName = "SBK0000001";

	private static final String encoding = "UTF-8";
	public static final String PushAgentPName = "com.feelingk.pushagent.sbi";
	private static final String PushAgentHName = "sbi_push://sbi_pushagent_uri";

	public FLKPushAgentSender() {
	}

	/**
	 * @Method 설명 : Agent Service를 시작한다.
	 * @param context Context
	 * @param isWakeup WakeUp 여부 (PushServer로부터 GCM 메시지 수신시에 true값 세팅하여 사용)
	 */
	public static void sendToStart(Context context, boolean isWakeup) {
		Logs.e("sentToStart");
		Intent startIntent = new Intent(PushAgentPName + ".SERVICE_START");
		startIntent.setPackage(context.getPackageName());
		startIntent.setFlags((int)32);
		
		startIntent.putExtra("wakeUp", isWakeup);
		startIntent.putExtra("userInfo", "");
		startIntent.putExtra("useUserInfo", true);

		startIntent.putExtra("serverInfo", SaidaUrl.ReqUrl.URL_FLKPUSH.getReqUrl());
		startIntent.putExtra("ra_port", "9000");
		startIntent.putExtra("rm_port", "8000");
		startIntent.putExtra("rm_port_ssl", "8001");
		startIntent.putExtra("ri_port", "9001");
		startIntent.putExtra("rmr_port", "9600");

		context.sendBroadcast(startIntent);
	}

	public static void sendToStart(Context context, boolean useUserInfo, String userInfo) {
		Intent startIntent = new Intent(PushAgentPName + ".SERVICE_START");
		startIntent.setPackage(context.getPackageName());
		startIntent.setFlags((int)32);

		startIntent.putExtra("userInfo", userInfo);
		startIntent.putExtra("useUserInfo", useUserInfo);
		//SharedPreference.putSharedPreference(context, ModelConstant.KEY_INFO_USER_YN, useUserInfo);
		//SharedPreference.putSharedPreference(context, ModelConstant.KEY_INFO_USER, userInfo);

		context.sendBroadcast(startIntent);
	}


	/**
	 * TEST용 API로 실제 운영시 사용금지.
	 * @Method 설명 : Agent Service를 시작한다.
	 * @param context Context
	 */
	public static void sendToStart(Context context, String ip) {
		Intent startIntent = new Intent(PushAgentPName + ".SERVICE_START");
		startIntent.setPackage(context.getPackageName());
		startIntent.setFlags((int)32);
		
		startIntent.putExtra("serverInfo", ip);
		startIntent.putExtra("wakeUp", false);
		//startIntent.putExtra("isDeleteAgentID", true);
		
		context.sendBroadcast(startIntent);
	}
	
	/**
	 * @Method 설명 : Registration ID를 발급받는다.
	 * @param context Context
	 * @param pName 3rd Party App의 패키지 이름
	 * @param hName 메세지를 전달 받을 데이터 url 호스트 이름(manifest의 설정이름과 맞아야함)
	 * @param appName 3rd Party App의 이름 (무조건 10자리 고정)
	 * @throws UnsupportedEncodingException
	 */
	public static void sendToRegistration(Context context, String pName, String hName, String appName) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder(PushAgentHName);
		sb.append("?pname=").append(URLEncoder.encode(pName, encoding));
		sb.append("&hname=").append(URLEncoder.encode(hName, encoding));
		sb.append("&appname=").append(URLEncoder.encode(appName, encoding));
		
		Intent intent = new Intent(PushAgentPName + ".APP_REGISTRATION", Uri.parse(sb.toString()));
		intent.setPackage(context.getPackageName());
		intent.setFlags((int)32);
		context.sendBroadcast(intent);
	}
	

	/**
	 * @Method 설명 : 읽음확인 결과 전송을 위한 정보를 Agent로 전달
	 * @param context Context
	 * @param pName 3rd Party App의 패키지 이름
	 * @param hName 메세지를 전달 받을 데이터 url 호스트 이름(manifest의 설정이름과 맞아야함)
	 * @param regID Agent로 부터 발급받은 Registration ID
	 * @param pushMessage RECEIVED_APP_MSG_INFO 때 받은 FLKPushMessage 클레스데이터
	 * @throws UnsupportedEncodingException :
	 */
	public static void sendToReadMsgPushAgent(Context context, String pName, String hName, String regID, FLKPushMessage pushMessage) throws UnsupportedEncodingException {
		if(pushMessage.mIsRead == true) return;
		if(pushMessage.mReplyType == 2 || pushMessage.mReplyType == 3) {
			String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
			
			StringBuilder sb = new StringBuilder(PushAgentHName);
			sb.append("?pname=").append(URLEncoder.encode(pName, encoding));
			sb.append("&hname=").append(URLEncoder.encode(hName, encoding));
			sb.append("&pushid=").append(URLEncoder.encode(pushMessage.mPushId, encoding));
			sb.append("&deviceid=").append(URLEncoder.encode(regID, encoding));
			sb.append("&readtime=").append(URLEncoder.encode(date, encoding));
			sb.append("&legacyip=").append(URLEncoder.encode(pushMessage.mLegacyIp, encoding));
			sb.append("&legacyport=").append(URLEncoder.encode(String.valueOf(pushMessage.mLegacyPort), encoding));
			
			Intent intent = new Intent(PushAgentPName + ".RESPONSE_READ_MSG", Uri.parse(sb.toString()));
			intent.setPackage(context.getPackageName());
			intent.setFlags((int)32);
			context.sendBroadcast(intent);
		}
	}
	
	
	public static void sendToAgentPause(Context context, boolean flag) throws UnsupportedEncodingException {
        if (flag) {
            Intent intent = new Intent(PushAgentPName + ".AGENT_PAUSE");
            intent.setPackage(context.getPackageName());
            intent.setFlags((int)32);
            context.sendBroadcast(intent);
        }
        else {
            Intent intent = new Intent(PushAgentPName + ".AGENT_RESUME");
            intent.setPackage(context.getPackageName());
            intent.setFlags((int)32);
            context.sendBroadcast(intent);
        }
	}
	
	public static void sendToRegistration(Context context) {
		//String regID = SharedPreference.getSharedPreference(context, "REGID");
		String regID = Prefer.getPushRegID(context);

		if(TextUtils.isEmpty(regID)){
			try {
	        	///hName은 manifest의 host설정과 같아야한다.
	        	///appName은 무조건 10자리 영문이나 숫자로 입력해야 한다.
				/*
                String appname = SharedPreference.getSharedPreference(context.getApplicationContext(), FLKPushAgentSender.KEY_APP_ID);
                if (TextUtils.isEmpty(appname))
                    appname = FLKPushAgentSender.mAppName;
                    */
				FLKPushAgentSender.sendToRegistration(context, context.getPackageName(), FLKPushAgentSender.mHostName, mAppName);
			} catch (UnsupportedEncodingException e) {
				//e.printStackTrace();
			}
		}
	}
}
