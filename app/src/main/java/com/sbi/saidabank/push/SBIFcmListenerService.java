package com.sbi.saidabank.push;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;

import java.util.Map;

public class SBIFcmListenerService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String refreshedToken) {
        Logs.e("token : " + refreshedToken);
        super.onNewToken(refreshedToken);

        // 이 token을 서버에 전달 한다.
        if (!TextUtils.isEmpty(refreshedToken)) {
            Logs.e("AcuonFcmInstanceIDListenerService - FCM : " + refreshedToken);
            String saveToken = Prefer.getPushGCMToken(getApplicationContext());

            if (!refreshedToken.equals(saveToken)) {
                Prefer.setPushGCMToken(getApplicationContext(), refreshedToken);

                String flkPushId = Prefer.getPushRegID(getApplicationContext());
                if (!TextUtils.isEmpty(flkPushId)) {
                    Intent intent = new Intent(getApplicationContext().getPackageName() + Const.ACTION_REFRESH_FCM_TOKEN);
                    getApplicationContext().sendBroadcast(intent);
                }
            }
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Logs.e("message : " + message);
        super.onMessageReceived(message);

        String from = message.getFrom();
        Map<String, String> data = message.getData();
        /*
        String messageaaa = data.get("message");
        String title = data.get("title");
        String msg = data.get("content");

        // 전달 받은 정보로 뭔가를 하면 된다.
        Log.e("SBIFcmListenerService","from : " + from);
        Log.e("SBIFcmListenerService","messageaaa : " + messageaaa);
        Log.e("SBIFcmListenerService","title : " + title);
        Log.e("SBIFcmListenerService","msg : " + msg);
        */
        Logs.e("SBIFcmListenerService - message.getData().size() : " + message.getData().size());
        if (message.getData().size() > 0) {
            Log.e("SBIFcmListenerService", "Message data payload: " + message.getData());
            //푸시 Agent시작
            FLKPushAgentSender.sendToStart(this, true);
        }
    }
}
