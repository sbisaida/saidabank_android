package com.sbi.saidabank.define.datatype;

/**
 * Saidabank_android
 * Class: LicenseKey
 * Created by 950469 on 2018. 11. 20.
 * <p>
 * Description:
 * 로그인한 고객의 세션정보
 */
public class SessionInfo {

    long D1_TRNF_LMIT_AMT;      //1일 이체한도 금액
    long TI1_TRNF_LMIT_AMT;     //1회 이체한도 금액

}
