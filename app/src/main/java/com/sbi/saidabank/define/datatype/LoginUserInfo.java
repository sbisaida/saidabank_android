package com.sbi.saidabank.define.datatype;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * siadabank_android
 * Class: LoginUserInfo
 * Created by 950546
 * Date: 2019-01-02
 * Time: 오후 1:44
 * Description: LoginUserInfo
 */
public class LoginUserInfo {
    private static LoginUserInfo instance = null;

    private boolean isLogin;
    private boolean isFinishedLogin;
    private boolean isCompletedReg;
    private String MBR_NO;                              // 회원번호
    private String CUST_NO;                             // 고객번호
    private String CI_NO;                               // CI번호
    private String DI_NO;                               // DI번호
    private String CUST_NM;                             // 고객명
    private String BRDD;                                // 생년월일
    private String SEX_CD;                              // 성별코드
    private String CLPH_LCNO;                           // 휴대전화지역번호
    private String CLPH_TONO;                           // 휴대전화국번호
    private String CLPH_SRNO;                           // 휴대전화일련번호
    private String EMAD;                                // 이메일주소
    private String LAST_LOIN_DTTM;                      // 최종로그인일시
    private String AB_IP_BLCK_YN;                       // 해외IP차단여부
    private String PN_INPT_EROR_TCNT;                   // 핀입력오류횟수
    private String IDNF_CNFR_PRGS_STCD;                   // 신분증확인진행상태코드
    private String IDNF_RE_CNFR_DTTM;                   // 신분증재확인일시
    private String NRID;                                // 주민등록번호
    private String ODDP_ACCN;                           // 요구불수신계좌수
    private String SECU_MEDI_DVCD;                      // 보안매체구분코드
    private String OTP_VNDR_CD;                         // OTP벤더코드
    private String OTP_STCD;                            // OTP상태코드
    private String DLY_TRNF_SVC_ENTR_YN;                // 지연이체서비스가입여부
    private String DSGT_MNRC_ACCO_SVC_ENTR_YN;          // 지정입금계좌비스가입여부
    private String SMRT_WTCH_REG_YN;                    // 스마트출금등록여부
    private String SMPL_TRNF_REG_YN;                    // 간편이체등록여부
    private String LMIT_LMT_ACCO_HOLD_YN;               // 한도제한계좌보유여부
    private String CDD_EDD_CUST_CNFR_RESS_DT;           // CDD_EDD고객확인재평가일자
    private String CDD_EDD_CUST_CNFR_RESS_ELPS_DCNT;    // CDD_EDD고객확인재평가경과일수
    private String NRID_SEX_CD;                         // 주민등록번호 성별코드
    private String SMPH_OPSY_DVCD;                      // 스마트폰OS구분코드
    private String SMPH_DEVICE_ID;                      // 스마트폰디바이스ID
    private String MOTP_SMPH_DEVICE_ID;                 // 모바일OTP스마트폰디바이스ID
    private String SMPH_UUID_NO;                        // 스마트폰UUID번호
    private String SECU_MEDI_USE_BZWR_DVCD;             // 보안매체사용업무구분코드
    private String MOTP_END_YN;                         // 모바일OTP만료여부
    private String MOTP_EROR_TCNT;                      // 모바일OTP오류횟수
    private String SYS_DTTM;                            // 시스템일시
    private ArrayList<MyAccountInfo> myAccountInfo = new ArrayList<MyAccountInfo>(); // 계좌목록
    private String MOTPSerialNumber;

    public static LoginUserInfo getInstance() {
        if (instance == null) {
            synchronized (LoginUserInfo.class) {
                if (instance == null) {
                    instance = new LoginUserInfo();
                }
            }
        }
        return instance;
    }

    private LoginUserInfo() {
        isLogin = false;
    }

    public static void clearInstance() {
        if (instance == null)
            return;

        instance = null;
    }

    /**
     * 로그인 여부 체크
     *
     * @return true면 로그인 상태
     */
    public static boolean IsLogin() {
        if (instance == null)
            return false;

        return instance.isLogin() ? true : false;
    }

    public boolean isFinishedLogin() {
        return isFinishedLogin;
    }

    public void setFinishedLogin(boolean finishedLogin) {
        isFinishedLogin = finishedLogin;
    }

    public static void setInstance(LoginUserInfo instance) {
        LoginUserInfo.instance = instance;
    }

    public String getMBR_NO() {
        return MBR_NO;
    }

    public void setMBR_NO(String MBR_NO) {
        this.MBR_NO = MBR_NO;
    }

    public String getCUST_NO() {
        return CUST_NO;
    }

    public void setCUST_NO(String CUST_NO) {
        this.CUST_NO = CUST_NO;
    }

    public String getCI_NO() {
        return CI_NO;
    }

    public void setCI_NO(String CI_NO) {
        this.CI_NO = CI_NO;
    }

    public String getDI_NO() {
        return DI_NO;
    }

    public void setDI_NO(String DI_NO) {
        this.DI_NO = DI_NO;
    }

    public String getCUST_NM() {
        return CUST_NM;
    }

    public void setCUST_NM(String CUST_NM) {
        this.CUST_NM = CUST_NM;
    }

    public String getBRDD() {
        return BRDD;
    }

    public void setBRDD(String BRDD) {
        this.BRDD = BRDD;
    }

    public String getSEX_CD() {
        return SEX_CD;
    }

    public void setSEX_CD(String SEX_CD) {
        this.SEX_CD = SEX_CD;
    }

    public String getCLPH_LCNO() {
        return CLPH_LCNO;
    }

    public void setCLPH_LCNO(String CLPH_LCNO) {
        this.CLPH_LCNO = CLPH_LCNO;
    }

    public String getCLPH_TONO() {
        return CLPH_TONO;
    }

    public void setCLPH_TONO(String CLPH_TONO) {
        this.CLPH_TONO = CLPH_TONO;
    }

    public String getCLPH_SRNO() {
        return CLPH_SRNO;
    }

    public void setCLPH_SRNO(String CLPH_SRNO) {
        this.CLPH_SRNO = CLPH_SRNO;
    }

    public String getEMAD() {
        return EMAD;
    }

    public void setEMAD(String EMAD) {
        this.EMAD = EMAD;
    }

    public String getLAST_LOIN_DTTM() {
        return LAST_LOIN_DTTM;
    }

    public void setLAST_LOIN_DTTM(String LAST_LOIN_DTTM) {
        this.LAST_LOIN_DTTM = LAST_LOIN_DTTM;
    }

    public String getAB_IP_BLCK_YN() {
        return AB_IP_BLCK_YN;
    }

    public void setAB_IP_BLCK_YN(String AB_IP_BLCK_YN) {
        this.AB_IP_BLCK_YN = AB_IP_BLCK_YN;
    }

    public String getPN_INPT_EROR_TCNT() {
        return PN_INPT_EROR_TCNT;
    }

    public void setPN_INPT_EROR_TCNT(String PN_INPT_EROR_TCNT) {
        this.PN_INPT_EROR_TCNT = PN_INPT_EROR_TCNT;
    }

    public String getNRID() {
        return NRID;
    }

    public void setNRID(String NRID) {
        this.NRID = NRID;
    }

    public String getODDP_ACCN() {
        return ODDP_ACCN;
    }

    public void setODDP_ACCN(String ODDP_ACCN) {
        this.ODDP_ACCN = ODDP_ACCN;
    }

    public String getSECU_MEDI_DVCD() {
        return SECU_MEDI_DVCD;
    }

    public void setSECU_MEDI_DVCD(String SECU_MEDI_DVCD) {
        this.SECU_MEDI_DVCD = SECU_MEDI_DVCD;
    }

    public String getOTP_VNDR_CD() {
        return OTP_VNDR_CD;
    }

    public void setOTP_VNDR_CD(String OTP_VNDR_CD) {
        this.OTP_VNDR_CD = OTP_VNDR_CD;
    }

    public String getOTP_STCD() {
        return OTP_STCD;
    }

    public void setOTP_STCD(String OTP_STCD) {
        this.OTP_STCD = OTP_STCD;
    }

    public String getDLY_TRNF_SVC_ENTR_YN() {
        return DLY_TRNF_SVC_ENTR_YN;
    }

    public void setDLY_TRNF_SVC_ENTR_YN(String DLY_TRNF_SVC_ENTR_YN) {
        this.DLY_TRNF_SVC_ENTR_YN = DLY_TRNF_SVC_ENTR_YN;
    }

    public String getSMRT_WTCH_REG_YN() {
        return SMRT_WTCH_REG_YN;
    }

    public void setSMRT_WTCH_REG_YN(String SMRT_WTCH_REG_YN) {
        this.SMRT_WTCH_REG_YN = SMRT_WTCH_REG_YN;
    }

    public String getSMPL_TRNF_REG_YN() {
        return SMPL_TRNF_REG_YN;
    }

    public void setSMPL_TRNF_REG_YN(String SMPL_TRNF_REG_YN) {
        this.SMPL_TRNF_REG_YN = SMPL_TRNF_REG_YN;
    }

    public String getLMIT_LMT_ACCO_HOLD_YN() {
        return LMIT_LMT_ACCO_HOLD_YN;
    }

    public void setLMIT_LMT_ACCO_HOLD_YN(String LMIT_LMT_ACCO_HOLD_YN) {
        this.LMIT_LMT_ACCO_HOLD_YN = LMIT_LMT_ACCO_HOLD_YN;
    }

    public String getCDD_EDD_CUST_CNFR_RESS_DT() {
        return CDD_EDD_CUST_CNFR_RESS_DT;
    }

    public void setCDD_EDD_CUST_CNFR_RESS_DT(String CDD_EDD_CUST_CNFR_RESS_DT) {
        this.CDD_EDD_CUST_CNFR_RESS_DT = CDD_EDD_CUST_CNFR_RESS_DT;
    }

    public String getDSGT_MNRC_ACCO_SVC_ENTR_YN() {
        return DSGT_MNRC_ACCO_SVC_ENTR_YN;
    }

    public void setDSGT_MNRC_ACCO_SVC_ENTR_YN(String DSGT_MNRC_ACCO_SVC_ENTR_YN) {
        this.DSGT_MNRC_ACCO_SVC_ENTR_YN = DSGT_MNRC_ACCO_SVC_ENTR_YN;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    public boolean isCompletedReg() {
        return isCompletedReg;
    }

    public void setCompletedReg(boolean completedReg) {
        isCompletedReg = completedReg;
    }

    public String getNRID_SEX_CD() {
        return NRID_SEX_CD;
    }

    public void setNRID_SEX_CD(String NRID_SEX_CD) {
        this.NRID_SEX_CD = NRID_SEX_CD;
    }

    public String getCDD_EDD_CUST_CNFR_RESS_ELPS_DCNT() {
        return CDD_EDD_CUST_CNFR_RESS_ELPS_DCNT;
    }

    public void setCDD_EDD_CUST_CNFR_RESS_ELPS_DCNT(String CDD_EDD_CUST_CNFR_RESS_ELPS_DCNT) {
        this.CDD_EDD_CUST_CNFR_RESS_ELPS_DCNT = CDD_EDD_CUST_CNFR_RESS_ELPS_DCNT;
    }

    public String getSMPH_OPSY_DVCD() {
        return SMPH_OPSY_DVCD;
    }

    public void setSMPH_OPSY_DVCD(String SMPH_OPSY_DVCD) {
        this.SMPH_OPSY_DVCD = SMPH_OPSY_DVCD;
    }

    public String getSMPH_DEVICE_ID() {
        return SMPH_DEVICE_ID;
    }

    public void setSMPH_DEVICE_ID(String SMPH_DEVICE_ID) {
        this.SMPH_DEVICE_ID = SMPH_DEVICE_ID;
    }

    public String getSMPH_UUID_NO() {
        return SMPH_UUID_NO;
    }

    public void setSMPH_UUID_NO(String SMPH_UUID_NO) {
        this.SMPH_UUID_NO = SMPH_UUID_NO;
    }

    public String getSECU_MEDI_USE_BZWR_DVCD() {
        return SECU_MEDI_USE_BZWR_DVCD;
    }

    public void setSECU_MEDI_USE_BZWR_DVCD(String SECU_MEDI_USE_BZWR_DVCD) {
        this.SECU_MEDI_USE_BZWR_DVCD = SECU_MEDI_USE_BZWR_DVCD;
    }

    public String getIDNF_CNFR_PRGS_STCD() {
        return IDNF_CNFR_PRGS_STCD;
    }

    public void setIDNF_CNFR_PRGS_STCD(String IDNF_CNFR_PRGS_STCD) {
        this.IDNF_CNFR_PRGS_STCD = IDNF_CNFR_PRGS_STCD;
    }

    public String getIDNF_RE_CNFR_DTTM() {
        return IDNF_RE_CNFR_DTTM;
    }

    public void setIDNF_RE_CNFR_DTTM(String IDNF_RE_CNFR_DTTM) {
        this.IDNF_RE_CNFR_DTTM = IDNF_RE_CNFR_DTTM;
    }

    public String getMOTP_END_YN() {
        return MOTP_END_YN;
    }

    public void setMOTP_END_YN(String MOTP_END_YN) {
        this.MOTP_END_YN = MOTP_END_YN;
    }

    public String getMOTP_EROR_TCNT() {
        return MOTP_EROR_TCNT;
    }

    public void setMOTP_EROR_TCNT(String MOTP_EROR_TCNT) {
        this.MOTP_EROR_TCNT = MOTP_EROR_TCNT;
    }

    public String getSYS_DTTM() {
        return SYS_DTTM;
    }

    public void setSYS_DTTM(String SYS_DTTM) {
        this.SYS_DTTM = SYS_DTTM;
    }

    public ArrayList<MyAccountInfo> getMyAccountInfo() {
        return myAccountInfo;
    }

    public void setMyAccountInfo(ArrayList<MyAccountInfo> myAccountInfo) {
        this.myAccountInfo = myAccountInfo;
    }

    public String getMOTPSerialNumber() {
        return MOTPSerialNumber;
    }

    public void setMOTPSerialNumber(String MOTPSerialNumber) {
        this.MOTPSerialNumber = MOTPSerialNumber;
    }

    public String getMOTP_SMPH_DEVICE_ID() {
        return MOTP_SMPH_DEVICE_ID;
    }

    public void setMOTP_SMPH_DEVICE_ID(String MOTP_SMPH_DEVICE_ID) {
        this.MOTP_SMPH_DEVICE_ID = MOTP_SMPH_DEVICE_ID;
    }

    public void syncLoginSession(String ret) {

        try {
            JSONObject object = new JSONObject(ret);
            JSONArray jsonArray;
            String valueStr = object.optString("SECU_MEDI_DVCD");

            setSECU_MEDI_DVCD(valueStr);
            valueStr = object.optString("OTP_STCD");
            setOTP_STCD(valueStr);
            valueStr = object.optString("AB_IP_BLCK_YN");
            setAB_IP_BLCK_YN(valueStr);
            valueStr = object.optString("LAST_LOIN_DTTM");
            setLAST_LOIN_DTTM(valueStr);
            valueStr = object.optString("CLPH_TONO");
            setCLPH_TONO(valueStr);
            valueStr = object.optString("SEX_CD");
            setSEX_CD(valueStr);
            valueStr = object.optString("CUST_NO");
            setCUST_NO(valueStr);
            valueStr = object.optString("LMIT_LMT_ACCO_HOLD_YN");
            setLMIT_LMT_ACCO_HOLD_YN(valueStr);
            valueStr = object.optString("BRDD");
            setBRDD(valueStr);
            valueStr = object.optString("OTP_VNDR_CD");
            setOTP_VNDR_CD(valueStr);
            valueStr = object.optString("DSGT_MNRC_ACCO_SVC_ENTR_YN");
            setDSGT_MNRC_ACCO_SVC_ENTR_YN(valueStr);
            valueStr = object.optString("CLPH_LCNO");
            setCLPH_LCNO(valueStr);
            valueStr = object.optString("SMPL_TRNF_REG_YN");
            setSMPL_TRNF_REG_YN(valueStr);
            valueStr = object.optString("CDD_EDD_CUST_CNFR_RESS_DT");
            setCDD_EDD_CUST_CNFR_RESS_DT(valueStr);
            valueStr = object.optString("PN_INPT_EROR_TCNT");
            setPN_INPT_EROR_TCNT(valueStr);
            valueStr = object.optString("EMAD");
            setEMAD(valueStr);
            valueStr = object.optString("DLY_TRNF_SVC_ENTR_YN");
            setDLY_TRNF_SVC_ENTR_YN(valueStr);
            valueStr = object.optString("MBR_NO");
            setMBR_NO(valueStr);
            valueStr = object.optString("CI_NO");
            setCI_NO(valueStr);
            valueStr = object.optString("ODDP_ACCN");
            setODDP_ACCN(valueStr);
            valueStr = object.optString("CLPH_SRNO");
            setCLPH_SRNO(valueStr);
            valueStr = object.optString("SMRT_WTCH_REG_YN");
            setSMRT_WTCH_REG_YN(valueStr);
            valueStr = object.optString("NRID");
            setNRID(valueStr);
            valueStr = object.optString("CUST_NM");
            setCUST_NM(valueStr);
            valueStr = object.optString("DI_NO");
            setDI_NO(valueStr);
            valueStr = object.optString("NRID_SEX_CD");
            setNRID_SEX_CD(valueStr);
            valueStr = object.optString("CDD_EDD_CUST_CNFR_RESS_ELPS_DCNT");
            setCDD_EDD_CUST_CNFR_RESS_ELPS_DCNT(valueStr);
            valueStr = object.optString("SYS_DTTM");
            setSYS_DTTM(valueStr);
            valueStr = object.optString("SMPH_OPSY_DVCD");
            setSMPH_OPSY_DVCD(valueStr);
            valueStr = object.optString("SMPH_DEVICE_ID");
            setSMPH_DEVICE_ID(valueStr);
            valueStr = object.optString("MOTP_SMPH_DEVICE_ID");
            setMOTP_SMPH_DEVICE_ID(valueStr);
            valueStr = object.optString("SMPH_UUID_NO");
            setSMPH_UUID_NO(valueStr);
            valueStr = object.optString("IDNF_CNFR_PRGS_STCD");
            setIDNF_CNFR_PRGS_STCD(valueStr);
            valueStr = object.optString("IDNF_RE_CNFR_DTTM");
            setIDNF_RE_CNFR_DTTM(valueStr);
            valueStr = object.optString("SECU_MEDI_USE_BZWR_DVCD");
            setSECU_MEDI_USE_BZWR_DVCD(valueStr);
            valueStr = object.optString("MOTP_END_YN");
            setMOTP_END_YN(valueStr);
            valueStr = object.optString("MOTP_EROR_TCNT");
            setMOTP_EROR_TCNT(valueStr);
            jsonArray = object.optJSONArray("REC_DMNB_ACNO");
            getMyAccountInfo().clear();
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    MyAccountInfo myAccountInfo = new MyAccountInfo();
                    myAccountInfo.setRPRS_ACCO_YN(obj.optString("RPRS_ACCO_YN"));
                    myAccountInfo.setACCO_INDO(obj.optString("ACCO_IDNO"));
                    myAccountInfo.setACCO_ALS(obj.optString("ACCO_ALS"));
                    myAccountInfo.setNEW_DT(obj.optString("NEW_DT"));
                    myAccountInfo.setACCO_PRGS_STCD(obj.optString("ACCO_PRGS_STCD"));
                    myAccountInfo.setACCO_STCD(obj.optString("ACCO_STCD"));
                    myAccountInfo.setPROD_NM(obj.optString("PROD_NM"));
                    myAccountInfo.setPROD_CD(obj.optString("PROD_CD"));
                    myAccountInfo.setACN0(obj.optString("ACNO"));
                    myAccountInfo.setWTCH_POSB_AMT(obj.optString("WTCH_POSB_AMT"));
                    myAccountInfo.setACCO_BLNC(obj.optString("ACCO_BLNC"));
                    getMyAccountInfo().add(myAccountInfo);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setLogin(true);
    }
}
