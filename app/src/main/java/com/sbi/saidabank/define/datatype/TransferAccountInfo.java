package com.sbi.saidabank.define.datatype;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Saidabank_android
 * Class: TransferAccountInfo
 * Created by 950485 on 2018. 11. 23.
 * <p>
 * Description:
 *  최근/자주 이체한 계좌 정보
 */

public class TransferAccountInfo implements Serializable {
    private String DTA_DVCD;            // 자료구분코드
    private String FAVO_ACCO_YN;        // 즐겨찾기계좌여부
    private String MNRC_BANK_CD;        // 입금은행코드
    private String MNRC_ACNO;           // 입금계좌번호
    private String MNRC_ACCO_DEPR_NM;   // 입금계좌예금주명
    private String MNRC_ACCO_ALS;        // 입금계좌별칭
    private String MNRC_TLNO;           // 입금전화번호
    private String INQ_RNKN;            // 조회순위
    private String MNRC_BANK_NM;        // 입금은행명
    private String CLAS_NM;              // 클래스명

    public TransferAccountInfo() {
    }

    public TransferAccountInfo(JSONObject object) {
        DTA_DVCD = object.optString("DTA_DVCD");
        FAVO_ACCO_YN = object.optString("FAVO_ACCO_YN");
        MNRC_BANK_CD = object.optString("MNRC_BANK_CD");
        MNRC_ACNO = object.optString("MNRC_ACNO");
        MNRC_ACCO_DEPR_NM = object.optString("MNRC_ACCO_DEPR_NM");
        MNRC_ACCO_ALS = object.optString("MNRC_ACCO_ALS");
        MNRC_TLNO = object.optString("MNRC_TLNO");
        INQ_RNKN = object.optString("INQ_RNKN");
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");
        CLAS_NM = object.optString("CLAS_NM");
    }

    public String getDTA_DVCD() {
        return DTA_DVCD;
    }

    public void setDTA_DVCD(String DTA_DVCD) {
        this.DTA_DVCD = DTA_DVCD;
    }

    public String getFAVO_ACCO_YN() {
        return FAVO_ACCO_YN;
    }

    public void setFAVO_ACCO_YN(String FAVO_ACCO_YN) {
        this.FAVO_ACCO_YN = FAVO_ACCO_YN;
    }

    public String getMNRC_BANK_CD() {
        return MNRC_BANK_CD;
    }

    public void setMNRC_BANK_CD(String MNRC_BANK_CD) {
        this.MNRC_BANK_CD = MNRC_BANK_CD;
    }

    public String getMNRC_ACNO() {
        return MNRC_ACNO;
    }

    public void setMNRC_ACNO(String MNRC_ACNO) {
        this.MNRC_ACNO = MNRC_ACNO;
    }

    public String getMNRC_ACCO_DEPR_NM() {
        return MNRC_ACCO_DEPR_NM;
    }

    public void setMNRC_ACCO_DEPR_NM(String MNRC_ACCO_DEPR_NM) {
        this.MNRC_ACCO_DEPR_NM = MNRC_ACCO_DEPR_NM;
    }

    public String getMNRC_ACCO_ALS() {
        return MNRC_ACCO_ALS;
    }

    public void setMNRC_ACCO_ALS(String MNRC_ACCO_ALS) {
        this.MNRC_ACCO_ALS = MNRC_ACCO_ALS;
    }

    public String getMNRC_TLNO() {
        return MNRC_TLNO;
    }

    public void setMNRC_TLNO(String MNRC_TLNO) {
        this.MNRC_TLNO = MNRC_TLNO;
    }

    public String getINQ_RNKN() {
        return INQ_RNKN;
    }

    public void setINQ_RNKN(String INQ_RNKN) {
        this.INQ_RNKN = INQ_RNKN;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    public String getCLAS_NM() {
        return CLAS_NM;
    }

    public void setCLAS_NM(String CLAS_NM) {
        this.CLAS_NM = CLAS_NM;
    }
}
