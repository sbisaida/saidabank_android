package com.sbi.saidabank.define;

import com.sbi.saidabank.BuildConfig;

/**
 * Saidabank_android
 * Class: LicenseKey
 * Created by 950469 on 2018. 11. 6.
 * <p>
 * Description:
 * WAS SERVICE에 접근하기 위한 서비스 ID관리
 */
public enum WasServiceUrl {
    SMP0030101A02          ("APK Server version","smp0030101A02.jct"),
    SMP0030101A03          ("APK 다운로드","smp0030101A03.jct"),
    /**
     * LCCD="CLPH_CMCM_DVCD" (휴대전화통신사구분코드)
     * LCCD="BANK_CD" (은행코드)
     * LCCD="SCCM_CD" (증권사코드)
     * LCCD="DRVN_LCNS_LCNO" (운전면허지역코드)
     */
    CMM0010100A00          ("공통->코드조회","cmm0010100A00.jct"),
    /**
     * OCR 관련
     */
    CMM0010200A01          ("공통->OCR->신분증 이미지 파싱","cmm0010200A01.jct"),
    CMM0010200A02          ("공통->OCR->신분증 진위확인 + EDMS전송","cmm0010200A02.jct"),

    /**
     * 기기 및 회원상태 확인
     */
    CMM0010300A01          ("공통->기능->서비스가입로그인->기기상태확인","cmm0010300A01.jct"),
    CMM0010300A02          ("공통->서비스가입로그인->사이다뱅크회원상태조회","cmm0010300A02.jct"),
    CMM0010300A03          ("공통->서비스가입로그인->사이다뱅크회원가입","cmm0010300A03.jct"),
    CMM0010300A04          ("공통->서비스가입로그인->사이다뱅크회원기기변경","cmm0010300A04.jct"),
    CMM0010300A05          ("공통->서비스가입로그인->사이다뱅크회원로그인","cmm0010300A05.jct"),
    CMM0010300A06          ("공통->서비스가입로그인->네이티브세션정보갱신","cmm0010300A06.jct"),
    CMM0010300A07          ("공통->서비스가입로그인->사이다뱅크기기분실해제","cmm0010300A07.jct"),

    /**
     * SMS 인증
     */
    CMM0010400A01          ("공통->주민등록번호 SMS 인증번호요청","cmm0010400A01.jct"),
    CMM0010400A02          ("공통->주민등록번호 SMS 인증번호요청확인","cmm0010400A02.jct"),
    CMM0010401A01          ("공통->생년월일 SMS 인증번호요청","cmm0010401A01.jct"),
    CMM0010401A02          ("공통->생년월일 SMS 인증번호요청확인","cmm0010401A02.jct"),

    /**
     * 실명인증이체요청
     */
    CMM0010500A02          ("공통->실명인증이체요청","cmm0010500A02.jct"),
    CMM0010500A04          ("공통->실명인증문구검증","cmm0010500A04.jct"),

    /**
     * 전체계좌조회
     */
    CMM0010700A01          ("공통->전체계좌조회","cmm0010700A01.jct"),

    /**
     * 즐겨찾기계좌관리
     */
    CMM0010700A02          ("공통->즐겨찾기계좌관리","cmm0010700A02.jct"),

    /**
     * PIN인증오류횟수조회
     */
    CMM0010800A01          ("공통->PIN인증결과전송","cmm0010800A01.jct"),
    CMM0010800A02          ("공통->PIN인증오류횟수조회","cmm0010800A02.jct"),
    CMM0010800A03          ("공통->PIN인증관련SMS전송","cmm0010800A03.jct"),

    /**
     * 공통 기능
     */
    CMM0010900A01          ("공통->기능->로그아웃","cmm0010900A01.jct"),
    CMM0011500A01          ("공통->기능->시스템일시","cmm0011500A01.jct"),
    CMM0011300A01          ("공통->기능->오류정보전송","cmm0011300A01.jct"),

    /**
     * 최근/자주입금계좌조회
     */
    CMM0050100A01          ("공통->최근/자주입금계좌조회","cmm0050100A01.jct"),

    /**
     * 타기관 MOTP 검증
     */
    CMM0011700A01          ("이체->타기관 MOTP검증","cmm0011700A01.jct"),

    /**
     * 오늘하루그만보기 저장
     */
    CMM0011900A01          ("이체->타기관 MOTP검증","cmm0011900A01.jct"),

    /**
     * 이체초기정보처리
     */
    TRA0010100A01          ("이체->이체세션클리어","tra0010100A01.jct"),

    /**
     * 계좌별칭관리
     */
    TRA0010200A01          ("이체->계좌별칭관리","tra0010200A01.jct"),

    /**
     * 이체한도관리
     */
    TRA0010300A01          ("이체->이체한도관리","tra0010300A01.jct"),

    /**
     * 수취조회
     */
    TRA0010400A01          ("이체->수취조회","tra0010400A01.jct"),

    /**
     * 이체목록등록및삭제
     */
    TRA0010400A02          ("이체->이체목록등록및삭제","tra0010400A02.jct"),

    /**
     * 전자서명데이터
     */
    TRA0019900A01          ("이체->전자서명데이터","tra0019900A01.jct"),

    /**
     * 이체목록제외처리
     */
    CMM0050100A02          ("이체->이체목록제외처리","cmm0050100A02.jct"),

    /**
     * MOTP 검증
     */
    TRA0010400A03          ("이체->사이다뱅크MOTP검증","tra0010400A03.jct"),

    /**
     * 이체목록등록및삭제
     */
    TRA0010500A01         ("이체->이체요청","tra0010500A01.jct"),

    /**
     * 간편이체약관동의여부관리
     */
    TRA0020100A01         ("이체->간편이체약관동의여부관리","tra0020100A01.jct"),

    /**
     * 휴대전화번호로고객정보조회
     */
    TRA0020200A01         ("이체->휴대전화번호로고객정보조회","tra0020200A01.jct"),

    /**
     * 간편이체한도금액체크
     */
    TRA0020300A01         ("이체->간편이체한도금액체크","tra0020300A01.jct"),

    /**
     * 간편이체서비스신청처리
     */
    TRA0020400A01         ("이체->간편이체서비스신청처리","tra0020400A01.jct"),

    /**
     * 간편이체문자전송
     */
    TRA0020500A01         ("이체->간편이체문자전송","tra0020500A01.jct"),

    /**
     * 메인정보조회
     */
    MAI0010100A01         ("메인->메인정보조회","mai0010100A01.jct"),

    /**
     * 상품목록조회
     */
    MAI0010200A01         ("메인->상품목록조회","mai0010200A01.jct"),

    /**
     * 대출일부상환
     */
    LON0050100A01         ("대출일부상환","lon0050100A01.jct"),

    /**
     * 신용대출이어하기
     */
    MAI0010300A01         ("신용대출이어하기","mai0010300A01.jct"),

    /**
     * 마이너스이어하기
     */
    MAI0010300A02         ("마이너스이어하기","mai0010300A02.jct"),

    /**
     * 소액마이너스이어하기
     */
    MAI0010300A03         ("소액마이너스이어하기","mai0010300A03.jct"),

    /**
     * 보통예금이어하기
     */
    MAI0010300A04         ("보통예금이어하기","mai0010300A04.jct"),

    /**
     * 만기해지
     */
    MAI0010300A05         ("만기해지","mai0010300A05.jct"),

    /**
     * 여신계좌이어하기
     */
    MAI0010300A06         ("여신계좌이어하기","mai0010300A06.jct"),

    /**
     * 입출금계좌이어하기취소
     */
    MAI0010400A01         ("입출금계좌이어하기취소","mai0010400A01.jct"),

    /**
     * 여신계좌이어하기취소
     */
    MAI0010400A02         ("여신계좌이어하기취소","mai0010400A02.jct"),

    /**
     * URL 링크
     */
    LGN0020100            ("신분증보완상태 확인 페이지","lgn0020100.act"),
    LGN0010100            ("cdd/edd 안내 페이지","lgn0010100.act"),
    CMM0010600A01         ("약관 화면","cmm0010600A01.jct"),
    CUS0030101            ("타행OTP 사고신고해지","cus0030101.act"),
    CRT0080401            ("타행OTP 오류횟수 초기화","crt0080401.act"),
    CRT0030101            ("MOTP 발급","crt0030101.act"),
    CRT0040101            ("MOTP 재발급 사고신고 타기관OTP","crt0040101.act"),
    CRT0040401            ("MOTP 재발급 다른기기","crt0040401.act"),
    CRT0040501            ("MOTP 재발급 같은기기","crt0040501.act"),
    MAI0060100            ("메인탭 : Home 알림함","mai0060100.act"),
    MAI0040100            ("메인탭 : MY(내자산현황)","mai0040100.act"),
    MAI0030100            ("메인탭 : 상품","mai0030100.act"),
    MAI0020100            ("메인탭 : 메뉴","mai0020100.act"),
    MAI0010500            ("메인 이벤트 참여시 url","mai0010500.act"),
    MYP0010100            ("마이페이지","myp0010100.act"),
    UNT0030100            ("입출금가입","unt0030100.act"),
    INQ0010100            ("보통예금거래내역","inq0010100.act"),
    UNT0060100            ("입출금계좌관리","unt0060100.act"),
    UNT0180100            ("정기예금계좌관리","unt0180100.act"),
    UNT0290100            ("정기예금만기설정","unt0290100.act"),
    UNT0340100            ("정기예금만기해지","unt0340100.act"),
    UNT0360100            ("정기예금거래내역","unt0360100.act"),
    UNT0190100            ("정기적금추가납입","unt0190100.act"),
    UNT0210100            ("정기적금만기설정","unt0210100.act"),
    UNT0250100            ("정기적금만기해지","unt0250100.act"),
    UNT0260100            ("정기적금계좌관리","unt0260100.act"),
    UNT0370100            ("정기적금거래내역","unt0370100.act"),
    LON0050100            ("대출관리","lon0050100.act"),
    LON0050900            ("대출일부상환","lon0050900.act"),
    LON0050200            ("대출거래내역","lon0050200.act"),
    CKC0010101            ("카드신청","ckc0010101.act"),
    CKC0040101            ("카드내역","ckc0040101.act"),
    CRD0010100            ("나이스신용정보조회","crd0010100.act"),
    ERR0030100            ("이체FDS차단","err0030100.act"),
    ;

    private String mServiceId;

    WasServiceUrl(String comment, String serviceId) {
        mServiceId = serviceId;
    }

    public String getServiceUrl() {
        StringBuilder wasServiceUrl = new StringBuilder();
        if (!BuildConfig.DEBUG)
            wasServiceUrl.append(SaidaUrl.OPER_SERVER);
        else {
            if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV)
                wasServiceUrl.append(SaidaUrl.DEV_SERVER);
            else if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_TEST)
                wasServiceUrl.append(SaidaUrl.TEST_SERVER);
            else
                wasServiceUrl.append(SaidaUrl.OPER_SERVER);
        }


        wasServiceUrl.append("/");
        wasServiceUrl.append(mServiceId);

        return wasServiceUrl.toString();
    }

    public static String getUrl() {
        StringBuilder wasServiceUrl = new StringBuilder();
        if (!BuildConfig.DEBUG)
            wasServiceUrl.append(SaidaUrl.OPER_SERVER);
        else {
            if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV)
                wasServiceUrl.append(SaidaUrl.DEV_SERVER);
            else if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_TEST)
                wasServiceUrl.append(SaidaUrl.TEST_SERVER);
            else
                wasServiceUrl.append(SaidaUrl.OPER_SERVER);
        }

        return wasServiceUrl.toString();
    }
}
