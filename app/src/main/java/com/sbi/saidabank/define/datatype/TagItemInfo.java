package com.sbi.saidabank.define.datatype;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Saidabank_android
 * Class: TagItemInfo
 * Created by 950485 on 2019. 03. 22.
 * <p>
 * Description:메인화면 태그 정보
 */

public class TagItemInfo implements Serializable {
    private String TRNF_BANK_CD;    // 이체은행코드
    private String TRNF_ACNO;       // 이체계좌번호
    private String TRNF_DEPR_NM;    // 이체예금주명

    public TagItemInfo() {

    }

    public TagItemInfo(JSONObject object) {
        TRNF_BANK_CD = object.optString("TRNF_BANK_CD");
        TRNF_ACNO = object.optString("TRNF_ACNO");
        TRNF_DEPR_NM = object.optString("TRNF_DEPR_NM");
    }

    public String getTRNF_BANK_CD() {
        return TRNF_BANK_CD;
    }

    public void setTRNF_BANK_CD(String TRNF_BANK_CD) {
        this.TRNF_BANK_CD = TRNF_BANK_CD;
    }

    public String getTRNF_ACNO() {
        return TRNF_ACNO;
    }

    public void setTRNF_ACNO(String TRNF_ACNO) {
        this.TRNF_ACNO = TRNF_ACNO;
    }

    public String getTRNF_DEPR_NM() {
        return TRNF_DEPR_NM;
    }

    public void setTRNF_DEPR_NM(String TRNF_DEPR_NM) {
        this.TRNF_DEPR_NM = TRNF_DEPR_NM;
    }
}
