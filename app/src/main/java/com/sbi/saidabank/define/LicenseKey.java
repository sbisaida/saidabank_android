package com.sbi.saidabank.define;

/**
 * Saidabank_android
 * Class: LicenseKey
 * Created by 950469 on 2018. 11. 6.
 * <p>
 * Description:
 * 솔루션 라이센스키
 */
public class LicenseKey {
    /**
     * V3
     */
    public static final String V3_LICENSE_KEY                 = "40040020-17087142";
    public static final String RC_LICENSE_KEY                 = "90041410-35695986";

    /**
     * Espider
     */
    public static final String ESPIDER_LICENSE_KEY            = "98215963-aa7a-11e8-8a14-80c16e782f98";

    /**
     * AppsFlyer
     */
    public static final String AF_DEV_KEY                     = "xhHM9BoZyKGsQdDyswhJV6";
}
