package com.sbi.saidabank.define;

public class Const {
    /**
     * 메인웹뷰 인텐트
     */
    public static final String INTENT_MAINWEB_URL                 = "url";

    /**
    * 보안키패드 인텐트 정의
    */
    public static final String INTENT_TRANSKEY_PADTYPE            = "keypad_type";
    public static final String INTENT_TRANSKEY_TEXTTYPE           = "text_type";
    public static final String INTENT_TRANSKEY_TITLE              = "title";
    public static final String INTENT_TRANSKEY_LABEL1             = "label1";
    public static final String INTENT_TRANSKEY_LABEL2             = "label2";
    public static final String INTENT_TRANSKEY_HINT               = "hint";
    public static final String INTENT_TRANSKEY_MAXLENGTH          = "max_length";
    public static final String INTENT_TRANSKEY_MINLENGTH          = "min_length";
    public static final String INTENT_TRANSKEY_PRECAUTION         = "precaution";
    public static final String INTENT_TRANSKEY_INPUTID1           = "input_id1";//보안키패드 호출한 컨트롤 아이디1
    public static final String INTENT_TRANSKEY_INPUTID2           = "input_id2";//보안키패드 호출한 컨트롤 아이디2
    public static final String INTENT_TRANSKEY_PARAMETER          = "parameter";//임시저장을 위한 파라미터값
    public static final String INTENT_TRANSKEY_CAPTION            = "caption_show";//말풍선
    public static final String INTENT_TRANSKEY_DUMMY_DATA1        = "dummy_data1";//가짜비밀번호
    public static final String INTENT_TRANSKEY_DUMMY_DATA2        = "dummy_data2";//가짜비밀번호
    public static final String INTENT_TRANSKEY_PLAIN_DATA1        = "plain_data1";//비밀번호
    public static final String INTENT_TRANSKEY_PLAIN_DATA2        = "plain_data2";//비밀번호
    public static final String INTENT_TRANSKEY_CERT_CHIPER_DATA   = "cert_pass_chiper_data";//이니텍 인증서 비밀번호를 위한 암호화 데이타
    public static final String INTENT_TRANSKEY_SECURE_DATA1       = "secure_data1";//암호화데이타
    public static final String INTENT_TRANSKEY_SECURE_DATA2       = "secure_data2";//암호화데이타

    /**
     * 퍼미션 요청코드
     * (intro,webmain,인증서,user agent 화면에서 사용)
     */
    public static final int REQUEST_PERMISSION_READ_PHONE_STATE    = 1001;
    public static final int REQUEST_PERMISSION_CALL                = 1002;
    public static final int REQUEST_PERMISSION_CAMERA              = 1003;
    public static final int REQUEST_PERMISSION_EXTERNAL_STORAGE    = 1004;
    //public static final int REQUEST_PERMISSION_SMS                 = 1005;
    public static final int REQUEST_PERMISSION_READ_CONTACTS       = 1006;
    public static final int REQUEST_PERMISSION_SYNC_CONTACTS       = 1007;

    /**
     * Was 요청 파리미터값
     */
    public static final String REQUEST_WAS_YES               = "Y";
    public static final String REQUEST_WAS_NO                = "N";

    /**
     * java script 기본 결과값
     */
    public static final String BRIDGE_RESULT_KEY             = "result";
    public static final String BRIDGE_RESULT_TRUE            = "true";
    public static final String BRIDGE_RESULT_FALSE           = "false";
    public static final String BRIDGE_RESULT_YES             = "Y";
    public static final String BRIDGE_RESULT_NO              = "N";

    /**
     * 코드 조회 결과값 공통 
     */
    public static final String REQUEST_COMMON_HEAD             = "COMMON_HEAD";
    public static final String REQUEST_COMMON_ERROR            = "ERROR";
    public static final String REQUEST_COMMON_MESSAGE          = "MESSAGE";
    public static final String REQUEST_COMMON_CODE             = "CODE";
    public static final String REQUEST_COMMON_RESP_CD          = "RESP_CD";
    public static final String REQUEST_COMMON_RESP_CNTN        = "RESP_CNTN";
    public static final String REQUEST_COMMON_TRUE             = "true";
    public static final String REQUEST_COMMON_SUCCESS_CODE     = "01";
    public static final String REQUEST_COMMON_FAIL_CODE        = "02";

    /**
     * 인증서 관련 인텐트
     */
    public static final String INTENT_CERT_PURPOSE         = "intent_cert_purpose";
    public static final String INTENT_CERT_IDDATA          = "intent_cert_iddata";//주민번호나 기타 저장에 필요한 데이타 담아둘때.
    public static final String INTENT_CERT_PATH            = "intent_cert_path";
    public static final String INTENT_CERT_PASSWORD        = "intent_cert_password";
    public static final String INTENT_CERT_INDEX           = "intent_cert_index";
    public static final String INTENT_CERT_AUTHCODE        = "intent_cert_authcode";
    public static final String INTENT_CERT_SERVICEDATA     = "intent_cert_servicedata";
    public static final String INTENT_CERT_BUNDLEDATA      = "intent_cert_bundledata";
    public static final String INTENT_CERT_USERNAME        = "intent_cert_username";
    public static final String INTENT_CERT_TYPE            = "intent_cert_certtype";
    public static final String INTENT_CERT_ISSUERNAME      = "intent_cert_issuername";
    public static final String INTENT_CERT_EXPIREDATE      = "intent_cert_expiredate";
    public static final String INTENT_CERT_DATA            = "intent_cert_data";
    public static final String INTENT_CERT_NO_SCRAPING     = "intent_cert_no_scraping";
    public static final String INTENT_CERT_ISSUER_DN       = "intent_cert_issuer_dn";
    public static final String INTENT_CERT_SUBJECT_DN      = "intent_cert_subject_dn";

    /**
     * 메인 관련 인텐트
     */
    public static final String INTENT_MAIN_TRANSFER_SAVINGS    = "intent_main_transfer_savings";

    /**인증서 리스트 호출 목적
     *
     */
    public enum CertJobType {
        SCRAPING,
        MANAGER,
        EXPORT
    }

    /**
     * Espider
     */
    public static final int ESPIDER_TYPE_HEALTH_PAY            = 1;
    public static final int ESPIDER_TYPE_HEALTH_QUALIFICATION  = 2;
    public static final int ESPIDER_TYPE_NATIONAL_SUBSCRIBERS  = 3;
    public static final int ESPIDER_TYPE_NATIONAL_PAY          = 4;
    public static final int ESPIDER_TYPE_NATIONAL_PAY_HISTORY  = 5;
    public static final int ESPIDER_TYPE_REVENUE_REGISTER      = 6;
    public static final int ESPIDER_TYPE_REVENUE_TAX           = 7;
    public static final int ESPIDER_TYPE_REVENUE_TAX_INCOME    = 8;
    public static final int ESPIDER_TYPE_INCOME_QUALIFICATION  = 9;

    public static final String ESPIDER_STATUS                            = "status";
    public static final String ESPIDER_STATUS_START                      = "start";
    public static final String ESPIDER_STATUS_CANCEL                     = "cancel";
    public static final String ESPIDER_STATUS_COMPLETE                   = "complete";

    public static final String ESPIDER_TYPE_HEALTH_PAY_CODE              = "KRPP0002010020";
    public static final String ESPIDER_TYPE_HEALTH_QUALIFICATION_CODE    = "KRPP0002010010";
    public static final String ESPIDER_TYPE_NATIONAL_PAY_HISTORY_CODE    = "KRPP0001010060";
    public static final String ESPIDER_TYPE_REVENUE_TAX_CODE             = "KRNT0001010040";
    public static final String ESPIDER_TYPE_INCOME_QUALIFICATION_CODE    = "KRNT0001012000";

    public static final int ESPIDER_TYPE_RETURN_SUCCESS            = 0;
    public static final int ESPIDER_TYPE_RETURN_FAIL               = 1;

    /**
     * ssenstone operation
     */
    public static final String SSENSTONE_REGISTER            = "Reg";
    public static final String SSENSTONE_AUTHENTICATE        = "Auth";
    public static final String SSENSTONE_DEREGISTER          = "Dereg";
    public static final String SSENSTONE_CHANGE_PIN          = "changePin";

    public static final int SSENSTONE_MIN_PATTERN            = 6;
    public static final int SSENSTONE_PINCODE_LENGTH         = 6;
    public static final int SSENSTONE_PIN_DUPLICATE          = 3;
    public static final int SSENSTONE_AUTH_COUNT_FAIL        = 5;

    /**
     * Safe card keypad 관련 인텐트
     */
    public static final String SAFE_CARD_FIRST_NUM           = "safe_key_first_num";
    public static final String SAFE_CARD_SECOND_NUM          = "safe_key_second_num";
    public static final String SAFE_CARD_FIRST_KEY           = "safe_key_first_key";
    public static final String SAFE_CARD_SECOND_KEY          = "safe_key_second_key";
    public static final String SAFE_CARD_FIRST_SECURE_KEY   = "safe_key_first_secure_key";
    public static final String SAFE_CARD_SECOND_SECURE_KEY  = "safe_key_second_secure_key";
    public static final String SAFE_CARD_FIRST_LENGTH        = "safe_key_first_length";
    public static final String SAFE_CARD_SECOND_LENGTH       = "safe_key_v_length";

    /**
     * Crop 관련 인텐트
     */
    public static final String CROP_IMAGE_URI                = "crop_image_uri";
    public static final int REQUEST_PROFILE_ALBUM            = 2000;
    public static final int REQUEST_PROFILE_IMAGE            = 2001;
    public static final int REQUEST_PROFILE_CAMERA           = 2002;
    public static final int REQUEST_CROP_TO_IMAGE            = 2003;
    public static final int REQUEST_CROP_IMAGE               = 20000;
    public static final String CROP_IMAGE_PATH               = "sbiprofile.jpg";

    /**
     * 공통 전달 인텐트(로그인, SsenStone, OTP, OCR)
     */
    public static final String INTENT_COMMON_INFO        = "intent_common_info";
    public static final String INTENT_SERVICE_ID         = "intent_service_id";
    public static final String INTENT_BIZ_DV_CD          = "intent_biz_dv_cd";
    public static final String INTENT_TRN_CD             = "intent_trn_cd";
    public static final String INTENT_PARAM              = "param";

    /**
     * 입력창 한글 유효성
     */
    public static final int INDEX_EDITBOX_NAME = 0;
    public static final int INDEX_EDITBOX_BIRTH = 1;
    public static final int INDEX_EDITBOX_EMAIL = 2;
    public static final int STATE_EDITBOX_NORMAL = 0;
    public static final int STATE_EDITBOX_FOCUSED = 1;
    public static final int STATE_EDITBOX_ERROR = 2;
    public static final int STATE_VALID_STR_NORMAL = 0;
    public static final int STATE_VALID_STR_LENGTH_ERR = 1;
    public static final int STATE_VALID_STR_CHAR_ERR = 2;

    public static final String ID_TYPE_JUMIM                 = "01";
    public static final String ID_TYPE_DRIVER                = "02";

    /**
     * 신분증 인증 인텐트
     */
    public static final String INTENT_IDENTIFICATION_IMG     = "intent_id_img";

    /**
     * 타행 계좌 인증 인텐트
     */
    public static final String INTENT_VERIFY_ACCOUNT_SQNO        = "intent_verify_account_sqno";
    public static final String INTENT_VERIFY_ACCOUNT_NUMBER      = "intent_verify_account_number";
    public static final String INTENT_VERIFY_ACCOUNT_BANK_CODE   = "intent_verify_account_bank_code";
    public static final String INTENT_VERIFY_ACCOUNT_CUST_NAME   = "intent_verify_account_cust_name";
    public static final String INTENT_VERIFY_ACCOUNT_DVCD        = "intent_verify_account_dvcd";
    public static final String INTENT_VERIFY_ACCOUNT_PROPNO      = "intent_verify_account_propno";

    /**
     * OCR Type
     */
    public static final String IDENTIFICATION_TYPE_ID        = "ID";
    public static final String IDENTIFICATION_TYPE_DOC       = "DOC";

    /**
     * 계좌번호 이체 인텐트
     */
    public static final String INTENT_LIST_TRANSFER_RECEIPT_INFO  = "intent_list_transfer_receipt_info";
    public static final String INTENT_LIST_TRANSFER_NAME          = "intent_transfer_name";
    public static final String INTENT_TRANSFER_BALANCE            = "intent_transfer_balance";
    public static final String INTENT_WITHDRAW_NAME               = "intent_withdraw_name";
    public static final String INTENT_WITHDRAWABLE_AMOUNT         = "intent_withdrawable_amount";
    public static final String INTENT_PROD_NAME                   = "intent_prod_name";
    public static final String INTENT_ACCO_ALS                    = "intent_acco_als";
    public static final String INTENT_BANK_NM                     = "intent_bank_name";
    public static final String INTENT_TAG_BANK_CODE               = "intent_tag_bank_code";
    public static final String INTENT_TAG_ACCOUNT                 = "intent_tag_account";

    /**
     * 휴대전화번호 이체 인텐트
     */
    public static final String INTENT_TRANSFER_PHONE_INFO        = "intent_transfer_phone_info";
    public static final String INTENT_TRANSFER_PHONE_RET         = "intent_transfer_phone_ret";
    public static final String CONTACTS_INFO_PATH                = "contactsInfo.dat";
    public static final String INTENT_TRANSFER_PHONE_KAKAO_MSG   = "intent_transfer_phone_kakao_msg";

    /**
     * 이체 실패 인텐트
     */
    public static final String INTENT_TRANSFER_ENTRY_TYPE        = "intent_transfer_entry_type";  // 0 : 계좌이체, 1 : 휴대폰번호이체
    public static final String INTENT_TRANSFER_FAIL_TYPE         = "intent_transfer_fail_type";   // 0 : 일반실패, 1 : 잔액부족
    public static final String INTENT_TRANSFER_FAIL_RET         = "intent_transfer_fail_ret";

    /**
     * 이체화면에서 계좌번호 입력 시, 입력창 활성화 최저값
     */
    public static final int MAX_TRANSFER_ACOOUNT_LENGTH      = 5;
    public static final int MAX_TRANSFER_PHONE_LENGTH        = 11;

    /**
     * 주소록 인텐트
     */
    public static final String INTENT_CONTACTS_LIST           = "intent_contacts_list";
    public static final String INTENT_CONTACTS_PHONE_INFO     = "intent_contacts_phone_info";

	/**
     * Picker type
     */
    public static final int PICKER_TYPE_DATE                  = 1;
    public static final int PICKER_TYPE_TIME                  = 2;
    public static final int PICKER_TYPE_DATE_WEB_YMD          = 3;
    public static final int PICKER_TYPE_DATE_WEB_YM           = 4;
    public static final int PICKER_TYPE_DATE_CYCLE_WEB_YM     = 5;
    public static final int PICKER_TYPE_CUSTOM_DATA_WEB       = 6;

    /**
     * ARS 인증 call 수신 여부 (불필요시 삭제 예정)
     */
    public static boolean isIncomingARS;

    public static final int OTP_PW_ENTRY_WEB                 = 1;
    public static final int OTP_PW_ENTRY_NATIVE              = 2;

    /**
     * OTP PW 등록, 변경, 인증 입력화면 진입 타잎
     */
    public static final int OTP_PW_ACCESS_REG_OTP            = 1;
    public static final int OTP_PW_ACCESS_REG_MOTP           = 2;
    public static final int OTP_PW_ACCESS_REG_PW             = 3;
    public static final int OTP_PW_ACCESS_CHNG_OTP           = 4;
    public static final int OTP_PW_ACCESS_CHNG_MOTP          = 5;
    public static final int OTP_PW_ACCESS_CHNG_PW            = 6;
    public static final int OTP_PW_ACCESS_AUTH_OTP           = 7;
    public static final int OTP_PW_ACCESS_AUTH_MOTP          = 8;
    public static final int OTP_PW_ACCESS_AUTH_PW            = 9;
    public static final int OTP_PW_ACCESS_AUTH_LOSTPIN       = 10;

    public static final int OTP_PW_NUM_LENGTH                = 6;
    public static final int OTP_PW_JUMIN_LENGTH              = 7;
    public static final int OTP_PW_NUM_DUPLICATE             = 3;
    public static final int OTP_PW_OTP_AUTH_COUNT_FAIL       = 10;
    public static final int OTP_PW_PW_AUTH_COUNT_FAIL        = 5;

    public static final String OTP_PW_TITLE_JUMIN            ="주민번호뒷자리";

    /**
     * OTP 인증 시 인텐트
     */
    public static final String INTENT_OTP_AUTH_TOOLS         = "intent_otp_auth_tools";
    public static final String INTENT_OTP_AUTH_SIGN          = "intent_otp_auth_sign";
    public static final String INTENT_OTP_TA_VERSION         = "intent_otp_ta_version";
    public static final String INTENT_OTP_SERIAL_NUMBER      = "intent_otp_serial_number";

    /*
     * 이체시 보이스 피싱 노출 액수
     */
    public static final Double SHOW_VOICE_PHISHING_AMOUNT    = 5000000.;

    /*
     * 다건 이체 최대 갯수
     */
    public static final int MAX_TRANSFER_AT_ONCE             = 5;

    /**
     * 은행/증권사 단계
     */
    public enum BANK_STOCK_MODE {
        BANK_MODE,
        STOCK_MODE
    }

    /**
     * 계좌 개설
     */
    public enum ADD_ACCOUNT_MODE {
        NOMAL_MODE,     // 입출금
        SAVINGS_MODE    // 예적금
    }

    /**
     * 카드 개설
     */
    public enum ADD_CARD_MODE {
        APPLY_MODE,      // 카드신청
        APPLYING_MODE,   // 카드신청중
        REGISTER_MODE,   // 카드등록
        INQUIRY_MODE     // 카드내역
    }

    /*
     * 푸시 인텐트 타입
     * */
    public static final String ACTION_PUSH_MESSAGE           = ".ACTION_PUSH_MESSAGE";
    public static final String ACTION_REFRESH_FCM_TOKEN      = ".ACTION_REFRESH_FCM_TOKEN";
    public static final String ACTION_PUSH_RECEIVE           = ".ACTION_PUSH_RECEIVE";
    public static final String INTENT_PUSH_DATA              = "push_data";
    public static final String INTENT_PUSH_REGID             = "push_reg_id";

    // 메인 메뉴 Fragment Type
    public static final int MainMenu_TYPE_Main               = 0;
    public static final int MainMenu_TYPE_Goods              = 1;
    public static final int MainMenu_TYPE_My                 = 2;
    public static final int MainMenu_TYPE_Menu               = 3;

    /**
     * 디버깅 서버 선택
     */
    public static final int DEBUGING_SERVER_NONE             = -1;
    public static final int DEBUGING_SERVER_DEV              = 0;
    public static final int DEBUGING_SERVER_TEST             = 1;
    public static final int DEBUGING_SERVER_OPERATION        = 2;

    /**
     * 야간모드 시간
     */
    public static final int NIGHTTIME_END                    = 5;
    public static final int NIGHTTIME_START                  = 21;

    /**
     * 이체 관련
     */
    public static final int REQUEST_TRANFER_SAVINGS        = 30000;

    /**
     * 메인 관련
     */
    public static final String ACCOUNT_TOP                  = "999";
    public static final String ACCOUNT_NO                  = "998";
    public static final String SLIDING_EXTEND              = "100";
    public static final String SLIDING_SMALL_EXTEND        = "101";
    public static final String SLIDING_ADD_LIMIT           = "102";
    public static final String SLIDING_DOWN_INTEREST       = "103";
    public static final String ACCOUNT_TAG                 = "201";
    public static final String ACCOUNT_NOMAL_REP           = "300";
    public static final String ACCOUNT_GO_ON_NOMAL         = "310";
    public static final String ACCOUNT_GO_ON_LOAN         = "320";
    public static final String ACCOUNT_GO_ON_MINUS         = "321";
    public static final String ACCOUNT_GO_ON_SMALL_MINUS   = "322";
    public static final String ACCOUNT_NOMAL               = "330";
    public static final String ACCOUNT_SAVINGS             = "400";
    public static final String ACCOUNT_INSTALL_SAVINGS     = "500";
    public static final String ACCOUNT_LOAN                = "600";
    public static final String ACCOUNT_REPORT              = "997";
    public static final String ACCOUNT_ADS                 = "996";

    /**
     * 로그아웃
     */
    public static final String INTENT_LOGOUT_TYPE          = "intent_logout_type";
    public static final int LOGOUT_TYPE_USER               = 0;
    public static final int LOGOUT_TYPE_TIMEOUT            = 1;
    public static final int LOGOUT_TYPE_FORCE              = 2;

    /**
     * 신분증 촬영 실패 타입
     */
    public static final int ID_SHOT_FAIL_SHOT              = 0;
    public static final int ID_SHOT_FAIL_CHECK             = 1;

    /**
     * Appsflyer 회원가입
     */
    public static final String APPSFLYER_MEMBER            = "join membership_";

    /**
     * 점검 시간 알림 전문 에러 코드
     */
    public static final String ERRORCODE_EFWK0002          = "EFWK0002";
}
