package com.sbi.saidabank.define.datatype;

import org.json.JSONObject;

/**
 * Saidabank_android
 * Class: LicenseKey
 * Created by 950469 on 2018. 11. 6.
 * <p>
 * Description:
 * 휴대폰 인증시 통신사 정보
 */
public class RequestCodeInfo {
    private String CD_ABRV_NM;  // 코드 대분류
    private String CD_NM;       // 코드 이름
    private String SCCD;        //  코드값

    public RequestCodeInfo(JSONObject object){
        CD_ABRV_NM = object.optString("CD_ABRV_NM");
        CD_NM = object.optString("CD_NM");
        SCCD = object.optString("SCCD");
    }

    public String getCD_ABRV_NM() {
        return CD_ABRV_NM;
    }

    public void setCD_ABRV_NM(String CD_ABRV_NM) {
        this.CD_ABRV_NM = CD_ABRV_NM;
    }

    public String getCD_NM() {
        return CD_NM;
    }

    public void setCD_NM(String CD_NM) {
        this.CD_NM = CD_NM;
    }

    public String getSCCD() {
        return SCCD;
    }

    public void setSCCD(String SCCD) {
        this.SCCD = SCCD;
    }
}
