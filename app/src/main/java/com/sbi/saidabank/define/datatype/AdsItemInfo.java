package com.sbi.saidabank.define.datatype;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Saidabank_android
 * Class: TagItemInfo
 * Created by 950485 on 2019. 03. 28.
 * <p>
 * Description:메인화면 광고 정보
 */

public class AdsItemInfo implements Serializable {
    private String IMG_URL_ADDR;    // 이미지 url
    private String LINK_URL_ADDR;   // 링크 url

    public AdsItemInfo() {

    }

    public AdsItemInfo(JSONObject object) {
        IMG_URL_ADDR = object.optString("IMG_URL_ADDR");
        LINK_URL_ADDR = object.optString("LINK_URL_ADDR");
    }

    public String getIMG_URL_ADDR() {
        return IMG_URL_ADDR;
    }

    public void setIMG_URL_ADDR(String IMG_URL_ADDR) {
        this.IMG_URL_ADDR = IMG_URL_ADDR;
    }

    public String getLINK_URL_ADDR() {
        return LINK_URL_ADDR;
    }

    public void setLINK_URL_ADDR(String LINK_URL_ADDR) {
        this.LINK_URL_ADDR = LINK_URL_ADDR;
    }
}
