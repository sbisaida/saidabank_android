package com.sbi.saidabank.define;

import com.sbi.saidabank.BuildConfig;

public class SaidaUrl {

    //public static final String OPER_SERVER = "http://192.30.1.97";//운영서버
    //public static final String DEV_SERVER = "http://192.30.1.97";//개발서버
    //public static final String TEST_SERVER = "http://192.30.1.97";//테스트서버
    public static final String DEV_SERVER = "https://devapp.saidabank.co.kr";//개발서버
    public static final String TEST_SERVER = "https://testapp.saidabank.co.kr";//테스트서버
    public static final String OPER_SERVER = "https://app.saidabank.co.kr";//운영서버
    private static final String DEV_AUTH_SERVER = "http://121.254.187.167:80";//개발 인증서버
    private static final String TEST_AUTH_SERVER = "https://auth.sbisb.co.kr";//테스트 인증서버
    private static final String OPER_AUTH_SERVER = "https://auth.sbisb.co.kr";//운영 인증서버
    private static final String DEV_PUSH_SERVER = "10.100.1.211";//개발 푸쉬서버
    private static final String TEST_PUSH_SERVER = "121.254.187.172";//테스트 푸쉬서버
    private static final String OPER_PUSH_SERVER = "push.saidabank.co.kr";//운영 푸쉬서버

    public static final String DEV_SERVER_M = "https://devm.saidabank.co.kr";//개발서버
    public static final String TEST_SERVER_M = "https://testm.saidabank.co.kr";//테스트서버
    public static final String OPER_SERVER_M = "https://m.saidabank.co.kr";//운영서버

    public static final String DEV_CHATBOT_SERVER  = "http://dev.chat2.sbisb.co.kr:8080";//개발 챗봇서버
    public static final String OPER_CHATBOT_SERVER = "https://chat2.sbisb.co.kr";//운영 챗봇서버

    public static int serverIndex = Const.DEBUGING_SERVER_OPERATION;

    public static String getBaseWebUrl() {
        if (!BuildConfig.DEBUG) {
            return OPER_SERVER;
        } else {
            if (serverIndex == Const.DEBUGING_SERVER_DEV)
                return DEV_SERVER;
            else if (serverIndex == Const.DEBUGING_SERVER_TEST)
                return TEST_SERVER;
            else
                return OPER_SERVER;
        }
    }

    public static String getBaseMWebUrl() {
        if (!BuildConfig.DEBUG) {
            return OPER_SERVER_M;
        } else {
            if (serverIndex == Const.DEBUGING_SERVER_DEV)
                return DEV_SERVER_M;
            else if (serverIndex == Const.DEBUGING_SERVER_TEST)
                return TEST_SERVER_M;
            else
                return OPER_SERVER_M;
        }
    }

    public enum ReqUrl {

        URL_CERT_EXPORT_IMPORT_URL      (DEV_SERVER, TEST_SERVER, OPER_SERVER, "/sw/initech/GetCertificate_v12.jsp"),
        URL_FDS                         (DEV_SERVER, TEST_SERVER, OPER_SERVER, "/sp"),//FDS수집서버 주소

        URL_SSENSTONE                   (DEV_SERVER, TEST_SERVER, OPER_SERVER, "/sw"),//패턴,지문,얼굴 서버주소
        //URL_SSENSTONE                   (DEV_AUTH_SERVER, DEV_AUTH_SERVER, DEV_AUTH_SERVER, "/sp"),//패턴,지문,얼굴 서버주소
        URL_FLKPUSH                     (DEV_PUSH_SERVER, TEST_PUSH_SERVER, OPER_PUSH_SERVER, ""),//Push 서버주소
        URL_CHATBOT                     (DEV_CHATBOT_SERVER, DEV_CHATBOT_SERVER, OPER_CHATBOT_SERVER, "/client-page/saida/index.html"),//챗봇 서버주소
        ;

        private String[] mSaidaUrl;

        ReqUrl(String dev_url, String test_url, String oper_url, String common) {
            if(mSaidaUrl == null)
                mSaidaUrl = new String[3];

            mSaidaUrl[0] = new StringBuilder().append(dev_url).append(common).toString();
            mSaidaUrl[1] = new StringBuilder().append(test_url).append(common).toString();
            mSaidaUrl[2] = new StringBuilder().append(oper_url).append(common).toString();
        }

        public String getReqUrl() {
            String goUrl;
            if (!BuildConfig.DEBUG) {
                goUrl = mSaidaUrl[2];
            } else {
                if (serverIndex == Const.DEBUGING_SERVER_DEV)
                    goUrl = mSaidaUrl[0];
                else if (serverIndex == Const.DEBUGING_SERVER_TEST)
                    goUrl = mSaidaUrl[1];
                else
                    goUrl = mSaidaUrl[2];
            }

            return goUrl;
        }

    }
}
