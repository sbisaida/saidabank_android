package com.sbi.saidabank.define.datatype;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Saidabank_android
 * Class: MyAccountInfo
 * Created by 950485 on 2019. 03. 07.
 * <p>
 * Description:
 * 가입 은행상품 정보
 */

public class BankGoodInfo implements Serializable {
    private String PROD_CD;              // 상품코드
    private String MENU_URL_ADDR;        // 메뉴URL주소
    private String EPOS_PROD_NM;         // 노출상품명
    private String PROD_INTR_INFO_CNTN;  // 상품금리정보내용

    public BankGoodInfo() {

    }

    public BankGoodInfo(JSONObject object) {
        PROD_CD = object.optString("PROD_CD");
        MENU_URL_ADDR = object.optString("MENU_URL_ADDR");
        EPOS_PROD_NM = object.optString("EPOS_PROD_NM");
        PROD_INTR_INFO_CNTN = object.optString("PROD_INTR_INFO_CNTN");
    }

    public String getPROD_CD() {
        return PROD_CD;
    }

    public void setPROD_CD(String PROD_CD) {
        this.PROD_CD = PROD_CD;
    }

    public String getMENU_URL_ADDR() {
        return MENU_URL_ADDR;
    }

    public void setMENU_URL_ADDR(String MENU_URL_ADDR) {
        this.MENU_URL_ADDR = MENU_URL_ADDR;
    }

    public String getEPOS_PROD_NM() {
        return EPOS_PROD_NM;
    }

    public void setEPOS_PROD_NM(String EPOS_PROD_NM) {
        this.EPOS_PROD_NM = EPOS_PROD_NM;
    }

    public String getPROD_INTR_INFO_CNTN() {
        return PROD_INTR_INFO_CNTN;
    }

    public void setPROD_INTR_INFO_CNTN(String PROD_INTR_INFO_CNTN) {
        this.PROD_INTR_INFO_CNTN = PROD_INTR_INFO_CNTN;
    }
}
