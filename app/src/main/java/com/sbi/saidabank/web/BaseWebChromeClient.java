package com.sbi.saidabank.web;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.sbi.saidabank.customview.BaseWebView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Logs;


/**
 * WebChromeClient Custom
 */
public class BaseWebChromeClient extends WebChromeClient {

	/**
	 * application context
	 */
    private Activity mContext = null;
    private FrameLayout mWebViewContainer;
    private JavaScriptBridge mJsBridge;
    public BaseWebChromeClient(Activity activity, FrameLayout container, JavaScriptBridge bridge){
		mContext = activity;
        mWebViewContainer = container;
        mJsBridge = bridge;
	}

    @Override
	public boolean onConsoleMessage(ConsoleMessage cm) {
		StringBuilder message = new StringBuilder("Console: ")
				.append(cm.message())
				.append(" / SourceId: ")
				.append(cm.sourceId())
				.append(" / LineNumber: ")
				.append(cm.lineNumber());
		Logs.i("javascript >> " + message.toString());

		//페이지를 로드하지 못한것이기에 Activity종료하도록.
        /*
		if(cm.message().contains("deviceBackHistory is not defined")){
            mActivity.finish();
        }
        */
        return true;
	}

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        Logs.i("onJsAlert : "+ message);

        if(BuildConfig.DEBUG){
            DialogUtil.alert(mContext,"jsAlert", message,new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result.confirm();
                }
            });
        }

        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
        Logs.i("onJsConfirm : "+ message);

        if(BuildConfig.DEBUG){
            DialogUtil.alert(mContext, message, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result.confirm();
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result.cancel();
                }
            });
        }

        return true;
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        Logs.i("onReceivedTitle : "+ title);

        super.onReceivedTitle(view, title);
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {

        BaseWebView newWebView = new BaseWebView(mContext);
        int webViewCnt = mWebViewContainer.getChildCount();
        newWebView.setTag(webViewCnt+1);

        BaseWebClient client = new BaseWebClient(mContext,mWebViewContainer);
        newWebView.setWebViewClient(client);

        BaseWebChromeClient chromeClient = new BaseWebChromeClient(mContext,mWebViewContainer,mJsBridge);
        newWebView.setWebChromeClient(chromeClient);
        newWebView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);

        mWebViewContainer.addView(newWebView);

        WebView.WebViewTransport transport = (WebView.WebViewTransport)resultMsg.obj;
        transport.setWebView(newWebView);
        resultMsg.sendToTarget();



        return true;
        //return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
    }

    @Override
    public void onCloseWindow(WebView window) {
        super.onCloseWindow(window);
        Logs.i("onCloseWindow : " + window.getTag());
        mWebViewContainer.removeView(window);    // 화면에서 제거
    }
}