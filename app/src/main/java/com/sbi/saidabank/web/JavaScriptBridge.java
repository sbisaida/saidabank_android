package com.sbi.saidabank.web;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;

import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ButtonObject;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.message.template.SocialObject;
import com.kakao.message.template.TextTemplate;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.util.helper.log.Logger;
import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.WebTermsActivity;
import com.sbi.saidabank.activity.certification.CertAuthPWActivity;
import com.sbi.saidabank.activity.certification.CertListActivity;
import com.sbi.saidabank.activity.common.AnotherBankAccountVerifyActivity;
import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtherOtpRegActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwRegActivity;
import com.sbi.saidabank.activity.login.CertifyPhonePersonalInfoActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.login.PersonalInfoWithFullIdNumberActivity;
import com.sbi.saidabank.activity.main.MainActivity;
import com.sbi.saidabank.activity.mtranskey.TransKey2LineActivity;
import com.sbi.saidabank.activity.setting.ManageAuthWaysActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.activity.transaction.TransferPhoneActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.dialog.TransferNeedAccountDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.CommonUserInfo;
import com.sbi.saidabank.define.datatype.ContactsInfo;
import com.sbi.saidabank.define.datatype.LoginUserInfo;
import com.sbi.saidabank.push.FLKPushAgentSender;
import com.sbi.saidabank.solution.appsflyer.AppsFlyerManager;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.jex.JexAESEncrypt;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.softsecurity.transkey.TransKeyActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.CLIPBOARD_SERVICE;


public class JavaScriptBridge {
    private static final long MIN_CLICK_INTERVAL 			    = 600;    // ms
    private static final long MIN_PROGRESS_DISMISS_INTERVAL     = 300;    // ms
    public static final String CALL_NAME = "Android";

    //private WeakReference<WebMainActivity> activityReference;
    private Activity mContext;
    /** UI 스레드 처리를 위한 핸들러 */
    private Handler mUiHandler;

    //private int mApiNum;

    /**
     * 20181112 - API호출이 연속으로 발생하여 앞의 업무를 처리전에 다시 호출될 경우
     * CallBackFuncName이 변경되는 문제가 발생할수 있다.
     */
    //private String mCallbackFuncName;
    private HashMap<Integer,String> mCallbackFuncName;
    private HashMap<Integer,String> mJsonObjectParam;

    //private int mCertSelectIdx;
    private FrameLayout mWebViewContainer;

    private int mWebViewFragmentType;
    private long mLastClickTime;


    public JavaScriptBridge(Activity activity, FrameLayout webViewContainer) {
        //activityReference = new WeakReference<>(activity);
        mContext = (Activity) activity;
        mWebViewContainer = webViewContainer;
        mUiHandler = new Handler();
        mCallbackFuncName = new HashMap<Integer,String>();
        mJsonObjectParam = new HashMap<Integer,String>();
    }

    public JavaScriptBridge(Activity activity, FrameLayout webViewContainer, int webViewFragmentType) {
        //activityReference = new WeakReference<>(activity);
        mContext = (Activity) activity;
        mWebViewContainer = webViewContainer;
        mUiHandler = new Handler();
        mCallbackFuncName = new HashMap<Integer,String>();
        mJsonObjectParam = new HashMap<Integer,String>();
        mWebViewFragmentType = webViewFragmentType;
    }

    public void destroyJavaScriptBrigdge(){
        //activityReference = null;
    }

    /*
    콜백함수를 스택으로 구성하여 여러번 호출되어도 됨.
     */
    private void setCallbackFunc(int apiNum,String callBackName){
        mCallbackFuncName.put(apiNum,callBackName);
        Logs.i("mCallbackFuncName size() : " + mCallbackFuncName.size());
    }

    public String getCallBackFunc(int apiNum) {
        String callBackName = mCallbackFuncName.get(apiNum);
        mCallbackFuncName.remove(apiNum);

        Logs.i("mCallbackFuncName size() : " + mCallbackFuncName.size());
        return callBackName;
    }

    public String getCallBackFunc(int apiNum,boolean clear) {
        String callBackName = mCallbackFuncName.get(apiNum);
        if(clear)
            mCallbackFuncName.remove(apiNum);

        Logs.i("mCallbackFuncName size() : " + mCallbackFuncName.size());
        return callBackName;
    }

    /*
    Json 파라미더를 를 스택으로 구성하여 여러번 호출되어도 값을 저장하여 꺼내올수 있도록 구성.
     */
    public void setJsonObjecParam(int apiNum,String param) {
        mJsonObjectParam.put(apiNum,param);
    }

    public String getJsonObjecParam(int apiNum){

        String param = mJsonObjectParam.get(apiNum);
        mJsonObjectParam.remove(apiNum);

        Logs.i("getJsonObjecParam size() : " + mCallbackFuncName.size());
        return param;
    }



    /**
     * 자바스크립를 실행한다.
     * @param javascript 자바스크립트 코드
     */
    public void callJavascript(String javascript) {
        Logs.e("callJavascript - loadUrl : " + javascript);
        BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
        webView.loadUrl("javascript:" + javascript);
    }


    /**
     * 자바스크립트 함수를 호출한다.
     * @param funcName 자바스크립트 함수명
     * @param jsonObj 함수 파라미터 목록
     */
    public void callJavascriptFunc(String funcName, JSONObject jsonObj) {

        if (TextUtils.isEmpty(funcName)) return;

        String func = "";
        if (jsonObj == null) {
            func = String.format("%s()", funcName);
        } else {
            func = String.format("%s(%s)", funcName, jsonObj);
        }

        callJavascript(func);
        Logs.e("func : " + func);
    }

    /**
     * JSON 요청 문자열에서 API 구분값을 얻어온다.
     * @param json JSON 요청 문자열
     * @return API 구분
     * }
     */
    private int getApiNum(JSONObject json) {

        int api = JavaScriptApi.API_UNKNOWN;
        try {
            JSONObject header = json.getJSONObject("header");
            String apiStr = header.getString("api");
            if (TextUtils.isEmpty(apiStr)) {
                Logs.shwoToast(mContext, "안드로이드 호출 함수 번호가 없습니다.");
                return -1;
            } else {
                api = Integer.parseInt(apiStr);
                String callbackFuncName = header.optString("callback", "");
                setCallbackFunc(api,callbackFuncName);
            }
        } catch (NumberFormatException | JSONException e) {
            Logs.printException(e);
        }

        return api;

    }

    /**
     * 네이티브 기능을 수행하기 위한 자바스크립트 인터페이스 메소드
     * @param jsonEncodeString json 문자열
     * {"header":{
     *              "api":"100",
     *              "call_back":""
     *          },
     *  "body":{
     *      //각 호출 함수에 맞게 파라미터값
     *  }
     */
    @JavascriptInterface
    public void callApi(String jsonEncodeString) {

        Logs.e("callApi - jsonEncodeString : " + jsonEncodeString);

        String jsonString = JavaScriptApi.makeNewJsonString(jsonEncodeString);

        Logs.e("callApi - jsonString : " + jsonString);

        try {
            final JSONObject json = new JSONObject(jsonString);

            mUiHandler.post(new Runnable() {
                public void run() {

                    try {
                        int apiNum = getApiNum(json);
                        Logs.e("callApi - apiCode : " + apiNum);
                        switch (apiNum) {
                            //======================================================================
                            // 다이얼로그
                            //======================================================================
                            case JavaScriptApi.API_100://Alert Dialog
                                api_100_AlertDialog(json);
                                break;
                            case JavaScriptApi.API_101://Confirm Dialog
                                api_101_ConfirmDialog(json);
                                break;
                            case JavaScriptApi.API_102://Progress Dialog
                                api_102_ProgressDialog(json);
                                break;
                            case JavaScriptApi.API_103://Logout Dialog
                                api_103_LogoutDialog();
                                break;
                            case JavaScriptApi.API_104://세션 끊겨 로그아웃
                                api_104_Logout();
                                break;
                            case JavaScriptApi.API_105://Intro부터 시작
                                api_105_startIntro();
                                break;
                            //======================================================================
                            // 보안키패드
                            //======================================================================
                            case JavaScriptApi.API_200://1줄 보안키패드 호출
                                api_200_1LineShowTransKey(json);
                                break;
                            case JavaScriptApi.API_201://dot타입 보안키패드 호출 등록
                                api_201_showDotTypeTransKey(json);
                                break;

                            case JavaScriptApi.API_202://dot타입 보안키패드 호출 인증
                                api_202_showDotTypeTransKey(json);
                                break;
                            case JavaScriptApi.API_203://타기관OTP 비밀번호 등록
                                api_203_regOtherOTP(json);
                                break;
                            case JavaScriptApi.API_204://타기관OTP 비밀번호 인증
                                api_204_authOtherOTP(json);
                                break;
                            case JavaScriptApi.API_205://모바일OTP 비밀번호 등록
                                api_205_regMOTP(json);
                                break;
                            case JavaScriptApi.API_206://모바일OTP 비밀번호 인증
                                api_206_authMOTP(json);
                                break;
                            //======================================================================
                            // DataPicker
                            //======================================================================
                            case JavaScriptApi.API_250://datapicker 호출
                                api_250_showDataPicker(json);
                                break;
                            //======================================================================
                            // cdd/edd 고객정보 재확인
                            //======================================================================
                            case JavaScriptApi.API_300://cdd/edd 고객정보 재확인 일주일간 열지 않음
                                api_300_notOpenFor1Week();
                                break;
                            //======================================================================
                            // Web->Native 화면 호출
                            //======================================================================
                            case JavaScriptApi.API_301://Web->Native 화면 호출
                                api_301_callNativeScreen(json);
                                break;
                            //======================================================================
                            // 새창으로 Web 호출
                            //======================================================================
                            case JavaScriptApi.API_302://새창으로 Web 호출
                                api_302_callWebView(json);
                                break;
                            //======================================================================
                            // 푸시
                            //======================================================================
                            case JavaScriptApi.API_304://푸시 앱 아이디
                                api_304_requestPushAppID();
                                break;
                            case JavaScriptApi.API_305://푸시 설정 상태
                                api_305_requestPushNotiState();
                                break;
                            case JavaScriptApi.API_306://푸시 설정으로 가기
                                api_306_openPushNotiSettingMenu();
                                break;
                            //======================================================================
                            // 챗봇
                            //======================================================================
                            case JavaScriptApi.API_400://챗봇 열기
                                api_400_openChatBot(json);
                                break;
                            case JavaScriptApi.API_401://챗봇에서 신규 화면 열기
                                api_401_openChatBotNewWebView(json);
                                break;
                            case JavaScriptApi.API_402://챗봇에서 고객번호 가져가기
                                api_402_requestChatBotCustomNumber();
                                break;
                            case JavaScriptApi.API_403://챗봇 닫기
                                api_403_closeChatBot();
                                break;
                            //======================================================================
                            // 지문,핀코드 인증
                            //======================================================================
                            case JavaScriptApi.API_500:
                                api_500_pinSign(json);
                                break;

                            case JavaScriptApi.API_501:
                                api_501_fingerprintSign(json);
                                break;

                            case JavaScriptApi.API_502:
                                api_502_selfCertify(json);
                                break;

                            case JavaScriptApi.API_503:
                                api_503_certifyAnotherBank(json);
                                break;
                            //======================================================================
                            // 스크래핑
                            //======================================================================
                            case JavaScriptApi.API_600:
                                api_600_scraping(json);
                                break;

                            case JavaScriptApi.API_601:
                                api_601_scraping(json);
                                break;

                            //======================================================================
                            // M-OTP
                            //======================================================================
                            //case JavaScriptApi.API_700:
                            //    break;
                            case JavaScriptApi.API_700:
                                api_700_reqMOTPTAVersion();
                                break;
                            case JavaScriptApi.API_701:
                                api_701_saveMOTPIssueInfo(json);
                                break;
                            case JavaScriptApi.API_702:
                                api_702_reqMOTPSerialNum();
                                break;
                            case JavaScriptApi.API_703:
                                api_703_reqMOTPCode(json);
                                break;
                            case JavaScriptApi.API_704:
                                api_704_reqMOTPDissue(json);
                                break;
                            //======================================================================
                            // 비대면/FDS
                            //======================================================================
                            case JavaScriptApi.API_800:
                                api_800_runOcrCamera(json);
                                break;
                            case JavaScriptApi.API_801:
                                api_801_searchFDSInfo(json);
                                break;
                            case JavaScriptApi.API_802:
                                api_802_DeviceInfo(json);
                                break;

                            //======================================================================
                            // 기타여러작업
                            //======================================================================
                            case JavaScriptApi.API_900:
                                api_900_screenCaptureSave(json);
                                break;
                            case JavaScriptApi.API_901:
                                api_901_jexEnc(json);
                                break;
                            case JavaScriptApi.API_902:
                                api_902_jexDec(json);
                                break;
                            case JavaScriptApi.API_903:
                                api_903_pdfVIew(json);
                                break;
                            case JavaScriptApi.API_904:
                                api_904_makeCall(json);
                                break;
                            case JavaScriptApi.API_905:
                                api_905_appVersionInfo();
                                break;
                            case JavaScriptApi.API_906:
                                api_906_sessionSync();
                                break;
                            case JavaScriptApi.API_907:
                                api_907_getProfileImage(json);
                                break;
                            case JavaScriptApi.API_908:
                                api_908_openUrl(json);
                                break;
                            case JavaScriptApi.API_910:
                                api_910_shareSNS(json);
                                break;
                            case JavaScriptApi.API_911:
                                api_911_copyClipborad(json);
                                break;
                            case JavaScriptApi.API_912:
                                api_912_sendKakaoTalk(json);
                                break;
                            case JavaScriptApi.API_913:
                                api_913_sendAppsflyerEvent(json);
                                break;
                            case JavaScriptApi.API_920:
                                api_920_selectTab(json);
                                break;
                            case JavaScriptApi.API_930:
                                api_930_requestContacts();
                                break;
                            default:
                                break;
                        }
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }

                }
            });
        } catch (JSONException e) {
            Logs.printException(e);
        }
    }

    /*
    * ==============================================================================================
    * 아래서부터 인터페이스 함수들...
    * ==============================================================================================
    * */
    /**
     * Alert 다이얼로그 호출 요청을 처리한다.
     * @param json JSON 데이터
     * @throws JSONException
     * }
     */
    private void api_100_AlertDialog(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");

        String title = body.optString("title");
        if(TextUtils.isEmpty(title))
            title= mContext.getString(R.string.common_notice);
        String msg = body.getString("msg");

        String btnText = body.optString("ok_btn");
        if(TextUtils.isEmpty(btnText))
            btnText= mContext.getString(R.string.common_confirm);


        if(TextUtils.isEmpty(msg)) return;

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_100), null);
            }
        };

        DialogUtil.alert(mContext, title, msg, btnText,okClick);
    }

    /**
     * Conform 다이얼로그 호출 요청을 처리한다.
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_101_ConfirmDialog(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");

        String title = body.optString("title");
        if(TextUtils.isEmpty(title))
            title= mContext.getString(R.string.common_notice);
        String msg = body.getString("msg");

        String cancel = body.optString("cancel_btn");
        if(TextUtils.isEmpty(cancel))
            cancel= mContext.getString(R.string.common_no);

        String ok = body.optString("ok_btn");
        if(TextUtils.isEmpty(cancel))
            ok= mContext.getString(R.string.common_yes);

        if(TextUtils.isEmpty(msg)) return;

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("click_btn", "ok");
                } catch (JSONException e) {
                    Logs.printException(e);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_101), jsonObj);
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("click_btn", "cancel");
                } catch (JSONException e) {
                    Logs.printException(e);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_101), jsonObj);
            }
        };

        DialogUtil.alert(mContext, title, ok, cancel, msg, okClick, cancelClick);

    }

    /**
     * Progress 다이얼로그 호출 요청을 처리한다.
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_102_ProgressDialog(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");

        boolean show = body.getBoolean("show");

        if (mContext == null)
            return;

        if ((Activity)mContext instanceof MainActivity) {
            if(show){
                progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
                ((MainActivity)mContext).showProgress(mWebViewFragmentType);
            }else{
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((MainActivity)mContext).dismissProgress(mWebViewFragmentType);
            }
        } else {
            if(show){
                progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
                ((BaseActivity)mContext).showProgressDialog();
            }else{
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((BaseActivity)mContext).dismissProgressDialog();
            }
        }
    }

    /**
     * Logout 다이얼로그 호출 요청을 처리한다.
     */
    private void api_103_LogoutDialog() {
        if ((Activity)mContext instanceof MainActivity || (Activity)mContext instanceof WebMainActivity) {
            ((BaseActivity) mContext).showLogoutDialog();
        }
    }

    /**
     * 세션 끊겨 로그아웃
     */
    private void api_104_Logout() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
        if (Utils.isAppOnForeground(mContext)) {
            SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
            mApplicationClass.allActivityFinish(true);
            LoginUserInfo.clearInstance();
            LoginUserInfo.getInstance().setLogin(false);
            Intent intent = new Intent(mContext, LogoutActivity.class);
            intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_FORCE);
            mContext.startActivity(intent);
        } else {
            LoginUserInfo.getInstance().setLogin(false);
            LogoutTimeChecker.getInstance(mContext).setmIsBackground(true);
        }
    }

    /**
     * Intro부터 시작 : 회원 탈퇴 시 호출
     */
    private void api_105_startIntro() {
        String motpSerial = LoginUserInfo.getInstance().getMOTPSerialNumber();
        if (!TextUtils.isEmpty(motpSerial)) {
            MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
            motpManager.reqDissue(new MOTPManager.MOTPEventListener() {
                @Override
                public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                }
            }, motpSerial);
        }
        SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
        mApplicationClass.allActivityFinish(true);
        LoginUserInfo.clearInstance();
        LoginUserInfo.getInstance().setLogin(false);
        Prefer.clearAuthSharedPreferences(mContext);
        Intent intent = new Intent(mContext, IntroActivity.class);
        mContext.startActivity(intent);
    }

    /**
     * 1줄 Transkeypad호출
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_200_1LineShowTransKey(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String type = body.getString("type");
        int min = body.getInt("min_length");
        int max = body.getInt("max_length");

        String title = body.getString("title");
        String label = body.getString("label");
        String input_id = body.getString("input_id");

        int keyType = TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;
        if (type.equals("number")) {
            keyType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;
        }
        Intent intentKey = new Intent(mContext, CertAuthPWActivity.class);
        intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
        intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, title);
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, label);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, max);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, min);
        intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID1, input_id);

        mContext.startActivityForResult(intentKey, JavaScriptApi.API_200);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);

    }

    /*
    /**
     * 2줄 Transkeypad호출
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_201_2LineShowTransKey(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String type = body.getString("type");
        int min = body.getInt("min_length");
        int max = body.getInt("max_length");

        String title = body.getString("title");
        String label1 = body.getString("label1");
        String label2 = body.getString("label2");
        String input_id1 = body.getString("input_id1");
        String input_id2 = body.getString("input_id2");


        int keyType = TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;
        if (type.equals("number")) {
            keyType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;
        }
        Intent intentKey = new Intent(mContext, TransKey2LineActivity.class);
        intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
        intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, title);
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, label1);
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL2, label2);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, max);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, min);
        intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID1, input_id1);
        intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID2, input_id2);
        mContext.startActivityForResult(intentKey, JavaScriptApi.API_201);
    }

    /**
     * dot형 보안키패드 호출 등록
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_201_showDotTypeTransKey(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        int max = body.getInt("max_length");
        String title = body.getString("title");

        Intent intent = new Intent(mContext, OtpPwRegActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_REG_PW);
        commonUserInfo.setOtpTitle(title);
        commonUserInfo.setOtpPWLength(max);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);

        mContext.startActivityForResult(intent, JavaScriptApi.API_201);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * dot형 보안키패드 호출 인증
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_202_showDotTypeTransKey(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        int max = body.getInt("max_length");
        String title = body.getString("title");
        boolean forgetAuth = body.getBoolean("use_forget_auth");

        Intent intent = new Intent(mContext, OtpPwAuthActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_PW);
        commonUserInfo.setOtpUseForgetAuth(forgetAuth);
        commonUserInfo.setOtpTitle(title);
        commonUserInfo.setOtpPWLength(max);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);

        mContext.startActivityForResult(intent, JavaScriptApi.API_202);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);

    }

    /**
     * 타기관OTP 비밀번호 등록
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_203_regOtherOTP(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        int max = body.getInt("max_length");
        String title = body.getString("title");

        Intent intent = new Intent(mContext, OtherOtpRegActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_REG_OTP);
        commonUserInfo.setOtpTitle(title);
        commonUserInfo.setOtpPWLength(max);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);

        mContext.startActivityForResult(intent, JavaScriptApi.API_203);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 타기관OTP 비밀번호 인증
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_204_authOtherOTP(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        int max = body.getInt("max_length");
        String title = body.getString("title");
        boolean forgetAuth = body.getBoolean("use_forget_auth");

        Intent intent = new Intent(mContext, OtherOtpAuthActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);
        commonUserInfo.setOtpUseForgetAuth(forgetAuth);
        commonUserInfo.setOtpTitle(title);
        commonUserInfo.setOtpPWLength(max);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);

        mContext.startActivityForResult(intent, JavaScriptApi.API_204);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 모바일OTP 비밀번호 등록
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_205_regMOTP(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        int max = body.getInt("max_length");
        String title = body.getString("title");

        Intent intent = new Intent(mContext, OtpPwRegActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_REG_MOTP);
        commonUserInfo.setOtpTitle(title);
        commonUserInfo.setOtpPWLength(max);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);

        mContext.startActivityForResult(intent, JavaScriptApi.API_205);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 모바일OTP 비밀번호 인증
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_206_authMOTP(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        int max = body.getInt("max_length");
        String title = body.getString("title");
        boolean forgetAuth = body.getBoolean("use_forget_auth");

        Intent intent = new Intent(mContext, OtpPwAuthActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP);
        commonUserInfo.setOtpUseForgetAuth(forgetAuth);
        commonUserInfo.setOtpTitle(title);
        commonUserInfo.setOtpPWLength(max);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);

        mContext.startActivityForResult(intent, JavaScriptApi.API_206);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * datapicker 호출
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_250_showDataPicker(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String pickerType = body.optString("picker_type");
        String workType = body.optString("work_type");

        if (TextUtils.isEmpty(pickerType)) {
            return;
        }

        if ("date".equals(pickerType)) {
            String startDate = body.optString("startdate");
            String endDate = body.optString("enddate");
            String selectedDate = body.optString("selecteddate");
            final int webPickerType = (TextUtils.isEmpty(workType) || "ymd".equals(workType)) ? Const.PICKER_TYPE_DATE_WEB_YMD : Const.PICKER_TYPE_DATE_WEB_YM;

            if (webPickerType == Const.PICKER_TYPE_DATE_WEB_YM) {
                SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(mContext, webPickerType, startDate, endDate, selectedDate);
                slidingDateTimerPickerDialog.setOnWebCallConfirmListener(new SlidingDateTimerPickerDialog.OnWebCallConfirmListener() {
                    @Override
                    public void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name) {
                        JSONObject jsonObj = new JSONObject();
                        try {
                            if (webPickerType == Const.PICKER_TYPE_DATE_WEB_YMD)
                                jsonObj.put("date", String.format("%04d", year) + "" + String.format("%02d", month) + "" + String.format("%02d", day));
                            else
                                jsonObj.put("date", String.format("%04d", year) + "" + String.format("%02d", month));
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                    }
                });
                slidingDateTimerPickerDialog.show();
            } else {
                Calendar calendar = Calendar.getInstance();
                Calendar minDate = Calendar.getInstance();
                Calendar maxxDate = Calendar.getInstance();

                if (!TextUtils.isEmpty(selectedDate)) {
                    selectedDate = selectedDate.replaceAll("[^0-9]", "");
                    int startYear = Integer.parseInt(selectedDate.substring(0, 4));
                    int startMon = Integer.parseInt(selectedDate.substring(4, 6));
                    int startDay = Integer.parseInt(selectedDate.substring(6, 8));
                    calendar.set(startYear, startMon - 1, startDay);
                }

                if (!TextUtils.isEmpty(startDate)) {
                    startDate = startDate.replaceAll("[^0-9]", "");
                    int startYear = Integer.parseInt(startDate.substring(0, 4));
                    int startMon = Integer.parseInt(startDate.substring(4, 6));
                    int startDay = Integer.parseInt(startDate.substring(6, 8));
                    minDate.set(startYear, startMon - 1, startDay);
                } else {
                    minDate.set(1950, 0, 1);
                }

                if (!TextUtils.isEmpty(endDate)) {
                    endDate = endDate.replaceAll("[^0-9]", "");
                    int endYear = Integer.parseInt(endDate.substring(0, 4));
                    int endMon = Integer.parseInt(endDate.substring(4, 6));
                    int endDay = Integer.parseInt(endDate.substring(6, 8));
                    maxxDate.set(endYear, endMon - 1, endDay);
                } else {
                    maxxDate.set(2100, 11, 31);
                }

                DatePickerFragmentDialog.newInstance(DateTimeBuilder.get().withSelectedDate(calendar.getTimeInMillis()).withMinDate(minDate.getTimeInMillis()).withMaxDate(maxxDate.getTimeInMillis()).withTheme(R.style.datepickerCustom)).show(((FragmentActivity) mContext).getSupportFragmentManager(), "DatePickerFragmentDialog");
            }

        } else if ("cycle".equals(pickerType)) {
            final boolean hasDaily;
            final boolean hasWeek;
            JSONObject selectedDate = body.optJSONObject("selectedcycle");

            if (TextUtils.isEmpty(workType)) {
                hasDaily = true;
                hasWeek = true;
            } else {
                if ("mwd".equals(workType)) {
                    hasDaily = true;
                    hasWeek = true;
                } else if ("mw".equals(workType)) {
                    hasDaily = false;
                    hasWeek = true;
                } else if ("m".equals(workType)) {
                    hasDaily = false;
                    hasWeek = false;
                } else {
                    hasDaily = true;
                    hasWeek = true;
                }
            }

            SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(mContext, Const.PICKER_TYPE_DATE_CYCLE_WEB_YM, true, hasDaily, hasWeek, selectedDate);
            slidingDateTimerPickerDialog.setOnWebCallConfirmListener(new SlidingDateTimerPickerDialog.OnWebCallConfirmListener() {
                @Override
                public void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name) {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        switch (selectedType) {
                            case 0:
                                jsonObj.put("cycle", "01");
                                jsonObj.put("code", String.format("%02d", selectedIndex));
                                break;
                            case 1:
                                jsonObj.put("cycle", "07");
                                jsonObj.put("code", String.valueOf(selectedIndex));
                                break;
                            case 2:
                                jsonObj.put("cycle", "99");
                                jsonObj.put("code", "");
                                break;
                            default:
                                break;
                        }
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                }
            });
            slidingDateTimerPickerDialog.show();
        } else if ("data".equals(pickerType)) {
            JSONArray jsonarray = body.optJSONArray("user_custom");
            String selectedData = body.optString("selecteddata");
            Logs.e("jsonarray.length() : " + jsonarray.length());
            if (jsonarray.length() <= 0) {
                return;
            }
            SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(mContext, Const.PICKER_TYPE_CUSTOM_DATA_WEB, jsonarray, selectedData);
            slidingDateTimerPickerDialog.setOnWebCallConfirmListener(new SlidingDateTimerPickerDialog.OnWebCallConfirmListener() {
                @Override
                public void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name) {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("code", code);
                        jsonObj.put("name", name);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                }
            });
            slidingDateTimerPickerDialog.show();
        } else {
            return;
        }
    }

    /**
     * cdd/edd 고객정보 재확인 일주일간 열지 않기
     */
    private void api_300_notOpenFor1Week() {
        Map param = new HashMap();
        // 서버시간 가져오기
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        ((WebMainActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
                    @Override
                    public void endHttpRequest(String ret) {
                        progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                        //((WebMainActivity)mContext).dismissProgressDialog();
                        Calendar calendar = Calendar.getInstance();
                        // default 시간은 디바이스 시간으로 저장
                        String curTime = String.valueOf(calendar.get(Calendar.YEAR)) + String.valueOf(calendar.get(Calendar.MONTH) + 1) + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                        Logs.i("curTime : " + curTime);
                        if (TextUtils.isEmpty(ret)) {
                            Logs.e(mContext.getResources().getString(R.string.msg_debug_no_response));
                        }

                        JSONObject object = null;
                        try {
                            object = new JSONObject(ret);
                            if (object == null) {
                                Logs.e(mContext.getResources().getString(R.string.msg_debug_err_response));
                            }
                            JSONObject objectHead = null;
                            objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                            String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                            if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                                Logs.e(error);
                            }
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }

                        String valueStr = object.optString("SYS_DTTM");

                        if (!TextUtils.isEmpty(valueStr)) {
                            curTime = valueStr.substring(0, 8);
                            Logs.i("servertime : " + curTime);
                        }
                        Prefer.setCDDEDDNotOpenFor1Week(mContext, curTime);
                        //고객정보 재확인 안내 화면 종료하고 메인으로 이동
                        Intent intent = new Intent(mContext, MainActivity.class);
                        mContext.startActivity(intent);
                        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
                        ((BaseActivity)mContext).dismissProgressDialog();
                        mContext.finish();
                    }
                });
    }

    /**
     * Web->Native 화면 호출
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_301_callNativeScreen(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String screenName = body.getString("screen_name");

        if (TextUtils.isEmpty(screenName))
            return;

        Intent intent;
        String param = null;

        if (body.has(Const.INTENT_PARAM)) {
            param = body.optString(Const.INTENT_PARAM);
        }

        if ("main".equals(screenName)) {
            if ((Activity)mContext instanceof WebMainActivity) {
                SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
                if (mApplicationClass.getActivitySize() > 0) {
                    final Activity activity = mApplicationClass.getOpenActivity(".MainActivity");
                    if (activity != null) {
                        if (param != null) {
                            intent = new Intent();
                            intent.putExtra(Const.INTENT_PARAM, param);
                            mContext.setResult(mContext.RESULT_OK);
                        }
                    } else {
                        intent = new Intent(mContext, MainActivity.class);
                        mContext.startActivity(intent);
                    }
                } else {
                    intent = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(intent);
                }
                progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
                ((BaseActivity)mContext).dismissProgressDialog();
                mContext.finish();
            }
            //메인화면의 web fragment에서 호출일 떄는 종료하지 않음
            return;
        } else if ("crt0000100".equals(screenName)) {
            intent = new Intent(mContext, ManageAuthWaysActivity.class);
        } else if ("tra0010100".equals(screenName)) {
            LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
            String custNo = loginUserInfo.getCUST_NO();
            String oddpAccn = loginUserInfo.getODDP_ACCN();

            if (TextUtils.isEmpty(custNo)) {
                //고객번호 없음
                TransferNeedAccountDialog transferMakeAccountDialog = new TransferNeedAccountDialog(mContext);
                transferMakeAccountDialog.show();
                return;
            }
            else if (TextUtils.isEmpty(oddpAccn) || Integer.parseInt(oddpAccn) < 1) {
                // 계좌 없음
                String msg = mContext.getString(R.string.msg_transfer_invalid_account);
                DialogUtil.alert(mContext, msg, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }); 
                return;
            } else {
                intent = new Intent(mContext, TransferAccountActivity.class);
            }
        } else if ("tra0020100".equals(screenName)) {
            intent = new Intent(mContext, TransferPhoneActivity.class);
        } else {
            return;
        }

        if (param != null) {
            //각 화면에서 전달받을 파라메터로 구분처리
            intent.putExtra(Const.INTENT_PARAM, param);
        }

        if ("tra0010100".equals(screenName) || "tra0020100".equals(screenName))
            mContext.startActivityForResult(intent, JavaScriptApi.API_301);
        else
            mContext.startActivity(intent);
    }

    /**
     * 새창으로 Web 호출
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_302_callWebView(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String link = body.getString("link");

        if (TextUtils.isEmpty(link))
            return;

        Intent intent = new Intent(mContext, WebMainActivity.class);
        intent.putExtra("url", SaidaUrl.getBaseWebUrl() + "/" + link);
        if (body.has(Const.INTENT_PARAM)) {
            JSONObject param = body.optJSONObject(Const.INTENT_PARAM);
            StringBuilder strParam = new StringBuilder();
            if (param != null) {
                for ( int i = 0 ; i < param.length() ; i++) {
                    if (i != 0)
                        strParam.append("&");
                    try {
                        strParam.append(param.names().getString(i) + "=" + URLEncoder.encode(param.getString(param.names().getString(i)), "EUC-KR"));
                    } catch (UnsupportedEncodingException e) {
                        //e.printStackTrace();
                    }
                }

                Logs.e("param : " + strParam);

                intent.putExtra(Const.INTENT_PARAM, strParam.toString());
            }
        }

        mContext.startActivityForResult(intent, JavaScriptApi.API_302);
    }

    /**
     * 푸시 앱 아이디
     */
    private void api_304_requestPushAppID() {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("app_id", FLKPushAgentSender.mAppName);
            jsonObj.put("push_id", Prefer.getPushRegID(mContext));
            jsonObj.put("gcm_id", Prefer.getPushGCMToken(mContext));
            jsonObj.put("os_type", "1");
            jsonObj.put("os_ver", Build.VERSION.RELEASE);
            jsonObj.put("app_ver", Utils.getVersionName(mContext));
            callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_304), jsonObj);
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    /**
     * 푸시 설정 상태
     */
    private void api_305_requestPushNotiState() {
        try {
            String state;

            if (Utils.isNotificationsEnabled(mContext))
                state = "YES";
            else
                state = "NO";
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("push_state", state);
            callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_305), jsonObj);
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    /**
     * 푸시 설정으로 가기
     */
    private void api_306_openPushNotiSettingMenu() {
        Utils.openNotificationSettingMenu(mContext);
    }

    /**
     * 챗봇 열기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_400_openChatBot(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String link = body.getString("link");

        if (TextUtils.isEmpty(link))
            return;

        Intent intent = new Intent(mContext, WebMainActivity.class);
        intent.putExtra("url", link);

        mContext.startActivityForResult(intent, JavaScriptApi.API_400);
    }

    /**
     * 챗봇에서 신규 화면 열기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_401_openChatBotNewWebView(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String screenName = body.getString("screen_name");

        if (TextUtils.isEmpty(screenName))
            return;

        Intent intent = new Intent(mContext, WebMainActivity.class);
        intent.putExtra("url", SaidaUrl.getBaseWebUrl() + "/" + screenName);

        if (body.has(Const.INTENT_PARAM)) {
            if (body.has(Const.INTENT_PARAM)) {
                JSONObject param = body.optJSONObject(Const.INTENT_PARAM);
                StringBuilder strParam = new StringBuilder();
                if (param != null) {
                    for ( int i = 0 ; i < param.length() ; i++) {
                        if (i != 0)
                            strParam.append("&");

                        try {
                            strParam.append(param.names().getString(i) + "=" + URLEncoder.encode(param.getString(param.names().getString(i)), "EUC-KR"));
                        } catch (UnsupportedEncodingException e) {
                            //e.printStackTrace();
                        }
                    }

                    Logs.e("param : " + strParam);
                    intent.putExtra(Const.INTENT_PARAM, strParam.toString());
                }
            }
        }

        // 신규 화면 열기 전에 메인과 신규 사이에 떠있는 화면을 모두 닫는다.
        SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
        mApplicationClass.allActivityFinish(false);

        mContext.startActivityForResult(intent, JavaScriptApi.API_401);
    }

    /**
     * 챗봇에서 고객번호 가져가기
     */
    private void api_402_requestChatBotCustomNumber() {
        String custNo = LoginUserInfo.getInstance().getCUST_NO();
        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();

        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("mbrNo", mbrNo);
            jsonObj.put("cusNo", custNo);
            callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_402), jsonObj);
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    /**
     * 챗봇 닫기
     */
    private void api_403_closeChatBot() {
        // 현재 챗봇 화면만 닫는다.
        if ((Activity)mContext instanceof WebMainActivity) {
            progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
            ((BaseActivity)mContext).dismissProgressDialog();
            mContext.finish();
        }
    }

    /**
     * PIN 전자서명
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_500_pinSign(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String signDocText = body.optString("sign_doc_text");
        String signText = body.optString("sign_text");
        String svcId = body.optString("svc_id");
        String bizDvcd = body.optString("biz_dv_cd");
        String trnCd = body.optString("trn_cd");

        String op = Const.SSENSTONE_AUTHENTICATE;

        Intent intent = new Intent(mContext, PincodeAuthActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
        commonUserInfo.setOperation(op);
        commonUserInfo.setSignData(signText);
        commonUserInfo.setSignDocData(signDocText);
        intent.putExtra(Const.INTENT_SERVICE_ID, svcId);
        intent.putExtra(Const.INTENT_BIZ_DV_CD, bizDvcd);
        intent.putExtra(Const.INTENT_TRN_CD, trnCd);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        mContext.startActivityForResult(intent, JavaScriptApi.API_500);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 지문 전자서명
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_501_fingerprintSign(JSONObject json) throws JSONException {
        String op = Const.SSENSTONE_AUTHENTICATE;

        final FidoDialog fidoDialog = DialogUtil.fido(mContext, true,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                },
                new Dialog.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        StonePassManager.getInstance(mContext.getApplication()).cancelFingerPrint();
                    }
                });

        fidoDialog.setCanceledOnTouchOutside(false);

        StonePassManager.StonePassListener spListener = new StonePassManager.StonePassListener() {
            @Override
            public void stonePassResult(String op, int errorCode, String errorMsg) {
                Logs.e("Fido - stonePassResult : errorCode[" + errorCode + "], errorMsg[" + errorMsg + "]");
                String msg = "";

                switch (errorCode) {
                    case 2: case 4:  case 8: case 9: case 11: case 1001:
                        //msg = "지문 인식을 하지 못했습니다. 다시 시도해주세요.";
                        //msg = "code ["+errorCode + "]-" + msg;
                        //Logs.shwoToast(mContext, msg);
                        fidoDialog.showFidoCautionMsg(true);
                        return;
                    default:
                        break;
                }

                if (fidoDialog != null)
                    fidoDialog.dismiss();

                fidoDialog.showFidoCautionMsg(false);

                switch (errorCode) {
                    case 1200:
                        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                            msg = "등록했습니다.";
                            Prefer.setFidoRegStatus(mContext,true);
                            Prefer.setFlagFingerPrintChange(mContext, false);
                        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                            msg = "인증했습니다.";
                        } else {
                            msg = "해제했습니다.";
                            Prefer.setFidoRegStatus(mContext,false);
                        }
                        break;

                    case 1404:
                        msg = "사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.";

                    case 1491:
                        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                            msg = "다른 사용자 지문으로 이미 등록되어 있습니다.";
                        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                            if (Prefer.getFidoRegStatus(mContext)) {
                                msg = "인증을 실패했습니다.";
                            } else {
                                msg = "사용자 정보가 없습니다. 먼저 등록하시기 바랍니다.";
                            }
                        }
                        break;

                    case 0:
                        msg = "해제했습니다.";
                        break;

                    case 3:
                        msg = "취소하셨습니다.";
                        break;

                    case 10:
                        msg = errorMsg;
                        break;

                    case 9998:
                        msg = "서버로 부터 리턴 값이 널(Null)입니다..";
                        break;

                    case 9999:
                        msg = "작업중 json에러가 발생했습니다.";
                        break;

                    default:
                        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                            msg = "등록을 실패했습니다.";
                        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                            msg = "인증을 실패했습니다.";
                        } else {
                            msg = "해제를 실패했습니다.";
                        }
                        break;
                }
                msg = "code [" + errorCode + "]\n" + msg;
                Logs.shwoToast(mContext, msg);

                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("signed_data", errorCode);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_501), jsonObj);
                } catch (JSONException e) {
                }
            }
        };

        StonePassManager.FingerPrintDlgListener fpdListener = new StonePassManager.FingerPrintDlgListener() {
            @Override
            public void showFingerPrintDialog() {
                if (fidoDialog != null)
                    fidoDialog.show();
            }
        };

        StonePassManager.getInstance(mContext.getApplication()).ssenstoneFIDO(mContext, spListener, fpdListener, op,
                null, "FINGERPRINT", "FINGERPRINT");
    }


    /**
     * 휴대폰 본인 인증
     */
    private void api_502_selfCertify(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        boolean isFullIdnumber = body.optBoolean("full_identify_no", false);
        boolean useSession = body.optBoolean("use_session", false);
        Intent intent;
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);

        // 세션정보로 처리
        if (useSession) {
            LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
            // 세션정보 없을 때 직접 입력
            if (TextUtils.isEmpty(loginUserInfo.getMBR_NO())) {
                commonUserInfo.setUseSession(false);
                if (isFullIdnumber) {
                    intent = new Intent(mContext, PersonalInfoWithFullIdNumberActivity.class);
                } else {
                    intent = new Intent(mContext, CertifyPhonePersonalInfoActivity.class);
                }
            } else {
                commonUserInfo.setUseSession(true);
                String userName = loginUserInfo.getCUST_NM();
                if (!isFullIdnumber) {
                    String userBirth = loginUserInfo.getBRDD();
                    String userSex = loginUserInfo.getNRID_SEX_CD();

                    if(TextUtils.isEmpty(userName) || TextUtils.isEmpty(userBirth) || TextUtils.isEmpty(userSex)) {
                        Logs.shwoToast(mContext, "로그인 정보 오류");
                        return;
                    }
                    commonUserInfo.setBirth(userBirth);
                    commonUserInfo.setSexCode(userSex);
                } else {
                    String jumin = loginUserInfo.getNRID();
                    if(TextUtils.isEmpty(userName) || TextUtils.isEmpty(jumin)) {
                        Logs.shwoToast(mContext, "로그인 정보 오류");
                        return;
                    }
                    commonUserInfo.setIdnumber(jumin);
                }

                intent = new Intent(mContext, CertifyPhoneActivity.class);
                commonUserInfo.setName(userName);
            }
        } else {// 직접 입력
            commonUserInfo.setUseSession(false);
            if (isFullIdnumber) {
                intent = new Intent(mContext, PersonalInfoWithFullIdNumberActivity.class);
            } else {
                intent = new Intent(mContext, CertifyPhonePersonalInfoActivity.class);
            }
        }
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        mContext.startActivityForResult(intent, JavaScriptApi.API_502);
    }

    /**
     * 타행계좌인증
     */
    private void api_503_certifyAnotherBank(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String bzwrDvcd = body.optString("nn_meet_bzwr_dvcd");
        String propNo = body.optString("prop_no");

        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();

        String userName = loginUserInfo.getCUST_NM();
        if (TextUtils.isEmpty(userName)) {
            Logs.shwoToast(mContext, "로그인 정보 오류");
            return;
        }

        Intent intent = new Intent(mContext, AnotherBankAccountVerifyActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
        commonUserInfo.setName(userName);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD, bzwrDvcd);
        intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO, propNo);
        mContext.startActivityForResult(intent, JavaScriptApi.API_503);
    }

    /**
     * 스크래핑
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_600_scraping(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String identify_no = body.optString("identify_no");
        String userName = body.optString("user_name");
        boolean isCertOnly = body.optBoolean("cert_only");

        Intent intent = new Intent(mContext, CertListActivity.class);
        intent.putExtra(Const.INTENT_CERT_PURPOSE,Const.CertJobType.SCRAPING);
        intent.putExtra(Const.INTENT_CERT_IDDATA, identify_no);
        intent.putExtra(Const.INTENT_CERT_SERVICEDATA, body.toString());
        intent.putExtra(Const.INTENT_CERT_USERNAME, userName);
        intent.putExtra(Const.INTENT_CERT_NO_SCRAPING, isCertOnly);

        mContext.startActivityForResult(intent, JavaScriptApi.API_600);
    }

    /**
     * 스크래핑
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_601_scraping(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String identify_no =  body.optString("identify_no");
        String userName = body.optString("user_name");
        boolean isCertOnly = body.optBoolean("cert_only");

        Intent intent = new Intent(mContext, CertListActivity.class);
        intent.putExtra(Const.INTENT_CERT_PURPOSE,Const.CertJobType.SCRAPING);
        intent.putExtra(Const.INTENT_CERT_IDDATA, identify_no);
        intent.putExtra(Const.INTENT_CERT_SERVICEDATA, body.toString());
        intent.putExtra(Const.INTENT_CERT_USERNAME, userName);
        intent.putExtra(Const.INTENT_CERT_NO_SCRAPING, isCertOnly);

        mContext.startActivityForResult(intent, JavaScriptApi.API_601);
    }

    /**
     * MOTP 시리얼 넘버 요청
     */
    private void api_700_reqMOTPTAVersion() {
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        ((WebMainActivity)mContext).showProgressDialog();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.getTaVersion(new MOTPManager.MOTPEventListener() {
            @Override
            public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((WebMainActivity)mContext).dismissProgressDialog();
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    jsonObj.put("ta_version", reqData1);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_700), jsonObj);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * MOTP 발급정보 저장
     */
    private void api_701_saveMOTPIssueInfo(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String otpKey1 =  body.optString("otp_key1");
        String otpKey2 = body.optString("otp_key2");
        final String serialNum = body.optString("serial_num");
        String issueDate = body.optString("issue_date");
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        ((WebMainActivity)mContext).showProgressDialog();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.saveIssueInfo(new MOTPManager.MOTPEventListener() {
            @Override
            public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((WebMainActivity)mContext).dismissProgressDialog();
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    if ("0000".equals(resultCode))
                        LoginUserInfo.getInstance().setMOTPSerialNumber(serialNum);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_701), jsonObj);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        }, otpKey1, otpKey2, serialNum, issueDate);
    }

    /**
     * MOTP 일련번호 요청
     */
    private void api_702_reqMOTPSerialNum() {
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        ((WebMainActivity)mContext).showProgressDialog();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.getSerialNum(new MOTPManager.MOTPEventListener() {
            @Override
            public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((WebMainActivity)mContext).dismissProgressDialog();
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    jsonObj.put("serial_num", reqData1);
                    jsonObj.put("vender_code", reqData2);
                    jsonObj.put("issue_date", reqData3);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_702), jsonObj);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * MOTP 번호요청
     */
    private void api_703_reqMOTPCode(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String otpTime =  body.optString("otp_time");
        String serrialNum = body.optString("serial_num");
        String hti = body.optString("hti");
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        ((WebMainActivity)mContext).showProgressDialog();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.reqOTPCode(new MOTPManager.MOTPEventListener() {
            @Override
            public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((WebMainActivity)mContext).dismissProgressDialog();
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    jsonObj.put("otp_code", reqData1);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_703), jsonObj);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        }, otpTime, serrialNum, hti);
    }

    /**
     * MOTP 폐기요청
     */
    private void api_704_reqMOTPDissue(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String serrialNum = body.optString("serial_num");
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        ((WebMainActivity)mContext).showProgressDialog();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.reqDissue(new MOTPManager.MOTPEventListener() {
            @Override
            public void MOTPEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((WebMainActivity)mContext).dismissProgressDialog();
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    if ("0000".equals(resultCode))
                        LoginUserInfo.getInstance().setMOTPSerialNumber("");
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_704), jsonObj);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        }, serrialNum);
    }

    /**
     *  OCR촬영
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_800_runOcrCamera(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        String[] perList = new String[]{
                Manifest.permission.CAMERA
        };

        if (PermissionUtils.checkPermission(mContext,perList, JavaScriptApi.API_800)) {
            JSONObject body = json.getJSONObject("body");
            String docType =  body.optString("doc_type");

            //폰의 화면을 캡쳐한다.
            Intent intent = new Intent(mContext, CameraActivity.class);

            if (Const.IDENTIFICATION_TYPE_ID.equalsIgnoreCase(docType)) {
                intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
                intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
                intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO,   "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
                intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
            } else if (Const.IDENTIFICATION_TYPE_DOC.equalsIgnoreCase(docType)) {
                intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_DOCUMENT_A4);
                intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_PORTRAIT);
                intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO,   "카메라 영역에 [문서]를 맞추면 자동촬영 됩니다");
                intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [문서]를 맞추고 촬영해 주세요");
            }

            intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, "");
            mContext.startActivityForResult(intent, JavaScriptApi.API_800);
        } else {
            if (json != null)
                setJsonObjecParam(JavaScriptApi.API_800,json.toString());
        }
    }

    /**
     * FDS정보 조회
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_801_searchFDSInfo(JSONObject json) throws JSONException {
        FDSManager.getInstance().getFDSInfo(mContext,(WebMainActivity)mContext);
    }

    /**
     * 단말기 기기 정보
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_802_DeviceInfo(JSONObject json) throws JSONException {
        String item1 = FDSManager.getInstance().getEncIMEI(mContext);
        Logs.i("item1 : " + item1);
        String item2 = FDSManager.getInstance().getEncUUID(mContext);
        Logs.i("item2 : " + item2);

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("os", "android");
        jsonObj.put("device_info1", item1);
        jsonObj.put("device_info2", item2);

        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_802), jsonObj);
    }

    /**
     * 화면캡쳐해서 파일에 저장
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_900_screenCaptureSave(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String fileName = body.optString("file_name");
        boolean fileSave = body.optBoolean("file_save", true);
        String[] perList = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
        };

        if (fileSave) {
            if(TextUtils.isEmpty(fileName)){
                fileName = Utils.getCurrentTime("");
            }else{
                fileName = fileName + "_" + Utils.getCurrentTime("");
            }

            fileName = fileName + ".jpg";

            if(PermissionUtils.checkPermission(mContext,perList,JavaScriptApi.API_900)){
                BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
                boolean ret = ImgUtils.captureSave((Activity)mContext,webView,fileName);

                JSONObject jsonObj = new JSONObject();
                jsonObj.put("result", ret);

                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_900), jsonObj);
            } else {
                setJsonObjecParam(JavaScriptApi.API_900,fileName);
            }
        } else {
            if(PermissionUtils.checkPermission(mContext,perList,JavaScriptApi.API_900)) {
                BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
                byte[] imgData = ImgUtils.captureSave(webView);

                JSONObject jsonObj = new JSONObject();
                if (imgData != null && imgData.length > 0) {
                    jsonObj.put("result", true);
                    jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                } else {
                    jsonObj.put("result", false);
                }

                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_900), jsonObj);
            } else {
                setJsonObjecParam(JavaScriptApi.API_900,"webreturn");
            }
        }
    }

    private void api_901_jexEnc(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String plain_text =  body.optString("plain_text");
        String enc_text = "";
        JSONObject jsonObj = new JSONObject();
        if(!TextUtils.isEmpty(plain_text)) {
            try {
                enc_text = JexAESEncrypt.encrypt(plain_text);
            } catch (Exception e) {
                Logs.printException(e);
            }
            jsonObj.put("enc_text", enc_text);
        }
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_901), jsonObj);
    }

    private void api_902_jexDec(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String enc_text =  body.optString("enc_text");
        String plain_text = "";
        JSONObject jsonObj = new JSONObject();
        if(!TextUtils.isEmpty(enc_text)) {
            try {
                plain_text = JexAESEncrypt.decrypt(enc_text);
            } catch (Exception e) {
                Logs.printException(e);
            }
            jsonObj.put("plain_text", plain_text);
        }
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_902), jsonObj);
    }

    /**
     * PDF뷰어
     * @param json
     * @throws JSONException
     */
    private void api_903_pdfVIew(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String title =  body.optString("title");
        String url =  body.optString("url");

        Intent intent = new Intent(mContext, WebTermsActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("url", SaidaUrl.getBaseWebUrl() + "/"  + url);
        mContext.startActivityForResult(intent, JavaScriptApi.API_903);
    }

    /**
     * 전화 걸기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_904_makeCall(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        final String telNo = body.getString("tel_no");
        if (TextUtils.isEmpty(telNo)) {
            Logs.shwoToast(mContext, R.string.common_msg_empty_phone_nmumber);
            return;
        }
        String msg = mContext.getString(R.string.common_msg_ask_call_now,telNo);//"["+tel_no+"] 번호로 전화를 연결하시겠습니까?";
        DialogUtil.alert(mContext, msg, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.makeCall(mContext,telNo);
                }
            },
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
    }

    /**
     * 앱버전정보 확인
     */
    private void api_905_appVersionInfo() {
        JSONObject jsonObj = new JSONObject();
        try {

            jsonObj.put("current_version", Utils.getVersionName(mContext));
            jsonObj.put("device_os_version", Build.VERSION.RELEASE);
            jsonObj.put("device_model_name", Build.MODEL);

        } catch (JSONException e) {
            //e.printStackTrace();
        }
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_905), jsonObj);
    }

    /**
     * 세션동기화
     */
    private void api_906_sessionSync() {
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(mContext.getResources().getString(R.string.msg_debug_no_response));
                    return;
                } else {
                    try {
                        JSONObject object = new JSONObject(ret);
                        if (object == null) {
                            Logs.e(mContext.getResources().getString(R.string.msg_debug_err_response));
                            Logs.e(ret);
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            Logs.e(ret);
                            return;
                        }
                        Logs.e("ret : " + ret);
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().syncLoginSession(ret);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
            }
        });
    }

    /**
     * 프로필 사진 가져오기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_907_getProfileImage(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String workType = body.optString("work_type");
        String[] perList01 = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
        };
        String[] perList02 = new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
        };

        if (!TextUtils.isEmpty(workType)) {
            if (workType.equals("album")) {
                if (PermissionUtils.checkPermission(mContext,perList01, Const.REQUEST_PROFILE_ALBUM)) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                    mContext.startActivityForResult(intent, Const.REQUEST_PROFILE_ALBUM);
                } else {
                    if (json != null)
                        setJsonObjecParam(JavaScriptApi.API_907,json.toString());
                }
            } else if (workType.equals("camera")) {
                if (PermissionUtils.checkPermission(mContext,perList02, Const.REQUEST_PROFILE_CAMERA)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            String imageFileName = "sbi_" + timeStamp;
                            //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                            File storageDir = mContext.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
                            photoFile = File.createTempFile(
                                    imageFileName,
                                    ".jpg",
                                    storageDir
                            );
                        } catch (IOException ex) {
                            Logs.printException(ex);
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri mTargetPhotoURI;
                            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentapiVersion < Build.VERSION_CODES.N) {
                                mTargetPhotoURI =  Uri.fromFile(photoFile);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                                ((SaidaApplication)mContext.getApplicationContext()).setProfileUri(mTargetPhotoURI);
                                mContext.startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
                            } else {
                                ContentValues contentValues = new ContentValues(1);
                                contentValues.put(MediaStore.Images.Media.DATA, photoFile.getAbsolutePath());
                                mTargetPhotoURI = mContext.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                                ((SaidaApplication)mContext.getApplicationContext()).setProfileUri(mTargetPhotoURI);
                                mContext.startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
                            }
                        }
                    }
                } else {
                    if (json != null)
                        setJsonObjecParam(JavaScriptApi.API_907,json.toString());
                }
            } else if (workType.equals("image")) {
                if (PermissionUtils.checkPermission(mContext,perList01, Const.REQUEST_PROFILE_IMAGE)) {
                    //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    File storageDir = mContext.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
                    if (!storageDir.exists()) {
                        storageDir.mkdir();
                    }

                    File file = new File(storageDir, Const.CROP_IMAGE_PATH);
                    if (file.exists()) {
                        int size = (int) file.length();
                        byte[] imgData = new byte[size];
                        try {
                            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                            buf.read(imgData, 0, imgData.length);
                            buf.close();
                        } catch (FileNotFoundException e) {
                            //e.printStackTrace();
                        } catch (IOException e) {
                            //e.printStackTrace();
                        }
                        JSONObject jsonObj = new JSONObject();
                        if (imgData != null && imgData.length > 0) {
                            jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                        } else {
                            jsonObj.put("result", false);
                        }
                        Logs.e("이미지 전달");
                        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_907), jsonObj);
                    } else {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("result", false);
                        Logs.e("이미지 전달 실패");
                        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_907), jsonObj);
                    }
                } else {
                    if (json != null)
                        setJsonObjecParam(JavaScriptApi.API_907,json.toString());
                }
            }
        }
    }

    /**
     * 외부 브라우저로 url 열기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_908_openUrl(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String url = body.getString("url");
        if (TextUtils.isEmpty(url)) {
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Logs.printException(e);
        }
    }

    /**
     * SNS 공유하기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_910_shareSNS(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        Logs.e("api_910_shareSNS");
        JSONObject body = json.getJSONObject("body");
        String strMsg = body.optString("str_msg");
        String strImgData = body.optString("str_imgdata");
        if (TextUtils.isEmpty(strMsg) && TextUtils.isEmpty(strImgData)) {
            Logs.e("data null");
            return;
        }

        Intent share = new Intent(Intent.ACTION_SEND);
        if (!TextUtils.isEmpty(strImgData)) {
            Logs.e("img share");
            byte[] imgdata = Base64.decode(strImgData.getBytes(), Base64.DEFAULT);
            Bitmap bitmap = ImgUtils.byteArrayToBitmap(imgdata);
            if(bitmap != null) {
                //이미지 파일 생성
                File storageDir = mContext.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
                if (!storageDir.exists()) {
                    storageDir.mkdir();
                }
                File imageFile = new File(storageDir, "shareimg.jpg");
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(imageFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                    outputStream.flush();
                    outputStream.close();
                } catch (FileNotFoundException e) {
                    Logs.printException(e);
                    return;
                } catch (IOException e) {
                    Logs.printException(e);
                    return;
                }

                Uri uri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".shareimagefileprovider", imageFile);
                    share.setDataAndType(Uri.fromFile(imageFile), "application/vnd.android.package-archive");
                } else {
                    uri = Uri.fromFile(imageFile);
                }
                Logs.e("uri : " + uri);
                share.setType("image/jpg");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                mContext.startActivity(Intent.createChooser(share, "공유하기"));
            } else {
                Logs.e("bitmap is null");
            }
        } else if (!TextUtils.isEmpty(strMsg)) {
            Logs.e("text share");
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, strMsg);
            mContext.startActivity(Intent.createChooser(share, "공유하기"));
        }
    }

    /**
     * 클립보드 복사
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_911_copyClipborad(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String copyText = body.optString("copy_text");

        if (!TextUtils.isEmpty(copyText)) {
            ClipboardManager clipboardManager = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("label", copyText);
            clipboardManager.setPrimaryClip(clipData);
            //Logs.shwoToast(mContext, "클립보드에 복사되었습니다.");
        }
    }

    /**
     * 카카오톡 보내기
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_912_sendKakaoTalk(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String str_link = body.optString("str_link");
        String img_link = body.optString("img_link");
        String str_msg = body.optString("str_msg");
        String str_btn = body.optString("str_link_btn_name");

        if (Utils.isInstallApp(mContext, "com.kakao.talk") == false) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://search?q=pname:com.kakao.talk"));
            mContext.startActivity(intent);
            return;
        }

        if (TextUtils.isEmpty(str_btn))
            str_btn = "확인하기";

        Map<String, String> callbackParameters = new HashMap<>();

        if (TextUtils.isEmpty(img_link)) {
            TextTemplate txtTemplateparams = TextTemplate.newBuilder(
                    str_msg,
                    LinkObject.newBuilder()
                            .setWebUrl(str_link)
                            .setMobileWebUrl(str_link)
                            .build())
                    .setButtonTitle(str_btn)
                    .build();

            KakaoLinkService.getInstance().sendDefault(mContext, txtTemplateparams, callbackParameters, new ResponseCallback<KakaoLinkResponse>() {
                @Override
                public void onFailure(ErrorResult errorResult) {
                    Logger.e(errorResult.toString());
                }

                @Override
                public void onSuccess(KakaoLinkResponse result) {

                }
            });
        } else {
            FeedTemplate feedTemplateparams = FeedTemplate.newBuilder(
                    ContentObject.newBuilder(str_msg,
                        img_link,
                        LinkObject.newBuilder().setWebUrl(str_link)
                            .setMobileWebUrl(str_link).build())
                            .build())
                    .addButton(new ButtonObject(str_btn, LinkObject.newBuilder().setWebUrl(str_link).setMobileWebUrl(str_link).build()))
                    .build();

            KakaoLinkService.getInstance().sendDefault(mContext, feedTemplateparams, callbackParameters, new ResponseCallback<KakaoLinkResponse>() {
                @Override
                public void onFailure(ErrorResult errorResult) {
                    Logger.e(errorResult.toString());
                }

                @Override
                public void onSuccess(KakaoLinkResponse result) {

                }
            });
        }
    }


    /**
     * Appsflyer
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_913_sendAppsflyerEvent(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String event_name = body.optString("event_name");

        if (TextUtils.isEmpty(event_name)) {
            Logs.e("event_name is null.");
            return;
        }

        AppsFlyerManager.getInstance(mContext).sendAppsFlyerTrackEvent(event_name, "");
    }
    /**
     * tabbar 이동
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_920_selectTab(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복실행 방지
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String str_index = body.optString("str_index");

        if (TextUtils.isEmpty(str_index)) {
            Logs.e("str index is null.");
            return;
        }

        if ((Activity)mContext instanceof MainActivity) {
            ((MainActivity)mContext).selectTab(Integer.parseInt(str_index));
        }
    }

    /**
     * 휴대폰 주소록 추출
     */
    private void api_930_requestContacts() {
        String[] perList = new String[]{
                Manifest.permission.READ_CONTACTS,
        };

        if (PermissionUtils.checkPermission(mContext, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
            JSONObject jsonObj = new JSONObject();
            ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();
            ContactsInfo contactsInfo;
            Cursor contactCursor = null;
            String name;
            String phonenumber;
            JSONArray jsonArray = new JSONArray();
            try {
                Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

                String[] projection = new String[] {
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.Contacts.PHOTO_ID};

                String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

                contactCursor = mContext.getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
                if (contactCursor.moveToFirst()) {
                    do {
                        phonenumber = contactCursor.getString(1);
                        if (phonenumber.length() <= 0)
                            continue;

                        name = contactCursor.getString(2);
                        phonenumber = phonenumber.replaceAll("[^0-9]", "");
                        //phonenumber = phonenumber.replaceAll("-", "");
                        if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                            continue;
                        }

                        // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 핸드폰 번호인 경우
                        if (!TextUtils.isEmpty(name) && listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                            contactsInfo = new ContactsInfo();
                            contactsInfo.setTLNO(phonenumber);
                            contactsInfo.setFLNM(name);
                            listContactsInPhonebook.add(contactsInfo);

                            JSONObject itemObject = new JSONObject();
                            itemObject.put("TLNO", phonenumber);
                            itemObject.put("FLNM", name);
                            jsonArray.put(itemObject);
                        }
                    } while (contactCursor.moveToNext());
                }
            } catch (Exception e) {
                //e.printStackTrace();
            } finally {
                if (contactCursor != null) {
                    contactCursor.close();
                }

                try {
                    jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                    jsonObj.put("addr_data", jsonArray);
                } catch (JSONException e) {
                    //e.printStackTrace();
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_930), jsonObj);
            }
        }
    }

    //로딩 프로그레스바 깜박임 방지
    private Handler progressDismissDelayHandler = new Handler();
    private Runnable progressDismissRunnable = new Runnable() {
        @Override
        public void run() {
            //Do Something
            if (mContext == null)
                return;
            if ((Activity)mContext instanceof MainActivity)
                ((MainActivity)mContext).dismissProgress(mWebViewFragmentType);
            else
                ((BaseActivity)mContext).dismissProgressDialog();
        }
    };
}
