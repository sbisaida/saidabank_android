package com.interezen.mobile.android.app;
interface II3GService
{
	void setCollServerInfo(String sCollAddr, int nCollPort, int nTimout);
	void setWasServerInfo(String sWasAddr, int nWasPort, String sUrl);
	int runI3GMobile();
	Map runi3GGetEXData(boolean flag);
	String getProtocolString( int nAgentCode, String sPackagePath, String sUserId, String sService_No, String succ_flag, String sMData );
	String getInfoString();
	String getI3GAndVersion();
	String getMAC();
	String getMACHDD();
	String getMACNormalString();
	String getItem1();
	String getItem2(int iFCode);
	String getMDUInfo(int iOSType, int iCstmCode, int iFCode);
}